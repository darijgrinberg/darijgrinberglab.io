# Darij Grinberg's website

This is a mirror of my website, hosted at https://www.cip.ifi.lmu.de/~grinberg/ .
The mirror can be found at https://darijgrinberg.gitlab.io/ .
