\documentclass{beamer}%
\usepackage[all,cmtip]{xy}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{framed}
\usepackage{comment}
\usepackage{color}
\usepackage{tabls}
\usepackage{tikz}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Friday, April 05, 2024 10:34:15}
%TCIDATA{SuppressPackageManagement}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{Language=American English}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\usetikzlibrary{arrows}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}
\definecolor{grau}{rgb}{.5 , .5 , .5}
\definecolor{dunkelgrau}{rgb}{.35 , .35 , .35}
\definecolor{schwarz}{rgb}{0 , 0 , 0}
\definecolor{violet}{RGB}{143,0,255}
\definecolor{forestgreen}{RGB}{34, 100, 34}
\newcommand{\red}{\color{red}}
\newcommand{\grey}{\color{grau}}
\newcommand{\green}{\color{forestgreen}}
\newcommand{\violet}{\color{violet}}
\newcommand{\blue}{\color{blue}}
\newcommand{\defn}[1]{{\color{darkred}\emph{#1}}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\kk}{\mathbf{k}}
\newcommand{\set}[1]{\left\{ #1 \right\}}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\tup}[1]{\left( #1 \right)}
\newcommand{\ive}[1]{\left[ #1 \right]}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\mono}{\hookrightarrow}
\newcommand{\epi}{\twoheadrightarrow}
\newcommand{\iso}{\overset{\cong}{\to}}
\newcommand{\arinj}{\ar@{_{(}->}}
\newcommand{\arinjrev}{\ar@{^{(}->}}
\newcommand{\arsurj}{\ar@{->>}}
\newcommand{\arelem}{\ar@{|->}}
\newcommand{\arback}{\ar@{<-}}
\newcommand{\symd}{\mathbin{\bigtriangleup}}
\newcommand{\Ker}{\operatorname{Ker}}
\newcommand{\Coker}{\operatorname{Coker}}
\newcommand{\id}{\operatorname{id}}
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{\node[shape=circle,fill,inner sep=2pt] (char) {\textcolor{white}{#1}};}}
\newcommand{\icast}{\circled{$\ast$}}
\newcommand\arxiv[1]{\href{http://www.arxiv.org/abs/#1}{\texttt{arXiv:#1}}}
\setlength\tablinesep{3pt}
\setlength\arraylinesep{3pt}
\setlength\extrarulesep{3pt}
\newenvironment{noncompile}{}{}
\excludecomment{noncompile}
\usetheme{Frankfurt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{itemize/enumerate body begin}{}
\setbeamertemplate{itemize/enumerate subbody begin}{\normalsize}
\beamersetuncovermixins{\opaqueness<1>{0}}{\opaqueness<2->{15}}
\newcommand{\STRUT}{\vrule width 0pt depth 8pt height 0pt}
\newcommand{\ASTRUT}{\vrule width 0pt depth 0pt height 11pt}
\setbeamertemplate{headline}{}
\begin{document}

\title{Rook sums in the \\symmetric group algebra}
\author{Darij Grinberg (Drexel University)}
\date{Howard University, DC, 2024-04-07}

\begin{noncompile}
\textbf{Abstract.} Let $\mathcal{A}$ be the group algebra $\mathbf{k}\left[
S_{n}\right]  $ of the $n$-th symmetric group $S_{n}$ over a commutative ring
$\mathbf{k}$. For any two subsets $A$ and $B$ of $\left[  n\right]  $, we
define the elements%
\[
\nabla_{B,A}:=\sum_{\substack{w\in S_{n};\\w\left(  A\right)  =B}%
}w\ \ \ \ \ \ \ \ \ \ \text{and}\ \ \ \ \ \ \ \ \ \ \widetilde{\nabla}%
_{B,A}:=\sum_{\substack{w\in S_{n};\\w\left(  A\right)  \subseteq B}}w
\]
of $\mathcal{A}$. We study these elements, showing in particular that their
minimal polynomials factor into linear factors (with integer coefficients). We
express the product $\nabla_{D,C}\nabla_{B,A}$ as a $\mathbb{Z}$-linear
combination of $\nabla_{U,V}$'s.
More generally, for any two set compositions (i.e., ordered set partitions)
$\mathbf{A}$ and $\mathbf{B}$ of $\left\{  1,2,\ldots,n\right\}  $, we define
$\nabla_{\mathbf{B},\mathbf{A}}\in\mathcal{A}$ to be the sum of all
permutations $w\in S_{n}$ that send each block of $\mathbf{A}$ to the
corresponding block of $\mathbf{B}$. This generalizes $\nabla_{B,A}$. The
factorization property of minimal polynomials does not extend to the
$\nabla_{\mathbf{B},\mathbf{A}}$, but we describe the ideal spanned by the
$\nabla_{\mathbf{B},\mathbf{A}}$ and its complement.
\end{noncompile}


\frame{\titlepage\textbf{slides:} {\color{red}
\url{http://www.cip.ifi.lmu.de/~grinberg/algebra/dc2024.pdf}} \medskip
\newline\textbf{paper (draft):} {\color{red}
\url{https://www.cip.ifi.lmu.de/~grinberg/algebra/rooksn.pdf}} }

\begin{frame}
\frametitle{\ \ \ \ The symmetric group algebra}

\begin{itemize}
\item \textbf{Definition.} Fix a commutative ring $\mathbf{k}$. (The main
examples are $\mathbb{Z}$ and $\mathbb{Q}$.)

For each $n\in\mathbb{N}$, let $S_{n}$ be the $n$-th symmetric group, and
$\mathbf{k}\left[  S_{n}\right]  $ its group algebra over $\mathbf{k}$. So%
\[
\mathbf{k}\left[  S_{n}\right]  =\left\{  \text{formal linear combinations
}\sum_{w\in S_{n}}\alpha_{w}w\text{ with }\alpha_{w}\in\mathbf{k}\right\}  .
\]


Also, let $\left[  n\right]  :=\left\{  1,2,\ldots,n\right\}  $ for each
$n\in\mathbb{N}$.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Rook-to-rook sums: definition}

\begin{itemize}
\item \textbf{Definition.} For any two subsets $A$ and $B$ of $\left[
n\right]  $, we define the elements%
\[
\nabla_{B,A}:=\sum_{\substack{w\in S_{n};\\w\left(  A\right)  =B}%
}w\ \ \ \ \ \ \ \ \ \ \text{and}\ \ \ \ \ \ \ \ \ \ \widetilde{\nabla}%
_{B,A}:=\sum_{\substack{w\in S_{n};\\w\left(  A\right)  \subseteq B}}w
\]
of $\mathbf{k}\left[  S_{n}\right]  $. We shall refer to these elements as
\textbf{rectangular rook sums}. \pause


\item \textbf{Examples.}
\begin{align*}
\nabla_{\varnothing,\varnothing}  &  =\nabla_{\left[  n\right]  ,\left[
n\right]  }=\left(  \text{sum of all }w\in S_{n}\right)  ;\\
\nabla_{\left\{  2\right\}  ,\left\{  1\right\}  }  &  =\left(  \text{sum of
all }w\in S_{n}\text{ sending }1\text{ to }2\right)  ;\\
\widetilde{\nabla}_{\left\{  2,3\right\}  ,\left\{  1\right\}  }  &  =\left(
\text{sum of all }w\in S_{n}\text{ sending }1\text{ to }2\text{ or }3\right)
.
\end{align*}

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Rook-to-rook sums: simple properties}

\begin{itemize}
\item \textbf{Proposition.} Let $A$ and $B$ be two subsets of $\left[
n\right]  $. Then:

\begin{enumerate}
\item[\textbf{(a)}] We have $\nabla_{B,A}=0$ if $\left\vert A\right\vert
\neq\left\vert B\right\vert $.

\item[\textbf{(b)}] We have $\widetilde{\nabla}_{B,A}=0$ if $\left\vert
A\right\vert >\left\vert B\right\vert $. \pause


\item[\textbf{(c)}] We have $\widetilde{\nabla}_{B,A}=\sum
\limits_{\substack{V\subseteq B;\\\left\vert V\right\vert =\left\vert
A\right\vert }}\nabla_{V,A}$. \pause


\item[\textbf{(d)}] We have $\nabla_{B,A}=\nabla_{\left[  n\right]  \setminus
B,\ \left[  n\right]  \setminus A}$.

\item[\textbf{(e)}] If $\left\vert A\right\vert =\left\vert B\right\vert $,
then $\nabla_{B,A}=\widetilde{\nabla}_{B,A}$. \pause

\end{enumerate}

Next, let $S:\mathbf{k}\left[  S_{n}\right]  \rightarrow\mathbf{k}\left[
S_{n}\right]  $ be the \textbf{antipode} of $\mathbf{k}\left[  S_{n}\right]
$; this is the $\mathbf{k}$-linear map sending each permutation $w\in S_{n}$
to $w^{-1}$. Then:

\begin{enumerate}
\item[\textbf{(f)}] We have $S\left(  \nabla_{B,A}\right)  =\nabla_{A,B}$.

\item[\textbf{(g)}] We have $S\left(  \widetilde{\nabla}_{B,A}\right)
=\widetilde{\nabla}_{\left[  n\right]  \setminus A,\ \left[  n\right]
\setminus B}$.
\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Minimal polynomials: a question}

\begin{itemize}
\item The simplest rectangular rook sum is
\[
\nabla_{\varnothing,\varnothing}=\left(  \text{sum of all }w\in S_{n}\right)
.
\]
Easily, $\nabla_{\varnothing,\varnothing}^{2}=n!\nabla_{\varnothing
,\varnothing}$, so that%
\[
P\left(  \nabla_{\varnothing,\varnothing}\right)
=0\ \ \ \ \ \ \ \ \ \ \text{for the polynomial }P\left(  x\right)  =x\left(
x-n!\right)  .
\]
\pause


\item \textbf{Question:} What polynomials $P$ satisfy $P\left(  \nabla
_{B,A}\right)  =0$ or $P\left(  \widetilde{\nabla}_{B,A}\right)  =0$ for
arbitrary $A,B$ ?

In particular, what is the minimal polynomial of $\widetilde{\nabla}_{B,A}$ ?
(The only interesting $\nabla_{B,A}$'s are those for $\left\vert A\right\vert
=\left\vert B\right\vert $, and they agree with $\widetilde{\nabla}_{B,A}$, so
that we need not study them separately.)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Minimal polynomials: experimental data}

\begin{itemize}
\item \textbf{Example.} The minimal polynomial of $\widetilde{\nabla
}_{\left\{  2,4,5,6\right\}  ,\ \left\{  1,2\right\}  }$ for $n=6$ is
$(x-288)x(x+12)(x+36)$. \pause


\item \textbf{Example.} The minimal polynomial of $\widetilde{\nabla
}_{\left\{  1,2,5,6\right\}  ,\ \left\{  1,2,3\right\}  }$ for $n=6$ is
$(x-144)(x+16)x^{2}$. \pause


\item Looks like the minimal polynomial always splits over $\mathbb{Z}$ (i.e.,
factors into linear factors)! \pause

\item How can we prove this?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ A product rule}

\begin{itemize}
\item A crucial step in the proof is a product rule for $\nabla$s:

\item \textbf{Theorem (product rule).} Let $A,B,C,D$ be four subsets of
$\left[  n\right]  $ such that $\left\vert A\right\vert =\left\vert
B\right\vert $ and $\left\vert C\right\vert =\left\vert D\right\vert $. Then,%
\[
\nabla_{D,C}\nabla_{B,A}=\omega_{B,C}\sum_{\substack{U\subseteq D,\\V\subseteq
A;\\\left\vert U\right\vert =\left\vert V\right\vert }}\left(  -1\right)
^{\left\vert U\right\vert -\left\vert B\cap C\right\vert }\dbinom{\left\vert
U\right\vert }{\left\vert B\cap C\right\vert }\nabla_{U,V}.
\]
Here, for any two subsets $B$ and $C$ of $\left[  n\right]  $, we set%
\[
\omega_{B,C}:=\left\vert B\cap C\right\vert !\cdot\left\vert B\setminus
C\right\vert !\cdot\left\vert C\setminus B\right\vert !\cdot\left\vert \left[
n\right]  \setminus\left(  B\cup C\right)  \right\vert !\in\mathbb{Z}.
\]
\pause


\item \textbf{Proof.} Nice exercise in enumeration! First step is to show
that
\[
\nabla_{D,C}\nabla_{B,A}=\omega_{B,C}\sum_{\substack{w\in S_{n};\\\left\vert
w\left(  A\right)  \cap D\right\vert =\left\vert B\cap C\right\vert }}w.
\]

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ A product rule, restated}

\begin{itemize}
\item Recall that $\widetilde{\nabla}_{B,A}$ is the sum of all $\nabla_{V,A}%
$'s for $V\subseteq B$ satisfying $\left\vert V\right\vert =\left\vert
A\right\vert $. Thus, the product rule rewrites as follows:

\item \textbf{Theorem (product rule, rewritten).} Let $A,B,C,D$ be four
subsets of $\left[  n\right]  $ such that $\left\vert A\right\vert =\left\vert
B\right\vert $ and $\left\vert C\right\vert =\left\vert D\right\vert $. Then,%
\[
\nabla_{D,C}\nabla_{B,A}=\omega_{B,C}\sum_{V\subseteq A}\left(  -1\right)
^{\left\vert V\right\vert -\left\vert B\cap C\right\vert }\dbinom{\left\vert
V\right\vert }{\left\vert B\cap C\right\vert }\widetilde{\nabla}_{D,V}.
\]

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ An incomplete filtration}

\begin{itemize}
\item Now, fix a subset $D$ of $\left[  n\right]  $. Define%
\[
\mathcal{F}_{k}:=\operatorname*{span}\left\{  \widetilde{\nabla}_{D,C}%
\ \mid\ C\subseteq\left[  n\right]  \text{ with }\left\vert C\right\vert \leq
k\right\}
\]
for each $k\in\mathbb{Z}$. \pause
Of course,
\[
\mathcal{F}_{n}\supseteq\mathcal{F}_{n-1}\supseteq\cdots\supseteq
\mathcal{F}_{0}\supseteq\mathcal{F}_{-1}=0.
\]
\only<2>{It is easy to see that $\mathcal{F}_{0}$ is spanned by
$\widetilde{\nabla}_{D,\varnothing}=\nabla_{\varnothing,\varnothing}%
=\sum_{w\in S_{n}}w$.} \pause


\item For any subset $C\subseteq\left[  n\right]  $ and any $k\in\mathbb{N}$,
we define the integer%
\[
\delta_{D,C,k}:=\sum\limits_{\substack{B\subseteq D;\\\left\vert B\right\vert
=k}}\omega_{B,C}\left(  -1\right)  ^{k-\left\vert B\cap C\right\vert }%
\dbinom{k}{\left\vert B\cap C\right\vert }\in\mathbb{Z}.
\]
\pause


\item \textbf{Proposition.} Let $C\subseteq\left[  n\right]  $ satisfy
$\left\vert C\right\vert =\left\vert D\right\vert $. Let $k\in\mathbb{N}$.
Then,%
\[
\left(  \nabla_{D,C}-\delta_{D,C,k}\right)  \mathcal{F}_{k}\subseteq
\mathcal{F}_{k-1}.
\]
\pause


\item \textbf{Proof.} Follows from the rewritten product rule.

\vspace{9.0338pc}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Annihilating polynomials, 1}

\begin{itemize}
\item So we have proved $\left(  \nabla_{D,C}-\delta_{D,C,k}\right)
\mathcal{F}_{k}\subseteq\mathcal{F}_{k-1}$ whenever $\left\vert C\right\vert
=\left\vert D\right\vert $ and $k\in\mathbb{N}$. \pause


Since $\nabla_{D,C}\in\mathcal{F}_{n}$ and $\mathcal{F}_{-1}=0$, this entails
\[
\left(  \prod_{k=0}^{\left\vert D\right\vert }\left(  \nabla_{D,C}%
-\delta_{D,C,k}\right)  \right)  \nabla_{D,C}=0.
\]
\pause


\item However, the $\mathcal{F}_{k}$ depend only on $D$, not on $C$, so that
we can apply the same reasoning to any linear combination%
\[
\nabla_{D,\alpha}:=\sum_{\substack{C\subseteq\left[  n\right]  ;\\\left\vert
C\right\vert =\left\vert D\right\vert }}\alpha_{C}\nabla_{D,C}%
\]
of $\nabla_{D,C}$'s instead of a single $\nabla_{D,C}$. \pause

\item Thus we find:
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Annihilating polynomials, 2}

\begin{itemize}
\item \textbf{Theorem.} Let $D\subseteq\left[  n\right]  $. Let $\alpha
=\left(  \alpha_{C}\right)  _{C\subseteq\left[  n\right]  ;\ \left\vert
C\right\vert =\left\vert D\right\vert }$ be a family of scalars in
$\mathbf{k}$ indexed by the $\left\vert D\right\vert $-element subsets of
$\left[  n\right]  $. Then,%
\[
\left(  \prod_{k=0}^{\left\vert D\right\vert }\left(  \nabla_{D,\alpha}%
-\delta_{D,\alpha,k}\right)  \right)  \nabla_{D,\alpha}=0,
\]
where%
\begin{align*}
\nabla_{D,\alpha}  &  :=\sum_{\substack{C\subseteq\left[  n\right]
;\\\left\vert C\right\vert =\left\vert D\right\vert }}\alpha_{C}\nabla
_{D,C}\in\mathbf{k}\left[  S_{n}\right]  \ \ \ \ \ \ \ \ \ \ \text{and}\\
\delta_{D,\alpha,k}  &  :=\sum_{\substack{C\subseteq\left[  n\right]
;\\\left\vert C\right\vert =\left\vert D\right\vert }}\alpha_{C}\delta
_{D,C,k}\in\mathbf{k}.
\end{align*}
\pause


\item Thus, the minimal polynomial of $\nabla_{D,\alpha}$ splits over
$\mathbf{k}$. \pause


\item In particular, the minimal polynomial of $\widetilde{\nabla}_{D,C}$
splits over $\mathbb{Z}$ (since $\widetilde{\nabla}_{D,C}=\nabla_{D,\alpha}$
for an appropriate $\alpha$).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The formal Nabla-algebra: definition and conjecture}

\begin{itemize}
\item The product rule for the $\nabla$'s suggests another question. \pause


\item The $\nabla$'s are not linearly independent (e.g., we have $\nabla
_{B,A}=\nabla_{\left[  n\right]  \setminus B,\ \left[  n\right]  \setminus A}$).

What happens if we create linearly independent \textquotedblleft abstract
$\nabla$'s\textquotedblright\ (call them $\Delta$'s) and define their product
using the product rule? \pause


\item \textbf{Definition.} For any two subsets $A$ and $B$ of $\left[
n\right]  $ satisfying $\left\vert A\right\vert =\left\vert B\right\vert $,
introduce a formal symbol $\Delta_{B,A}$. Let $\mathcal{D}$ be the free
$\mathbf{k}$-module with basis $\left(  \Delta_{B,A}\right)  _{A,B\subseteq
\left[  n\right]  \text{ with }\left\vert A\right\vert =\left\vert
B\right\vert }$. Define a multiplication on $\mathcal{D}$ by%
\[
\Delta_{D,C}\Delta_{B,A}:=\omega_{B,C}\sum_{\substack{U\subseteq
D,\\V\subseteq A;\\\left\vert U\right\vert =\left\vert V\right\vert }}\left(
-1\right)  ^{\left\vert U\right\vert -\left\vert B\cap C\right\vert }%
\dbinom{\left\vert U\right\vert }{\left\vert B\cap C\right\vert }\Delta
_{U,V}.
\]
\pause


\item \textbf{Theorem.} This makes $\mathcal{D}$ into a nonunital $\mathbf{k}%
$-algebra. \pause


\item \textbf{Conjecture.} If $n!$ is invertible in $\mathbf{k}$, then this
algebra $\mathcal{D}$ has a unity.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The formal Nabla-algebra: examples}

\begin{itemize}
\item \textbf{Example.} For $n=1$, the nonunital algebra $\mathcal{D}$ has
basis $\left(  u,v\right)  $ with $u=\Delta_{\varnothing,\varnothing}$ and
$v=\Delta_{\left\{  1\right\}  ,\left\{  1\right\}  }$, and multiplication%
\[
uu=uv=vu=u,\ \ \ \ \ \ \ \ \ \ vv=v.
\]
It is just $\mathbf{k}\times\mathbf{k}$. \pause


\item \textbf{Example.} For $n=2$, the nonunital algebra $\mathcal{D}$ has
basis $\left(  u,v_{11},v_{12},v_{21},v_{22},w\right)  $ with $u=\Delta
_{\varnothing,\varnothing}$ and $v_{ij}=\Delta_{\left\{  i\right\}  ,\left\{
j\right\}  }$ and $w=\Delta_{\left[  2\right]  ,\left[  2\right]  }$. The
multiplication on $\mathcal{D}$ is%
\begin{align*}
uu  &  =uw=wu=2u,\ \ \ \ \ \ \ \ \ \ uv_{ij}=v_{ij}u=u,\\
v_{dc}v_{ba}  &  =u-v_{da}\ \ \ \ \ \ \ \ \ \ \text{if }b\neq c;\\
v_{dc}v_{ba}  &  =v_{da}\ \ \ \ \ \ \ \ \ \ \text{if }b=c,\\
v_{ij}w  &  =v_{i1}+v_{i2},\ \ \ \ \ \ \ \ \ \ wv_{ij}=v_{1j}+v_{2j},\\
ww  &  =2w.
\end{align*}
This nonunital $\mathbf{k}$-algebra $\mathcal{D}$ has a unity if and only if
$2$ is invertible in $\mathbf{k}$. This unity is $\dfrac{1}{4}\left(
v_{11}+v_{22}-v_{12}-v_{21}+2w\right)  $.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The formal Nabla-algebra: questions}

\begin{itemize}
\item \textbf{Question.} Is $\mathcal{D}$ a known object? Since $\mathcal{D}$
is a free $\mathbf{k}$-module of rank $\dbinom{2n}{n}$, could $\mathcal{D}$ be
a nonunital $\mathbb{Z}$-form of the planar rook algebra (which is known to be
$\cong\prod_{k=0}^{n}\mathbf{k}^{\dbinom{n}{k}\times\dbinom{n}{k}}$)? \pause

\item \textbf{Question.} Barring that, is there a nice proof of the above theorem?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Row-to-row sums}

\begin{itemize}
\item Let us generalize the $\nabla_{B,A}$. \pause


\item \textbf{Definition.} A \textbf{set composition} of $\left[  n\right]  $
is a tuple $\mathbf{U}=\left(  U_{1},U_{2},\ldots,U_{k}\right)  $ of disjoint
nonempty subsets of $\left[  n\right]  $ such that $U_{1}\cup U_{2}\cup
\cdots\cup U_{k}=\left[  n\right]  $. We set $\ell\left(  \mathbf{U}\right)
=k$ and call $k$ the \textbf{length} of $\mathbf{U}$. \pause


\item \textbf{Definition.} Let $\operatorname*{SC}\left(  n\right)  $ be the
set of all set compositions of $\left[  n\right]  $. \pause


\item \textbf{Definition.} If $\mathbf{A}=\left(  A_{1},A_{2},\ldots
,A_{k}\right)  $ and $\mathbf{B}=\left(  B_{1},B_{2},\ldots,B_{k}\right)  $
are two set compositions of $\left[  n\right]  $ having the same length, then
we define the \textbf{row-to-row sum}%
\[
\nabla_{\mathbf{B},\mathbf{A}}:=\sum_{\substack{w\in S_{n};\\w\left(
A_{i}\right)  =B_{i}\text{ for all }i}}w\ \ \ \ \ \ \ \ \ \ \text{in
}\mathbf{k}\left[  S_{n}\right]  .
\]
\pause


\item \textbf{Example.} We have
\[
\nabla_{B,A}=\nabla_{\mathbf{B},\mathbf{A}}\ \ \ \ \ \ \ \ \ \ \text{for
}\mathbf{B}=\left(  B,\ \left[  n\right]  \setminus B\right)  \text{ and
}\mathbf{A}=\left(  A,\ \left[  n\right]  \setminus A\right)  .
\]

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Simple properties and non-properties}

\begin{itemize}
\item \textbf{Proposition.} Let $\mathbf{A}=\left(  A_{1},A_{2},\ldots
,A_{k}\right)  $ and $\mathbf{B}=\left(  B_{1},B_{2},\ldots,B_{k}\right)  $.

\begin{enumerate}
\item[\textbf{(a)}] We have $\nabla_{\mathbf{B},\mathbf{A}}=0$ unless
$\left\vert A_{i}\right\vert =\left\vert B_{i}\right\vert $ for all $i$.

\item[\textbf{(b)}] We have $\nabla_{\mathbf{B},\mathbf{A}}=\nabla
_{\mathbf{B}\sigma,\mathbf{A}\sigma}$ for any $\sigma\in S_{k}$ (acting on set
compositions by permuting the blocks).

\item[\textbf{(c)}] We have $S\left(  \nabla_{\mathbf{B},\mathbf{A}}\right)
=\nabla_{\mathbf{A},\mathbf{B}}$, where $S\left(  w\right)  =w^{-1}$ for all
$w\in S_{n}$ as before. \pause

\end{enumerate}

\item The minimal polynomial of $\nabla_{\mathbf{B},\mathbf{A}}$ does not
always split over $\mathbb{Z}$ unless $\ell\left(  \mathbf{A}\right)  \leq2$.
\pause


\item The $\nabla_{\mathbf{B},\mathbf{A}}$ are not entirely new:

The \textbf{Murphy basis} of $\mathbf{k}\left[  S_{n}\right]  $ consists of
the elements $\nabla_{\mathbf{B},\mathbf{A}}$ for the \textbf{standard} set
compositions $\mathbf{A}$ and $\mathbf{B}$ of $\left[  n\right]  $. Here,
\textquotedblleft standard\textquotedblright\ means that the blocks are the
rows of a standard Young tableau (in particular, they must be of partition shape).

See \href{https://core.ac.uk/download/pdf/82422274.pdf}{G. E. Murphy,
\textit{On the Representation Theory of the Symmetric Groups and Associated
Hecke Algebras}, 1991}.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The span of the generalized $\nabla$'s, 1}

\begin{itemize}
\item \textbf{Theorem.} Let $\mathcal{A}=\mathbf{k}\left[  S_{n}\right]  $.
Let $k\in\mathbb{N}$. We define two $\mathbf{k}$-submodules $\mathcal{I}_{k}$
and $\mathcal{J}_{k}$ of $\mathcal{A}$ by
\[
\mathcal{I}_{k}:=\operatorname*{span}\left\{  \nabla_{\mathbf{B},\mathbf{A}%
}\ \mid\ \mathbf{A},\mathbf{B}\in\operatorname*{SC}\left(  n\right)  \text{
with }\ell\left(  \mathbf{A}\right)  =\ell\left(  \mathbf{B}\right)  \leq
k\right\}
\]
and%
\[
\mathcal{J}_{k}:=\mathcal{A}\cdot\operatorname*{span}\left\{
\boldsymbol{\alpha}_{U}^{-}\ \mid\ U\subseteq\left[  n\right]  \text{ of size
}k+1\right\}  \cdot\mathcal{A},
\]
where%
\[
\boldsymbol{\alpha}_{U}^{-}:=\sum_{\sigma\in S_{U}}\left(  -1\right)
^{\sigma}\sigma\in\mathbf{k}\left[  S_{n}\right]  .
\]
Then: \pause


\begin{enumerate}
\item[\textbf{(a)}] Both $\mathcal{I}_{k}$ and $\mathcal{J}_{k}$ are ideals of
$\mathcal{A}$, and are preserved under $S$.
\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The span of the generalized $\nabla$'s, 2}

\begin{itemize}
\item \textbf{Theorem (cont'd).}

\begin{enumerate}
\item[\textbf{(b)}] We have
\begin{align*}
\mathcal{I}_{k}  &  =\mathcal{J}_{k}^{\perp}=\operatorname*{LAnn}%
\mathcal{J}_{k}=\operatorname*{RAnn}\mathcal{J}_{k}%
\ \ \ \ \ \ \ \ \ \ \text{and}\\
\mathcal{J}_{k}  &  =\mathcal{I}_{k}^{\perp}=\operatorname*{LAnn}%
\mathcal{I}_{k}=\operatorname*{RAnn}\mathcal{I}_{k}.
\end{align*}
Here, $\mathcal{U}^{\perp}$ means orthogonal complement wrt the standard
bilinear form on $\mathcal{A}$, whereas $\operatorname*{LAnn}$ and
$\operatorname*{RAnn}$ mean left and right annihilators. \pause


\item[\textbf{(c)}] The $\mathbf{k}$-module $\mathcal{I}_{k}$ is free of rank
= \# of $\left(  1,2,\ldots,k+1\right)  $-avoiding permutations in $S_{n}$.

\item[\textbf{(d)}] The $\mathbf{k}$-module $\mathcal{J}_{k}$ is free of rank
= \# of $\left(  1,2,\ldots,k+1\right)  $-nonavoiding permutations in $S_{n}$.
\pause


\item[\textbf{(e)}] The quotients $\mathcal{A}/\mathcal{J}_{k}$ and
$\mathcal{A}/\mathcal{I}_{k}$ are also free, with the same ranks as
$\mathcal{I}_{k}$ and $\mathcal{J}_{k}$ (respectively), and with bases
consisting of (residue classes of) the relevant permutations.
\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The span of the generalized $\nabla$'s, 3}

\begin{itemize}
\item \textbf{Theorem (cont'd).}

\begin{enumerate}
\item[\textbf{(f)}] If $n!$ is invertible in $\mathbf{k}$, then $\mathcal{A}%
=\mathcal{I}_{k}\oplus\mathcal{J}_{k}$ (internal direct sum) as $\mathbf{k}%
$-modules, and $\mathcal{A}\cong\mathcal{I}_{k}\times\mathcal{J}_{k}$ as
$\mathbf{k}$-algebras. \pause

\end{enumerate}

\item \textbf{Proof.} When $\mathbf{k}$ is a char-$0$ field, this can be done
using representations (note that $\nabla_{\mathbf{B},\mathbf{A}}$ vanishes on
each Specht module $S^{\lambda}$ with $\ell\left(  \lambda\right)
>\ell\left(  \mathbf{A}\right)  $). In particular, $\mathcal{A}\cong%
\mathcal{I}_{k}\times\mathcal{J}_{k}$ is (up to iso? morally?) a coarsening of
the Artin--Wedderburn decomposition of $\mathcal{A}$. \pause

The case of general $\mathbf{k}$ is harder and has to be done from scratch.
\pause


\item \textbf{Question.} Is there a product rule for the $\nabla
_{\mathbf{B},\mathbf{A}}$'s?

\item \textbf{Question.} How much of the representation theory of $S_{n}$ can
be developed using the $\nabla_{\mathbf{B},\mathbf{A}}$'s? (e.g., I think you
can prove $\sum_{\lambda\vdash n}\left(  f^{\lambda}\right)  ^{2}=n!$ using
the Murphy basis and the Garnir relations.)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Unrelated(?): A commuting family, 1}

\begin{itemize}
\item Here is something rather different. \pause

\item The following is joint work with Theo Douvropoulos, inspired by the work
of Mukhin/Tarasov/Varchenko on the Gaudin Bethe ansatz. \pause


\item \textbf{Definition.} Let $\sigma\in S_{n}$ be a permutation. Then, we
define%
\begin{align*}
\operatorname*{exc}\sigma &  :=\left(  \text{\# of }i\in\left[  n\right]
\text{ such that }\sigma\left(  i\right)  >i\right)
\ \ \ \ \ \ \ \ \ \ \text{and}\\
\operatorname*{anxc}\sigma &  :=\left(  \text{\# of }i\in\left[  n\right]
\text{ such that }\sigma\left(  i\right)  <i\right)
\end{align*}
(the \textquotedblleft\textbf{excedance number}\textquotedblright\ and the
\textquotedblleft\textbf{anti-excedance number}\textquotedblright\ of $\sigma
$). \pause


\item For any $a,b\in\mathbb{N}$, define%
\[
\mathbf{X}_{a,b}:=\sum_{\substack{\sigma\in S_{n};\\\operatorname*{exc}%
\sigma=a;\\\operatorname*{anxc}\sigma=b}}\sigma\in\mathbf{k}\left[
S_{n}\right]  .
\]


\item \textbf{Conjecture.} The elements $\mathbf{X}_{a,b}$ for all
$a,b\in\mathbb{N}$ commute (for fixed $n$).

\item Checked for all $n\leq7$ using SageMath.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Unrelated(?): A commuting family, 2}

\begin{itemize}
\item The antipode plays well with these elements:%
\[
S\left(  \mathbf{X}_{a,b}\right)  =\mathbf{X}_{b,a}.
\]


\item \textbf{Question.} What can be said about the $\mathbf{k}$-subalgebra
$\mathbf{k}\left[  \mathbf{X}_{a,b}\ \mid\ a,b\in\left\{  0,1,\ldots
,n\right\}  \right]  $ of $\mathbf{k}\left[  S_{n}\right]  $ ? Note:%
\[%
\begin{tabular}
[c]{|c||c|c|c|c|c|c|}\hline
$n$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$\\\hline
$\dim\left(  \mathbb{Q}\left[  \mathbf{X}_{a,b}\right]  \right)  $ & $1$ & $2$
& $4$ & $10$ & $26$ & $76$\\\hline
\end{tabular}
\ \ \ .
\]
So far, this looks like the \# of involutions in $S_{n}$, which is exactly the
dimension of the Gelfand--Zetlin subalgebra (generated by the
Young--Jucys--Murphy elements)!

\item What is the exact relation?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Thank you,}

\begin{itemize}
\item \textbf{Per Alexandersson} and \textbf{Theo Douvropoulos} for
conversations in 2023 that motivated this project.

\item \textbf{Nadia Lafreni\`{e}re, Jon Novak, Vic Reiner, Richard P. Stanley}
for helpful comments.

\item \textbf{the organizers} for the invitation.

\item \textbf{you} for your patience.
\end{itemize}
\end{frame}

\begin{noncompile}
TODO: Add the following:
\begin{enumerate}
\item More about Chow's $x,y$ generalization. What can be extended?
\item Proof ideas for Main Theorems II-III in more details.
\item Antipode formula (even though known).
\item s-positivity for path digraphs.
\item w-generalization (Zabrocki).
\item The rest of the statements made in EC2add.
\end{enumerate}
\end{noncompile}



\end{document}