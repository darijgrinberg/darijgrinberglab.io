\documentclass[numbers=enddot,12pt,final,onecolumn,notitlepage]{scrartcl}%
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[all,cmtip]{xy}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{framed}
\usepackage{comment}
\usepackage{color}
\usepackage{hyperref}
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\usepackage{tikz}
\usepackage{needspace}
\usepackage{tabls}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Thursday, January 30, 2020 11:58:14}
%TCIDATA{SuppressPackageManagement}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{Language=American English}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\usetikzlibrary{arrows}
\newcounter{exer}
\newcounter{exera}
\theoremstyle{definition}
\newtheorem{theo}{Theorem}[section]
\newenvironment{theorem}[1][]
{\begin{theo}[#1]\begin{leftbar}}
{\end{leftbar}\end{theo}}
\newtheorem{lem}[theo]{Lemma}
\newenvironment{lemma}[1][]
{\begin{lem}[#1]\begin{leftbar}}
{\end{leftbar}\end{lem}}
\newtheorem{prop}[theo]{Proposition}
\newenvironment{proposition}[1][]
{\begin{prop}[#1]\begin{leftbar}}
{\end{leftbar}\end{prop}}
\newtheorem{defi}[theo]{Definition}
\newenvironment{definition}[1][]
{\begin{defi}[#1]\begin{leftbar}}
{\end{leftbar}\end{defi}}
\newtheorem{remk}[theo]{Remark}
\newenvironment{remark}[1][]
{\begin{remk}[#1]\begin{leftbar}}
{\end{leftbar}\end{remk}}
\newtheorem{coro}[theo]{Corollary}
\newenvironment{corollary}[1][]
{\begin{coro}[#1]\begin{leftbar}}
{\end{leftbar}\end{coro}}
\newtheorem{conv}[theo]{Convention}
\newenvironment{condition}[1][]
{\begin{conv}[#1]\begin{leftbar}}
{\end{leftbar}\end{conv}}
\newtheorem{quest}[theo]{Question}
\newenvironment{algorithm}[1][]
{\begin{quest}[#1]\begin{leftbar}}
{\end{leftbar}\end{quest}}
\newtheorem{warn}[theo]{Warning}
\newenvironment{conclusion}[1][]
{\begin{warn}[#1]\begin{leftbar}}
{\end{leftbar}\end{warn}}
\newtheorem{conj}[theo]{Conjecture}
\newenvironment{conjecture}[1][]
{\begin{conj}[#1]\begin{leftbar}}
{\end{leftbar}\end{conj}}
\newtheorem{exam}[theo]{Example}
\newenvironment{example}[1][]
{\begin{exam}[#1]\begin{leftbar}}
{\end{leftbar}\end{exam}}
\newtheorem{exmp}[exer]{Exercise}
\newenvironment{exercise}[1][]
{\begin{exmp}[#1]\begin{leftbar}}
{\end{leftbar}\end{exmp}}
\newtheorem{exetwo}[exera]{Additional exercise}
\newenvironment{addexercise}[1][]
{\begin{exetwo}[#1]\begin{leftbar}}
{\end{leftbar}\end{exetwo}}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\iffalse
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\fi
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}
\setlength\tablinesep{3pt}
\setlength\arraylinesep{3pt}
\setlength\extrarulesep{3pt}
\voffset=0cm
\hoffset=-0.7cm
\setlength\textheight{22.5cm}
\setlength\textwidth{15.5cm}
\newenvironment{verlong}{}{}
\newenvironment{vershort}{}{}
\newenvironment{noncompile}{}{}
\excludecomment{verlong}
\includecomment{vershort}
\excludecomment{noncompile}
\newcommand{\id}{\operatorname{id}}
\ihead{Why $\operatorname{Ring}\left(A,k\right)/G$ injects into $\operatorname{Ring}\left(A^{G},k\right)$}
\ohead{page \thepage}
\cfoot{}
\begin{document}

\title{Why $\operatorname*{Ring}\left(  A,k\right)  /G$ injects into
$\operatorname*{Ring}\left(  A^{G},k\right)  $}
\author{Darij Grinberg}
\date{\today}
\maketitle

I shall use the following notations:

\begin{itemize}
\item If $G$ is a group, and if $S$ is a $G$-set, then $S^{G}$ shall denote
the set of all fixed points under $G$ in $S$. (In other words, $S^{G}=\left\{
s\in S\ \mid\ gs=s\text{ for all }g\in G\right\}  $.)

\item If $G$ is a group, and if $S$ is a $G$-set, then $S/G$ shall denote the
set of all $G$-orbits on $S$. (In other words, $S/G=\left\{  Gs\ \mid\ s\in
S\right\}  $.)

\item If $U$ and $V$ are two rings, then $\operatorname*{Ring}\left(
U,V\right)  $ denotes the set of all ring homomorphisms from $U$ to $V$.
\end{itemize}

The crux of \cite[Lemma 4.9]{KucSch16} is the following elementary fact:

\begin{proposition}
\label{prop.4.9}Let $A$ be a commutative ring. Let $G$ be a finite group
acting on $A$ by ring automorphisms. Let $k$ be an integral domain. Notice
that $\operatorname*{Ring}\left(  A,k\right)  $ becomes a $G$-set in an
obvious way (namely, by setting $\left(  gx\right)  \left(  a\right)
=x\left(  g^{-1}a\right)  $ for all $g\in G$, $x\in\operatorname*{Ring}\left(
A,k\right)  $ and $a\in A$). Then, the map%
\begin{align*}
\operatorname*{Ring}\left(  A,k\right)  /G &  \rightarrow\operatorname*{Ring}%
\left(  A^{G},k\right)  ,\\
Gx &  \mapsto x\mid_{A^{G}}%
\end{align*}
is injective.
\end{proposition}

In other words, this says that if two ring homomorphisms $x:A\rightarrow k$
and $y:A\rightarrow k$ are identical on the invariant ring $A^{G}$ (that is,
we have $\left.  x\mid_{A^{G}}\right.  =\left.  y\mid_{A^{G}}\right.  $), then
$x$ and $y$ are in the same $G$-orbit on $\operatorname*{Ring}\left(
A,k\right)  $.

I shall give an elementary proof of Proposition \ref{prop.4.9} (using nothing
but Viete's formulas and basic properties of polynomial rings). First, let me
prove a lemma:

\begin{lemma}
\label{lem.4.9.1}Let $A$ be a commutative ring. Let $G$ be a finite group
acting on $A$ by ring automorphisms. Let $k$ be an integral domain. Let $x$
and $y$ be two elements of $\operatorname*{Ring}\left(  A,k\right)  $ such
that $\left.  x\mid_{A^{G}}\right.  =\left.  y\mid_{A^{G}}\right.  $. Let
$a\in A$. Then, there exists some $g\in G$ such that $x\left(  a\right)
=y\left(  ga\right)  $.
\end{lemma}

\begin{proof}
[Proof of Lemma \ref{lem.4.9.1}.]If $S$ is a finite set, if $R$ is a
commutative ring, if $\left(  b_{s}\right)  _{s\in S}\in R^{S}$ is a family of
elements of $R$, and if $\ell\in\mathbb{N}$, then we shall let $e_{\ell
}\left(  \left(  b_{s}\right)  _{s\in S}\right)  $ denote the $\ell$-th
elementary symmetric polynomial of the elements $b_{s}$ (with $s\in S$).
Explicitly, it is given by%
\[
e_{\ell}\left(  \left(  b_{s}\right)  _{s\in S}\right)  =\sum
_{\substack{T\subseteq S;\\\left\vert T\right\vert =\ell}}\prod_{t\in T}%
b_{t}.
\]
For example,%
\begin{align*}
e_{0}\left(  \left(  b_{s}\right)  _{s\in S}\right)   &
=1\ \ \ \ \ \ \ \ \ \ \text{and}\ \ \ \ \ \ \ \ \ \ e_{1}\left(  \left(
b_{s}\right)  _{s\in S}\right)  =\sum_{s\in S}b_{s}\\
\text{and}\ \ \ \ \ \ \ \ \ \ e_{\left\vert S\right\vert }\left(  \left(
b_{s}\right)  _{s\in S}\right)   &  =\prod_{s\in S}b_{s}.
\end{align*}
The following fact is a form of Viete's relations:

\begin{statement}
\textit{Fact 1:} Let $S$ be a finite set. Let $R$ be a commutative ring. Let
$\left(  b_{s}\right)  _{s\in S}\in R^{S}$ be a family of elements of $R$. Let
$t\in R$. Then,%
\[
\prod_{s\in S}\left(  t-b_{s}\right)  =\sum_{\ell=0}^{\left\vert S\right\vert
}t^{\left\vert S\right\vert -\ell}\left(  -1\right)  ^{\ell}e_{\ell}\left(
\left(  b_{s}\right)  _{s\in S}\right)  .
\]

\end{statement}

(Fact 1 follows easily by expanding the product $\prod_{s\in S}\left(
t-b_{s}\right)  $ and collecting like powers of $t$.)

Now, let us return to the proof of Lemma \ref{lem.4.9.1}. Fix $\ell
\in\mathbb{N}$. Set $\varepsilon_{\ell}=e_{\ell}\left(  \left(  ga\right)
_{g\in G}\right)  \in A$.

Each element of the group $G$ merely permutes the elements of the family
$\left(  ga\right)  _{g\in G}$. Thus, the element $e_{\ell}\left(  \left(
ga\right)  _{g\in G}\right)  $ is invariant under $G$ (being defined as a
symmetric polynomial in this family), and thus lies in $A^{G}$. Thus,
$e_{\ell}\left(  \left(  ga\right)  _{g\in G}\right)  \in A^{G}$, so that
$\varepsilon_{\ell}=e_{\ell}\left(  \left(  ga\right)  _{g\in G}\right)  \in
A^{G}$. Hence,%
\begin{equation}
x\left(  \varepsilon_{\ell}\right)  =\underbrace{\left(  x\mid_{A^{G}}\right)
}_{=\left.  y\mid_{A^{G}}\right.  }\left(  \varepsilon_{\ell}\right)  =\left(
y\mid_{A^{G}}\right)  \left(  \varepsilon_{\ell}\right)  =y\left(
\varepsilon_{\ell}\right)  . \label{pf.lem.4.9.1.1}%
\end{equation}


But from $\varepsilon_{\ell}=e_{\ell}\left(  \left(  ga\right)  _{g\in
G}\right)  $, we obtain
\begin{equation}
x\left(  \varepsilon_{\ell}\right)  =x\left(  e_{\ell}\left(  \left(
ga\right)  _{g\in G}\right)  \right)  =e_{\ell}\left(  \left(  x\left(
ga\right)  \right)  _{g\in G}\right)  \label{pf.lem.4.9.1.2a}%
\end{equation}
(since $x$ is a ring homomorphism while $e_{\ell}$ is a natural
transformation) and similarly%
\begin{equation}
y\left(  \varepsilon_{\ell}\right)  =e_{\ell}\left(  \left(  y\left(
ga\right)  \right)  _{g\in G}\right)  . \label{pf.lem.4.9.1.2b}%
\end{equation}
Hence, (\ref{pf.lem.4.9.1.2a}) yields%
\begin{equation}
e_{\ell}\left(  \left(  x\left(  ga\right)  \right)  _{g\in G}\right)
=x\left(  \varepsilon_{\ell}\right)  =y\left(  \varepsilon_{\ell}\right)
=e_{\ell}\left(  \left(  y\left(  ga\right)  \right)  _{g\in G}\right)  .
\label{pf.lem.4.9.1.4}%
\end{equation}


Now, forget that we fixed $\ell$. We thus have shown that
(\ref{pf.lem.4.9.1.4}) holds for every $\ell\in\mathbb{N}$.

In the polynomial ring $k\left[  t\right]  $, we have%
\begin{equation}
\prod_{g\in G}\left(  t-x\left(  ga\right)  \right)  =\sum_{\ell
=0}^{\left\vert G\right\vert }t^{\left\vert G\right\vert -\ell}\left(
-1\right)  ^{\ell}e_{\ell}\left(  \left(  x\left(  ga\right)  \right)  _{g\in
G}\right)  \label{pf.lem.4.9.1.6a}%
\end{equation}
(by Fact 1, applied to $R=k\left[  t\right]  $ and $S=G$ and $\left(
b_{s}\right)  _{s\in S}=\left(  x\left(  ga\right)  \right)  _{g\in G}$) and
similarly%
\begin{equation}
\prod_{g\in G}\left(  t-y\left(  ga\right)  \right)  =\sum_{\ell
=0}^{\left\vert G\right\vert }t^{\left\vert G\right\vert -\ell}\left(
-1\right)  ^{\ell}e_{\ell}\left(  \left(  y\left(  ga\right)  \right)  _{g\in
G}\right)  . \label{pf.lem.4.9.1.6b}%
\end{equation}
From (\ref{pf.lem.4.9.1.4}), we see that the right hand sides of
(\ref{pf.lem.4.9.1.6a}) and (\ref{pf.lem.4.9.1.6b}) are equal. Hence, so are
the left hand sides. In other words,%
\[
\prod_{g\in G}\left(  t-x\left(  ga\right)  \right)  =\prod_{g\in G}\left(
t-y\left(  ga\right)  \right)
\]
in $k\left[  t\right]  $. If we evaluate both sides of this equality at
$t=x\left(  a\right)  $, we obtain%
\begin{equation}
\prod_{g\in G}\left(  x\left(  a\right)  -x\left(  ga\right)  \right)
=\prod_{g\in G}\left(  x\left(  a\right)  -y\left(  ga\right)  \right)  .
\label{pf.lem.4.9.1.8}%
\end{equation}
The factor of the product $\prod_{g\in G}\left(  x\left(  a\right)  -x\left(
ga\right)  \right)  $ for $g=1$ is $0$. Thus, the whole product is $0$. In
other words, the left hand side of (\ref{pf.lem.4.9.1.8}) is $0$. Hence, so is
the right hand side. In other words, $\prod_{g\in G}\left(  x\left(  a\right)
-y\left(  ga\right)  \right)  =0$. Since $k$ is an integral domain, this shows
that there exists some $g\in G$ such that $x\left(  a\right)  -y\left(
ga\right)  =0$. In other words, there exists some $g\in G$ such that $x\left(
a\right)  =y\left(  ga\right)  $. Lemma \ref{lem.4.9.1} is proven.
\end{proof}

\begin{proof}
[Proof of Proposition \ref{prop.4.9}.]We must show that if $x$ and $y$ are two
elements of $\operatorname*{Ring}\left(  A,k\right)  $ such that $\left.
x\mid_{A^{G}}\right.  =\left.  y\mid_{A^{G}}\right.  $, then $Gx=Gy$.

Indeed, assume the contrary. Then, there exist two elements $x$ and $y$ of
$\operatorname*{Ring}\left(  A,k\right)  $ such that $\left.  x\mid_{A^{G}%
}\right.  =\left.  y\mid_{A^{G}}\right.  $ but $Gx\neq Gy$. Consider these $x$
and $y$. From $Gx\neq Gy$, we obtain $x\notin Gy$. Hence, for every $g\in G$,
we have $x\neq gy$. Hence, for every $g\in G$, there exists some $a_{g}\in A$
such that $x\left(  a_{g}\right)  \neq\left(  gy\right)  \left(  a_{g}\right)
$. Consider this $a_{g}$.

For each $g\in G$, introduce a new indeterminate $s_{g}$. For each commutative
ring $B$, we let $\widetilde{B}$ denote the polynomial ring $B\left[
s_{g}\ \mid\ g\in G\right]  $ in all these indeterminates. The polynomial ring
$\widetilde{k}=k\left[  s_{g}\ \mid\ g\in G\right]  $ is an integral domain
(since $k$ is an integral domain). The polynomial ring $\widetilde{A}=A\left[
s_{g}\ \mid\ g\in G\right]  $ is equipped with a $G$-action by automorphisms:
namely, we let $G$ act on the coefficients (that is, the inclusion
$A\rightarrow\widetilde{A}$ should be $G$-equivariant), while leaving all
indeterminates $s_{g}$ unchanged (that is, we have $hs_{g}=s_{g}$ for all
$g,h\in G$; not $hs_{g}=s_{hg}$).

Thus, a polynomial $f\in\widetilde{A}=A\left[  s_{g}\ \mid\ g\in G\right]  $
is a fixed point under $G$ if and only if all its coefficients are fixed
points under $G$. In other words, $\widetilde{A}^{G}=A^{G}\left[  s_{g}%
\ \mid\ g\in G\right]  $.

Define an element $a$ of $\widetilde{A}$ by $a=\sum_{h\in G}a_{h}s_{h}$.

Any ring homomorphism $f:A\rightarrow k$ canonically induces a ring
homomorphism $\widetilde{f}$ from $\widetilde{A}=A\left[  s_{g}\ \mid\ g\in
G\right]  $ to $\widetilde{k}=k\left[  s_{g}\ \mid\ g\in G\right]  $ which
homomorphism acts as $f$ on the coefficients (that is, $\widetilde{f}\left(
\alpha\right)  =f\left(  \alpha\right)  $ for each $\alpha\in k$) while
leaving the indeterminates $s_{g}$ unchanged (that is, $\widetilde{f}\left(
s_{g}\right)  =s_{g}$ for each $g\in G$). Thus, in particular, the two ring
homomorphisms $x$ and $y$ from $A$ to $k$ canonically induce two ring
homomorphisms $\widetilde{x}$ and $\widetilde{y}$ from $\widetilde{A}=A\left[
s_{g}\ \mid\ g\in G\right]  $ to $\widetilde{k}=k\left[  s_{g}\ \mid\ g\in
G\right]  $ (which homomorphisms act as $x$ and $y$ (respectively) on the
coefficients while leaving the indeterminates unchanged). These new ring
homomorphisms $\widetilde{x}$ and $\widetilde{y}$ have the property that
\[
\left.  \widetilde{x}\mid_{A^{G}\left[  s_{g}\ \mid\ g\in G\right]  }\right.
=\left.  \widetilde{y}\mid_{A^{G}\left[  s_{g}\ \mid\ g\in G\right]  }\right.
\]
(since $\left.  x\mid_{A^{G}}\right.  =\left.  y\mid_{A^{G}}\right.  $ and
since $\widetilde{x}\left(  s_{g}\right)  =s_{g}=\widetilde{y}\left(
s_{g}\right)  $ for each $g\in G$). This rewrites as
\[
\left.  \widetilde{x}\mid_{\widetilde{A}^{G}}\right.  =\left.  \widetilde{y}%
\mid_{\widetilde{A}^{G}}\right.
\]
(since $\widetilde{A}^{G}=A^{G}\left[  s_{g}\ \mid\ g\in G\right]  $). Hence,
Lemma \ref{lem.4.9.1} (applied to $\widetilde{A}$, $\widetilde{k}$,
$\widetilde{x}$ and $\widetilde{y}$ instead of $A$, $k$, $x$ and $y$) shows
that there exists some $g\in G$ such that $\widetilde{x}\left(  a\right)
=\widetilde{y}\left(  ga\right)  $. Consider this $g$.

From $a=\sum_{h\in G}a_{h}s_{h}$, we obtain
\begin{equation}
\widetilde{x}\left(  a\right)  =\widetilde{x}\left(  \sum_{h\in G}a_{h}%
s_{h}\right)  =\sum_{h\in G}x\left(  a_{h}\right)  s_{h} \label{pf.prop.4.9.3}%
\end{equation}
(by the definition of $\widetilde{x}$), but also
\[
ga=g\sum_{h\in G}a_{h}s_{h}=\sum_{h\in G}ga_{h}s_{h}.
\]
Applying the map $\widetilde{y}$ to the latter equality, we find%
\[
\widetilde{y}\left(  ga\right)  =\widetilde{y}\left(  \sum_{h\in G}ga_{h}%
s_{h}\right)  =\sum_{h\in G}y\left(  ga_{h}\right)  s_{h}%
\ \ \ \ \ \ \ \ \ \ \left(  \text{by the definition of }\widetilde{y}\right)
.
\]
Hence, (\ref{pf.prop.4.9.3}) yields%
\[
\sum_{h\in G}x\left(  a_{h}\right)  s_{h}=\widetilde{x}\left(  a\right)
=\widetilde{y}\left(  ga\right)  =\sum_{h\in G}y\left(  ga_{h}\right)  s_{h}.
\]
Comparing coefficients before $s_{h}$ in this equality, we conclude that%
\begin{equation}
x\left(  a_{h}\right)  =y\left(  ga_{h}\right)  \ \ \ \ \ \ \ \ \ \ \text{for
all }h\in G. \label{pf.prop.4.9.6}%
\end{equation}
Applying this to $h=g^{-1}$, we find $x\left(  a_{g^{-1}}\right)  =y\left(
ga_{g^{-1}}\right)  $. But the definition of $a_{g^{-1}}$ yields $x\left(
a_{g^{-1}}\right)  \neq\left(  g^{-1}y\right)  \left(  a_{g^{-1}}\right)
=y\left(  \underbrace{\left(  g^{-1}\right)  ^{-1}}_{=g}a_{g^{-1}}\right)
=y\left(  ga_{g^{-1}}\right)  $, which contradicts $x\left(  a_{g^{-1}%
}\right)  =y\left(  ga_{g^{-1}}\right)  $. This contradiction completes our proof.
\end{proof}

\begin{thebibliography}{99999999}                                                                                         %


\bibitem[KucSch16]{KucSch16}\href{https://arxiv.org/abs/1609.04717v2}{Robert
A. Kucharczyk, Peter Scholze, \textit{Topological realisations of absolute
Galois groups}, arXiv:1609.04717v2.}
\end{thebibliography}


\end{document}