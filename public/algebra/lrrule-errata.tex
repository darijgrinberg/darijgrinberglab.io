\documentclass[numbers=enddot,12pt,final,onecolumn,notitlepage]{scrartcl}%
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[all,cmtip]{xy}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{comment}
\usepackage{color}
\usepackage{hyperref}
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\usepackage{amsthm}
\usepackage{ytableau}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Sunday, April 10, 2016 23:30:37}
%TCIDATA{SuppressPackageManagement}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\newcommand{\id}{\operatorname*{id}}
\setlength\textheight{22.5cm}
\setlength\textwidth{15cm}
\ihead{Errata to ``A short proof of the Littlewood-Richardson rule''}
\ohead{\today}
\begin{document}

\begin{center}
\textbf{A short proof of the Littlewood-Richardson rule}

\textit{Vesselin Gasharov}

\texttt{gasharov - lrrule.ps} (preprint available at \url{http://www.math.cornell.edu/~vesko/papers/lrrule.ps})

\textbf{Errata (collected by Darij Grinberg)}

\bigskip


\end{center}

The following list of errata refers to the preprint version of Vesselin
Gasharov's article \textquotedblleft A short proof of the
Littlewood-Richardson rule\textquotedblright\ available from his website
(\url{http://www.math.cornell.edu/~vesko/papers/lrrule.ps}). The same errors
appear in the published version
(\href{http://www.sciencedirect.com/science/article/pii/S0195669898902128}{European
Journal of Combinatorics, Volume 19, Issue 4, May 1998, Pages 451--453}),
although the page numbers in the published version are different.

I will refer to the results appearing in the preprint by the numbers under
which they appear in it.

\begin{itemize}
\item \textbf{Page 1, Definition 1.1:} Here, the author defines $N\left(
i,w_{\leq r}\right)  $ to mean ``the number of occurrences of the symbol $i$
in $w_{\leq r}$''. There is nothing wrong about this definition, but later in
the text the notation $N\left(  i,v\right)  $ is used for various words $v$
which aren't always given in the form $w_{\leq r}$ for some $w$ and $r$. So a
more general definition would be good, such as the following one: ``For any
$i\geq1$ and any word $v$, let $N\left(  i,v\right)  $ denote the number of
occurrences of the symbol $i$ in $v$.''

\item \textbf{Page 1, Definition 1.1:} While $w_{\leq r}$ is defined in this
definition, $w_{>r}$ (a notation used in the proof of Proposition 2.1) is not
defined. It should be defined, for example as follows: ``For $0\leq r\leq n$,
let $w_{>r}$ denote the word $w_{r+1}...w_{n}$.''

\item \textbf{Page 1:} I am not sure what ``Theorem 1.2 is not more general''
means. If it means to say that Theorem 1.2 follows easily from the classical
formulation (which requires $\theta=0$), then I don't see how it follows from
it. If it merely means that the classical formulation is enough to compute all
$\left\langle s_{\lambda\diagup\mu},s_{\nu\diagup\theta}\right\rangle $, that
is true, but I would word it differently to avoid confusion.

\item \textbf{Page 2:} Replace ``For a partition $\pi$'' by ``For a
permutation $\pi$''.

\item \textbf{Page 3, proof of Proposition 2.1:} Here it is claimed that ``The
fact that all $\left(  i+1\right)  $'s in $R$ which are in $w_{>r}$ are free
implies'' (4). This is a slightly incomplete argument, because the fact that
all $\left(  i+1\right)  $'s in $R$ which are in $w_{>r}$ are free does not
guarantee that there are no non-free $\left(  i+1\right)  $'s in the row
directly under $R$. Fortunately, this gap is easy to fill; here is the precise argument:

There are no $\left(  i+1\right)  $'s in $w_{>r}$ in columns weakly to the
right of $C$ (because any such $\left(  i+1\right)  $'s would lie weakly to
the right and strictly below $w_{r}$, so they would have to be $>w_{r}$
(because the tableau $P$ is column-strict), which is absurd because
$w_{r}=i+1$). In particular, there are no non-free $\left(  i+1\right)  $'s in
$w_{>r}$ in these columns. This (combined with the fact that all $\left(
i+1\right)  $'s in $R$ which are in $w_{>r}$ are free) yields that for each
non-free $\left(  i+1\right)  $ in $w_{>r}$, the $i$ directly above it also
belongs to $w_{>r}$. Hence, the non-free $\left(  i+1\right)  $'s in $w_{>r}$
are in 1-to-1 correspondence with the non-free $i$'s in $w_{>r}$, so that
their contributions to $N\left(  i,w_{>r}\right)  $ and to $N\left(
i+1,w_{>r}\right)  $ are the same.

\item \textbf{Page 4, proof of Theorem 1.2:} The ``$Tab$'' should be a roman
``$\operatorname*{Tab}$''.

\item \textbf{Page 4, proof of Theorem 1.2:} The equality%
\[
\sum\limits_{\pi\in S_{l}}\operatorname*{sgn}\left(  \pi\right)  \left\langle
s_{\lambda\diagup\mu},h_{\pi\left(  \nu\right)  -\theta}\right\rangle
=\sum\limits_{\pi\in S_{l}}\operatorname*{sgn}\left(  \pi\right)  \left\vert
\operatorname*{Tab}\left(  \lambda\diagup\mu,\pi\left(  \nu\right)
-\theta\right)  \right\vert
\]
might need a couple more explanations. The proof of this equality goes as follows:

It is clearly enough to show that $\left\langle s_{\lambda\diagup\mu}%
,h_{\pi\left(  \nu\right)  -\theta}\right\rangle =\left\vert
\operatorname*{Tab}\left(  \lambda\diagup\mu,\pi\left(  \nu\right)
-\theta\right)  \right\vert $ for every $\pi\in S_{l}$. So let $\pi\in S_{l}$.
When the $l$-tuple $\pi\left(  \nu\right)  -\theta$ has a negative entry, both
$h_{\pi\left(  \nu\right)  -\theta}$ and $\left\vert \operatorname*{Tab}%
\left(  \lambda\diagup\mu,\pi\left(  \nu\right)  -\theta\right)  \right\vert $
are $0$, so that the equality $\left\langle s_{\lambda\diagup\mu}%
,h_{\pi\left(  \nu\right)  -\theta}\right\rangle =\left\vert
\operatorname*{Tab}\left(  \lambda\diagup\mu,\pi\left(  \nu\right)
-\theta\right)  \right\vert $ is trivial in this case. Hence, we can WLOG
assume that we are not in this case. Assume this. Then, the $l$-tuple
$\pi\left(  \nu\right)  -\theta$ consists of nonnegative integers only. Let
$\kappa$ denote the partition obtained by removing all zero entries from this
$l$-tuple $\pi\left(  \nu\right)  -\theta$ and reordering all the remaining
entries in nonincreasing order.

Recall that $s_{\lambda\diagup\mu}=\sum\limits_{\substack{P\text{ is a
tableau}\\\text{of shape }\lambda\diagup\mu}}x^{P}$. Hence, if $\eta$ is any
$l$-tuple of nonnegative integers, then%
\begin{equation}
\left(  \text{the coefficient of }s_{\lambda\diagup\mu}\text{ before }x^{\eta
}\right)  =\left\vert \operatorname*{Tab}\left(  \lambda\diagup\mu
,\eta\right)  \right\vert .\label{p4.3}%
\end{equation}


Now, it is known that $\left(  h_{\lambda}\right)  _{\lambda\text{ is a
partition}}$ and $\left(  m_{\lambda}\right)  _{\lambda\text{ is a partition}%
}$ are orthogonal bases of the vector space of symmetric functions (where
$m_{\lambda}$ denotes the $\lambda$-th monomial symmetric function). Hence,
for every symmetric function $f$ and every partition $\tau$, we have%
\begin{align*}
\left\langle f,h_{\tau}\right\rangle  &  =\left(  \text{the }m_{\tau
}\text{-coordinate of }f\text{ with respect to the basis }\left(  m_{\lambda
}\right)  _{\lambda\text{ is a partition}}\right)  \\
&  =\left(  \text{the coefficient of }f\text{ before }x^{\tau}\right)  .
\end{align*}
Hence, if $f$ is a symmetric function, and $\phi$ is a tuple of nonnegative
integers, and if $\tau$ is the partition obtained by removing all zero entries
from this tuple $\phi$ and reordering all the remaining entries in
nonincreasing order, then we have%
\begin{align*}
\left\langle f,h_{\phi}\right\rangle  &  =\left\langle f,h_{\tau}\right\rangle
\ \ \ \ \ \ \ \ \ \ \left(
\begin{array}
[c]{c}%
\text{since }h_{\phi}=h_{\tau}\text{ (because the product }h_{\phi}\\
\text{depends neither on the order of its factors}\\
\text{nor on the appearance of }h_{0}=1\text{ factors)}%
\end{array}
\right)  \\
&  =\left(  \text{the coefficient of }f\text{ before }x^{\tau}\right)  \\
&  =\left(  \text{the coefficient of }f\text{ before }x^{\phi}\right)  \\
&  \ \ \ \ \ \ \ \ \ \ \left(
\begin{array}
[c]{c}%
\text{since the function }f\text{ is symmetric, and thus its}\\
\text{coefficients before any two monomials with the}\\
\text{same multisets of positive exponents are equal}%
\end{array}
\right)  .
\end{align*}
Applying this to $f=s_{\lambda\diagup\mu}$, $\tau=\pi\left(  \nu\right)
-\theta$ and $\phi=\kappa$, we obtain%
\begin{align*}
\left\langle s_{\lambda\diagup\mu},h_{\kappa}\right\rangle  &  =\left(
\text{the coefficient of }s_{\lambda\diagup\mu}\text{ before }x^{\pi\left(
\nu\right)  -\theta}\right)  \\
&  =\left\vert \operatorname*{Tab}\left(  \lambda\diagup\mu,\pi\left(
\nu\right)  -\theta\right)  \right\vert \ \ \ \ \ \ \ \ \ \ \left(  \text{by
(\ref{p4.3}), applied to }\eta=\pi\left(  \nu\right)  -\theta\right)  ,
\end{align*}
qed.

\item \textbf{Page 4, proof of Theorem 1.2:} Replace \textquotedblleft%
$w$\textquotedblright\ by \textquotedblleft$w:=w\left(  P\right)
$\textquotedblright\ in \textquotedblleft implies that $w$ is a $\theta
$-lattice permutation\textquotedblright.

\item \textbf{Page 4, proof of Theorem 1.2:} After \textquotedblleft which
implies that $\pi$ is the identity permutation\textquotedblright, maybe add an
explanation why this is true. For example, one such explanation would be
\textquotedblleft(because $\pi\left(  \nu\right)  _{i+1}\leq\pi\left(
\nu\right)  _{i}$ rewrites as $\nu_{\pi\left(  i+1\right)  }-\pi\left(
i+1\right)  <\nu_{\pi\left(  i\right)  }-\pi\left(  i\right)  $, which can
hold for all $i\geq1$ only when $\pi=\operatorname*{id}$)\textquotedblright.
\end{itemize}


\end{document}