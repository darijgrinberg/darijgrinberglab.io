% Abstract:

% The symmetric group algebra is the $n$-th graded component of the Malvenuto--Reutenauer Hopf algebra of permutations. But it has its own multiplication (given by composition of permutations) which is no less mysterious. I will present three open questions about this multiplication: the apparent commutativity of certain excedance-related sums; a $\mathbb{Z}$-module basis for the Gelfand--Tsetlin subalgebra; and a $\mathbb{Z}$-module basis for bad-shaped Specht modules. The latter two questions are related to the modular representation theory of symmetric groups.

\documentclass{beamer}%
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{array}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{etex}
\usepackage{amsthm}
\usepackage{color}
\usepackage{wasysym}
\usepackage[all]{xy}
\usepackage{textpos}
\usepackage{url}
\usepackage{color}
\usepackage{epsfig,amsfonts,bbm,mathrsfs}
\usepackage{verbatim}
\usepackage{amsfonts}
\usepackage{ytableau}
\usepackage{tikz}
\usepackage{hyperref}%
\setcounter{MaxMatrixCols}{30}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Tuesday, June 18, 2024 13:47:41}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\definecolor{grau}{rgb}{.5 , .5 , .5}
\definecolor{dunkelgrau}{rgb}{.35 , .35 , .35}
\definecolor{schwarz}{rgb}{0 , 0 , 0}
\definecolor{violet}{RGB}{143,0,255}
\definecolor{forestgreen}{RGB}{34, 100, 34}
\newcommand{\red}{\color{red}}
\newcommand{\grey}{\color{grau}}
\newcommand{\green}{\color{forestgreen}}
\newcommand{\violet}{\color{violet}}
\newcommand{\blue}{\color{blue}}
\newcommand{\bIf}{\textbf{If} }
\newcommand{\bif}{\textbf{if} }
\newcommand{\bthen}{\textbf{then} }
\newcommand{\ZZ}{{\mathbb Z}}
\newcommand{\NN}{{\mathbb N}}
\newcommand{\QQ}{{\mathbb Q}}
\newcommand{\RR}{{\mathbb R}}
\newcommand{\CC}{{\mathbb C}}
\newcommand\arxiv[1]{\href{http://www.arxiv.org/abs/#1}{\texttt{arXiv:#1}}}
\newcommand{\OO}{\operatorname {O}}
\newcommand{\id}{\operatorname {id}}
\newcommand{\Sym}{\operatorname {Sym}}
\newcommand{\Nm}{\operatorname {N}}
\newcommand{\GL}{\operatorname {GL}}
\newcommand{\SL}{\operatorname {SL}}
\newcommand{\Or}{\operatorname {O}}
\newcommand{\im}{\operatorname {Im}}
\newcommand{\Iso}{\operatorname {Iso}}
\newcommand{\zero}{\mathbf{0}}
\newcommand{\ord}{\operatorname*{ord}}
\newcommand{\bbK}{{\mathbb{K}}}
\newcommand{\whP}{{\widehat{P}}}
\newcommand{\Trop}{\operatorname*{Trop}}
\newcommand{\TropZ}{{\operatorname*{Trop}\mathbb{Z}}}
\newcommand{\rato}{\dashrightarrow}
\newcommand{\lcm}{\operatorname*{lcm}}
\newcommand{\tlab}{\operatorname*{tlab}}
\newcommand{\are}{\ar@{-}}
\newcommand{\set}[1]{\left\{ #1 \right\}}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\tup}[1]{\left( #1 \right)}
\newcommand{\ive}[1]{\left[ #1 \right]}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\lf}[2]{#1^{\underline{#2}}}
\newcommand{\upslack}{\mathchoice{\rotatebox[origin=c]{180}{$\displaystyle A$}}{\rotatebox[origin=c]{180}{$\textstyle A$}}{\rotatebox[origin=c]{180}{$\scriptstyle A$}}{\rotatebox[origin=c]{180}{$\scriptscriptstyle A$}}}
\newcommand{\downslack}{A}
\newcommand{\bfupslack}{\mathchoice{\rotatebox[origin=c]{180}{$\displaystyle \mathbf{A}$}}{\rotatebox[origin=c]{180}{$\textstyle \mathbf{A}$}}{\rotatebox[origin=c]{180}{$\scriptstyle \mathbf{A}$}}{\rotatebox[origin=c]{180}{$\scriptscriptstyle \mathbf{A}$}}}
\newcommand{\bfdownslack}{\mathbf{A}}
\newcommand{\bfU}{\mathbf{U}}
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}
\usetheme{Frankfurt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{itemize/enumerate body begin}{}
\setbeamertemplate{itemize/enumerate subbody begin}{\normalsize}
\beamersetuncovermixins{\opaqueness<1>{0}}{\opaqueness<2->{15}}
\newcommand{\STRUT}{\vrule width 0pt depth 8pt height 0pt}
\newcommand{\ASTRUT}{\vrule width 0pt depth 0pt height 11pt}
\theoremstyle{plain}
\newtheorem{conj}[theorem]{Conjecture}
\setbeamertemplate{headline}{}
\begin{document}

\author{Darij Grinberg (Drexel University)}
\title{Three questions on symmetric group algebras}
\date{2024-06-20, Sophus Lie Conference Center, Nordfjordeid, NO}

\frame{\titlepage\textbf{slides:} {\color{red}
\url{http://www.cip.ifi.lmu.de/~grinberg/algebra/nfe2024.pdf}}}

\begin{frame}
\frametitle{\ \ \ \ The symmetric group algebra}

\begin{itemize}
\item Fix an $n\in\mathbb{N}$ and a commutative ring $\mathbf{k}$.

\item Let $\mathcal{A}=\mathbf{k}\left[  S_{n}\right]  $ be the group algebra
of the symmetric group $S_{n}$ (aka $\mathfrak{S}_{n}$) over $\mathbf{k}$.

\pause

\item It consists of formal linear combinations of the $n!$ permutations $w\in
S_{n}$. Multiplication is composition of permutations (+ expanding sums).

\pause

\item \textbf{Example:} For $n=3$, we have
\begin{align*}
\left(  1+s_{1}\right)  \left(  1-s_{1}\right)   &  =1+s_{1}-s_{1}-s_{1}%
^{2}=1+s_{1}-s_{1}-1=0;\\
\left(  1+s_{2}\right)  \left(  1+s_{1}+s_{1}s_{2}\right)   &  =1+s_{2}%
+s_{1}+s_{2}s_{1}+s_{1}s_{2}+s_{2}s_{1}s_{2}\\
&  =\sum_{w\in S_{3}}w.
\end{align*}
Here, $s_{i}$ is the simple transposition swapping $i$ with $i+1$.

\pause

\item As a $\mathbf{k}$-module, $\mathcal{A}$ is just the $n$-th graded
component of the Malvenuto--Reutenauer Hopf algebra
$\mathbf{\operatorname*{FQSym}}$, but its multiplication is the inner (not the
standard) multiplication.
\end{itemize}

\vspace{9.0677pc}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ The Young--Jucys--Murphy elements}

\begin{itemize}
\item Some of the nicest elements of $\mathcal{A}$ are the
\textbf{Young--Jucys--Murphy elements}%
\[
\mathbf{m}_{k}:=t_{1,k}+t_{2,k}+\cdots+t_{k-1,k}\ \ \ \ \ \ \ \ \ \ \text{for
all }1\leq k\leq n,
\]
where $t_{i,j}$ is the transposition swapping $i$ with $j$.

\only<2>{\item Note that $\mathbf{m}_{1}=0$.}

\pause \pause

\item \textbf{Theorem (easy exercise).} The $n$ elements $\mathbf{m}%
_{1},\mathbf{m}_{2},\ldots,\mathbf{m}_{n}$ commute.

\pause

\item The subalgebra of $\mathcal{A}$ they generate is called the
\textbf{Gelfand--Tsetlin subalgebra} $\operatorname*{GZ}\nolimits_{n}$.

\pause

\only<5>{
\item \textbf{Theorem (Murphy, ca. 1980?).} If $\mathbf{k}$ is a field of
characteristic $0$, then $\operatorname*{GZ}\nolimits_{n}$ (as a $\mathbf{k}%
$-vector space) has dimension equal to
\begin{align*}
& \left(  \text{\# of involutions in }S_{n}\right)\\
&  =\sum_{\lambda\vdash
n}\left(  \text{\# of standard Young tableaux of shape }\lambda\right)
\end{align*}
(\href{https://oeis.org/A000085}{OEIS sequence A000085}).}

\pause

\item \textbf{Question 1:} Does $\operatorname*{GZ}\nolimits_{n}$ have a basis
for arbitrary $\mathbf{k}$ ? (equivalently, for $\mathbf{k}=\mathbb{Z}$)

\pause
\item \textbf{Question 1':} Let $\mathbf{k}=\mathbb{Z}/p$ for a prime $p$. Is
$\dim_{\mathbf{k}}\operatorname*{GZ}\nolimits_{n}$ independent on $p$ ?

\pause
\item \textbf{Question 1+:} Does $\operatorname*{GZ}\nolimits_{n}$ have a
combinatorially meaningful basis for $\mathbf{k}=\mathbb{Z}$ ?

\pause
\only<9>{
\item For $\mathbf{k}=\mathbb{Q}$, it has a basis $\left(  \mathbf{e}%
_{T,T}\right)  _{\lambda\vdash n;\ T\text{ is a standard tableau of shape
}\lambda}$ coming from the seminormal basis of $\mathbf{k}\left[
S_{n}\right]  $.
}

\pause
\item Questions 1 and 1' true for $n\leq6$.
\end{itemize}

\vspace{9.0338pc}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Specht modules: a quick introduction}

\begin{itemize}
\item Let $D$ be a diagram with $n$ cells. For instance, for $n=9$, we can
have%
\[
D=\begin{tikzpicture}[scale=0.5]
\draw[fill=red!50] (1, 0) rectangle (2, 1);
\draw[fill=red!50] (0, 1) rectangle (1, 2);
\draw[fill=red!50] (1, 1) rectangle (2, 2);
\draw[fill=red!50] (0, 1) rectangle (-1, 0);
\draw[fill=red!50] (1, 0) rectangle (0, -1);
\draw[fill=red!50] (0, 0) rectangle (-1, -1);
\draw[fill=red!50] (3, -1) rectangle (4, 0);
\draw[fill=red!50] (4, 0) rectangle (5, 1);
\draw[fill=red!50] (5, 1) rectangle (6, 2);
\end{tikzpicture}\ \ \ \ \ \ \ \ \ \ \text{or}%
\ \ \ \ \ \ \ \ \ \ D=\begin{tikzpicture}[scale=0.5]
\draw[fill=red!50] (0, 0) rectangle (1, 1);
\draw[fill=red!50] (0, 1) rectangle (1, 2);
\draw[fill=red!50] (0, 2) rectangle (1, 3);
\draw[fill=red!50] (0, 3) rectangle (1, 4);
\draw[fill=red!50] (1, 2) rectangle (2, 3);
\draw[fill=red!50] (1, 3) rectangle (2, 4);
\draw[fill=red!50] (2, 2) rectangle (3, 3);
\draw[fill=red!50] (2, 3) rectangle (3, 4);
\draw[fill=red!50] (3, 3) rectangle (4, 4);
\end{tikzpicture}\ \ .
\]

\pause

\item Let $T$ be any filling of $D$ with the numbers $1,2,\ldots,n$. (Not
necessarily standard!)
For example, if $D$ is the first diagram above, we can have
\[
T = \ytableaushort{\none35\none\none\none2,6\none1\none\none9,48\none\none7} \ \ .
\]

\vspace{9pc}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Are Specht modules pure?}

\begin{itemize}
\item The \textbf{Specht module $\mathcal{S}^{D}$} is the left ideal of $\mathcal{A}$
generated by
\[
\left(
\sum_{\substack{w\in S_{n}\text{ preserves}\\\text{the columns of }T}}\left(
-1\right)  ^{w}w\right)
\left(\sum_{\substack{w\in S_{n}\text{ preserves}\\\text{the rows of
}T}}w\right).
\]

\pause
\only<2>{
\item Alternatively we can define $\mathcal{S}^{D}$ as a span of polytabloids
or of determinants or in several other ways.
}

\pause
\only<3>{
\item If $D$ is a (skew) Young diagram, $\mathcal{S}^{D}$ has many famous
properties and relates to Schur functions. I am interested here in the general case.}
\pause

\item \textbf{Question 2:} Is $\mathcal{S}^{D}$ a direct addend of $\mathcal{A}$ as a $\mathbf{k}$-module?

\item Proving this for $\mathbf{k}=\mathbb{Z}$ would suffice.

\item \textbf{Question 2':} Let $\mathbf{k}=\mathbb{Z}/p$ for a prime $p$.
Is $\dim_{\mathbf{k}}\mathcal{S}^D$ independent on $p$ ?

\item \textbf{Question 2+:} Does $\mathcal{S}^{D}$ have a combinatorially
meaningful basis? 

\pause
\item Well-known positive answers when $D$ is a skew Young diagram (Garnir's
standard basis theorem).

\item The answers are still positive when $D$ is row-convex (Reiner/Shimozono 1993).

\item Same questions exist for Schur and Weyl modules (over
$\operatorname*{GL}\nolimits_{n}$), but not sure if still equivalent.
\end{itemize}

\vspace{9.0338pc}


\end{frame}

\begin{frame}
\frametitle{\ \ \ \ Another unexpected commutativity}

\begin{itemize}
\item For any permutation $w\in S_{n}$, define%
\begin{align*}
\operatorname*{exc}w &  :=\left(  \text{\# of }i\in\left[  n\right]  \text{
such that }w\left(  i\right)  >i\right)  \ \ \ \ \ \ \ \ \ \ \text{and}\\
\operatorname*{anxc}w &  :=\left(  \text{\# of }i\in\left[  n\right]  \text{
such that }w\left(  i\right)  <i\right)  .
\end{align*}


\item For any $a,b\in\mathbb{N}$, define%
\[
\mathbf{X}_{a,b}:=\sum_{\substack{w\in S_{n};\\\operatorname*{exc}%
w=a;\\\operatorname*{anxc}w=b}}w\in
\mathcal{A} = \mathbf{k}\left[  S_{n}\right]  .
\]


\item \textbf{Question 3:} Is it true that all these $\mathbf{X}_{a,b}$
commute (for fixed $n$ and varying $a,b$) ? In other words, do we have
$\mathbf{X}_{a,b}\mathbf{X}_{c,d}=\mathbf{X}_{c,d}\mathbf{X}_{a,b}$ for all
$a,b,c,d\in\mathbb{N}$ ?

\item Checked for all $n\leq7$.

\item This generalizes a limiting case of the Bethe subalgebra (Mukhin/Tarasov/Varchenko).
\end{itemize}
\end{frame}


\end{document}