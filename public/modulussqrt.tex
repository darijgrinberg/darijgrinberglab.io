\documentclass[12pt,final,notitlepage,onecolumn]{article}%
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}%
\setcounter{MaxMatrixCols}{30}%
\usepackage{graphicx}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Friday, April 21, 2023 23:24:45}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\voffset=-1.5cm
\hoffset=-2.5cm
\setlength\textheight{22cm}
\setlength\textwidth{13.5cm}
\begin{document}

%proofreading completed


\begin{center}
\textbf{A modulus and square root inequality}

\textbf{A problem by Darij Grinberg}
\end{center}

For any four reals $a,$ $b,$ $c,$ $d$, prove the inequality%
\begin{align*}
&  \left\vert a+b-c-d\right\vert +\left\vert a+c-b-d\right\vert +\left\vert
a+d-b-c\right\vert \\
&  \geq\left\vert \sqrt{a^{2}+b^{2}}-\sqrt{c^{2}+d^{2}}\right\vert +\left\vert
\sqrt{a^{2}+c^{2}}-\sqrt{b^{2}+d^{2}}\right\vert +\left\vert \sqrt{a^{2}%
+d^{2}}-\sqrt{b^{2}+c^{2}}\right\vert .
\end{align*}


\bigskip

\textbf{Solution by Darij Grinberg.}

We will show the following generalization of the problem:

\begin{quote}
\textbf{Theorem 1.} Let $V$ be an Euclidean space. Then, for any four vectors
$a,$ $b,$ $c,$ $d$ in $V,$ the inequality%
\begin{align*}
&  \left\vert a+b-c-d\right\vert +\left\vert a+c-b-d\right\vert +\left\vert
a+d-b-c\right\vert \\
&  \geq\left\vert \sqrt{a^{2}+b^{2}}-\sqrt{c^{2}+d^{2}}\right\vert +\left\vert
\sqrt{a^{2}+c^{2}}-\sqrt{b^{2}+d^{2}}\right\vert +\left\vert \sqrt{a^{2}%
+d^{2}}-\sqrt{b^{2}+c^{2}}\right\vert
\end{align*}
holds.
\end{quote}

Here, we are using the following notations:

\begin{itemize}
\item For any vector $v\in V,$ we denote by $\left\vert v\right\vert $ the
length (i. e., the Euclidean norm) of $v.$

\item For any two vectors $v$ and $w$ in $V,$ we denote by $vw$ the scalar
product of $v$ and $w.$ (Note that this is not really a multiplication, since
$vw$ is a scalar while $v$ and $w$ are vectors. In general, for three vectors
$u,$ $v,$ $w$ in an Euclidean space, we do \textit{not} have $uv\cdot
w=vw\cdot u.$)

\item For any vector $v\in V,$ we abbreviate $vv$ as $v^{2}.$
\end{itemize}

Before we begin proving Theorem 1, we recapitulate two important facts about
Euclidean spaces:

\textbf{Cauchy-Schwarz inequality in vector form:} If $u$ and $v$ are two
vectors in an Euclidean space, then $\left\vert uv\right\vert \leq\left\vert
u\right\vert \cdot\left\vert v\right\vert .$

\textbf{Triangle inequality:} If $u$ and $v$ are two vectors in an Euclidean
space, then $\left\vert u\right\vert +\left\vert v\right\vert \geq\left\vert
u+v\right\vert .$

\textit{Proof of Theorem 1.} Denote%
\begin{align*}
t  &  =a+b+c+d;\\
x  &  =a+b-c-d;\\
y  &  =a+c-b-d;\\
z  &  =a+d-b-c.
\end{align*}


Then,%
\begin{align*}
tx+yz  &  =\left(  a+b+c+d\right)  \left(  a+b-c-d\right)  +\left(
a+c-b-d\right)  \left(  a+d-b-c\right) \\
&  =\left(  \left(  a+b\right)  +\left(  c+d\right)  \right)  \left(  \left(
a+b\right)  -\left(  c+d\right)  \right)  +\left(  \left(  a-b\right)
+\left(  c-d\right)  \right)  \left(  \left(  a-b\right)  -\left(  c-d\right)
\right) \\
&  =\left(  \left(  a+b\right)  ^{2}-\left(  c+d\right)  ^{2}\right)  +\left(
\left(  a-b\right)  ^{2}-\left(  c-d\right)  ^{2}\right) \\
&  =\left(  \underbrace{\left(  a+b\right)  ^{2}+\left(  a-b\right)  ^{2}%
}_{\substack{=\left(  a^{2}+2ab+b^{2}\right)  +\left(  a^{2}-2ab+b^{2}\right)
\\=2a^{2}+2b^{2}}}\right)  -\left(  \underbrace{\left(  c+d\right)
^{2}+\left(  c-d\right)  ^{2}}_{\substack{=\left(  c^{2}+2cd+d^{2}\right)
+\left(  c^{2}-2cd+d^{2}\right)  \\=2c^{2}+2d^{2}}}\right)  =2\left(  \left(
a^{2}+b^{2}\right)  -\left(  c^{2}+d^{2}\right)  \right)  ,
\end{align*}
so that%
\begin{equation}
\left(  a^{2}+b^{2}\right)  -\left(  c^{2}+d^{2}\right)  =\dfrac{1}{2}\left(
tx+yz\right)  . \label{1}%
\end{equation}


The triangle inequality, applied to the two vectors $\left(  a+b,a-b\right)  $
and $\left(  c+d,c-d\right)  $ in the Euclidean space $V\oplus V\ \ \ \ $%
\footnote{The canonical scalar product on this space $V\oplus V$ is defined by
$\left(  e,f\right)  \left(  g,h\right)  =eg+fh$ for any $\left(  e,f\right)
\in V\oplus V$ and $\left(  g,h\right)  \in V\oplus V.$ In particular, we thus
have $\left(  e,f\right)  ^{2}=e^{2}+f^{2}$ for any $\left(  e,f\right)  \in
V\oplus V.$}, yields%
\[
\left\vert \left(  a+b,a-b\right)  \right\vert +\left\vert \left(
c+d,c-d\right)  \right\vert \geq\left\vert \left(  a+b,a-b\right)  +\left(
c+d,c-d\right)  \right\vert .
\]
Since%
\begin{align*}
\left\vert \left(  a+b,a-b\right)  \right\vert  &  =\sqrt{\left(
a+b,a-b\right)  ^{2}}=\sqrt{\left(  a+b\right)  ^{2}+\left(  a-b\right)  ^{2}%
};\\
\left\vert \left(  c+d,c-d\right)  \right\vert  &  =\sqrt{\left(
c+d,c-d\right)  ^{2}}=\sqrt{\left(  c+d\right)  ^{2}+\left(  c-d\right)  ^{2}%
};\\
\left\vert \left(  a+b,a-b\right)  +\left(  c+d,c-d\right)  \right\vert  &
=\left\vert \left(  \left(  a+b\right)  +\left(  c+d\right)  ,\left(
a-b\right)  +\left(  c-d\right)  \right)  \right\vert \\
&  =\sqrt{\left(  \left(  a+b\right)  +\left(  c+d\right)  ,\left(
a-b\right)  +\left(  c-d\right)  \right)  ^{2}}\\
&  =\sqrt{\left(  \left(  a+b\right)  +\left(  c+d\right)  \right)
^{2}+\left(  \left(  a-b\right)  +\left(  c-d\right)  \right)  ^{2}},
\end{align*}
this rewrites as%
\[
\sqrt{\left(  a+b\right)  ^{2}+\left(  a-b\right)  ^{2}}+\sqrt{\left(
c+d\right)  ^{2}+\left(  c-d\right)  ^{2}}\geq\sqrt{\left(  \left(
a+b\right)  +\left(  c+d\right)  \right)  ^{2}+\left(  \left(  a-b\right)
+\left(  c-d\right)  \right)  ^{2}}.
\]
But%
\begin{align*}
\left(  a+b\right)  ^{2}+\left(  a-b\right)  ^{2}  &  =\left(  a^{2}%
+2ab+b^{2}\right)  +\left(  a^{2}-2ab+b^{2}\right)  =2a^{2}+2b^{2}=2\left(
a^{2}+b^{2}\right)  ;\\
\left(  c+d\right)  ^{2}+\left(  c-d\right)  ^{2}  &  =\left(  c^{2}%
+2cd+d^{2}\right)  +\left(  c^{2}-2cd+d^{2}\right)  =2c^{2}+2d^{2}=2\left(
c^{2}+d^{2}\right)  ;\\
\left(  a+b\right)  +\left(  c+d\right)   &  =a+b+c+d=t;\\
\left(  a-b\right)  +\left(  c-d\right)   &  =a+c-b-d=y.
\end{align*}
Hence, this becomes%
\[
\sqrt{2\left(  a^{2}+b^{2}\right)  }+\sqrt{2\left(  c^{2}+d^{2}\right)  }%
\geq\sqrt{t^{2}+y^{2}}.
\]
But $\sqrt{t^{2}+y^{2}}\geq\dfrac{1}{\sqrt{2}}\left(  \left\vert t\right\vert
+\left\vert y\right\vert \right)  $\ \ \ \ \footnote{In fact, the AM-QM
inequality yields $\sqrt{\dfrac{\left\vert t\right\vert ^{2}+\left\vert
y\right\vert ^{2}}{2}}\geq\dfrac{\left\vert t\right\vert +\left\vert
y\right\vert }{2}.$ Thus, $\sqrt{t^{2}+y^{2}}=\sqrt{\left\vert t\right\vert
^{2}+\left\vert y\right\vert ^{2}}=\sqrt{2}\cdot\sqrt{\dfrac{\left\vert
t\right\vert ^{2}+\left\vert y\right\vert ^{2}}{2}}\geq\sqrt{2}\cdot
\dfrac{\left\vert t\right\vert +\left\vert y\right\vert }{2}=\dfrac{\sqrt{2}%
}{2}\left(  \left\vert t\right\vert +\left\vert y\right\vert \right)
=\dfrac{1}{\sqrt{2}}\left(  \left\vert t\right\vert +\left\vert y\right\vert
\right)  .$}. Hence,%
\[
\sqrt{2\left(  a^{2}+b^{2}\right)  }+\sqrt{2\left(  c^{2}+d^{2}\right)  }%
\geq\dfrac{1}{\sqrt{2}}\left(  \left\vert t\right\vert +\left\vert
y\right\vert \right)  .
\]
Dividing this by $\sqrt{2},$ we obtain%
\begin{align}
\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}  &  \geq\underbrace{\dfrac{1}{\sqrt{2}%
}\cdot\dfrac{1}{\sqrt{2}}}_{=1/2}\left(  \left\vert t\right\vert +\left\vert
y\right\vert \right)  ;\ \ \ \ \ \ \ \ \ \ \text{in other words,}\nonumber\\
\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}  &  \geq\dfrac{1}{2}\left(  \left\vert
t\right\vert +\left\vert y\right\vert \right)  . \label{2}%
\end{align}


By switching $c$ with $d$ (and, consequently, switching $y$ with $z$) in the
above argument, we can similarly prove%
\begin{align}
\sqrt{a^{2}+b^{2}}+\sqrt{d^{2}+c^{2}}  &  \geq\dfrac{1}{2}\left(  \left\vert
t\right\vert +\left\vert z\right\vert \right)  ,\ \ \ \ \ \ \ \ \ \ \text{what
rewrites as}\nonumber\\
\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}  &  \geq\dfrac{1}{2}\left(  \left\vert
t\right\vert +\left\vert z\right\vert \right)  . \label{3}%
\end{align}


Now,%
\begin{align}
&  \left\vert \sqrt{a^{2}+b^{2}}-\sqrt{c^{2}+d^{2}}\right\vert =\left\vert
\dfrac{\left(  \sqrt{a^{2}+b^{2}}-\sqrt{c^{2}+d^{2}}\right)  \left(
\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}\right)  }{\sqrt{a^{2}+b^{2}}+\sqrt
{c^{2}+d^{2}}}\right\vert \nonumber\\
&  =\left\vert \dfrac{\left(  a^{2}+b^{2}\right)  -\left(  c^{2}+d^{2}\right)
}{\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}}\right\vert =\left\vert \dfrac
{\dfrac{1}{2}\left(  tx+yz\right)  }{\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}%
}\right\vert \ \ \ \ \ \ \ \ \ \ \left(  \text{by (1)}\right) \nonumber\\
&  =\dfrac{1}{2}\cdot\dfrac{\left\vert tx+yz\right\vert }{\sqrt{a^{2}+b^{2}%
}+\sqrt{c^{2}+d^{2}}}\ \ \ \ \ \ \ \ \ \ \left(  \text{since }\dfrac{1}%
{2}\text{ and }\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}\text{ are positive}%
\right) \nonumber\\
&  \leq\dfrac{1}{2}\cdot\dfrac{\left\vert tx\right\vert +\left\vert
yz\right\vert }{\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}}%
\ \ \ \ \ \ \ \ \ \ \left(  \text{since }\left\vert tx+yz\right\vert
\leq\left\vert tx\right\vert +\left\vert yz\right\vert \text{ by the triangle
inequality}\right) \nonumber\\
&  =\dfrac{1}{2}\cdot\left(  \underbrace{\dfrac{\left\vert tx\right\vert
}{\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}}}_{\leq\dfrac{\left\vert tx\right\vert
}{\dfrac{1}{2}\left(  \left\vert t\right\vert +\left\vert y\right\vert
\right)  }\text{ by (2)}}+\underbrace{\dfrac{\left\vert yz\right\vert }%
{\sqrt{a^{2}+b^{2}}+\sqrt{c^{2}+d^{2}}}}_{\leq\dfrac{\left\vert yz\right\vert
}{\dfrac{1}{2}\left(  \left\vert t\right\vert +\left\vert z\right\vert
\right)  }\text{ by (3)}}\right) \nonumber\\
&  \leq\dfrac{1}{2}\cdot\left(  \dfrac{\left\vert tx\right\vert }{\dfrac{1}%
{2}\left(  \left\vert t\right\vert +\left\vert y\right\vert \right)  }%
+\dfrac{\left\vert yz\right\vert }{\dfrac{1}{2}\left(  \left\vert t\right\vert
+\left\vert z\right\vert \right)  }\right)  =\dfrac{\left\vert tx\right\vert
}{\left\vert t\right\vert +\left\vert y\right\vert }+\dfrac{\left\vert
yz\right\vert }{\left\vert t\right\vert +\left\vert z\right\vert }\leq
\dfrac{\left\vert t\right\vert \cdot\left\vert x\right\vert }{\left\vert
t\right\vert +\left\vert y\right\vert }+\dfrac{\left\vert y\right\vert
\cdot\left\vert z\right\vert }{\left\vert t\right\vert +\left\vert
z\right\vert }\nonumber\\
&  \ \ \ \ \ \ \ \ \ \ \left(  \text{since }\left\vert tx\right\vert
\leq\left\vert t\right\vert \cdot\left\vert x\right\vert \text{ and
}\left\vert yz\right\vert \leq\left\vert y\right\vert \cdot\left\vert
z\right\vert \text{ by the Cauchy-Schwarz inequality in vector form}\right)
\nonumber\\
&  =\dfrac{\left\vert t\right\vert }{\left\vert t\right\vert +\left\vert
y\right\vert }\cdot\left\vert x\right\vert +\dfrac{\left\vert z\right\vert
}{\left\vert t\right\vert +\left\vert z\right\vert }\cdot\left\vert
y\right\vert . \label{4}%
\end{align}
By cyclically permuting the variables $b,$ $c,$ and $d$ (and, consequently,
cyclically permuting $x,$ $y,$ and $z$) in the above argument, we can
similarly prove the two inequalities%
\begin{align}
\left\vert \sqrt{a^{2}+c^{2}}-\sqrt{b^{2}+d^{2}}\right\vert  &  \leq
\dfrac{\left\vert t\right\vert }{\left\vert t\right\vert +\left\vert
z\right\vert }\cdot\left\vert y\right\vert +\dfrac{\left\vert x\right\vert
}{\left\vert t\right\vert +\left\vert x\right\vert }\cdot\left\vert
z\right\vert ;\label{5}\\
\left\vert \sqrt{a^{2}+d^{2}}-\sqrt{b^{2}+c^{2}}\right\vert  &  \leq
\dfrac{\left\vert t\right\vert }{\left\vert t\right\vert +\left\vert
x\right\vert }\cdot\left\vert z\right\vert +\dfrac{\left\vert y\right\vert
}{\left\vert t\right\vert +\left\vert y\right\vert }\cdot\left\vert
x\right\vert . \label{6}%
\end{align}
Now, the three inequalities (4), (5), (6) yield%
\begin{align*}
&  \left\vert \sqrt{a^{2}+b^{2}}-\sqrt{c^{2}+d^{2}}\right\vert +\left\vert
\sqrt{a^{2}+c^{2}}-\sqrt{b^{2}+d^{2}}\right\vert +\left\vert \sqrt{a^{2}%
+d^{2}}-\sqrt{b^{2}+c^{2}}\right\vert \\
&  \leq\left(  \dfrac{\left\vert t\right\vert }{\left\vert t\right\vert
+\left\vert y\right\vert }\cdot\left\vert x\right\vert +\dfrac{\left\vert
z\right\vert }{\left\vert t\right\vert +\left\vert z\right\vert }%
\cdot\left\vert y\right\vert \right)  +\left(  \dfrac{\left\vert t\right\vert
}{\left\vert t\right\vert +\left\vert z\right\vert }\cdot\left\vert
y\right\vert +\dfrac{\left\vert x\right\vert }{\left\vert t\right\vert
+\left\vert x\right\vert }\cdot\left\vert z\right\vert \right)  +\left(
\dfrac{\left\vert t\right\vert }{\left\vert t\right\vert +\left\vert
x\right\vert }\cdot\left\vert z\right\vert +\dfrac{\left\vert y\right\vert
}{\left\vert t\right\vert +\left\vert y\right\vert }\cdot\left\vert
x\right\vert \right) \\
&  =\left(  \dfrac{\left\vert t\right\vert }{\left\vert t\right\vert
+\left\vert y\right\vert }\cdot\left\vert x\right\vert +\dfrac{\left\vert
y\right\vert }{\left\vert t\right\vert +\left\vert y\right\vert }%
\cdot\left\vert x\right\vert \right)  +\left(  \dfrac{\left\vert t\right\vert
}{\left\vert t\right\vert +\left\vert z\right\vert }\cdot\left\vert
y\right\vert +\dfrac{\left\vert z\right\vert }{\left\vert t\right\vert
+\left\vert z\right\vert }\cdot\left\vert y\right\vert \right)  +\left(
\dfrac{\left\vert t\right\vert }{\left\vert t\right\vert +\left\vert
x\right\vert }\cdot\left\vert z\right\vert +\dfrac{\left\vert x\right\vert
}{\left\vert t\right\vert +\left\vert x\right\vert }\cdot\left\vert
z\right\vert \right) \\
&  =\underbrace{\left(  \dfrac{\left\vert t\right\vert }{\left\vert
t\right\vert +\left\vert y\right\vert }+\dfrac{\left\vert y\right\vert
}{\left\vert t\right\vert +\left\vert y\right\vert }\right)  }_{=1}%
\cdot\left\vert x\right\vert +\underbrace{\left(  \dfrac{\left\vert
t\right\vert }{\left\vert t\right\vert +\left\vert z\right\vert }%
+\dfrac{\left\vert z\right\vert }{\left\vert t\right\vert +\left\vert
z\right\vert }\right)  }_{=1}\cdot\left\vert y\right\vert +\underbrace{\left(
\dfrac{\left\vert t\right\vert }{\left\vert t\right\vert +\left\vert
x\right\vert }+\dfrac{\left\vert x\right\vert }{\left\vert t\right\vert
+\left\vert x\right\vert }\right)  }_{=1}\cdot\left\vert z\right\vert \\
&  =\left\vert x\right\vert +\left\vert y\right\vert +\left\vert z\right\vert
=\left\vert a+b-c-d\right\vert +\left\vert a+c-b-d\right\vert +\left\vert
a+d-b-c\right\vert ,
\end{align*}
and Theorem 1 is proven.


\end{document}