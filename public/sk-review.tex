\documentclass[numbers=enddot,12pt,final,onecolumn,notitlepage]{scrartcl}%
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[all,cmtip]{xy}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{comment}
\usepackage{color}
\usepackage{hyperref}
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\usepackage{amsthm}
\usepackage{tikz}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Friday, January 19, 2024 15:46:34}
%TCIDATA{SuppressPackageManagement}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\usetikzlibrary{positioning}
\theoremstyle{definition}
\newtheorem{theo}{Theorem}[section]
\newenvironment{theorem}[1][]
{\begin{theo}[#1]\begin{leftbar}}
{\end{leftbar}\end{theo}}
\newtheorem{prop}[theo]{Proposition}
\newenvironment{proposition}[1][]
{\begin{prop}[#1]\begin{leftbar}}
{\end{leftbar}\end{prop}}
\newtheorem{defi}[theo]{Definition}
\newenvironment{definition}[1][]
{\begin{defi}[#1]\begin{leftbar}}
{\end{leftbar}\end{defi}}
\newtheorem{coro}[theo]{Corollary}
\newenvironment{corollary}[1][]
{\begin{coro}[#1]\begin{leftbar}}
{\end{leftbar}\end{coro}}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{noncompile}{}{}
\excludecomment{noncompile}
\setlength\textheight{22.5cm}
\setlength\textwidth{15cm}
\ihead{``Mathematics via Problems 1--2'' review}
\ohead{\today}
\begin{document}

\begin{center}
\textbf{Mathematics via Problems, part 1: Algebra} by \textit{Arkadiy
Skopenkov}, MSRI/AMS 2021;

\textbf{Mathematics via Problems, part 2: Geometry} by \textit{Alexey A.
Zaslavsky and Mikhail B. Skopenkov}, MSRI/AMS 2021;

\textbf{Review by Darij Grinberg}
\end{center}

The mathematical problem book is a literary genre that predates many genres of
fiction. In the 8th century AD, Alcuin of York collected $53$ logical and
arithmetic puzzles \textquotedblleft to sharpen the young\textquotedblright.
As mathematical competitions spread in the 20th century, the genre expanded,
and entire series of problem collections were published, often organized by
topic or origin.

The 1950s saw the appearance of books in the Soviet Union that attempted to
combine the pleasant with the instructive. Rather than present a hodgepodge of
isolated curiosities, the authors carefully selected problems that would lead
the reader to rediscover entire fields of mathematics semi-autonomously.
Unlike a textbook that spells out each proof, algorithm or example in detail,
such a problem book would guide the reader to self-discover important
landmarks through a sequence of leading questions. When chosen well and
arranged in a good order, these questions would be nowhere as difficult as the
original problems that motivated the creation of the theory, but still
challenging enough to make for a memorable experience, thus strengthening the
learning and motivating the tools and theorems that crystallize out of their
solution. Three classic texts following this approach are \cite{DynUsp},
\cite{YagYag} and \cite{Gelfand}.

In the West, a similar paradigm had arisen in the 1910s under the name of
\textquotedblleft Moore method\textquotedblright, nowadays better known as
\textquotedblleft IBL\textquotedblright\ (\textquotedblleft inquiry-based
learning\textquotedblright). However, the two formats differ in how they
handle the case of the reader getting stuck on a question. Soviet-style
\textquotedblleft learning by problem-solving\textquotedblright\ books include
solutions to all problems, which can be consulted after one runs out of ideas.
These solutions are typically put in a separate chapter, which often spans
more than half of the book. Sometimes, even an intermediate (short) chapter
with hints appears, to avoid needlessly \textquotedblleft
spoiling\textquotedblright\ an entire problem. In contrast, IBL texts would
typically eschew any hints; they are meant to be read in groups, with hurdles
being surmounted through discussion and collaboration (and perhaps some
guidance from a trainer). A popular example of an IBL text is \cite{Bogart}.

The two books under review come from the Soviet tradition; they are the first
two volumes of a (so far) three-volume series, which intends to cover a
variety of mathematical topics by way of problems. However, unlike many other
Russian texts, they don't always give complete solutions. The first one
actually does so on the rarest occasions, while leaving most problems with
mere hints, answers or nothing at all; in this, it bears more resemblance to
American IBL texts.

Both Soviet-style and IBL-style problem books have two audiences: \textbf{(a)}
self-learners who are reading the books on their own with no outside help,
\textbf{(b)} learners working in groups and under guidance of a trainer, and
\textbf{(c)} lecturers and trainers using the books as raw materials. It is
easiest for a book to be useful to audience \textbf{(c)}, as such readers
often have prior familiarity with the material and thus are unlikely to get
stuck or confused. Audience \textbf{(b)} can rely on a trainer to smoothen the
rough edges of such a book when there are any. Audience \textbf{(a)}, however,
can be significantly hindered by pedagogical flaws of the text, such as
out-of-order problems, missing hints or lacking guidance on which problems are
crucial and which can be skipped.

\bigskip

\textbf{Part 1: Algebra.}

The first book is concerned with \textit{algebra} in the most classical sense:
the solution of polynomial equations (usually in one variable, over
$\mathbb{Z}$, $\mathbb{Q}$, $\mathbb{R}$ or $\mathbb{C}$ or sometimes modulo
$n$). It starts at the level of elementary olympiad problems, and ends in a
rather advanced Galois-theoretical jungle. While necessarily introducing some
algebraic concepts such as number fields and their extensions, it eschews
basic abstractions such as groups, often in favor of examples and ad-hoc constructions.

Chapters 1-2 are a crash course in elementary number theory, leading from
divisibility all the way to quadratic reciprocity in just 30 pages. Negative
numbers are treated inconsistently, and some important results are left
unproven. Hints are few and far between, and not always for the hardest or
most important results (e.g., all general claims about primitive roots are
left as exercises with no hints). The order of the material also makes for a
bumpy ride; in particular, prime numbers are treated before remainders and
gcds, which needlessly complicates some problems. With the exception of a few
nonstandard exercises and the rather unique (but not overly interesting)
Section 1.7, all of the material here can be found in classical textbooks such
as \cite{NiZuMo}, \cite{ADM}, \cite{Burton}, \cite{Tattersall} or
\cite{DenDen}, which are less hurried and can be read in a semi-IBL fashion if
so desired by treating the theorems as problems. It would perhaps be good and
topical to include some properties of Dedekind sums, which are elementary but
rarely appear in elementary literature.

Chapter 3 is devoted to polynomials and complex numbers. Section 3.2, which
surveys various methods for solving cubic and quartic equations, is a
highlight; it derives Cardano's and Ferrari's formulas elegantly and
elementarily (and gives good hints for the problems, unlike many other parts
of the book).\footnote{A\ particularly nice trick is the use of the identity
$x^{3}+y^{3}+z^{3}-3xyz=\left(  x+y+z\right)  \left(  x^{2}+y^{2}%
+z^{2}-yz-zx-xy\right)  $ for solving the cubic equation $x^{3}+px+q=0$.
Indeed, this identity shows that $x^{3}+y^{3}+z^{3}-3xyz=0$ whenever $x,y,z$
are three numbers satisfying $x+y+z=0$. Thus, $x=-u-v$ is a root of the
polynomial $x^{3}+px+q$ when $p=3uv$ and $q=u^{3}+v^{3}$; hence, it only
remains to write the known coefficients $p$ and $q$ as $3uv$ and $u^{3}+v^{3}%
$, which is easy. Cardano's formula thus follows.} The other sections are a
mixed bag: They too include some interesting material, marred by difficulty
spikes and cryptic hints. (For example, the hint to problem 3.6.3 is good, but
uses the notion of \textquotedblleft lexicographic order\textquotedblright%
\ without definition.) Section 3.9 takes a side trip into discrete geometry,
using some of the prior results to refute Borsuk's conjecture on sets in
$n$-dimensional space. This is deep, very technical and far from the topic of
this book; I am not sure if it is treated any better here than in
\cite{AigZie}.

Chapter 4, about permutations, is particularly unfortunate. In Section 4.3
(which, to my knowledge, is not used in the rest of the book), the
fundamentals of enumeration under group action are presented (culminating in
the not-really-Burnside lemma with applications), in a language that eschews
the notion of a group (even of a permutation group!) in favor of confusing
ad-hoc visualizations involving carousels, trains and cockroaches. In the
condensed form they are presented here (with telegraphic hints and --
surprisingly for this subject! -- not a single picture), they are almost
incomprehensible. Had I not known the proof of Burnside's lemma (4.3.10)
beforehand, I would not have understood the \textquotedblleft train
station\textquotedblright\ metaphor used to motivate it here.\footnote{For
those equally confused: A \textquotedblleft train\textquotedblright\ is an
$n$-tuple of colors. The cyclic group $C_{n}$ acts on such $n$-tuples by
cyclically rotating the entries. A \textquotedblleft station\textquotedblright%
\ is an orbit of this action. The \textquotedblleft period\textquotedblright%
\ of a train is the smallest $k>0$ such that the $k$-th power of the generator
fixes the train. A \textquotedblleft passenger\textquotedblright\ is a pair of
a train and an element of $\left\{  0,1,\ldots,k-1\right\}  $, modelling an
element of the cyclic group.} How is this approach any better than
\cite[\S \S 6.1-6.2]{Guichard} or \cite[\S 6]{Bogart}? And why assume that a
reader of an algebra book is afraid of formulas? Sections 4.1 and 4.2, while
written better, contain nothing that I could not find in standard texts on
combinatorics, except for the (nice) problem 4.2.6.

Chapter 5 is a short introduction to olympiad-level inequalities: Jensen,
AM--GM, Cauchy--Schwarz, H\"{o}lder and their ilk. It does not go deep but
covers the basics, well enough for whetting the reader's appetite. Much more
can be found in books like \cite{AndSau}, \cite{Lee}, \cite{Steele},
\cite{Cirtoaje}, \cite{Hung-secrets}.

Chapters 6 and 7 represent a slight detour towards and into some basic
analysis, which is used to enumerate real roots of a polynomial. The selection
here is eclectic and includes finite differences, infinite sums, Sturm's
method and some infinitary combinatorics. Much of this is not readily found in
problem books, but once again the reader is left wanting for hints or
solutions. Particularly unique is Section 6.6, which proves the transcendence
of the Liouville and Mahler numbers. The proofs are elementary and
combinatorial, although the presentation feels less clear than it could be.

Chapter 8 is the culmination of the book. It is concerned with questions of
solving polynomial equations (over $\mathbb{Q}$) using radicals, real radicals
and compass-and-ruler constructions. In particular, most of the classical
(im)possibilities of Galois theory are proved here: $\sqrt[3]{2}$ and the
regular $9$-gon are not constructible with compass and ruler, but regular
$p$-gons for $p=2^{2^{n}}+1$ prime are (Gauss); some quintics are not solvable
in radicals (Abel--Ruffini); a cubic with three real and no rational roots is
not solvable in real radicals (\textquotedblleft casus
irreducibilis\textquotedblright). The author takes a rather concrete approach,
eschewing groups and Galois theory in favor of resolvents, symmetric
polynomials and some low-level linear algebra. I am rather delighted to see
these proofs taking center stage in a book, after the stepmotherly treatment
they tend to receive in modern texts on algebra, which usually name-check them
to motivate Galois theory but rarely stoop down to prove them, let alone
constructively! (Stewart's \cite{Stewart}, at least, constructs the $17$-gon.)

The highlight here is Section 8.2, which covers the \textquotedblleft positive
part\textquotedblright\ of the theory: Gauss's famous \textit{Disquisitiones}
result that regular $n$-gons are constructible for all positive integers $n$
whose Euler totient $\varphi\left(  n\right)  $ is a power of $2$; and a
related result that $e^{2\pi i/p}$ can be expressed through $\left(
p-1\right)  $-st roots for any prime $p$. The proofs are less than a page long
(\S 8.2.E) and quite nice, yet surprisingly missing from all texts I have seen
except for \cite[Theorem 20.13 and proof of Theorem 21.3]{Stewart} (which is
much less concrete and elementary).

The remaining 40 pages of the book (\S 8.3 and \S 8.4) address the
\textquotedblleft negative part\textquotedblright\ of the theory
(non-constructibility and non-expressibility). Here, unfortunately, all the
weaknesses of the present book manifest themselves to their fullest.
Abstraction and complexity rise significantly, as the author discusses
symmetry classes of polynomials and normal field extensions, while still
duteously avoiding the word \textquotedblleft group\textquotedblright. The
advantage of this quasi-concreteness is not clear, as the results being proved
are mostly negative. Hints and proofs are not always given, and when they are,
they often cite later problems. A lot of apocryphal material is included (not
just Abel--Ruffini and Gauss, but also criteria for real radicals or
expressibility with a single radical) that is not easily found in textbooks,
but the road is long and highly bumpy. I am afraid that few readers will find
the destination worth the trip.

What can thus be made of this book? It is clearly not a soulless write-only
rehashing of already well-exposed material to lengthen the author's CV; there
is a lot here that is not easily found elsewhere, and several good pedagogical
choices can be seen in the selection of problems (along with some questionable
ones in their ordering). Olympiad trainers -- the above-mentioned audience
\textbf{(c)} -- will likely find good material to work with in the book. Yet
the sparsity of hints and solutions, which are often insufficient for the
non-expert reader, limits its usefulness for audience \textbf{(a)} and perhaps
even for \textbf{(b)}. For instance, how much can you get out of the hint to
problem 3.9.1?\footnote{Something closer to an actual hint can be found in
\url{https://stanford.edu/~sfh/discrete_geo3.pdf} .} And this is a first
problem in its section, seemingly a warm-up exercise; if it was clearly marked
as a bonus problem, then the reader could at least skip it. Audience
\textbf{(b)} will at least have a trainer to tell which problems are important.

Not only in this aspect does the book read like a draft rushed into print. It
also teems with typos, ambiguous wording (\textquotedblleft
any\textquotedblright\ is a dangerous word) and occasionally more serious
gaps. Laudably, the author is tracking errata on his website (
\url{https://old.mccme.ru//circles//oim/algebra_eng.pdf} ). A second edition
with fewer errors, more hints and a better ordering will greatly improve the
usability of the book. If this lengthens the text, some digressions (such as
infinite sums and the Burnside lemma) could be removed to focus on its main
topic (polynomials and their roots). Sections 3.2 and 8.2 are the treasures
hidden in this book, and it is a pity to see them buried under so much debris.

\bigskip

\textbf{Part 2: Geometry.}

The second book focuses on Euclidean geometry -- mostly planar, and mostly
rather elementary, which does not mean \textquotedblleft
simple\textquotedblright\ or \textquotedblleft well-known\textquotedblright.
It is therefore close kin to \cite{Prasolov}, \cite{Sharygin} and (to some
extent) \cite{Chen}, although it expects a somewhat higher level of
sophistication from the reader; this makes it a good sequel to \cite{Prasolov}.

Chapter 1 deals with the geometry of the triangle. While far from
comprehensive (only 38 pages!), it collects a fair amount of interesting and
beautiful problems, often (not always) with complete solutions. Topics such as
Simson lines and mixtilinear incircles (here called \textquotedblleft
semi-inscribed circles\textquotedblright) are presented through sequences of
well-chosen problems, arranged in a pedagogically reasonable order to allow
each to be solved using the ones before (although occasionally, the more basic
problems from a later section get used as well).

The last four sections of Chapter 1 are my favorite parts of this book,
covering the mid-arc points (Section 1.7), the mixtilinear incircles (1.8),
the generalized Napoleon theorem (1.9) and isogonal conjugation (1.10). The
generalized Napoleon theorem, in particular, is a gem of elementary geometry
that is far less known than it deserves. It goes back to Rigby \cite[Theorem
3.1]{Rigby} and has recently been re-exposed in \cite{Beluhov} and
\cite{Egamberganov}, but its best statement is as follows:

\begin{theorem}
[Rigby 1988]\label{thm.gn}Let $ABC$ and $MNP$ be two triangles, and $T$ any
point in the plane. Let $A_{1},B_{1},C_{1}$ be three points in the plane such
that $\bigtriangleup ABC_{1}\sim\bigtriangleup NMT$ and $\bigtriangleup
BCA_{1}\sim\bigtriangleup PNT$ and $\bigtriangleup CAB_{1}\sim\bigtriangleup
MPT$. Here, the symbol \textquotedblleft$\sim$\textquotedblright\ means
\textquotedblleft directly similar\textquotedblright\ (i.e., similar and
having the same orientation), and we understand a triangle to be an ordered
triple of its vertices (so $\bigtriangleup ABC$ is not similar to
$\bigtriangleup BAC$).

Then, $\bigtriangleup A_{1}B_{1}C_{1}\sim\bigtriangleup MNP$. (See Figure
1.)
\end{theorem}

\begin{figure}[ptb]
\label{fig.1}
\centering
\begin{tikzpicture}[scale=1]
\begin{scope}[every node/.style={}]
\coordinate(A) at (0,0);
\node(Al) [left=0.8mm of A] {$A$};
\coordinate(B) at (1,2);
\node(Bl) [above=0.8mm of B] {$B$};
\coordinate(C) at (4,0);
\node(Cl) [right=0.8mm of C] {$C$};
\coordinate(M) at (7,-1);
\node(Ml) [left=0.8mm of M] {$M$};
\coordinate(N) at (9,2);
\node(Nl) [above=0.8mm of N] {$N$};
\coordinate(P) at (10,-1);
\node(Pl) [right=0.8mm of P] {$P$};
\coordinate(T) at (8.8,0);
\node(Tl) [below=0.8mm of T] {$T$};
\coordinate(A1) at (2.78,1.94);
\node(A1l) [right=0.8mm of A1] {$A_1$};
\coordinate(B1) at (1.6,-1.33);
\node(B1l) [below=0.8mm of B1] {$B_1$};
\coordinate(C1) at (-0.03,1.25);
\node(C1l) [left=0.8mm of C1] {$C_1$};
\end{scope}
\begin{scope}[every edge/.style={draw=black,very thick}, every loop/.style={}]
\path[-] (A) edge (B) (B) edge (C) (C) edge (A);
\path[-] (M) edge (N) (N) edge (P) (P) edge (M);
\end{scope}
\begin{scope}[every edge/.style={draw=red,very thick}, every loop/.style={}]
\path[-] (C) edge (B1) (B) edge (C1) (M) edge (T);
\end{scope}
\begin{scope}[every edge/.style={draw=blue,very thick}, every loop/.style={}]
\path[-] (A) edge (C1) (C) edge (A1) (N) edge (T);
\end{scope}
\begin{scope}[every edge/.style={draw=green,very thick}, every loop/.style={}]
\path[-] (B) edge (A1) (A) edge (B1) (P) edge (T);
\end{scope}
\begin{scope}[every edge/.style={draw=black,dotted}, every loop/.style={}]
\path[-] (A1) edge (B1) (B1) edge (C1) (C1) edge (A1);
\end{scope}
\end{tikzpicture}
\caption{The generalized Napoleon theorem. Some
edges have been colored as a visual aid (corresponding sides of similar
triangles have the same color).}%
\end{figure}

\begin{noncompile}
Moreover, the circles $BCA_{1}$, $CAB_{1}$, $ABC_{1}$ and $A_{1}B_{1}C_{1}$
have a common point. (\textquotedblleft Circle $XYZ$\textquotedblright\ means
the circle through three points $X$, $Y$ and $Z$.)
\end{noncompile}

There are $6$ degrees of freedom in this theorem (up to similitude), which is
an unusually high number for triangle geometry; thus, its broad applicability
is not unexpected, though hardly unwelcome! Napoleon's classical theorem is
obtained by taking $\bigtriangleup MNP$ equilateral and $T$ to be its center,
but several much less obvious choices yield other interesting results. In
particular, when $M,N,P,T$ are collinear, Theorem \ref{thm.gn} degenerates to
Menelaos's theorem. A recent result \cite[Theorem 3]{KB} by Kiss and
B\'{\i}r\'{o} also follows easily (exercise!) from Theorem \ref{thm.gn}:

\begin{corollary}
[Kiss, B\'{\i}r\'{o} 2021]Let $ABC$ be a triangle with orthocenter $H$ and
circumcenter $O$. Let $D=AH\cap BO$ and $E=BH\cap CO$ and $F=CH\cap AO$. Then,
$\bigtriangleup DEF$ is inversely similar to $\bigtriangleup ABC$ (that is,
similar with opposite orientation), and the points $H,D,E,F$ lie on one circle.
\end{corollary}

Eight further applications are shown in the book under review. To complete the
kaleidoscope, I would add that the theorem can be viewed as a geometric avatar
of divided symmetrization\footnote{To explain this, let us prove Theorem
\ref{thm.gn} using complex numbers: Identify the points $A,B,C,M,N,P,T,A_{1}%
,B_{1},C_{1}$ with complex numbers $a,b,c,m,n,p,t,a_{1},b_{1},c_{1}$, and
assume WLOG that $t=0$ (that is, place the origin at $T$). Then,
$\bigtriangleup ABC_{1}\sim\bigtriangleup NMT$ yields $\left(  a-c_{1}\right)
/\left(  b-c_{1}\right)  =\left(  n-t\right)  /\left(  m-t\right)  =n/m$
(since $t=0$), so that $c_{1}=\dfrac{ma-nb}{m-n}$, and similarly $a_{1}%
=\dfrac{nb-pc}{n-p}$ and $b_{1}=\dfrac{pc-ma}{p-m}$. Our goal is to prove that
$\bigtriangleup A_{1}B_{1}C_{1}\sim\bigtriangleup MNP$, that is, $\left(
a_{1}-b_{1}\right)  /\left(  b_{1}-c_{1}\right)  =\left(  m-n\right)  /\left(
n-p\right)  $, or, equivalently, $\left(  a_{1}-b_{1}\right)  /\left(
m-n\right)  =\left(  b_{1}-c_{1}\right)  /\left(  n-p\right)  $. But besides
being a straightforward and short computation, this can be interpreted in
terms of forward divided differences (\cite[\S 1--\S 2]{Floater}): Let $f$ be
a polynomial sending $m,n,p$ to $ma,nb,pc$. Then, $a_{1}=\dfrac{nb-pc}%
{n-p}=\dfrac{f\left(  n\right)  -f\left(  p\right)  }{n-p}=f\left[
n,p\right]  $ (this would be $\left[  n,p\right]  f$ in the notations of
\cite{Floater}, but we put the $f$ up front) and similarly $b_{1}=f\left[
p,m\right]  $. Hence, $\left(  a_{1}-b_{1}\right)  /\left(  m-n\right)
=\left(  f\left[  n,p\right]  -f\left[  p,m\right]  \right)  /\left(
m-n\right)  =f\left[  n,p,m\right]  $. Similarly, $\left(  b_{1}-c_{1}\right)
/\left(  n-p\right)  =f\left[  p,m,n\right]  $. So our claim becomes $f\left[
n,p,m\right]  =f\left[  p,m,n\right]  $, which is a particular case of the
symmetry of divided differences.}.

There is much more to like in Chapter 1 than can fit in this review; the book
makes use of its space sparingly and wisely here. Problems 1.1.7 (a
concurrency with many degrees of freedom) and 1.10.17 (Pascal's theorem proved
using isogonal conjugation) are two of my favorites.

The remaining chapters are often shorter and less generous about solutions.
Some sections contain complete solutions to almost all problems; others give
just a few bare hints or not even that; there appears to be no logic behind
this other than the predilections of the different authors. Nevertheless,
there is much to learn here if one is not completionist and is willing to skip
some unduly challenging problems. Chapter 2 highlights the properties of
circles (radical axes, Ptolemy's and Casey's theorems\footnote{Casey's theorem
(Problem 2.6.12) is given a stepmotherly treatment (the proof of sufficiency
is far from sufficient). However, this is typical of geometry textbooks.}).
Chapter 3 discusses several types of geometric transformations and their
applications. Chapter 4 introduces centers-of-mass (i.e., weighted averages of
vectors), cross-ratios and polarity. Complex numbers are briefly discussed in
Chapter 5, probably too briefly for a first encounter but useful as a
supplement (e.g.) to \cite[Chapter 6]{Chen}. Geometric constructions and loci
get their moments in Chapter 6.

Chapter 7 makes a daring foray into solid and $n$-dimensional geometry, with a
slant towards combinatorial questions (tilings and coverings) and volumes.
While this certainly adds variety to the book, I am bewildered by the thought
of ever confronting students (even the most talented ones) with this material
in this form. Already the introductory sections pose serious challenges if one
is not blessed with high visuospatial skills or wants to prove the answers
rigorously. In the following sections, every other problem introduces some new
concept (and some of them -- such as \textquotedblleft surface
area\textquotedblright\ and \textquotedblleft semi-regular
body\textquotedblright\ -- without definition). Even worse, barely any hints
are given in this chapter, unlike the rest of the book; this is reminiscient
of book 1 at its worst.
% The best that can be said about this chapter is that it is only 20 pages
% long.

Chapter 8 returns to regular programming -- and quality -- with a series of
miscellaneous topics: geometric optimization, areas, conics and curvilinear
triangles. The treatment of conics here is nowhere as comprehensive as in
\cite{AkoZas}, but it serves as a good appetizer.

All in all, apart from Chapter 7, the book is well-written, thought-out and
finished, even if more hints would be welcome in some of the sections. Errors
are rare: I have spotted only one incorrect problem (2.3.5) and (outside of
Chapter 7) only one undefined concept (the \textquotedblleft
excircles\textquotedblright\ -- also called \textquotedblleft
escribed\textquotedblright\ or \textquotedblleft exscribed\textquotedblright%
\ circles -- of a triangle). Here, too, the authors keep track of known errors
( \url{https://users.mccme.ru/mskopenkov/skopenkov-pdf/mbl26-erratum.pdf} ).
With the minor caveats above, I can recommend this book to students and
teachers alike (audiences \textbf{(a)}, \textbf{(b)}, \textbf{(c)}), provided
that it is not their first encounter with the subject, as it is not meant to
be introductory. But there is no lack of introductory texts on Euclidean
geometry in various forms and styles that can get the reader to the level of
experience needed for this present book.

\bigskip

\begin{thebibliography}{999}                                                                                              %


\bibitem[A+]{ADM}Titu Andreescu, Gabriel Dospinescu, Oleg Mushkarov,
\textit{Number Theory: Concepts and Problems}, XYZ Press 2017.\newline\url{https://bookstore.ams.org/xyz-27}

\bibitem[AS]{AndSau}Titu Andreescu, Mark Saul, \textit{Algebraic Inequalities:
New Vistas}, MSRI/AMS 2016.

\bibitem[AZ1]{AigZie}Martin Aigner, G\"{u}nter M. Ziegler, \textit{Proofs from
the BOOK}, 6th edition, Springer 2018.\newline\url{https://doi.org/10.1007/978-3-662-57265-8}

\bibitem[AZ2]{AkoZas}A. V. Akopyan, A. A. Zaslavsky, \textit{Geometry of
Conics}, Mathematical World \textbf{26}, AMS 2007.

\bibitem[Be]{Beluhov}N. I. Beluhov, \textit{On some pairs of perspective
triangles (\textquotedblleft O nekotoryh parah perspektivnyh
treugoljnikov\textquotedblright)}, Matematicheskoe Prosveshjenie Ser. 3,
\textbf{14} (2010), pp. 256--259.\newline\url{https://www.mathnet.ru/rus/mp347}

\bibitem[Bo]{Bogart}Kenneth P. Bogart, \textit{Combinatorics Through Guided
Discovery}, 2017.\newline\url{https://bogart.openmathbooks.org/pdf/ctgd.pdf}

\bibitem[Bu]{Burton}David M. Burton, \textit{Elementary Number Theory},
McGrawHill, 7th edition 2011.

\bibitem[Ch]{Chen}Evan Chen, \textit{Euclidean Geometry in Mathematical
Olympiads}, MAA Problem Books, MAA 2016.\newline\url{https://web.evanchen.cc/geombook.html}

\bibitem[Ci]{Cirtoaje}Vasile Cirtoaje, \textit{Algebraic inequalities}, Gil
2006.\newline\url{https://cut-the-knot.org/arithmetic/algebra/VasileCirtoaje.pdf}

\bibitem[DD]{DenDen}Joseph B. Dence, Thomas P. Dence, \textit{Elements of the
Theory of Numbers}, Academic Press 1998.

\bibitem[DU]{DynUsp}E. B. Dynkin, V. A. Uspenskii, \textit{Mathematical
Conversations}, Dover 2006. (Originally published in Russian in 1952 and
translated into English in 1963.)

\bibitem[Eg]{Egamberganov}Khakimboy Egamberganov, \textit{A Generalization of
the Napoleon's Theorem}, Mathematical Reflections \textbf{3} (2017).\newline\url{https://www.awesomemath.org/wp-pdf-files/math-reflections/mr-2017-03/generalization_of_the_napoleons_theorem.pdf}

\bibitem[Fl]{Floater}Michael S. Floater, \textit{Newton interpolation}, notes
at Universitetet i Oslo, 27 January 2014.\newline\url{https://www.uio.no/studier/emner/matnat/math/nedlagte-emner/MAT-INF4140/v14/undervisningsmateriale/newton.pdf}

\bibitem[G+]{Gelfand}S. I. Gelfand, M. L. Gerver, A. A. Kirillov, N. N.
Konstantinov, A. G. Kushnirenko, \textit{Sequences, Combinations, Limits}, MIT
Press 1969. (Originally published in Russian in 1965.)

\bibitem[Gu]{Guichard}David Guichard, \textit{An Introduction to Combinatorics
and Graph Theory}, 2023.\newline\url{https://www.whitman.edu/mathematics/cgt_online/book/}

\bibitem[Hu]{Hung-secrets}Pham Kim Hung, \textit{Secrets in Inequalities,
volume 1}, Gil 2007.

\bibitem[KB]{KB}\href{https://doi.org/10.4171/EM/431}{S\'{a}ndor Nagydobai
Kiss, B\'{a}lint B\'{\i}r\'{o}, \textit{Two remarkable triangles of a triangle
and their circumcircles}, Elem. Math. \textbf{76} (2021), pp. 106--114.}

\bibitem[Le]{Lee}Hojoo Lee, \textit{Topics in Inequalities}, 2007.\newline\url{https://www.isinj.com/mt-usamo/}

\bibitem[N+]{NiZuMo}Ivan Niven, Herbert S. Zuckerman, Hugh L. Montgomery,
\textit{An Introduction to the Theory of Numbers}, 5th edition, Wiley 1991.

\bibitem[Pr]{Prasolov}Viktor Prasolov, \textit{Problems in Plane and Solid
Geometry}, translated by Dmitry Leites.\newline\url{https://archive.org/details/planegeo/page/n1/mode/2up}

\bibitem[Ri]{Rigby}J. F. Rigby, \textit{Napoleon Revisited}, Journal of
Geometry \textbf{33} (1988), pp. 129--146.\newline\url{https://dynamicmathematicslearning.com/napoleon-revisited-rigby.pdf}

\bibitem[Sh]{Sharygin}I. F. Sharygin, \textit{Problems In Plane Geometry}, Mir
1988.\newline\url{https://archive.org/details/SharyginProblemsInPlaneGeometryScienceForEveryone}

\bibitem[St]{Steele}J. Michael Steele, \textit{The Cauchy--Schwarz Master
Class}, Cambridge University Press 2004.\newline\url{https://doi.org/10.1017/CBO9780511817106}

\bibitem[Sw]{Stewart}Ian Stewart, \textit{Galois Theory}, 5th edition, CRC
Press 2023.\newline\url{https://doi.org/10.1201/9781003213949}

\bibitem[Ta]{Tattersall}James J. Tattersall, \textit{Elementary number theory
in nine chapters}, Cambridge University Press, 2nd edition 2005.\newline\url{https://doi.org/10.1017/CBO9780511756344}

\bibitem[YY]{YagYag}A. M. Yaglom, I. M. Yaglom, \textit{Challenging
Mathematical Problems with Elementary Solutions}, volumes 1 and 2, Holden--Day
1964 / Dover 1967. (Originally published in Russian in 1954.)
\end{thebibliography}


\end{document}