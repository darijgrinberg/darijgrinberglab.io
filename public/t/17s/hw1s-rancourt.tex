\documentclass{article}
\usepackage[letterpaper, portrait, margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{fancyhdr}
\usepackage{verbatim}
\usepackage{changepage}
\usepackage{amssymb}
\usepackage{blindtext}
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \textbf{\Large\@title}\\
  {\large\@author}\\
  {\large\@date\ (revised version)}\\
\end{flushleft}\egroup
}
\makeatother
\newcommand{\id}{\operatorname{id}}
\newcommand{\rev}{\operatorname{rev}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ yields $\left\{ ... \right\}$.
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ yields $\left| ... \right|$.
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ yields $\left( ... \right)$.
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ yields $\left[ ... \right]$.
\newcommand{\verts}[1]{\operatorname{V}\left( #1 \right)}
% $\verts{...}$ yields $\operatorname{V}\left( ... \right)$.
\newcommand{\edges}[1]{\operatorname{E}\left( #1 \right)}
% $\edges{...}$ yields $\operatorname{E}\left( ... \right)$.
\newcommand{\arcs}[1]{\operatorname{A}\left( #1 \right)}
% $\arcs{...}$ yields $\operatorname{A}\left( ... \right)$.
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications.

\title{Math 5707 Homework 1}
\author{Nicholas Rancourt}

\begin{document}
 \thispagestyle{plain}
 \maketitle
 \pagestyle{fancy}
 \fancyhf{}
 \lhead{Nicholas Rancourt HW1}
 \rhead{\thepage}

\paragraph{Exercise 1.} Let $G$ be a simple graph. A \textit{triangle} in $G$ means a set ${a, b, c}$ of three distinct vertices $a$, $b$, and $c$ such that $ab$, $bc$, and $ca$ are edges of $G$. An \textit{anti-triangle} in G means a set ${a, b, c}$ of three distinct vertices $a$, $b$, and $c$ such that none of $ab$, $bc$, and $ca$ is an edge of $G$. A \textit{triangle-or-anti-triangle} in $G$ is a set that is either a triangle or an anti-triangle. \\ 
\vspace{4 mm}

\textbf{a.) \underline{Proposition:}} Assume that $|V(G)|\geq6$. Then $G$ has at least two triangle-or-anti-triangles.\\ 

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} We proved in class (Proposition 2.4.1) that there is at least one triangle-or-anti-triangle, call its vertices $\{a_1, a_2, a_3\}$. There are at least three additional distinct vertices, $b_1$, $b_2$, and $b_3$. If all of $b_1$, $b_2$, and $b_3$ are adjacent to each other, then $\{b_1, b_2, b_3\}$ is a triangle. If none of $b_1$, $b_2$, and $b_3$ is adjacent to any other, then $\{b_1, b_2, b_3\}$ is an anti-triangle. Assume without loss of generality that neither of these is true. Then there is at least one pair of vertices among $b_1$, $b_2$, and $b_3$ that are not adjacent, and at least one pair that are adjacent. Call the non-adjacent pair $u$ and $v$ and the adjacent pair $x$ and $y$. Note that $\{u,v\}$ and $\{x,y\}$ need not be disjoint. Now, there are two cases to consider: $\{a_1, a_2, a_3\}$ is either an triangle or an anti-triangle.\\

\textbf{Case 1: Triangle.} If any of $b_1$, $b_2$, and $b_3$ has two or more neighbors in $\{a_1, a_2, a_3\}$, it forms a triangle with any two of these neighbors. Otherwise, each of $u$ and $v$ has at most one neighbor in $\{a_1, a_2, a_3\}$. Therefore, there is one vertex in $\{a_1, a_2, a_3\}$ that has neither $u$ nor $v$ as a neighbor, forming an anti-triangle.\\

\textbf{Case 2: Anti-triangle.} If any of $b_1$, $b_2$, and $b_3$ has two or more non-neighbors in $\{a_1, a_2, a_3\}$, it forms an anti-triangle with any two of these non-neighbors. Otherwise, each of $x$ and $y$ has at least two neighbors in $\{a_1, a_2, a_3\}$. Therefore, there is at least one vertex in $\{a_1, a_2, a_3\}$ that has both $x$ and $y$ as neighbors, forming a triangle.\\
\end{adjustwidth}
\vspace{4 mm}

\textbf{b.) \underline{Proposition:}} Assume that $|V(G)|=m+6$ for some $m\in \mathbb{N}$. Then $G$ has at least $m+1$ triangle-or-anti-triangles.\\ 

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} This can be proven by induction on $m$. For convenience, \textit{triangle-or-anti-triangle} will be abbreviated TOAT. First note that for $m=0$ and $m=1$, the proposition already follows from part (a). Now suppose the proposition is true for an arbitrary $m\in\mathbb{N}$. Then, given a graph $G$ with $|V(G)|=m+8$, we may form a subgraph of $m+6$ vertices by removing any two distinct vertices $a\in V$ and $b\in V$, i.e. form the graph $G'=(V',E')$ where $V'=V\backslash\{a,b\}$ and $E'=E\backslash\{uv\in E\mid u\in\{a,b\} \text{ or } v \in\{a,b\}\}$. By the inductive hypothesis, $G'$ contains at least $m+1$ TOATs. If $G'$ contains $m+3$ or more TOATs, then the proposition holds. For the remainder of the proof, assume without loss of generality that $G'$ contains $\leq m+2$ TOATs.\\

Consider the number of distinct sets of four vertices in $V'$ that contain no TOAT. There are a total of $\binom{m+6}{4}$ sets of four vertices in $V'$, and at most $m+2$ TOATs. The number of sets of four vertices containing no TOAT will be minimized if no set of four vertices contains more than one TOAT. Since each TOAT will appear in exactly $m+3$ sets, there are at most $(m+3)(m+2)$ sets of four vertices containing at least one TOAT, and at least $\binom{m+6}{4}-(m+3)(m+2)$ sets containing no TOAT. This is greater than one, so we may choose one such set of four vertices $\{v_1, v_2, v_3, v_4\}$ in $V'$. Then by part (a) of the proof, the set $\{a,b,v_1,v_2,v_3,v_4\}$ contains at least two TOATs distinct from those in $G'$. Taken together with the $\geq m+1$ TOATs in $G'$, we then have at least $m+3$ TOATs in $G$. Therefore, the proposition holds for $m+2$. By induction, it holds for all $m\in\mathbb{N}$.
\end{adjustwidth}

\newpage
\paragraph{Exercise 2.} Let $G$ be a simple graph. Let $n=|\operatorname{V}(G)|$ be the number of vertices of $G$. Assume that $|\operatorname{E}(G)|<n(n-2)/4$. Then there exist three distinct vertices $a$, $b$, and $c$ of $G$ such that none of $ab$, $bc$, and $ca$ are edges of $G$.\\ 
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} I will prove the proposition by induction on $n$. To begin, note that $n$ must be $\geq 3$. Otherwise, the assumption that $|\operatorname{E}(G)|<n(n-2)/4$ would require a negative number of edges. The proposition is clearly true for $n=3$, since in this case $|\operatorname{E}(G)|<n(n-2)/4=3/4$ implies that $|\operatorname{E}(G)|$ must be $0$. Similarly for $n=4$, we get $|\operatorname{E}(G)|<2$, so there are four vertices and at most one edge.\\

For $n\geq 5$, assume the propostion holds for $n-2$. Since the number of edges $|\operatorname{E}(G)|<n(n-2)/4$ is less than the number of total possible edges $n(n-1)/2$, there exist two distinct vertices $v_1\in \operatorname{V}(G)$ and $v_2\in \operatorname{V}(G)$ such that $v_1v_2\notin \operatorname{E}(G)$. Form the graph $G'=(\operatorname{V}(G)\backslash\{v_1, v_2\},\operatorname{E}(G)\backslash\{uv_i|i\in\{1, 2\}, u\in\operatorname{V}(G)\})$. (This is the graph obtained from $G$ by removing the vertices $v_1$ and $v_2$ and all edges containing one of them.) If there are three distinct vertices in $\operatorname{V}(G')$ such that none is adjacent to either of the other two, then the proposition holds. Without loss of generality then, assume that there are no such three vertices.\\

Then, by the inductive hypothesis, we must have that $|\operatorname{E}(G')|\geq (n-2)(n-4)/4$. Then since $|\operatorname{E}(G)|<n(n-2)/4$, it follows that $\deg_G(v_1)+\deg_G(v_2)<\frac{n(n-2)}{4}-\frac{(n-2)(n-4)}{4}=n-2$. But there are $n-2$ vertices in $\operatorname{V}(G)$ apart from $v_1$ and $v_2$; so there must exist some distinct $v_3\in \operatorname{V}(G)$ that is adjacent to neither $v_1$ nor $v_2$. The three vertices $v_1$, $v_2$, and $v_3$ satisfy the proposition. By induction, this argument holds for all $n\geq 3$.
\end{adjustwidth}

\paragraph{Exercise 3.} Let $G$ be a simple graph. Let $\mathbf{w}$ be a path in $G$. Then the edges of $\mathbf{w}$ are distinct.\\ 
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} By the definition of a path, for path $\mathbf{w}=(v_0, v_1, \ldots, v_k)$, $v_i\neq v_j$ for all $0\leq i\neq j\leq k$. Suppose that for some $0\leq i\leq k$ and $0\leq j \leq k$, $v_iv_{i+1}=v_jv_{j+1}$. Then $\{v_i,v_{i+1}\}=\{v_j,v_{j+1}\}$. It cannot be that $v_i=v_{j+1}$ and $v_j=v_{i+1}$, because then we would have $i=j+1=(i+1)+1$. Therefore $v_i=v_j$ and $v_{i+1}=v_{j+1}$, so $i=j$. Thus each edge of $\mathbf{w}$ appears exactly once.
\end{adjustwidth}

\paragraph{Exercise 4.} Let $n\in \mathbb{N}$. What is the smallest possible size of a dominating set of the cycle graph $C_{3n}$?\\ 
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Solution:}} It's $n$. Since the vertices of a cycle graph all have degree 2, a vertex in a dominating set can have at most 2 neighbors outside the dominating set. Thus the size of a dominating set must be at least half the size of its complement, placing a lower limit of $n$ on the size of a dominating set. To show that it is possible to construct a dominating set of size $n$, pick any vertex $v_1\in\operatorname{V}(C_{3n})$ and enumerate the cycle as $(v_1, v_2, \ldots, v_{3n},v_1)$. The set $S=\{v_{3i-1}|1\leq i\leq n\}$ is a dominating set, since $v_{3i}$ and $v_{3i-2}$ are adjacent to $v_{3i-1}$ for all $1\leq i\leq n$, and these account for all elements of $\operatorname{V}(C_{3n})$ not in $S$.
\end{adjustwidth}

\paragraph{Exercise 5.}\hspace{0 pt}\\

\textbf{A.) \underline{Proposition 0.2:}}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(a)} If $\mathcal{A}$ and $\mathcal{B}$ are two equivalent logical statements, then $[\mathcal{A}]=[\mathcal{B}]$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} The equivalence of $\mathcal{A}$ and $\mathcal{B}$ means that $\mathcal{A}\iff\mathcal{B}$. $\mathcal{A}$ is either true or false, so we have one of the following two situations:
\begin{equation*}
([\mathcal{A}]=1)\iff\mathcal{A}\iff\mathcal{B}\iff([\mathcal{B}]=1)
\end{equation*}
\begin{equation*}
([\mathcal{A}]=0)\iff\neg\mathcal{A}\iff\neg\mathcal{B}\iff([\mathcal{B}]=0)
\end{equation*}
In either case, $[\mathcal{A}]=[\mathcal{B}]$.
\end{adjustwidth}
\end{adjustwidth}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(b)} If $\mathcal{A}$ is any logical statement, then $[\neg\mathcal{A}]=1-[\mathcal{A}]$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} $\mathcal{A}$ is either true or false. If $\mathcal{A}$ is true, then $\neg\mathcal{A}$ is false, so $[\mathcal{A}]=1$ and $[\neg\mathcal{A}]=0=1-[\mathcal{A}]$. If $\mathcal{A}$ is false, then $\neg\mathcal{A}$ is true, so $[\mathcal{A}]=0$ and $[\neg\mathcal{A}]=1=1-[\mathcal{A}]$. In either case, $[\neg\mathcal{A}]=1-[\mathcal{A}]$.
\end{adjustwidth}
\end{adjustwidth}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(c)} If $\mathcal{A}$ and $\mathcal{B}$ are two logical statements, then $[\mathcal{A}\land\mathcal{B}]=[\mathcal{A}][\mathcal{B}]$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} $\mathcal{A}\land\mathcal{B}$ is either true or false. If it is true, then both $\mathcal{A}$ and $\mathcal{B}$ are true, and we have $[\mathcal{A}][\mathcal{B}]=1\cdot1=1=[\mathcal{A}\land\mathcal{B}]$. If it is false, then at least one of $\mathcal{A}$ and $\mathcal{B}$ is false, so we have $[\mathcal{A}][\mathcal{B}]=0=[\mathcal{A}\land\mathcal{B}]$. In either case, $[\mathcal{A}\land\mathcal{B}]=[\mathcal{A}][\mathcal{B}]$.
\end{adjustwidth}
\end{adjustwidth}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(d)} If $\mathcal{A}$ and $\mathcal{B}$ are two logical statements, then $[\mathcal{A}\lor\mathcal{B}]=[\mathcal{A}]+[\mathcal{B}]-[\mathcal{A}][\mathcal{B}]$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} There are three cases to consider: (i) $\mathcal{A}$ and $\mathcal{B}$ are both true, (ii) $\mathcal{A}$ and $\mathcal{B}$ are both false, or (iii) exaclty one of $\mathcal{A}$ and $\mathcal{B}$ is true.
\begin{adjustwidth}{5 mm}{0 pt}
\textbf{(i)} If $\mathcal{A}$ and $\mathcal{B}$ are both true, then $[\mathcal{A}]=[\mathcal{B}]=[\mathcal{A}\lor\mathcal{B}]=1$, so $[\mathcal{A}]+[\mathcal{B}]-[\mathcal{A}][\mathcal{B}]=1+1-1\cdot 1=1=[\mathcal{A}\lor\mathcal{B}]$.\\
\textbf{(ii)} If $\mathcal{A}$ and $\mathcal{B}$ are both false, then $[\mathcal{A}]=[\mathcal{B}]=[\mathcal{A}\lor\mathcal{B}]=0$, so $[\mathcal{A}]+[\mathcal{B}]-[\mathcal{A}][\mathcal{B}]=0+0-0\cdot 0=0=[\mathcal{A}\lor\mathcal{B}]$.\\
\textbf{(iii)} If exactly one of $\mathcal{A}$ and $\mathcal{B}$ is true, then one of $[\mathcal{A}]$ and $[\mathcal{B}]$ is 1 and the other is 0, and $[\mathcal{A}\lor\mathcal{B}]=1$, so $[\mathcal{A}]+[\mathcal{B}]-[\mathcal{A}][\mathcal{B}]=1+0-1\cdot 0=1=[\mathcal{A}\lor\mathcal{B}]$.
\end{adjustwidth}
In each case, $[\mathcal{A}\lor\mathcal{B}]=[\mathcal{A}]+[\mathcal{B}]-[\mathcal{A}][\mathcal{B}]$.
\end{adjustwidth}
\end{adjustwidth}

\textbf{B.) \underline{Proposition 0.3:}} Let $Q$ be a subset of $P$.\\

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(a)} Then $|Q|=\sum\limits_{p\in P} [p\in Q]$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} Let $n=|Q|$ and $m=|P|$. We may partition $P$ into $Q$ and $P\backslash Q$, and index the elements of $Q$ as $q_1, q_2, \ldots, q_n$ and the elements of $P\backslash Q$ as $p_1, p_2, \ldots, p_m$. The sum can then be written as $\sum\limits_{p\in Q} [p \in Q] =\sum\limits_{i=1}^n[q_i\in Q]+\sum\limits_{j=1}^m[p_j\in Q]$. But the statement $q_i\in Q$ is true for all $1\leq i\leq n$, and the statement $p_j\in Q$ is false for all $1\leq j\leq m$, so the sum becomes $\sum\limits_{p\in Q}=\sum\limits_{i=1}^n1+\sum\limits_{j=1}^m0=n=|Q|. $\\
\end{adjustwidth}
\end{adjustwidth}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(b)} For each $p\in P$, let $a_p$ be a number (for example, a real number). Then, $\sum\limits_{p\in P}[p\in Q]a_p=\sum\limits_{p\in Q}a_p$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} By partitioning the elements of $P$ into $Q$ and $P\backslash Q$, the sum can be written as 
\begin{equation*}\sum\limits_{p\in P}[p\in Q]a_p=\sum\limits_{p\in Q}[p\in Q]a_p+\sum\limits_{p\in P\backslash Q}[p\in Q]a_p.\end{equation*}
But the statement $p\in Q$ is true for all $p\in Q$ and false for all $p\in P\backslash Q$, so the sum becomes
\begin{equation*}\sum\limits_{p\in P}[p\in Q]a_p=\sum\limits_{p\in Q}1\cdot a_p+\sum\limits_{p\in P\backslash Q}0\cdot a_p=\sum\limits_{p\in Q}a_p.\end{equation*}\\
\end{adjustwidth}
\end{adjustwidth}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{(c)} For each $p\in P$, let $a_p$ be a number (for example, a real number). Let $q\in P$. Then, $\sum\limits_{p\in P}[p=q]a_p=a_q$.\\

\begin{adjustwidth}{10 mm}{0 pt}
\textbf{\underline{Proof:}} By partitioning the elements of $P$ into $\{q\}$ and $P\backslash \{q\}$, the sum can be written as 
\begin{equation*}\sum\limits_{p\in P}[p=q]a_p=[q=q]a_q+\sum\limits_{p\in P\backslash \{q\}}[p=q]a_p.\end{equation*}
But the statement $q=q$ is true and the statement $p=q$ is false for all $p\in P\backslash \{q\}$, so the sum becomes
\begin{equation*}\sum\limits_{p\in P}[p=q]a_p=1\cdot a_q+\sum\limits_{p\in P\backslash \{q\}}0\cdot a_p=a_q.\end{equation*}\\
\end{adjustwidth}
\end{adjustwidth}

\textbf{C.) \underline{Proposition:}} Let $G$ be a simple graph. Then $\deg v=\sum\limits_{u\in\operatorname{V}(G)}[uv\in \operatorname{E}(G)]$ for each vertex $v$ of $G$.\\

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} Let $v$ be a vertex of $G$. Partition $\operatorname{V}(G)$ into $P=\{u\in\operatorname{V}(G)|uv\in \operatorname{E}(G)\}$ and $Q=\{u\in\operatorname{V}(G)|uv\notin\operatorname{E}(G)\}$. Then the sum is 
\begin{equation*}\sum\limits_{u\in\operatorname{V}(G)}[uv\in\operatorname{E}(G)]=\sum\limits_{u\in P}[uv\in\operatorname{E}(G)]+\sum\limits_{u\in Q}[uv\in\operatorname{E}(G)]=\sum\limits_{u\in P}1+\sum\limits_{u\in Q}0=|P|.\end{equation*}
But $|P|$ is the definition of $\deg v$, so we have $\deg v=\sum\limits_{u\in\operatorname{V}(G)}[uv\in \operatorname{E}(G)]$ for all $v\in\operatorname{V}(G)$.
\end{adjustwidth}

\textbf{D.) \underline{Proposition:}} Let $G$ be a simple graph. Then $2|\operatorname{E}(G)|=\sum\limits_{u\in\operatorname{V}(G)}\sum\limits_{v\in\operatorname{V}(G)}[uv\in \operatorname{E}(G)]$.\\

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} First note that for all terms of the sum in which $u=v$, $uv\notin\operatorname{E}(G)$, so $[uv\in\operatorname{E}(G)]=0$. These terms may therefore be eliminated from the sum. Further note that for all $a,b\in\operatorname{V}(G)$ where $a\neq b$, the value of $[ab\in\operatorname{E}(G)]$ will be counted exactly twice in the sum (once when $u=a$ and $v=b$, and again when $u=b$ and $v=a$). Therefore, the sum may be rewritten as
\begin{equation*}\sum\limits_{u\in\operatorname{V}(G)}\sum\limits_{v\in\operatorname{V}(G)}[uv\in \operatorname{E}(G)]=2\sum\limits_{uv\in\mathcal{P}_2\left(\operatorname{V}(G)\right)}[uv\in\operatorname{E}(G)].\end{equation*}
This sum can be split and simplified as below:
\begin{equation*}\sum\limits_{u\in\operatorname{V}(G)}\sum\limits_{v\in\operatorname{V}(G)}[uv\in \operatorname{E}(G)]=2\left(\sum\limits_{uv\in\operatorname{E}(G)}[uv\in\operatorname{E}(G)]+\sum\limits_{uv\in\mathcal{P}_2(\operatorname{V}(G))\backslash\operatorname{E}(G)}[uv\in\operatorname{E}(G)]\right)\end{equation*}
\begin{equation*}=2\left(\sum\limits_{uv\in\operatorname{E}(G)}1+\sum\limits_{uv\in\mathcal{P}_2(\operatorname{V}(G))\backslash\operatorname{E}(G)}0\right)=2|\operatorname{E}(G)|.\end{equation*}
\end{adjustwidth}

\paragraph{Exercise 6.} Let $k$ be a positive integer. Let $G$ be a graph. A subset $U$ will be called \textit{k-path-dominating} if for every $v\in\operatorname{V}(G)$, there exists a path of length $\leq k$ from $v$ to some element of $U$. Then the number of all $k$-path-dominating subsets of $\operatorname{V}(G)$ is odd.\\ 
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} This proof will parallel the proof of the Heinrich-Tittmann formula. Set $V = \operatorname{V}(G)$ and $E = \operatorname{E}(G)$, and assume WLOG that $V \neq \varnothing$.

The length of the shortest path between two vertices $u$ and $v$ will be written $d(u,v)$. (If no such path exists, then we set $d(u,v) = \infty$, where $\infty$ is a symbol that is not an actual number but which we consider to be larger than any integer.) Define a \textit{k-detached pair} $(A,B)$ as a pair of two disjoint subsets $A$ and $B$ of $V$ such that every $a \in A$ and $b \in B$ satisfy $d(a,b)>k$. Let $B\subseteq V$ and let $B'=\{v\in V|\exists b\in B\ \text{such that}\ d(b,v)\leq k\}$. With these definitions, the statement "$(A,B)$ is a $k$-detached pair" is equivalent to $A\subseteq V\backslash(B\cup B')$. Then the truth value $[B$ is a $k$-path dominating subset in $G]$ can be expressed as the following sum:
\begin{equation*}
[B\text{ is }k\text{-path-dominating in }G]=[V\backslash(B\cup B')=\emptyset]=\sum\limits_{A\subseteq V\backslash(B\cup B')}(-1)^{|A|}=\sum\limits_{\substack{A\subseteq V\\(A,B)\ k\text{-detached}}}(-1)^{|A|},
\end{equation*}
where the second equality comes from Lemma 3.3.5 in the course notes.\\

To use this sum to determine the number of $k$-path-dominating subsets, we will need to define some more sets. Let $S=\{k$-detached pairs $(A,B)\mid|A|$ and $|B|$ are both even and positive\}, let $T=\{k$-detached pairs $(A,B)\mid|A|$ and $|B|$ are both odd\}, and let $U=\{k$-detached pairs $(A,B)\mid|A|$ and $|B|$ are of opposite parity and positive\}.\\

It follows from the definition of $k$-detached pairs that a pair $(A,B)$ is a $k$-detached pair if and only if $(B,A)$ is a $k$-detached pair. To verify this, manipulate the definition as below:
\begin{align*}
((A,B)\ k\text{-detached pair})\iff&(A\subseteq V, B\subseteq V, A\cap B=\emptyset,\text{ and }\forall a\in A\forall b\in B\ d(a,b)>k)\\
\iff&(B\subseteq V, A\subseteq V, B\cap A=\emptyset,\text{ and }\forall b\in A\forall a\in B\ d(b,a)>k)\\
\iff&(B\subseteq V, A\subseteq V, B\cap A=\emptyset,\text{ and }\forall a\in B\forall b\in A\ d(a,b)>k)\\
\iff&((B,A)\ k\text{\text{-detached\ pair}}).
\end{align*}
Then a pair $(A,B)\in S$ if and only if $(B,A)\in S$. Similarly, a pair $(A,B)\in T$ if and only if $(B,A)\in T$.\\

Define the function $\sigma_S:S\to S$ by $\sigma_S(A,B)=(B,A)$, and define the function $\sigma_T:T\to T$ by $\sigma_T(A,B)=(B,A)$. Note that $\sigma_S\circ\sigma_S=\id_S$ and $\sigma_T\circ\sigma_T=\id_T$. Furthermore note that since for all $(A,B)\in S$ neither $A$ nor $B$ can be equal to $\emptyset$ and $A\cap B=\emptyset$, it follows that $\sigma_S(A,B)\neq(A,B)$ for all $(A,B)\in S$. Indeed, if it were true that $\sigma_S(A,B)=(A,B)$ for some $(A,B)\in S$, then the definition of a $k$-detached pair would require that $A=B=\emptyset$, which cannot be the case. The same argument shows that $\sigma_T(A,B)\neq(A,B)$ for all $(A,B)\in T$. The preceding properties of $S$, $T$, $\sigma_S$, and $\sigma_T$ allow the application of Corollary 3.4.5 from the course notes to show that $|S|$ and $|T|$ are both even.\\

Now we may use the above results and the same sum manipulation from the proof of the Heinrich-Tittmann formula to determine the number of $k$-path-dominating subsets. Let $\gamma$ be the number of $k$-path dominating subsets in $G$. Let the sums begin:
\begin{equation*}
\sum\limits_{\substack{(A,B)\ k\text{-detached\ pair}\\A\neq\emptyset,B\neq\emptyset}}\left((-1)^{|A|}+(-1)^{|B|}\right)
\end{equation*}
\begin{equation*}
=\sum\limits_{(A,B)\in S}\left((-1)^{|A|}+(-1)^{|B|}\right)+\sum\limits_{(A,B)\in T}\left((-1)^{|A|}+(-1)^{|B|}\right)+\sum\limits_{(A,B)\in U}\left((-1)^{|A|}+(-1)^{|B|}\right)
\end{equation*}
\begin{equation*}
=\sum\limits_{(A,B)\in S}2+\sum\limits_{(A,B)\in T}(-2)+\sum\limits_{(A,B)\in U}0=2|S|-2|T|
\end{equation*}
We can also evaluate this sum as:
\begin{align*}
&\sum\limits_{\substack{(A,B)\ k\text{-detached\ pair}\\A\neq\emptyset,B\neq\emptyset}}\left((-1)^{|A|}+(-1)^{|B|}\right)=\sum\limits_{\substack{(A,B)\ k\text{-detached\ pair}\\A\neq\emptyset,B\neq\emptyset}}(-1)^{|A|}+\sum\limits_{\substack{(A,B)\ k\text{-detached\ pair}\\A\neq\emptyset,B\neq\emptyset}}(-1)^{|B|} \\
&=2\sum\limits_{\substack{(A,B)\ k\text{-detached\ pair}\\A\neq\emptyset,B\neq\emptyset}}(-1)^{|A|}=2\sum\limits_{\substack{B\subseteq V\\B\neq\emptyset}}\sum\limits_{\substack{A\subseteq V,A\neq\emptyset\\(A,B)\ k\text{-detached\ pair}}}(-1)^{|A|} \\
&=2\sum\limits_{\substack{B\subseteq V\\B\neq\emptyset}}\left(\sum\limits_{\substack{A\subseteq V\\(A,B)\ k\text{-detached\ pair}}}(-1)^{|A|}-1\right)=2\sum\limits_{\substack{B\subseteq V\\B\neq\emptyset}}[B\text{\ is }k\text{-path-dominating}]-2\sum\limits_{\substack{B\subseteq V\\B\neq\emptyset}}1 \\
&=2\gamma-2(2^{|V|}-1),
\end{align*}
where $\gamma$ now stands for the number of $k$-path-dominating subsets.\footnote{We have used the simple observation that each $k$-path-dominating subset is nonempty.}
By equating this with the previous result we got for this sum we get:
\begin{equation*}
2\gamma-2(2^{|V|}-1)=2|S|-2|T|\implies\gamma=2^{|V|}-1+|S|-|T|.
\end{equation*}
Since $|S|$, $|T|$, and $2^{|V|}$ are even, $\gamma$ is odd.
\end{adjustwidth}

\paragraph{Exercise 7.} Let $G$ be a simple graph with $\operatorname{V}(G)\neq \emptyset$. Then the following two statements are equivalent:
\begin{itemize}
\item \textit{Statement 1:} The graph $G$ is connected.
\item \textit{Statement 2:} For every two nonempty subsets $A$ and $B$ of $\operatorname{V}(G)$ satisfying $A\cap B=\emptyset$ and $A\cup B=\operatorname{V}(G)$, there exist $a\in A$ and $b\in B$ such that $ab\in \operatorname{E}(G)$.
\end{itemize}
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} First assume \textit{Statement 1} holds, i.e. the graph $G$ is connected. Let $A\subseteq\operatorname{V}(G)$ and $B\subseteq\operatorname{V}(G)$ be sets satisfying $A\cap B=\emptyset$ and $A\cup B=\operatorname{V}(G)$. Since $G$ is connected, for any $a\in A$ and $b\in B$ there exists a path from $a$ to $b$. If the path is of length one, then $ab\in\operatorname{E}(G)$, and \textit{Statement 2} holds. Otherwise, there are intermediate vertices in the path. Rename $a$ as $v_0$ and $b$ as $v_n$, where $n$ is the length of the path. Call the vertices in the path $(v_0, v_1, v_2, \ldots, v_n)$. Since the path starts in $A$ and terminates in $B$, there must be a first vertex of the path that that lies in $B$. Call this vertex $v_i$. Since $v_i$ is the first vertex in $B$, we have $v_{i-1}\in A$, $v_i\in B$, and $v_{i-1}v_i\in\operatorname{E}(G)$. Therefore if \textit{Statement 1} holds, then \textit{Statement 2} also holds.\\

Now assume \textit{Statement 2} holds. Suppose that $G$ is not connected. Then there exist $a\in\operatorname{V}(G)$ and $b\in\operatorname{V}(G)$ such that there is no path from $a$ to $b$ in $G$. Consider the sets $A=\{v\in\operatorname{V}(G)|$there exists a path in $G$ from $a$ to $v\}$ and $B=\operatorname{V}(G)\backslash A$. These sets are nonempty ($A$ contains at least $a$ and $B$ contains at least $b$), and they satisfy the conditions $A\cap B=\emptyset$ and $A\cup B=\operatorname{V}(G)$. If there are elements $u\in A$ and $v\in B$ such that $uv\in\operatorname{E}(G)$, then by the transitivity of path existence, there must be a path from $a$ to $v$ (because there is a path from $a$ to $u$, but also an edge from $u$ to $v$). This would imply that $v\in A$, a contradiction. Thus there are no elements $u\in A$ and $v\in B$ such that $uv\in\operatorname{E}(G)$, which contradicts the assumption of \textit{Statement 2}. Therefore, if \textit{Statement 2} is true, \textit{Statement 1} is also true.\\

Since \textit{Statement 1}$\implies$\textit{Statement 2} and \textit{Statement 2}$\implies$\textit{Statement 1}, the statements are equivalent.
\end{adjustwidth}

\paragraph{Exercise 8.} Let $V$ be a nonempty finite set. Let $G$ and $H$ be two simple graphs such that $\operatorname{V}(G)=\operatorname{V}(H)=V$. Assume that for each $u\in V$ and $v\in V$, there exists a path from $u$ to $v$ in $G$ or a path from $u$ to $v$ in $H$. Then at least one of the graphs $G$ and $H$ is connected.\\
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} %The proposition is trivial for $|V|=1$. % and $|V|=2$.
%For $|V|\geq 2$, we argue as follows:
A subset $U$ of $V$ is said to be \textit{$G$-connected} if for each two vertices $a, b \in U$, there is a walk from $a$ to $b$ in $G$ (or, equivalently, there is a path from $a$ to $b$ in $G$). (Note that the walk is allowed to use vertices outside of $U$; but even if we didn't allow this, the argument would not change.) Similarly, the notion of ``$H$-connected'' is defined. Clearly, if we can show that the whole set $V$ is either $G$-connected or $H$-connected, then we are done.

For this, it clearly suffices to prove the following claim:
\begin{quote}
\textit{Claim 1:} Each nonempty subset $U$ of $V$ is $G$-connected or $H$-connected.
\end{quote}
So let us prove Claim 1.

We proceed by induction over $\abs{U}$. If $\abs{U} = 1$, then Claim 1 holds for obvious reasons. Let us now assume that $\abs{U} > 1$, and assume (as induction hypothesis) that Claim 1 is proven for each smaller nonempty subset of $V$. Let us pick any $x \in U$. The subset $U \setminus \set{x}$ is smaller than $U$ but still a nonempty subset of $V$. Hence, by the induction hypothesis, Claim 1 holds for it; in other words, $U \setminus \set{x}$ is $G$-connected or $H$-connected. WLOG assume that $U \setminus \set{x}$ is $G$-connected (otherwise, the same argument works, except that we need to switch the roles of $G$ and $H$). The following two cases are possible:
\begin{enumerate}
\item[\textbf{(i)}] There exists some $u \in U \setminus{x}$ such that there is a walk from $u$ to $x$ in $G$.
\item[\textbf{(ii)}] For each $u \in U \setminus \set{x}$, there is no walk from $u$ to $x$ in $G$.
\end{enumerate}

\begin{itemize}
\item
In case \textbf{(i)}, there exists some $u \in U \setminus{x}$ such that there is a walk from $u$ to $x$ in $G$. But since $U \setminus \set{x}$ is $G$-connected, there also exists a walk from $v$ to $x$ in $G$ for every $v \in U \setminus{x}$. Hence, by the transitivity of walk existence, there is a walk from $v$ to $x$ for every $v \in U \setminus{x}$. Therefore, the set $U$ is $G$-connected.\\
\item
In case \textbf{(ii)}, for each $u \in U \setminus \set{x}$, there is no walk from $u$ to $x$ in $G$. Hence, for each $u \in U \setminus \set{x}$, there is a path from $u$ to $x$ in $H$ (by the assumption made in this exercise). This, of course, also holds for $u = x$ (using the empty walk); thus, it holds for each $u \in U$. Hence, by the transitivity and symmetry of walk existence, for all $u,v\in U$, there is a walk in $H$ from $u$ to $v$ (obtained by first traversing a walk from $u$ to $x$, and then traversing a walk from $v$ to $x$ backwards). Thus, the set $U$ is $H$-connected.\\ 
\end{itemize}

We have thus proven Claim 1 for our set $U$ in both cases; this completes the inductive proof. And this, as we have seen, solves the exercise.
\end{adjustwidth}

\newpage
\paragraph{Exercise 9.} Let $G=(V,E)$ be a simple graph. The \textit{complement graph} $\overline{G}$ of $G$ is defined to be the simple graph $(V, \mathcal{P}_2(V)\backslash E)$. At least one of the following two statements holds:
\begin{itemize}
\item \textit{Statement 1:} For each $u\in V$ and $v\in V$, there exists a path from $u$ to $v$ in $G$ of length $\leq 3$.
\item \textit{Statement 2:} For each $u\in V$ and $v\in V$, there exists a path from $u$ to $v$ in $\overline{G}$ of length $\leq 2$.
\end{itemize}
\vspace{4 mm}

\begin{adjustwidth}{15 mm}{0 pt}
\textbf{\underline{Proof:}} Suppose that \textit{Statement 1} is not true. Then there exist distinct vertices $u,v\in V$ such that there is no path from $u$ to $v$ in $G$ of length $\leq 3$. This clearly requires that $uv\notin\operatorname{E}(G)$, implying that $uv\in\operatorname{E}(\overline{G})$. Let us see what more we can say about these $u$ and $v$.\\

Let $X=\{x\in V|ux\in\operatorname{E}(G)\}$ and let $Y=\{y\in V|vy\in\operatorname{E}(G)\}$. The suppostion requires that for all $x\in X$ and $y\in Y$, we have $xy\notin\operatorname{E}(G)$ (since otherwise, $\left(u,x,y,v\right)$ would be a path from $u$ to $v$ in $G$ of length $3 \leq 3$) and $x \neq y$ (since otherwise, $\left(u,x,v\right)$ would be a path from $u$ to $v$ in $G$ of length $2 \leq 3$). Therefore, for all $x\in X$ and $y\in Y$, we have $xy\in\operatorname{E}(\overline{G})$.\\

Note that if there is some $x\in V$ such that $x\in X$ and $x\in Y$, then there would be a path $(u, x, v)$ of length $2$. Hence it must hold that $X\cap Y=\emptyset$. This means that for all $x\in X$, $vx\notin\operatorname{E}(G)$, and therefore $vx\in\operatorname{E}(\overline{G})$. Similarly, for all $y\in Y$, $uy\in\operatorname{E}(\overline{G})$.\\

Now, we want to prove \textit{Statement 2}. We need to rename the $u$ and $v$ appearing in this statement, because we are already using the letters $u$ and $v$ for something else. So we must prove that for each $a \in V$ and $b \in V$, there exists a path from $a$ to $b$ in $\overline{G}$ of length $\leq 2$. Indeed, assume the contrary. Then, there are two vertices $a \in V$ and $b \in V$ such that there exists no such path. In particular, neither $\left(a,u,b\right)$ nor $\left(a,v,b\right)$ is such a path (where we should read $\left(a,u,b\right)$ as $\left(a,b\right)$ if $u$ equals one of $a$ and $b$, and similarly for $\left(a,v,b\right)$). Since $\left(a,u,b\right)$ is not a path in $\overline{G}$, we see that one of $au$ and $bu$ must be an edge in $G$; in other words, one of $a$ and $b$ is a neighbor of $a$; in other words, one of $a$ and $b$ belongs to $X$. Similarly, one of $a$ and $b$ belongs to $Y$. Since $X \cap Y = \emptyset$, we conclude that \textbf{one} of $a$ and $b$ must belong to $X$, whereas \textbf{the other} belongs to $Y$. Hence, WLOG $a \in X$ and $b \in Y$ (otherwise, just switch the roles of $a$ and $b$). Then, $ab\in\operatorname{E}(\overline{G})$ (because we know that $xy\in\operatorname{E}(\overline{G})$ for all $x\in X$ and $y\in Y$), which means that there is a path of length $1$ from $a$ to $b$ in $\overline{G}$, contradicting our assumption that no path of length $\leq 2$ exists. This shows that \textit{Statement 2} holds.

Since we have now proven (\textit{not Statement 1})$\implies$\textit{Statement 2}, at least one of the statements is true.
\end{adjustwidth}

\end{document}