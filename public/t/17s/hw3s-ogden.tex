\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{comment}

\DeclareMathOperator{\conn}{conn}
\DeclareMathOperator{\mo}{mod}
\DeclareMathOperator{\ecc}{eccentricity}

\title{Mathematics 5707 Homework 3}
\author{Jacob Ogden}
\date{March 8, 2017 (corrected version)}

\begin{document}

\maketitle
\setlength{\parindent}{0ex}
\parskip=5mm

\textbf{Exercise 1.} Let $T$ be a tree. Let $(v_0,v_1,...,v_k)$ be a longest path of $T$. Prove that each center of $T$ belongs to this path. \par

\textbf{Proof:} The path $(v_0,v_1,...,v_k)$ is a longest path in $T$ and has length $k$. Thus, $v_1$ and $v_k$ have eccentricity $k$. Similarly, $v_1$ and $v_{k-1}$ have eccentricity $k-1$. The eccentricity continues to decrease as we move along the path $(v_0,v_1,...,v_k)$ towards its middle, and by symmetry we can see that it attains its minimum value at the center(s) (plural if the path is of odd length) of the path. We must show that there is no vertex in $T$ with eccentricity less than that of the centers of the path, i.e. that the centers of the path are the centers of $T$. To show this, note that any vertex not on the path $(v_0,v_1,...,v_k)$ is a greater distance from the ends of the path than the nearest vertex to it that lies on the path, since the path between any two vertices of $T$ is unique. Therefore, for any vertex $u \notin \{v_0, v_1,...,v_k\} $ there exists $v_i \in \{v_0, v_1,...,v_k\} $ such that $\ecc(u) > \ecc(v_i)$. Hence, the vertices of $T$ with minimum eccentricity, which are the centers of $T$, lie on the path $(v_0,v_1,...,v_k)$. $\blacksquare$
\par
\vspace{5mm}

\textbf{Exercise 2. (a)} Consider the cycle graph $C_n$ for some $n\geq 2$. Its vertices are $1,2,...,n$, and its edges are $12, 23,...,(n-1)n,n1$. Find the number of spanning trees of $C_n$. \par

There are $n$ spanning trees of $C_n$.\par
\textbf{Proof:} Observe that a tree is formed by removing any edge from the cycle graph $C_n$ and there are $n$ edges that can be removed giving $n$ trees. Each of these trees is a spanning tree, since removing a single edge from a cycle preserves connectedness. Now, there are no additional spanning trees of $C_n$ since removing any edge from a tree makes it disconnected. Note that all spanning trees of $C_n$ are isomorphic and that in the case $n=2$ the two spanning trees differ only in the identities of their edge. $\blacksquare$ \par

\textbf{(b)} Consider the directed graph $\vec{C}_n$ for some $n\geq2$. It is a digraph with vertices $1,2,...,n$ and arcs $12,23,...,(n-1)n,n1$. Find the number of spanning trees of $\vec{C}_n$ with root 1.\par 

There is a unique spanning tree of $\vec{C}_n$ with root 1.\par 
\textbf{Proof:} As above, spanning trees of $\vec{C}_n$ are constructed by removing a single edge of $\vec{C}_n$. If such a spanning tree is to have root 1, the edge 12 must be removed, so there is only one way to construct such a tree. $\blacksquare$

\textbf{(c)} Fix $m \geq 1$. Let $G$ be the simple graph with $3m+2$ vertices $$a,b,x_1,x_2,...,x_m,y_1,y_2,...,y_m,z_1,z_2,...,z_m$$ and the following $3m+3$ edges: 
\begin{eqnarray*}
ax_1, ay_1, az_1, \\
x_ix_{i+1}, y_iy_{i+1}, z_iz_{i+1} & \quad \textrm{for all} \ i \in \{1,2,...,m-1\}, \\
x_mb, y_mb, z_mb.
\end{eqnarray*}
Compute the number of spanning trees of $G$.
\par
To begin, notice that $G$ has three cycles, so we construct spanning trees of $G$ by removing edges so as to disrupt all of these cycles while maintaining the connectedness of $G$. Also note that every pair of cycles shares one of the paths from $a$ to $b$. Thus, removing a single edge from two of the three paths from $a$ to $b$ gives us a spanning tree, since removing two edges from the same path from $a$ to $b$ gives a disconnected graph and removing only one edge leaves a cycle intact. Therefore, the number of spanning trees of $G$ can be calculated by finding the number of ways to choose the two paths from which we remove an edge and multiplying by the number of pairs of edges (one in each path) we can remove. Clearly, there are 3 ways to choose two paths given a set of three. Now, each path has $m+1$ edges, so there are $(m+1)^2$ pairs of edges we can remove to obtain a spanning tree for any pair of paths. Hence, $G$ has $3(m+1)^2$ spanning trees.
\vspace{5mm}

\begin{comment}
\textbf{Exercise 3.} If $G$ is a multigraph, then $\conn G$ shall denote the number of connected components of $G$. (Note that this is 0 when $G$ has no vertices, and 1 if $G$ is connected.)\par
Let $(V,H,\phi)$ be a multigraph. Let $E$ and $F$ be two subsets of $H$.\par
\textbf{(a)} Prove that 
\begin{eqnarray*}
\conn(V,E,\phi \vert_E)+\conn(V, F, \phi \vert_F) 
\leq \\ \conn(V,E\cup F,\phi \vert_{E \cup F})+\conn(V,E\cap F, \phi \vert_{E\cap F}).
\end{eqnarray*}

\par

\textbf{(b)} Give an example where the inequality does not become an equality.
\par
\end{comment}

\textbf{Exercise 4.} Let $T$ be a tree with more than one vertex. Let $L$ be the set of leaves of $T$. Prove that it is possible to add $|L|-1$ new edges to $T$ in such a way that the resulting multigraph has a Hamiltonian cycle.\par
\par

\textbf{Proof:} To begin, let $|L|=k$ and select two leaves, $\ell_1, \ell_k \in L$. There exists a unique path $P_1=(\ell_k, v_{1_1}, v_{2_1},...,v_{n_1}, \ell_1)$ from $\ell_k$ to $\ell_1$, and it includes none of the other leaves of $T$. We must add at most $k-1$ edges to $T$ to construct a cycle that includes all vertices of $T$. We do this in the following way: 
\begin{enumerate}
    \item Select a leaf $\ell_2$, add the edge $\ell_1 \ell_2$ to $T$.
    \item Find a path (possibly of length 0) from $\ell_2$ to a vertex that has a neighbor that lies on $P_1$ but shares no vertices with the latter. Denote this path $P_2=(\ell_2,v_{1_2},v_{2_2}, ... ,v_{n_2})$. (We use the subscript on the subscript to note the leaf at which this path originated and to distinguish the length of this path from future paths of this form, since they will not in general be of the same length.)
    \item Select another leaf $\ell_3$. Add the edge $v_{n_2}\ell_3$ to $T$.
    \item Find a path (possibly of length 0) from $\ell_3$ to a vertex that has a neighbor on $P_1$ or $P_2$, but shares no vertices with these paths. Call it $P_3=(\ell_3, v_{1_3}, v_{2_3},...,v_{n_3})$.
    \item Select a leaf $\ell_4$. Add the edge $v_{n_3}\ell_4$ to $T$.
    \item Continue this process until the edge $v_{n_{k-1}}\ell_k$ has been added to $T$.
\end{enumerate}
We have thus added $k-1$ edges, and we have the Hamiltonian cycle given by $(P_1,P_2,...,P_k, \ell_k)$, since we have added precisely the edges that connect the ends of these paths. $\blacksquare$ \par
\textbf{Example:} $T$ and its edges are in black, edges we construct are in red. \\ 
\begin{center}
\begin{tikzpicture}
 
\filldraw (0,0) circle (2pt) node [below] {$v_{1_1}$};
\filldraw (1,0) circle (2pt) node [below] {$\ell_1$};
\filldraw (-1,0) circle (2pt) node [below] {$\ell_k=\ell_7$};
\filldraw (0,1) circle (2pt) node [right] {$v_{4_2}=v_{n_2}$};
\filldraw (0,2) circle (2pt) node [right] {$v_{3_2}$};
\filldraw (0,3) circle (2pt) node [left] {$v_{2_2}$};
\filldraw (1,3) circle (2pt) node [below] {$v_{1_2}$};
\filldraw (2,3) circle (2pt) node [right] {$\ell_2$};
\filldraw (-1,2) circle (2pt) node [below] {$v_{2_5}=v_{n_5}$};
\filldraw (-2,2.5) circle (2pt) node [above] {$v_{1_5}$};
\filldraw (-2,1.5) circle (2pt) node [below] {$v_{1_6}=v_{n_6}$};
\filldraw (-3,1.5) circle (2pt) node [left] {$\ell_6$};
\filldraw (0.5,4) circle (2pt) node [above] {$\ell_3=v_{n_3}$};
\filldraw (-.5,4) circle (2pt) node [below] {$v_{1_4}=v_{n_4}$};
\filldraw (-1.5,4) circle (2pt) node [left] {$\ell_4$};
\filldraw (-3, 2.5) circle (2pt) node [left] {$\ell_5$};

\draw (-1,0) -- (1,0);
\draw (0,0) -- (0,3);
\draw (-1,2) -- (0,2);
\draw (-2,1.5) -- (-1,2);
\draw (-3,1.5) -- (-2,1.5);
\draw (-2,2.5) -- (-1,2);
\draw (0,3) -- (2,3);
\draw (0,3) -- (.5,4);
\draw (0,3) -- (-.5,4);
\draw (-.5,4) -- (-1.5,4);
\draw (-3,2.5) -- (-2,2.5);

\draw [red] (1,0) -- (2,3);
\draw [red] (0,1) -- (.5, 4);
\draw [red] (.5,4) .. controls (-.5,4.4) .. (-1.5,4);
\draw [red] (-.5, 4) -- (-3,2.5);
\draw [red] (-1, 2) -- (-3, 1.5);
\draw [red] (-2, 1.5) -- (-1,0);

 
\end{tikzpicture}
\end{center}
\vspace{5mm}

\textbf{Exercise 5.} Let $a$, $b$, and $c$ be three vertices of a strongly connected digraph $G=(V,A)$ such that $|V| \geq 4$. \par

\textbf{(a)} Prove that $d(a,b)+d(b,c)+d(c,a)\leq 3|V|-4$.\par
\textbf{Proof:} Choose a shortest path from $a$ to $b$, a shortest path from $b$ to $c$, and a shortest path from $c$ to $a$.

Observe that the greatest possible distance between any two vertices is $|V|-1$, in which case the path between these two vertices passes through every vertex. Then, we easily conclude that $d(a,b)+d(b,c)+d(c,a)\leq 3|V|-3$. Now suppose the case of equality. Then, the vertex $c$ lies on the shortest path from $a$ to $b$, the vertex $a$ lies on the shortest path from $b$ to $c$, and the vertex $b$ lies on the shortest path from $c$ to $a$. Now, suppose there exists a vertex $v$ between $c$ and $b$ on the path from $a$ to $b$. Then the path from $b$ to $c$ must visit $v$. However, if the path from $b$ to $c$ visits $v$ before visiting $a$, then we have a path from $c$ to $a$ which does not visit $b$, a contradiction. Also, if the path from $b$ to $c$ visits $v$ after visiting $a$, then we have a shorter path from $a$ to $b$, since there is an edge that allows us to skip $c$, another contradiction. Therefore, $c$ is the penultimate vertex on the path from $a$ to $b$. Hence, $cb \in A$. By symmetry, the same argument shows that $ac \in A$. Hence, $\left(c,b,a\right)$ is a path in $G$; therefore, $d\left(c, a\right) \leq 2$. By symmetry, we similarly get $d\left(b, c\right) \leq 2$ and $d\left(a, b\right) \leq 2$. Adding up, we find $d\left(a, b\right) + d\left(b, c\right) + d\left(c, a\right) \leq 2 + 2 + 2 = 6 < 3|V| - 3$ (since $|V| \geq 4 > 3$), which contradicts $d(a,b)+d(b,c)+d(c,a) = 3|V| - 3$. Therefore, $d(a,b)+d(b,c)+d(c,a)< 3|V|-3$ or equivalently, $d(a,b)+d(b,c)+d(c,a)\leq 3|V|-4$. $\blacksquare$

\textbf{(b)} For each $n\geq 5$, construct an example in which $|V|=n$ and $d(a,b)+d(b,c)+d(c,a)=3|V|-4$.
\begin{center}
\begin{tikzpicture}

\filldraw (0,0) circle (2pt) node [above] {$a$};
\filldraw (0,-1) circle (2pt) node [below] {$b$};
\filldraw (0,-2) circle (2pt) node [below] {$c$};
\filldraw (1,0) circle (2pt);
\filldraw (-1,0) circle (2pt);
\filldraw (0,1) circle (0pt) node {$n-5$ vertices};

\draw [->](-1,0) -- (-.1,-.9);
\draw [->](0,0) -- (.9,0);
\draw [->](0,-1) -- (0,-.1);
\draw [->](-1,0) -- (-.1,-1.8);
\draw [->](0,-2) -- (.9,-.2);
\draw [->](1,0) -- (.5,.8);
\draw [->](-.5,.8) -- (-.9,.2);



\end{tikzpicture}
\end{center}
\par
\vspace{5mm}

\textbf{Exercise 6.} We have learned that a simple graph $G$ (or multigraph $G$) has a proper 2-coloring if and only if all cycles of $G$ have even length. \par

\textbf{(a)} Is it true that if all cycles of a simple graph $G$ (or multigraph $G$) have length divisible by 3, then $G$ has a proper 3-coloring? \par

If all cycles of $G$ have length divisible by 3, then $G$ has a proper 3-coloring.\par

[No solution to this part submitted.]

\begin{comment}
% The following has been commented out because the algorithm is wrong.

\textbf{Proof:} Suppose all cycles of $G$ have length divisible by 3. Without loss of generality, we assume $G$ is connected, since if it is not we simply must find a proper 3-coloring for each connected component. Then, we claim that the following algorithm gives a proper 3-coloring of $G$: 
\begin{enumerate}
    \item Select a vertex $v$. Assign it the color 1.
    \item Color all neighbors of $v$. Give as many as possible the color 2, and give those remaining vertices that are adjacent to both $v$ and to one of its neighbors the color 3.
    \item Color all vertices that are a distance of 2 from $v$ the color 1.
    \item Color all vertices that are a distance of 3 from $v$ the color 2.
    \item Color the vertices that are a distance of 4 from $v$. If two such vertices are adjacent, color one of them 1 and the other 3. Otherwise, give each of these vertices the color 1. 
    \item Color the vertices that are a distance of 5 from $v$ the color 2.
    \item Color the vertices that are a distance of 6 from $v$ the color 1.
    \item Continue in this fashion until all of $G$ is colored. To all $u$ such that $d(u,v) \equiv 0 \ (\mo \ 6) $, assign the color 1. To all $u$ such that $d(u,v) \equiv 1\ (\mo\ 6)$, assign the color 2 if $u$ has no neighbors the same distance from $v$, and to all pairs of equidistant neighbors, assign one the color 2 and its neighbor the color 3. To all $u$ such that $d(u,v) \equiv 2 \ (\mo \ 6)$, assign the color 1. To all $u$ such that $d(u,v) \equiv 3 \ (\mo \ 6)$, assign the color 2. To all $u$ such that $d(u,v) \equiv 4 \ (\mo \ 6)$, assign the color 1 if $u$ has no neighbors the same distance from $v$, and to all pairs of equidistant neighbors, assign one the color 1 and its neighbor the color 3. To all $u$ such that $d(u,v) \equiv 5 \ (\mo \ 6 ) $ assign the color 2.
\end{enumerate}
It is easy to see why this algorithm works. In the second step, any neighbor of $v$ can be adjacent to at most one other neighbor of $v$, otherwise we have a cycle of length 4, a contradiction. This guarantees that the colors 2 and 3 are enough to color all neighbors of $v$, given $v$ has color 1. Similarly, if $d(u,v) \equiv 1 \ \textrm{or}\ 4 \ (\mo \ 6)$, $u$ can have at most one neighbor that is the same distance from $v$ as $u$. Now, at any given step, the vertices we wish to color have at most neighbors of two different colors (among their neighbors that have been colored already) since they cannot be adjacent to vertices that are 2 or more edges nearer to $v$ than they are, or else we have a contradiction. In all the steps in the algorithm where we claim to use only one color, we can do so because we know that none of the vertices that distance from $v$ can be adjacent without creating a cycle of length not divisible by 3. If $u_1, u_2$ are equidistant from $v$, and $d(u_1,v) \equiv 0, 2, 3,\  \textrm{or} \ 5 \ (\mo \ 6)$, then the edge $u_1u_2$ would create a cycle of length $\equiv 1, 5, 1,\ \textrm{or} \ 5 \ (\mo \ 6)$ respectively, none of which are divisible by 3. Finally, since $G$ is connected and finite, this algorithm colors all of $G$. Therefore, the above algorithm gives a proper 3-coloring of a graph in which all cycles are divisible by 3. $\blacksquare$ \par
\end{comment}
 
\textbf{(b)} Is it true that if a simple graph $G$ has a proper 3-coloring, then all cycles of $G$ have length divisible by 3? \par

It is not true that if $G$ has a proper 3-coloring, then all cycles of $G$ have length divisible by 3. \par
\textbf{Counterexample:} Consider the cycle graph $C_4$. It has a proper 3-coloring since it has a proper 2-coloring (which is trivially a proper 3-coloring). However, it has a cycle of length 4, not divisible by 3. Any graph whose cycles are all of even length and has a cycle of length $\equiv 2$ or $\equiv 4 \ ( \mo \ 6)$ is a counterexample. Another counterexample is the Petersen graph, which has cycles of length 5 and a proper 3-coloring.
\vspace{5mm}

\begin{comment}
\textbf{Exercise 7.} In class, we have proven the following fact: If $G=(V,E)$ if a simple graph, then $G$ has an independent set of size $\geq \frac{n}{1+d}$ where $n=|V|$ and $\displaystyle d=\frac{1}{n} \sum_{v\in V} \deg v.$ Use this to prove Tur\'an's theorem. 
\par
\vspace{5mm}
\end{comment}

\begin{comment}
\textbf{Exercise 8. (Extra credit)} For any $n$ numbers $x_1,x_2, ... , x_n$ we define $v(x_1,...,x_n)$ to be the number $$\prod_{1 \leq i <j \leq n} (x_j-x_i)= \det \left ( \left ( x_j^{i-1} \right )_{1\leq i \leq n, \ 1\leq j \leq n} \right ).$$
\par
Let $x_1, x_2, ..., x_n$ be $n$ numbers. Let $t$ be a further number. Prove at least one of the following facts combinatorially (i.e. without using any properties of the determinant other than its definition as a sum over permutations):
\par
\textbf{(a)} We have $$\sum_{k=1}^n v(x_1,x_2,...,x_{k-1},x_k+t,x_{k+1},...,x_n)=nv(x_1,x_2,...,x_n).$$
\par
\textbf{(b)} For each $m\in \{0,1,...,n-1\},$ we have $$\sum_{k=1}^n x_k^m(x_1,x_2,...,x_{k-1},t,x_{k+1},...,x_n)=t^mv(x_1,x_2,...,x_n).$$
\par
\textbf{(c)} We have $$\sum_{k=1}^n x_k v(x_1,x_2,...,x_{k-1},x_k+t,x_{k+1},...,x_n)$$
$$=\left ( \begin{pmatrix} n \\ 2 \end{pmatrix} t+ \sum_{k=1}^nx_k\right) v(x_1,...,x_n).$$
\end{comment}

\end{document}