Statistics on midterm 1:

Average score for exercise 1 (out of 10) is 9.8.
Average score for exercise 2 (out of 10) is 8.1.
Average score for exercise 3 (out of 10) is 8.25.
Average score for exercise 4 (out of 10) is 8.7.
Average score for exercise 5 (out of 10) is 1.85.
Average score for exercise 6 (out of 10) is 9.4.
Average score for exercise 7 (out of 10) is 4.55.
Average score for exercise 8 (out of 10) is 3.25.

Maximum total score (out of 100) is 97.5.
Average total score (out of 100) is 67.375.