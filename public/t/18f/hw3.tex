% Like most advanced LaTeX files, this one begins with a lot of
% boilerplate. You don't need to understand (or even read) most of it.
% All you need to do is fill in your name, UMN ID, email address,
% and the number of the pset. (Search for "METADATA" to find the place
% for this.) Then, you can go straight to the "EXERCISE 1"
% section and start writing your solutions.
% The "VARIOUS USEFUL COMMANDS" section is probably worth taking a
% look at at some point.

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw graphs.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).
\usepackage{ytableau}

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\rev}{\operatorname{rev}} % reversal of a walk
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
% $\floor{...}$ compiles to |_..._| (floor function).
\newcommand{\verts}[1]{\operatorname{V}\left( #1 \right)}
% $\verts{...}$ compiles to V(...) (vertex set of a graph/digraph).
\newcommand{\edges}[1]{\operatorname{E}\left( #1 \right)}
% $\edges{...}$ compiles to E(...) (edge set of a graph).
\newcommand{\arcs}[1]{\operatorname{A}\left( #1 \right)}
% $\arcs{...}$ compiles to A(...) (arc set of a digraph).
\newcommand{\lf}[2]{#1^{\underline{#2}}}
% $\lf{...1}{...2}$ compiles to $...1^{\underline{...2}}$.
% This is a notation for the falling factorial.
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\newcommand{\nnn}{\nonumber\\} % Don't number this line in an "align" environment, and move on to the next line.
\newcommand{\sslash}{\mathbin{/\mkern-6mu/}} % Quotient of division with remainder.

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{Darij Grinberg} % ENTER YOUR NAME HERE
\newcommand{\myid}{00000000} % ENTER YOUR UMN ID HERE
\newcommand{\mymail}{dgrinber@umn.edu} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{3} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to homework set \#\psetnumber} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname, \myid} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{University of Minnesota, School of Mathematics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 5705: Enumerative Combinatorics, \\
Fall 2018:
Homework \psetnumber\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}

\begin{document}

\maketitle % Print the title

\begin{center} % Delete this if you want to save space!
{\large due date: \textbf{Wednesday, 10 October 2018} at the beginning of class, \\
or before that by email or canvas.

Please solve \textbf{at most 4 of the 6 exercises}!}
\end{center}

%----------------------------------------------------------------------------------------
%	EXERCISE 1
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 1}

\subsection{Problem}

Let $A$ and $B$ be two sets, and let $f : A \to B$ be a map.
A \textit{left inverse} of $f$ shall mean
a map $g : B \to A$ such that $g \circ f = \id_A$.
We say that $f$ is \textit{left-invertible} if and only
if a left inverse of $f$ exists.
(It is usually not unique.)

Assume that the sets $A$ and $B$ are finite.

\begin{enumerate}

\item[\textbf{(a)}]
If the set $A$ is nonempty, then
prove that $f$ is left-invertible if and only if
$f$ is injective.\footnote{This holds even when $A$ and $B$
are infinite.
Feel free to prove this if you wish.}

\item[\textbf{(b)}]
Assume that $f$ is injective.
Prove that the number of left inverses of $f$ is
$\abs{A}^{\abs{B} - \abs{A}}$.

\end{enumerate}

\subsection{Solution}

[...]

%----------------------------------------------------------------------------------------
%	EXERCISE 2
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 2}

\subsection{Problem}

Let $A$ and $B$ be two sets, and let $f : A \to B$ be a map.
A \textit{right inverse} of $f$ shall mean
a map $h : B \to A$ such that $f \circ h = \id_B$.
We say that $f$ is \textit{right-invertible} if and only
if a right inverse of $f$ exists.
(It is usually not unique.)

Assume that the sets $A$ and $B$ are finite.

\begin{enumerate}

\item[\textbf{(a)}]
Prove that $f$ is right-invertible if and only if
$f$ is surjective.\footnote{This holds even when $A$ and $B$
are infinite, if you assume the axiom of choice.
But this is not the subject of our class.}

\item[\textbf{(b)}]
Prove that the number of right inverses of $f$ is
$\prod_{b \in B} \abs{f^{-1}\tup{b}}$.
Here, $f^{-1}\tup{b}$ denotes the \textbf{set} of all
$a \in A$ satisfying $f\tup{a} = b$.

\end{enumerate}

\subsection{Solution}

[...]

%----------------------------------------------------------------------------------------
%	EXERCISE 3
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 3}

\subsection{Problem}

\begin{enumerate}

\item[\textbf{(a)}]
Prove that
\[
\dbinom{-1/2}{n} = \tup{\dfrac{-1}{4}}^n \dbinom{2n}{n}
\qquad \text{for each $n \in \NN$.}
\]

\item[\textbf{(b)}]
Prove that
\[
\sum_{k=0}^n \dbinom{2k}{k} \dbinom{2\tup{n-k}}{n-k} = 4^n
\qquad \text{for each $n \in \NN$.}
\]

\end{enumerate}

[\textbf{Hint:} Part \textbf{(b)} is highly difficult to
prove combinatorially.
Try using part \textbf{(a)} instead.]

\subsection{Solution}

[...]

%----------------------------------------------------------------------------------------
%	EXERCISE 4
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 4}

\subsection{Problem}

Recall once again the
\textit{\href{https://en.wikipedia.org/wiki/Fibonacci_number}{Fibonacci
sequence}} $\tup{f_0, f_1, f_2, \ldots}$, which is defined
recursively by $f_0 = 0$, $f_1 = 1$, and
\begin{equation}
f_n = f_{n-1} + f_{n-2} \qquad \text{for all } n \geq 2 .
\label{eq.fib.rec}
\end{equation}
It is easy to see that $f_1, f_2, f_3, \ldots$ are positive
integers (which will allow us to divide by them soon).

For any $n \in \NN$ and $k \in \ZZ$, define the
rational number $\dbinom{n}{k}_F$ (a slight variation on
the corresponding binomial coefficient) by
\[
\dbinom{n}{k}_F
=
\begin{cases}
\dfrac{f_n f_{n-1} \cdots f_{n-k+1}}{f_k f_{k-1} \cdots f_1},
   & \text{ if } n \geq k \geq 0; \\
0, & \text{ otherwise} .
\end{cases}
\]

\begin{enumerate}

\item[\textbf{(a)}]
Let $n$ be a positive integer, and let
$k \in \NN$ be such that $n \geq k$.
Prove that
\[
\dbinom{n}{k}_F = f_{k+1} \dbinom{n-1}{k}_F + f_{n-k-1} \dbinom{n-1}{k-1}_F ,
\]
where we set $f_{-1} = 1$.

\item[\textbf{(b)}]
Prove that $\dbinom{n}{k}_F \in \NN$ for any
$n \in \NN$ and $k \in \NN$.

\end{enumerate}

\subsection{Solution}

[...]

%----------------------------------------------------------------------------------------
%	EXERCISE 5
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 5}

\subsection{Problem}

Let $j \in \NN$, $r \in \RR$ and $s \in \RR$.
Prove that
\[
\sum_{k=0}^j \tup{-1}^k \dbinom{j}{k} \dbinom{r-sk}{j} = s^j .
\]

[\textbf{Hint:}
First, argue that it suffices to prove this only for
$s \in \NN$ and $r \in \ZZ$ satisfying $r \geq sj$.
Next, consider $r$ distinct stones,
$sj$ of which are arranged in $j$ piles containing
$s$ stones each, while the remaining $r-sj$ stones
are forming a separate heap.
How many ways are there to pick $j$ of these $r$
stones such that each of the $j$ piles loses at
least one stone?]

\subsection{Solution}

[...]

%----------------------------------------------------------------------------------------
%	EXERCISE 6
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 6}

\subsection{Problem}

Let $n \in \NN$.
The summation sign $\sum_{I \subseteq \ive{n}}$ shall always
stand for a sum over all subsets $I$ of $\ive{n}$.
(This sum has $2^n$ addends.)

Let $A_1, A_2, \ldots, A_n$ be $n$ numbers or
polynomials or square matrices of the same size.
(Allowing matrices means that $A_i A_j$ is not necessarily
equal to $A_j A_i$, so beware of using the binomial
formula or similar identities!)

\begin{enumerate}

\item[\textbf{(a)}]
Show that
\[
\sum_{I \subseteq \ive{n}}
 \tup{-1}^{n-\abs{I}} \tup{\sum_{i \in I} A_i}^m
 = \sum_{\substack{\tup{i_1, i_2, \ldots, i_m} \in \ive{n}^m; \\ \set{i_1, i_2, \ldots, i_m} = \ive{n}}}
   A_{i_1} A_{i_2} \cdots A_{i_m}
 \qquad \text{for all $m \in \NN$.}
\]

(Example: If $n = 2$ and $m = 3$, then this is saying
\begin{align*}
\tup{A+B}^3 - A^3 - B^3 + 0^3
&= AAB + ABA + ABB + BAA + BAB + BBA ,
\end{align*}
where we have renamed $A_1$ and $A_2$ as $A$ and $B$.)

\item[\textbf{(b)}]
Show that
\[
\sum_{I \subseteq \ive{n}}
 \tup{-1}^{n-\abs{I}} \tup{\sum_{i \in I} A_i}^m
 = 0
 \qquad \text{for all $m \in \NN$ satisfying $m < n$.}
\]

(Example: If $n = 3$ and $m = 2$, then this is saying
\begin{align*}
\tup{A+B+C}^2 - \tup{A+B}^2 - \tup{A+C}^2 - \tup{B+C}^2 + A^2 + B^2 + C^2 - 0^2
&= 0 ,
\end{align*}
where we have renamed $A_1, A_2, A_3$ as $A, B, C$.)

\item[\textbf{(c)}]
Show that
\[
\sum_{I \subseteq \ive{n}}
 \tup{-1}^{n-\abs{I}} \tup{\sum_{i \in I} A_i}^n
 = \sum_{\sigma \in S_n} A_{\sigma\tup{1}} A_{\sigma\tup{2}} \cdots A_{\sigma\tup{n}} ,
\]
where $S_n$ stands for the set of all ($n!$) permutations of $\ive{n}$.

(Example: If $n = 3$, then this is saying
\begin{align*}
& \tup{A+B+C}^3 - \tup{A+B}^3 - \tup{A+C}^3 - \tup{B+C}^3 + A^3 + B^3 + C^3 - 0^3 \\
&= ABC + ACB + BAC + BCA + CAB + CBA ,
\end{align*}
where we have renamed $A_1, A_2, A_3$ as $A, B, C$.)

\end{enumerate}

[\textbf{Hint:} You can use the \textit{product rule}, which
says the following:

\begin{proposition}[Product rule]
Let $m$ and $n$ be two nonnegative integers.
Let $P_{u,v}$, for all $u \in \ive{m}$ and $v \in \ive{n}$,
be numbers or polynomials or square matrices of the same
size. Then,
\begin{align*}
&
\tup{P_{1,1} + P_{1,2} + \cdots + P_{1,n}}
\tup{P_{2,1} + P_{2,2} + \cdots + P_{2,n}}
\cdots
\tup{P_{m,1} + P_{m,2} + \cdots + P_{m,n}}
\\
&= \sum_{\tup{i_1, i_2, \ldots, i_m} \in \ive{n}^m}
    P_{1, i_1} P_{2, i_2} \cdots P_{m, i_m} .
\end{align*}
(This frightening formula merely says that a product of
sums can be expanded, and the result will be a sum
of products, with each of the latter products being
obtained by multiplying together one addend from each sum.
You have probably used this sometime already.)
\end{proposition}
]

\subsection{Solution}

[...]

\end{document}

