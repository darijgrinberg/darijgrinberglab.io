% Like most advanced LaTeX files, this one begins with a lot of
% boilerplate. You don't need to understand (or even read) most of it.
% All you need to do is fill in your name, UMN ID, email address,
% and the number of the pset. (Search for "METADATA" to find the place
% for this.) Then, you can go straight to the "EXERCISE 1"
% section and start writing your solutions.
% The "VARIOUS USEFUL COMMANDS" section is probably worth taking a
% look at at some point.

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw graphs.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).
\usepackage{ytableau}

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\Des}{\operatorname{Des}} % descent set of a permutation
\newcommand{\rev}{\operatorname{rev}} % reversal of a walk
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
% $\floor{...}$ compiles to |_..._| (floor function).
\newcommand{\verts}[1]{\operatorname{V}\left( #1 \right)}
% $\verts{...}$ compiles to V(...) (vertex set of a graph/digraph).
\newcommand{\edges}[1]{\operatorname{E}\left( #1 \right)}
% $\edges{...}$ compiles to E(...) (edge set of a graph).
\newcommand{\arcs}[1]{\operatorname{A}\left( #1 \right)}
% $\arcs{...}$ compiles to A(...) (arc set of a digraph).
\newcommand{\lf}[2]{#1^{\underline{#2}}}
% $\lf{...1}{...2}$ compiles to $...1^{\underline{...2}}$.
% This is a notation for the falling factorial.
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\newcommand{\nnn}{\nonumber\\} % Don't number this line in an "align" environment, and move on to the next line.
\newcommand{\sslash}{\mathbin{/\mkern-6mu/}} % Quotient of division with remainder.

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{Jacob Elafandi (edited by Darij Grinberg)} % ENTER YOUR NAME HERE
\newcommand{\myid}{} % ENTER YOUR UMN ID HERE
\newcommand{\mymail}{} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{4} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to homework set \#\psetnumber} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname, \myid} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{University of Minnesota, School of Mathematics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 5705: Enumerative Combinatorics, \\
Fall 2018:
Homework \psetnumber\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}
\date{October 28, 2018}

\begin{document}

\maketitle % Print the title
%----------------------------------------------------------------------------------------
%	EXERCISE 1
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 1}

\subsection*{Problem}

Let $n \in \NN$ and $\sigma \in S_n$.
Let $i$ and $j$ be two elements of $\ive{n}$ such that
$i<j$ and $\sigma \tup{i} > \sigma \tup{j}$.
Let $Q$ be the set of all $k \in \set{ i+1,i+2,\ldots,j-1 }$
satisfying $\sigma \tup{i} > \sigma \tup{k} > \sigma \tup{j}$.
Prove that
\[
\ell \tup{ \sigma\circ t_{i,j} }
= \ell \tup{\sigma} - 2 \abs{Q} - 1 .
\]

\subsection*{Remark}

This exercise implies that, in particular,
$\ell \tup{ \sigma\circ t_{i,j} } < \ell \tup{\sigma}$;
this answers the question on page 213 of the
\href{http://www.cip.ifi.lmu.de/~grinberg/t/18f/5705-2018-10-22.pdf}{notes from class (2018-10-22)}.

\subsection*{Solution}

For fixed $i,j \in [n]$ such that $i<j$, the inversions of $\pi \in S_n$ can be divided into ten disjoint categories (whose sizes sum to $\ell(\pi)$):
\begin{itemize}
    \item $A(\pi) = \{(k,y) \in [n]^2 : k<y \text{ and } \{k,y\} \cap \{i,j\} = \emptyset\}$.
    \item $B(\pi) = \{(k,i) \in [n]^2 : k<i \text{ and } \pi(k) > \pi(i) \}$.
    \item $C(\pi) = \{(i,k) \in [n]^2 : i<k<j \text{ and } \pi(i) > \pi(k) > \pi(j)\}$.
    \item $D(\pi) = \{(i,k) \in [n]^2 : i<k<j \text{ and } \pi(i) > \pi(k) \text{ and } \pi(j) > \pi(k)\}$.
    \item $E(\pi) = \{(i,k) \in [n]^2 : k>j \text{ and } \pi(i) > \pi(k)\}$.
    \item $F(\pi) = \{(k,j) \in [n]^2 : k<i \text{ and } \pi(k) > \pi(j) \}$.
    \item $G(\pi) = \{(k,j) \in [n]^2 : i<k<j \text{ and } \pi(k) > \pi(i) \text{ and } \pi(k) > \pi(j)\}$.
    \item $H(\pi) = \{(k,j) \in [n]^2 : i<k<j \text{ and } \pi(i) > \pi(k) > \pi(j)\}$.
    \item $I(\pi) = \{(j,k) \in [n]^2 : k>j \text{ and } \pi(j) > \pi(k)\}$.
    \item $J(\pi) = \{(i,j) : \pi(i) > \pi(j)\}$.
\end{itemize}
Note that $(\sigma \circ t_{i,j})(i) = \sigma(j)$ and $(\sigma \circ t_{i,j})(j) = \sigma(i)$.
Therefore, since $i<j$ and $\sigma(i) > \sigma(j)$, we see that the inversions of $\sigma \circ t_{i,j}$ can be divided into the following disjoint categories (whose sizes sum to $\ell(\pi \circ t_{i,j})$):
\begin{itemize}
    \item $A(\sigma \circ t_{i,j}) = A(\sigma)$.
    \item $\abs{B(\sigma \circ t_{i,j})} = \abs{F(\sigma)}$ (via the bijection $B(\sigma \circ t_{i,j}) \to F(\sigma)$ sending each $(k,i)$ to $(k,j)$);
          \\
          $\abs{F(\sigma \circ t_{i,j})} = \abs{B(\sigma)}$ (via the bijection $B(\sigma \circ t_{i,j}) \to F(\sigma)$ sending each $(k,j)$ to $(k,i)$).
    \item $D(\sigma \circ t_{i,j}) = D(\sigma)$.
    \item $\abs{E(\sigma \circ t_{i,j})} = \abs{I(\sigma)}$ (via the bijection $E(\sigma \circ t_{i,j}) \to I(\sigma)$ sending each $(i,k)$ to $(j,k)$);
          \\
          $\abs{I(\sigma \circ t_{i,j})} = \abs{E(\sigma)}$ (via the bijection $I(\sigma \circ t_{i,j}) \to E(\sigma)$ sending each $(j,k)$ to $(i,k)$).
    \item $G(\sigma \circ t_{i,j}) = G(\sigma)$.
    \item $C(\sigma \circ t_{i,j}) = H(\sigma \circ t_{i,j}) = J(\sigma \circ t_{i,j}) = \emptyset$.
\end{itemize}
Therefore $\ell(\sigma \circ t_{i,j}) = \ell(\sigma) - |C(\sigma)| - |H(\sigma)| - |J(\sigma)|$. By the definitions of $C$, $H$, $J$, and $Q$, one has that $|C(\sigma)| = |H(\sigma)| = |Q|$ and $|J(\sigma)| = 1$. The desired equality follows.

%----------------------------------------------------------------------------------------
%	EXERCISE 2
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 2}

\subsection*{Problem}

Let $n \in \NN$ and $\pi \in S_n$.

\begin{enumerate}
\item[\textbf{(a)}] Prove that
\[
\sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} > \pi\tup{j}}}
\tup{ \pi\tup{j} - \pi\tup{i} }
=
\sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} > \pi\tup{j}}}
\tup{ i - j }.
\]


\item[\textbf{(b)}] Prove that%
\[
\sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} < \pi\tup{j}}}
\tup{ \pi\tup{j} - \pi\tup{i} }
=
\sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} < \pi\tup{j}}}
\tup{ j - i }.
\]

\end{enumerate}

[\textbf{Hint:} Exercise 5.23 in \cite{detnotes}
says something about sums of the form appearing in part \textbf{(a)}.
(See also Nathaniel Gorski's solution of the same exercise in
\href{http://www.cip.ifi.lmu.de/~grinberg/t/18s/hw4s-gorski.pdf}{Spring 2018 Math 4707 homework set \#4}.)
You may want to use the result or the ideas.]

\subsection*{Solution}

The following solution was inspired by that of Nathaniel Gorski, as suggested in the hint.
For $i \in \ive{n}$, define the following subsets of $[n]$:
\begin{itemize}
    \item $U(i) = \{j \in [n]: j < i \text{ and } \pi(j) > \pi(i)\}$.
    \item $M(i) = \{j \in [n]: j < i \text{ and } \pi(j) < \pi(i)\}$.
    \item $L(i) = \{j \in [n]: j > i \text{ and } \pi(j) < \pi(i)\}$.
    \item $T(i) = \{j \in [n]: j > i \text{ and } \pi(j) > \pi(i)\}$.
\end{itemize}

\subsubsection*{Part (a)}
Note first that $U(i) \cup M(i) = \{j \in [n] : j < i\}$. Because $U(i)$ and $M(i)$ are disjoint, $|U(i)| + |M(i)| = |U(i) \cup M(i)| = i-1$.

Note also that $M(i) \cup L(i) = \{j \in [n] : \pi(j) < \pi(i)\}$. Because $\pi$ is a permutation of $[n]$, this union has size $\pi(i) - 1$. Because $M(i)$ and $L(i)$ are disjoint, $|M(i)| + |L(i)| = |M(i) \cup L(i)| = \pi(i)-1$.

Subtracting the second equality from the first yields $|U(i)| - |L(i)| = i - \pi(i)$. This enables the following string of computations:

\begin{align*}
    &~~\sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} > \pi\tup{j}}}
    \tup{ \pi\tup{j} - \pi\tup{i} }
    -
    \sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} > \pi\tup{j}}}
    \tup{ i - j }
    \\
    &=
    \sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} > \pi\tup{j}}}
    \tup{ \pi\tup{j} + j }
    -
    \sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} > \pi\tup{j}}}
    \tup{ \pi\tup{i} + i }
    \\
    &=
    \sum_{j = 1}^n \sum_{i \in U(j)}
    \tup{ \pi\tup{j} + j }
    -
    \sum_{i = 1}^n \sum_{j \in L(i)}
    \tup{ \pi\tup{i} + i }
    \\
    &=
    \sum_{j = 1}^n |U(j)|
    \tup{ \pi\tup{j} + j }
    -
    \sum_{i = 1}^n |L(i)|
    \tup{ \pi\tup{i} + i }
    \\
    &=
    \sum_{i=1}^n \tup{|U(i)| - |L(i)|} \tup{ \pi\tup{i} + i }
    \\
    &=
    \sum_{i=1}^n \tup{ i - \pi\tup{i} } \tup{ \pi\tup{i} + i }
    \\
    &=
    \sum_{i=1}^n i^2 - \sum_{i=1}^n \tup{\pi\tup{i}}^2
    \\
    &=
    (1^2 + 2^2 + \cdots + n^2) - (1^2 + 2^2 + \cdots + n^2)
    \\
    &= 0 .
\end{align*}
The desired equality follows.

\subsubsection*{Part (b)}
Note that $L(i) \cup T(i) = \{j \in [n]: j > i\}$. Because $L(i)$ and $T(i)$ are disjoint, $|L(i)| + |T(i)| = |L(i) \cup T(i)| = n-i$.

We know from part \textbf{(a)} that $|M(i)| + |L(i)| = \pi(i) - 1$. Subtracting the above equation from this one gives $|M(i)| - |T(i)| = \pi(i) + i - n - 1$. Therefore:
\begin{align*}
    &~~\sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} < \pi\tup{j}}}
    \tup{ \pi\tup{j} - \pi\tup{i} }
    -
    \sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} < \pi\tup{j}}}
    \tup{ j - i }
    \\
    &=
    \sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} < \pi\tup{j}}}
    \tup{ \pi\tup{j} - j }
    -
    \sum_{\substack{1\leq i<j\leq n;\\ \pi\tup{i} < \pi\tup{j}}}
    \tup{ \pi\tup{i} - i }
    \\
    &=
    \sum_{j=1}^n \sum_{i \in M(j)} (\pi(j)-j) - \sum_{i=1}^n \sum_{j \in T(i)} (\pi(i)-i)
    \\
    &=
    \sum_{j=1}^n |M(j)| (\pi(j)-j) - \sum_{i=1}^n |T(i)| (\pi(i)-i)
    \\
    &=
    \sum_{i=1}^n (|M(i)-|T(i)|) (\pi(i)-i)
    \\
    &=
    \sum_{i=1}^n (\pi(i)+i-n-1)(\pi(i)-i)
    \\
    &=
    \sum_{i=1}^n (\pi(i))^2 - \sum_{i=1}^n i^2 - (n-1) \left(\sum_{i=1}^n \pi(i) - \sum_{i=1}^n i \right)
    \\
    &=
    (1^2+\cdots+n^2) - (1^2+\cdots+n^2) - (n-1)\tup{(1 + \cdots + n) - (1 + \cdots + n)}
    \\
    &=
    0 .
\end{align*}
The desired equality follows.

%----------------------------------------------------------------------------------------
%	EXERCISE 3
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 3}

\subsection*{Problem}

Let $n \in \NN$.
For each $p \in \ZZ$, we let
\[
D_{n, p} = \set{ \sigma \in S_n \mid \sigma \text{ has exactly $p$ descents} } .
\]
(Recall that a \textit{descent} of a permutation $\sigma \in S_n$
denotes an element $k \in \ive{n-1}$ satisfying
$\sigma \tup{k} > \sigma \tup{k+1}$.)

Let $p \in \ZZ$.
Prove that $\abs{D_{n, p}} = \abs{D_{n, n-1-p}}$.

\subsection*{Solution}

Let $\sigma \in D_{n,p}$ and let $H=\{k \in [n-1] \mid \sigma(k) > \sigma(k+1)\}$ be the set of all descents of $\sigma$.
For all $k \in H$, we have $(w_0 \circ \sigma)(k) = n+1 - \sigma(k) < n+1-\sigma(k+1) = (w_0 \circ \sigma)(k+1)$, so $k$ is not a descent of $w_0 \circ \sigma$.
For all $k \in [n-1] \setminus H$, we have $(w_0 \circ \sigma)(k) = n+1 - \sigma(k) > n+1-\sigma(k+1) = (w_0 \circ \sigma)(k+1)$, so $k$ is a descent of $w_0 \circ \sigma$.
Thus $w_0 \circ \sigma$ has $n-1-|H| = n-1-p$ descents, so the map $f:D_{n,p} \to D_{n,n-1-p}$ defined by $f(\sigma) = w_0 \circ \sigma$ is well-defined.

For any $\sigma \in D_{n,p}$, we have $w_0 \circ f(\sigma) = w_0 \circ w_0 \circ \sigma = \sigma$. This implies that $f$ is injective, so $|D_{n,p}| \leq |D_{n,n-1-p}|$.

The same chain of reasoning, applied to $D_{n,p}$ from $D_{n,n-1-p}$, can be used to show that $|D_{n,n-1-p}| \leq |D_{n,p}|$. Therefore $|D_{n,p}| = |D_{n-1-p}|$.

%----------------------------------------------------------------------------------------
%	EXERCISE 7
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 7}

\subsection*{Problem}

Let $n \in \NN$ and $d \in \NN$.
An $n$-tuple $\tup{x_1, x_2, \ldots, x_n} \in \ive{d}^n$ is said to be
\textit{all-even} if each element of $\ive{d}$ occurs an even
number of times in this $n$-tuple (i.e., if for each $k \in \ive{d}$,
the number of all $i \in \ive{n}$ satisfying $x_i = k$ is even).
For example, the $4$-tuple $\tup{1,4,4,1}$ and the $6$-tuples
$\tup{1,3,3,5,1,5}$ and $\tup{2,4,2,4,3,3}$ are all-even, while
the $4$-tuples $\tup{1,2,2,4}$ and $\tup{2,4,6,4}$ are not.

Prove that the number of all all-even $n$-tuples
$\tup{x_1, x_2, \ldots, x_n} \in \ive{d}^{n}$ is
\[
\dfrac{1}{2^{d}}\sum_{k=0}^{d}\dbinom{d}{k}\left(  d-2k\right)  ^{n}.
\]


[\textbf{Hint:} Compute the sum
$\sum_{\tup{e_1, e_2, \ldots, e_d} \in \set{-1, 1}^d}
\tup{ e_1 + e_2 + \cdots + e_d }^n$ in two ways.
One way is to split it according to the
number of $i \in \ive{d}$ satisfying $e_i = -1$;
this is a number $k \in \set{0, 1, \ldots, d}$.
Another way is by using the product rule:
\[
\tup{ e_1 + e_2 + \cdots + e_d }^n
= \sum_{\tup{x_1, x_2, \ldots, x_n} \in \ive{d}^{n}}
e_{x_1} e_{x_2} \cdots e_{x_n}
\]
and then simplifying each sum
$\sum_{\tup{e_1, e_2, \ldots, e_d} \in \set{-1, 1}^d}
e_{x_1} e_{x_2} \cdots e_{x_n}$
using a form of destructive interference.
This is not unlike the number of 1-even $n$-tuples,
which we computed at the end of
\href{http://www.cip.ifi.lmu.de/~grinberg/t/18f/5705-2018-10-10.pdf}{the 2018-10-10 class}.]

\subsection*{Solution}
The following solution will use the shorthand symbols $\vec{x}$ and $\vec{e}$ to represent $(x_1, x_2, \ldots, x_n)$ and $(e_1, e_2, \ldots, e_d)$, respectively.

Let $f:\{-1,1\}^d \to \{0,1,\ldots,d\}$ send each $d$-tuple $\vec{e}$ to the number of $i \in [d]$ such that $e_i = -1$.
For each $k \in \ZZ$, there are exactly $\binom{d}{k}$ such $d$-tuples $\vec{e}$ satisfying $f(\vec{e})=k$. Thus:
\begin{align*}
    \sum_{\vec{e}\in\{-1,1\}^d} (e_1 + \cdots + e_d)^n
    &=
    \sum_{k=0}^d \sum_{\substack{\vec{e}\in\{-1,1\}^d; \\ f(\vec{e})=k}} (e_1 + \cdots + e_d)^n \\
    &=
    \sum_{k=0}^d \sum_{\substack{\vec{e}\in\{-1,1\}^d; \\ f(\vec{e})=k}} (k(-1) + (d-k)(1))^n \\
    &=
    \sum_{k=0}^d \binom{d}{k} (d-2k)^n .
\end{align*}
Now use the product rule to expand the left-hand sum, and flip the order of summation.
\begin{align*}
    \sum_{\vec{e}\in\{-1,1\}^d} (e_1 + \cdots + e_d)^n
    &=
    \sum_{\vec{e}\in\{-1,1\}^d}~  \sum_{\vec{x}\in[d]^n} e_{x_1} e_{x_2} \cdots e_{x_n}
    \\
    &=
    \sum_{\vec{x}\in[d]^n} ~ \sum_{\vec{e}\in\{-1,1\}^d} e_{x_1} e_{x_2} \cdots e_{x_n} .
\end{align*}

Let $g:[d]^n \times [d] \to \{0,1,\ldots,n\}$ send each $(\vec{x},j)$ to the number of $i \in [n]$ such that $x_i = j$.
Then:
$$
\sum_{\vec{x}\in[d]^n} ~ \sum_{\vec{e}\in\{-1,1\}^d} e_{x_1} e_{x_2} \cdots e_{x_n}
=
\sum_{\vec{x}\in[d]^n} ~ \sum_{\vec{e}\in\{-1,1\}^d} ~ \prod_{j \in [d]} e_j^{g(\vec{x},j)} .
$$
For every $n$-tuple $\vec{x} \in \ive{d}^n$ which is not all-even, there exists $h(\vec{x}) \in [d]$ such that $g(\vec{x},h(\vec{x}))$ is odd. For convenience's sake, let $\vec{e'}$ represent the $\tup{d-1}$-tuple $(e_1, \ldots, e_{h(\vec{x})-1}, e_{h(\vec{x})+1}, \ldots, e_d)$. Then:
\begin{align*}
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ not all-even}}} ~ \sum_{\vec{e}\in\{-1,1\}^d} ~ \prod_{j \in [d]} e_j^{g(\vec{x},j)}
    &=
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ not all-even}}} ~ \sum_{e_{h(\vec{x})} \in \{-1,1\}} ~ \sum_{\vec{e'}\in\{-1,1\}^{d-1}} \prod_{j \in [d]} e_j^{g(\vec{x},j)}
    \\
    &=
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ not all-even}}} ~ \sum_{e_{h(\vec{x})} \in \{-1,1\}} e_{h(\vec{x})}^{g(\vec{x},h(\vec{x}))} \sum_{\vec{e'}\in\{-1,1\}^{d-1}} \prod_{j \in [d] \setminus h(\vec{x})} e_j^{g(\vec{x},j)}
    \\
    &=
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ not all-even}}} ~ (-1 + 1) \sum_{\vec{e'}\in\{-1,1\}^{d-1}} \prod_{j \in [d] \setminus h(\vec{x})} e_j^{g(\vec{x},j)}
    \\
    &=
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ not all-even}}} 0 \\
    &= 0 .
\end{align*}
Therefore the only $n$-tuples $\vec{x}$ which contribute to the overall sum are those which are all-even. For these, $g(\vec{x},j)$ is even for all $j \in [d]$, so $e_j^{g(\vec{x},j)} = 1$ for all $j \in [d]$. Thus:
\begin{align*}
    \sum_{\vec{x}\in[d]^n} ~ \sum_{\vec{e}\in\{-1,1\}^d} ~ \prod_{j \in [d]} e_j^{g(\vec{x},j)}
    &=
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ all-even}}} ~ \sum_{\vec{e}\in\{-1,1\}^d} 1
    \\
    &=
    \sum_{\substack{\vec{x}\in[d]^n; \\ \vec{x} \text{ all-even}}} 2^d
    \\
    &=
    2^d \cdot (\text{number of all-even $n$-tuples}) .
\end{align*}
Chaining these equalities together yields
$$
\sum_{k=0}^d \binom{d}{k} (d-2k)^n = 2^d \cdot (\text{number of all-even $n$-tuples}) ,
$$
and dividing by $2^d$ gives the desired result.

\begin{thebibliography}{99999999}                                                                                         %

\bibitem[Grinbe16]{detnotes}Darij Grinberg, \textit{Notes on the combinatorial
fundamentals of algebra}, 10 January 2019.\newline%
\url{http://www.cip.ifi.lmu.de/~grinberg/primes2015/sols.pdf} \newline The
numbering of theorems and formulas in this link might shift when the project
gets updated; for a \textquotedblleft frozen\textquotedblright\ version whose
numbering is guaranteed to match that in the citations above, see
\url{https://github.com/darijgr/detnotes/releases/tag/2019-01-10} .

\end{thebibliography}

\end{document}
