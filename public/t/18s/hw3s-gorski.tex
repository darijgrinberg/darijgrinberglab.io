% LaTeX solution template for Math 4707 (Owen Levin, Darij Grinberg)
% ------------------------------------------------------------------

% Like most advanced LaTeX files, this one begins with a lot of
% boilerplate. You don't need to understand (or even read) most of it.
% All you need to do is fill in your name, UMN ID, email address,
% and the number of the pset. (Search for "METADATA" to find the place
% for this.) Then, you can go straight to the "EXERCISE 1"
% section and start writing your solutions.
% The "VARIOUS USEFUL COMMANDS" section is probably worth taking a
% look at at some point.

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw graphs.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\rev}{\operatorname{rev}} % reversal of a walk
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\verts}[1]{\operatorname{V}\left( #1 \right)}
% $\verts{...}$ compiles to V(...) (vertex set of a graph/digraph).
\newcommand{\edges}[1]{\operatorname{E}\left( #1 \right)}
% $\edges{...}$ compiles to E(...) (edge set of a graph).
\newcommand{\arcs}[1]{\operatorname{A}\left( #1 \right)}
% $\arcs{...}$ compiles to A(...) (arc set of a digraph).
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{Nathaniel Gorski (edited by Darij Grinberg)} % ENTER YOUR NAME HERE
\newcommand{\myid}{-} % ENTER YOUR UMN ID HERE
\newcommand{\mymail}{} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{3} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to homework set \#\psetnumber} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname, \myid} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{University of Minnesota, School of Mathematics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 4707: Combinatorics, Spring 2018\\
Homework \psetnumber\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	EXERCISE 1
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 1}

\begin{exercise}
Let $n \in \mathbb{N}$. Then
\[ \sum_{k=1}^n \dfrac{(-1)^{k-1}}{k}\dbinom{n}{k} = \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n}\]
\end{exercise}

\begin{proof}
This will be shown by induction on $n$. For each $n \in \NN$, let $\mathcal{A}(n)$ represent the statement ``$\sum_{k=1}^n \dfrac{(-1)^{k-1}}{k}\dbinom{n}{k} = \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n}$''. We will first show that $\mathcal{A}(0)$ is true.

Observe that if $n = 0$, then $ \sum_{k=1}^n \dfrac{(-1)^{k-1}}{k}\dbinom{n}{k}$ is the empty sum, which takes on a value of zero, and  $\dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n}$ is also the empty sum, which also takes on a value of zero. Hence, we have equality, so $\mathcal{A}(0)$ is true.

Now we will show that if $\mathcal{A}(n)$ is true for some $n \in \NN$, then it follows that $\mathcal{A}(n+1)$ is true.
Fix $n \in \NN$ and suppose that $\mathcal{A}(n)$ holds. Then, by the induction hypothesis, it holds that $\sum_{k=1}^n \dfrac{(-1)^{k-1}}{k}\dbinom{n}{k} = \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n}$.

An identity proven in class says that $\sum_{k=0}^{m}(-1)^{k-1}\dbinom{m}{k} = \ive{m = 0}$ for all $m \in \NN$. Applied to $m = n+1$, this yields $\sum_{k=0}^{n+1}(-1)^{k-1}\dbinom{n+1}{k} = \ive{n+1 = 0} = 0$.

First, observe that any positive integer $k$ satisfies
\begin{align}
\dbinom{n}{k-1} &= \dfrac{n(n-1)\cdots(n-k+2)}{(k-1)!} = \dfrac{k}{n+1}\dfrac{(n+1)(n)(n-1)\cdots(n-k+2)}{k!} \nonumber \\
&= \dfrac{k}{n+1}\dbinom{n+1}{k} .
\label{sol.1.1}
\end{align}
But the recurrence relation of the binomial coefficients leads to
\begin{align*}
& \sum_{k=1}^{n+1} \dfrac{(-1)^{k-1}}{k}\dbinom{n+1}{k} \\
&= \sum_{k=1}^{n+1}\dfrac{(-1)^{k-1}}{k}\left( \dbinom{n}{k} + \dbinom{n}{k-1}\right) \\
&= \sum_{k=1}^{n+1}\dfrac{(-1)^{k-1}}{k}\dbinom{n}{k} + \sum_{k=1}^{n+1}\dfrac{(-1)^{k-1}}{k}\dbinom{n}{k-1} \\
&= \sum_{k=1}^n \dfrac{(-1)^{k-1}}{k}\dbinom{n}{k} + \dfrac{(-1)^n}{n+1}\dbinom{n}{n+1} + \sum_{k=1}^{n+1}\dfrac{(-1)^{k-1}}{k}\dfrac{k}{n+1}\dbinom{n+1}{k} \\
&\qquad \tup{\begin{array}{c} \text{here, we split off the addend for } k=n+1 \text{ from the first sum,} \\ \text{ and rewrote the second sum using \eqref{sol.1.1}} \end{array}} \\
&= \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n} + 0 + \dfrac{1}{n+1}\sum_{k=1}^{n+1}(-1)^{k-1}\dbinom{n+1}{k} \\
&\qquad \tup{\text{using the induction hypothesis and using } \dbinom{n}{n+1} = 0 } \\
&= \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n} + \dfrac{1}{n+1}\left(\underbrace{\sum_{k=0}^{n+1}(-1)^{k-1}\dbinom{n+1}{k}}_{=0} - \underbrace{(-1)^{0-1}\dbinom{n+1}{0}}_{=-1}\right) \\
&= \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n} + \dfrac{1}{n+1} \tup{0 - \left(-1\right)} \\
&= \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n} + \dfrac{1}{n+1}
= \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n+1} .
\end{align*}
This means that $\mathcal{A}(n+1)$ holds.

So we know that $\mathcal{A}(0)$ holds, and if $n \in \NN$, and $\mathcal{A}(n)$ holds, then it follows that $\mathcal{A}(n+1)$ holds. Hence, by induction, $\mathcal{A}(n)$ holds for all $n \in \NN$. Hence, for all $n \in \NN$, it follows that
\[ \sum_{k=1}^n \dfrac{(-1)^{k-1}}{k}\dbinom{n}{k} = \dfrac{1}{1} + \dfrac{1}{2} + \cdots + \dfrac{1}{n} .\]

\end{proof}

\section*{Exercise 3}

\subsection*{Part A}

\begin{exercise}
Let $n \in \NN$. Then the number of $2$-multijective maps from $[2n]$ to $[n]$ is $\dfrac{(2n)!}{2^n}$.
\end{exercise}

\begin{proof}
Let $r$ represent the number of $2$-multijective maps from $[2n]$ to $[n]$. We will first count the number of permutations $\sigma$ of $[2n]$. On the one hand, there are $(2n)!$ of them. We now go about counting these another way.

To construct a permutation $\sigma$ of $\ive{2n}$, proceed as follows: First, choose a $2$-multijective map $m$ from $[2n]$ to $[n]$. There are $r$ ways to do this. Now, given this $m$, we define, for each $i \in \ive{n}$, the two values $\sigma\tup{2i-1}$ and $\sigma\tup{2i}$ to be the two preimages of $i$ under $m$ in some order. There are $2^n$ ways to do this (because for each of the $n$ elements $i \in \ive{n}$, we get to choose the order of the two preimages of $i$, and there are $2$ choices for this order).
Thus, in total, the procedure can be performed in $r \cdot 2^n$ many ways. Since it constructs every permutation $\sigma$ of $\ive{2n}$ exactly once (indeed, the $2$-multijection $m$ that was used to construct $\sigma$ can be reconstructed from $\sigma$, because it is the unique map $\ive{2n} \to \ive{n}$ sending both $\sigma\tup{2i-1}$ and $\sigma\tup{2i}$ to $i$ for all $i \in \ive{n}$), we thus see that the number of permutations $\sigma$ of $\ive{2n}$ is $r \cdot 2^n$.

Comparing our two expressions for this number, we conclude that $r \cdot 2^n = \tup{2n}!$. So $r = \dfrac{(2n)!}{2^n}$. And thus, the number of $2$-multijective maps from $[2n]$ to $[n]$ is given by $\dfrac{(2n)!}{2^n}$.
\end{proof}

\subsection*{Part B}

\begin{exercise}
Let $n \in \NN$. The number of all set compositions $\mathbf{C}$ of $[2n]$ such that each part of $\mathbf{C}$ has size $2$ is $\dfrac{(2n)!}{2^n}$.
\end{exercise}

\begin{proof}
Let $J$ be the set of $2$-multijective maps from $[2n]$ to $[n]$, and $C$ be the set of all set compositions $\mathbf{C}$ of $[2n]$ such that every part of $\mathbf{C}$ has exactly two elements. We will construct a bijection between these two sets.

First, define a map $\alpha:J\rightarrow C$ such that if $j \in J$, then $\alpha$ maps $j$ to a set composition $c$ of $[2n]$ into $n$ parts such that, for each $i \in [n]$, the $i$-th part of $c$ is the set of the two preimages of $i$ under $j$. Clearly, this map is well defined, as the number of elements in the image of each $j \in J$ is equal to $n$, which is also equal to the number of parts of each $c \in C$. And further, each $j \in J$ is a $2$-multijection, so that each element of $\ive{n}$ will have exactly $2$ preimages under $j$, which is also equal to the size of each part of each $c \in C$.

Now let $\beta:C\rightarrow J$ be defined such that, if $c \in C$, then $\beta$ maps $c$ to a $2$-multijection $j \in J$ such that, for each $i \in [n]$, the two elements of the $i$-th part of $c$ make up the preimage of $i$ under $j$. Clearly, this map is well defined, as each $c \in C$ will have $n$ parts, and also each $j \in J$ will have exactly $n$ image elements. And also, each part of each $c \in C$ will have $2$ elements, which is the number of preimages of each $i \in [n]$ under each $j \in J$.

We will now show that $\alpha$ and $\beta$ are mutually inverse. First, let $j \in J$. We will show that $(\beta \circ \alpha)(j) = j$. By definition, $\alpha$ maps $j$ to a set composition $c$ of $[2n]$ such that, for each $i \in [n]$, the $i$-th part of $c$ comprises the two preimages of $i$ under $j$. Then $\beta$ maps this new set composition to a $2$-multijective map such that, for each $i \in [n]$, the preimages of $i$ are the elements of the $i$-th part of $c$, which are precisely the two preimages of $i$ under $j$. Hence, this map is precisely $j$, and $(\beta \circ \alpha)(j) = j$. The proof that, for each $c \in C$, $(\alpha \circ \beta)(c) = c$ is very similar.

Hence, $\alpha$ and $\beta$ are two functions which are well defined and mutually inverse, and thus they define a bijection between $C$ and $J$, which means that $|C| = |J|$. In part \textbf{(a)}, we showed that $|J| = \dfrac{(2n)!}{2^n}$. So then $|C| = \dfrac{(2n)!}{2^n}$. And thus, the number of set compositions $\mathbf{C}$ of $[2n]$ such that each part of $\mathbf{C}$ has size $2$ is equal to $\dfrac{(2n)!}{2^n}$.
\end{proof}

\subsection*{Part C}

\begin{exercise}
Let $n \in \NN$. The number of perfect matchings of $[2n]$ is equal to $\dfrac{(2n)!}{2^nn!}$.
\end{exercise}

\begin{proof}
Let $M$ be the number of perfect matchings of $[2n]$. We will count the number of all set compositions $\mathbf{C}$ of $[2n]$ such that each part of $\mathbf{C}$ has $2$ elements. In part \textbf{(b)}, we showed that there are $\dfrac{(2n!)}{2^n}$ such set compositions.

Alternatively, we can construct such a set composition $\mathbf{C}$ of $[2n]$ by first choosing which elements will be together in some part of $\mathbf{C}$, without yet choosing in which part of $\mathbf{C}$ they will be in. In other words, we first choose the \textbf{set} of all parts of $\mathbf{C}$, but we don't yet decide which of them will be the first part, which the second, and so on. This is the same as choosing a perfect matching of $[2n]$, of which there are $M$ ways to do. We next select which part of this matching will represent which part of $\mathbf{C}$ by choosing a (total) order on the parts of the matching. Since $|[2n]| = 2n$, our perfect matching of $[2n]$ must have exactly $n$ parts, and hence, there are $n!$ different orderings which these parts can take. Clearly, these steps will create a unique set composition $\mathbf{C}$ of $[2n]$ such that each part of $\mathbf{C}$ has $2$ elements. And because there are $M$ ways of selecting the perfect matching of $[2n]$, and $n!$ different ways to order the parts of this perfect matching, there are $Mn!$ of such set compositions.

Hence, it follows that $Mn! = \dfrac{(2n)!}{2^n}$, so $M = \dfrac{(2n)!}{2^nn!}$. Thus, the number of prefect matchings of $[2n]$ is equal to $\dfrac{(2n)!}{2^nn!}$.
\end{proof}

\section*{Exercise 5}

\begin{exercise}
Let $n \in \NN$ and $d \in \NN$ with $d \geq 1$. Then the number of $1$-even $n$-tuples in $[d]^n$ is equal to $\dfrac{1}{2}(d^n + (d-2)^n)$.
\end{exercise}

\begin{proof}
Forget that $n$ is fixed. (But $d$ will be fixed.)

For each $n \in \NN$, let $t_n$ represent the number of $1$-even $n$-tuples in $[d]^n$.
%We thus need to prove the claim that $t_n = \dfrac{1}{2}(d^n + (d-2)^n)$ for all $n \in \NN$.

We will first find a recursive formula for $t_n$. Fix a positive integer $n$. We would like to count the number of $1$-even $n$-tuples in $[d]^n$. Let $s$ be such a tuple. Then we consider two possible cases for the final entry of $s$.

Case 1: The final entry of $s$ is not equal to $1$. If $s$ is $1$-even, and the last entry of $s$ is not equal to $1$, then there must be an even number of entries which are equal to $1$ among the first $n-1$ entries of $s$. This means that, if the first $n-1$ entries are considered as an $(n-1)$-tuple, that tuple would be $1$-even. Hence, $s$ is formed by concatenating an element of $[d]$ which is not equal to $1$ onto the end of a $1$-even $(n-1)$-tuple in $[d]^{n-1}$. Hence, there are $(d-1) t_{n-1}$ ways of forming a $1$-even $n$-tuple whose last entry is not equal to $1$.

Case 2: The final entry of $s$ is equal to $1$. If $s$ is $1$-even, and the last entry of $s$ is equal to $1$, then there must be an odd number of entries which are equal to $1$ among the first $n-1$ entries of $s$. This means that, if the first $n-1$ entries are considered as an $(n-1)$-tuple, that tuple would not be $1$-even. Hence, $s$ is formed by concatenating $1$ onto the end of a non-$1$-even $(n-1)$-tuple in $[d]^{n-1}$. Since a tuple is either $1$-even, or not $1$-even, and there are $d^{n-1}$ elements of $[d]^{n-1}$, we can conclude that the number of $(n-1)$-tuples which are not $1$-even is equal to $d^{n-1} - t_{n-1}$. Hence, there are $d^{n-1} - t_{n-1}$ ways of forming a $1$-even $n$-tuple whose last entry is equal to $1$.

Since a $1$-even $n$-tuple must fall either into case 1, or into case 2, but certainly not both, it follows that
\begin{align}
t_n = (d-1)t_{n-1} + (d^{n-1} - t_{n-1}) = (d-2)t_{n-1} + d^{n-1} .
\label{sol.5.rec}
\end{align}

Now, the claim of the exercise will be proven by induction on $n$. Unfix $n$. For each $n \in \NN$, let $\mathcal{A}(n)$ be the statement ``the number of $1$-even tuples of $[d]^n$ is equal to $\dfrac{1}{2}(d^n + (d-2)^n)$''. Observe that if $n = 0$, there is only one element of $[d]^n$, which is the empty tuple. This tuple has no elements, so zero of the entries of this tuple are equal to 1. Hence, this tuple is $1$-even, so there is one $1$-even tuple of $[d]^n$ (for $n = 0$). And observe that $\dfrac{1}{2}(d^0 + (d-2)^0) = \dfrac{1}{2}(1+1) = 1$. Hence, $\mathcal{A}(0)$ holds.

Now suppose that, for some $n \in \NN$, we knew that $\mathcal{A}(n)$ is true. We will show that this fact would imply that $\mathcal{A}(n+1)$ is true. By the induction hypothesis, we know that $t_{n} = \dfrac{1}{2}(d^n + (d-2)^n)$. Therefore, \eqref{sol.5.rec} (applied to $n+1$ instead of $n$) yields
\begin{align*}
t_{n+1} &= (d-2)t_{n} + d^{n} \\
&= (d-2)\dfrac{1}{2}(d^n + (d-2)^n) + d^{n}\\
&= \dfrac{1}{2}d^{n+1} - d^{n} + \dfrac{1}{2}(d-2)^{n+1} + d^n \\
&= \dfrac{1}{2}d^{n+1} + \dfrac{1}{2}(d-2)^{n+1} \\
&= \dfrac{1}{2}(d^{n+1} + (d-2)^{n+1}) .
\end{align*}
This means that there are $\dfrac{1}{2}(d^{n+1} + (d-2)^{n+1})$ $1$-even $(n+1)$-tuples in $[d]^{n+1}$, so $\mathcal{A}(n+1)$ holds.

We now know that $\mathcal{A}(0)$ holds, and for each $n \in \NN$, $\mathcal{A}(n)$ being true would imply that $\mathcal{A}(n+1)$ is true.
Hence, by induction, $\mathcal{A}(n)$ is true for all $n \in \NN$.
In other words, the number of $1$-even $n$-tuples in $[d]^n$ is equal to $\dfrac{1}{2}(d^n + (d-2)^n)$.
\end{proof}

\end{document}