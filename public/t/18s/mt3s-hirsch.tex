\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw graphs.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\rev}{\operatorname{rev}} % reversal of a walk
\newcommand{\outdeg}{\operatorname{outdeg}} % outdegree of a vertex
\newcommand{\source}{\operatorname{source}} % source of a vertex
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\verts}[1]{\operatorname{V}\left( #1 \right)}
% $\verts{...}$ compiles to V(...) (vertex set of a graph/digraph).
\newcommand{\edges}[1]{\operatorname{E}\left( #1 \right)}
% $\edges{...}$ compiles to E(...) (edge set of a graph).
\newcommand{\arcs}[1]{\operatorname{A}\left( #1 \right)}
% $\arcs{...}$ compiles to A(...) (arc set of a digraph).
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{James Hirsch (edited by Darij Grinberg)} % ENTER YOUR NAME HERE
\newcommand{\myid}{} % ENTER YOUR UMN ID HERE
\newcommand{\mymail}{} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{0} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to Midterm 3} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname, \myid} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{University of Minnesota, School of Mathematics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 4707: Combinatorics, Spring 2018\\
Midterm 3\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}

\begin{document}

\maketitle % Print the title
\section{Exercise 1}
\subsection{Problem}
Let $D=\left(  V,A,\varphi\right)  $ be an acyclic multidigraph. Prove that there
is a list $\left(  v_{1},v_{2},\ldots,v_{n}\right)  $ of elements of $V$ such that

\begin{itemize}
\item each element of $V$ appears exactly once in this list $\left(
v_{1},v_{2},\ldots,v_{n}\right)  $;

\item whenever $i$ and $j$ are two elements of $\left[  n\right]  $ such that
some arc of $D$ has source $v_{i}$ and target $v_{j}$, we must have $i<j$.
\end{itemize}
\subsection{Solution}
\begin{definition}
Let $D=\left(V,A,\varphi\right)$ be a multidigraph.
For a vertex $v\in V$, we define its \textit{outdegree} as
\[
\outdeg(v)=\vert\lbrace a\in A \mid \source(a)=v\rbrace\vert
\]
(where $\source(v)$ denotes the source of $v$).
\end{definition}

\begin{lemma}
\label{lem.digraph.acyclic.w=p} Let $D= \tup{V, A, \varphi}$ be an acyclic multidigraph.
Then, any walk in $D$ is a path.
\end{lemma}

\begin{proof}
Let $\mathbf{w}$ be any walk in $D$. We must show that $\mathbf{w}$ is a path.

Let $u_0, u_1, \ldots, u_k$ be the vertices of the walk $\mathbf{w}$, from first to last.
We claim that these vertices are distinct.
Indeed, assume the contrary.
Thus, there exist some elements $i$ and $j$ of $\set{0,1,\ldots,k}$ satisfying $i < j$ and $u_i = u_j$.
In other words, there exists some $j \in \set{0,1,\ldots,k}$ satisfying $u_j \in \set{u_0,u_1,\ldots,u_{j-1}}$.
Consider the \textbf{smallest} such $j$.
Then, there is an $i < j$ satisfying $u_j = u_i$ (since $u_j \in \set{u_0,u_1,\ldots,u_{j-1}}$).
Consider this $i$.
The vertices $u_i,u_{i+1},\ldots,u_{j-1}$ are distinct (because of the minimality of $j$),
but the vertex $u_j$ equals $u_i$.
Hence, the part of the walk $\mathbf{w}$ between $u_i$ and $u_j$ is a cycle of $D$.
Hence, $D$ contains a cycle.
But this contradicts the acyclicity of $D$.
This contradiction shows that our assumption was wrong.
Thus, the vertices $u_0, u_1, \ldots, u_k$ of the walk $\mathbf{w}$ are distinct.
In other words, this walk $\mathbf{w}$ is a path.
\end{proof}

\begin{lemma}
\label{mt3.1.2} Let $D= \tup{V, A, \varphi}$ be an acyclic multidigraph with $V \neq \varnothing$.
Then, there exists a vertex $v\in V$ having $\outdeg(v)= 0$.
\end{lemma}

\begin{proof}
Assume the contrary.
Then, for each $v\in V$ we have $\outdeg(v)\neq 0$.
Thus, a walk in $D$ can be constructed by starting at an arbitrary vertex $u$ and taking an arc leaving it (such an arc exists since $\outdeg(u)\neq 0$).
The target of this arc
% cannot be $u$, since $D$ is acyclic, and it
also has outdegree $\neq 0$, so it has an arc leaving it, which we take.
We continue taking arcs in this way until we have taken $\abs{V}$ many arcs.
The resulting walk has at least $\abs{V}$ many arcs, and thus it has $\abs{V} + 1$ many vertices.
But this walk is a path (by Lemma~\ref{lem.digraph.acyclic.w=p}), and thus its vertices are distinct.
Hence, this walk has at most $\abs{V}$ many vertices.
This contradicts the fact that it has $\abs{V} + 1$ many vertices.
So, there is a contradiction and our assumption was false.
\end{proof}

\begin{proposition}
\label{prop.1.3}
Let $D=\left( V,A, \varphi\right)$ be an acyclic multidigraph.
Then, there exists a list $\tup{v_1, v_2, \ldots, v_n}$ of elements of $V$ such that

\begin{itemize}
\item each element of $V$ appears exactly once in this list
$\tup{v_1, v_2, \ldots, v_n}$;

\item whenever $i$ and $j$ are two elements of $\ive{n}$ such that
some arc of $D$ has source $v_i$ and target $v_j$, we must have $i<j$.
\end{itemize}
\end{proposition}

\begin{proof}[Proof by induction on $\abs{V}$]
Base case: $\abs{V} =0$.
In this case, $D$ is a multidigraph with no vertices, and thus the empty list $\tup{}$ contains each element of $V$, and it is vacuously true that for any $i$ and $j$ in $\ive{0} = \varnothing$ such that an arc of $D$ has source $v_i$ and target $v_j$, we have $i < j$. So Proposition \ref{prop.1.3} is proven in the case where $\abs{V} =0$.

Inductive Step:
Fix $n \in \NN$. Assume as the inductive hypothesis that Proposition \ref{prop.1.3} holds for any acyclic multidigraph having $\abs{V}  =n$. We now want to show that Proposition \ref{prop.1.3} holds for any acyclic multidigraph $D=\left( V, A, \varphi\right)$ having $\abs{V} =n+1$.

So let $D = \tup{V, A, \varphi}$ be an acyclic multidigraph having $\abs{V} = n+1$.
Let $u \in V$ be a vertex of $D$ having $\outdeg(u)=0$ (such a vertex exists by Lemma \ref{mt3.1.2}).
Let $D'$ be the multidigraph $D$ with vertex $u$ and all arcs using $u$ removed.
Removing arcs cannot create a new cycle, so $D'$ is an acyclic multidigraph having $\abs{V'}=n$, where $V' = V\setminus\set{u}$ is its set of vertices.
By the inductive hypothesis, Proposition \ref{prop.1.3} holds for this multidigraph $D'$.
Thus, there is a list $\tup{v_1, v_2, \ldots, v_n}$ containing each element of $V' = V\setminus \set{u}$ exactly once and having the property that for any $i$ and $j$ in $\ive{n}$ such that some arc of $D'$ has source $v_i$ and target $v_j$, we have $i < j$.
Consider such a list.
Extend it to a list $\tup{v_1, v_2, \ldots, v_{n+1}}$ of vertices of $D$ by setting $v_{n+1} = u$.
Then, the list $\tup{v_1, v_2, \ldots, v_{n+1}} = \tup{v_1, v_2, \ldots, v_n, u}$ contains each element of $V$ exactly once.
Furthermore, whenever $i$ and $j$ are two elements of $\ive{n+1}$ such that some arc of $D$ has source $v_i$ and target $v_j$, we must have $i < j$.
(Indeed, in the case where $i$ and $j$ are both in $\ive{n}$, this follows from the analogous property of the list $\tup{v_1, v_2, \ldots, v_n}$.
In the case where $i$ is in $\ive{n}$ and $j=n+1$, the inequality $i < j$ is obvious.
The only remaining case -- the case where $i=n+1$ -- does not occur, because the vertex $v_{n+1} = u$ has outdegree $0$ and thus cannot be the source of any arc of $D$.)
Thus, the list $\tup{v_1, v_2, \ldots, v_{n+1}}$ satifies the conditions of Proposition \ref{prop.1.3} for our multidigraph $D$.
This completes the inductive step.
\end{proof}


\section{Exercise 2}
\subsection{Problem}
\label{exe.confluence.diamond}
Let $D$ be an acyclic multidigraph.
A vertex $v$
of $D$ is said to be a \textit{sink} if there is no arc of $D$ with source $v$.

If $u$ and $v$ are any two vertices of $D$, then:

\begin{itemize}
\item we write $u\longrightarrow v$ if and only if $D$ has an \textbf{arc}
with source $u$ and target $v$;

\item we write $u\overset{\ast}{\longrightarrow}v$ if and only if $D$ has a
\textbf{path} from $u$ to $v$.
\end{itemize}

The so-called \textit{no-watershed condition} says that for any three vertices
$u$, $v$ and $w$ of $D$ satisfying $u\longrightarrow v$ and $u\longrightarrow
w$, there exists a vertex $t$ of $D$ such that $v\overset{\ast
}{\longrightarrow}t$ and $w\overset{\ast}{\longrightarrow}t$.

Assume that the no-watershed condition holds. Prove that for each vertex $p$
of $D$, there exists \textbf{exactly one} sink $q$ of $D$ such that
$p\overset{\ast}{\longrightarrow}q$.
\subsection{Solution}
\begin{definition}
Let $D= \tup{V, A, \varphi}$ be an acyclic multidigraph. For a vertex $p\in V$, we define its \textit{height} $H \tup{p} $ as the maximum number of edges in a path in $D$ that starts at $p$.
\end{definition}
\begin{remark}
The height of a vertex is an integer between $0$ and $\abs{V}  -1$, since every vertex has a path to itself (which contains $0$ edges) and a path can have at most $\abs{V}-1$ edges (since it can contain at most $\abs{V}$ vertices).
\end{remark}
\begin{remark}
\label{mt3.2.3} Let $u$ and $v$ be two vertices of an acyclic multidigraph $D$ such that there is a path from $u$ to $v$ of nonzero length in $D$. Then, $H \tup{u} > H \tup{v}$. Indeed, a longest path in $D$ starting at $v$ can be concatenated with any path from $u$ to $v$ of nonzero length to form a longer path in $D$ starting at $u$ (and this concatenation is indeed a path, because of Lemma~\ref{lem.digraph.acyclic.w=p}).
\end{remark} 

\begin{proposition}
\label{prop.2.3}
Let $D$ be an acyclic multidigraph for which the no-watershed condition holds. Then, for each vertex $p$ of $D$, there is exactly one sink $q$ of $D$ such that $p\overset{\ast}{\longrightarrow}q$.
\end{proposition}

\begin{proof}[Proof by strong induction on $H \tup{p} $:]
Base case: $H \tup{p} =0$.

If the height of $p$ is $0$, then the only path in $D$ that starts at $p$ is the ``empty path'', and so there is no arc of $D$ with source $p$. So, in this case, $p$ is a sink of $D$, and $p$ is the unique sink of $D$ such that there is a path from $p$ to it. This proves Proposition \ref{prop.2.3} when $H \tup{p} =0$. This completes the induction base.

Inductive Step: Fix an integer $n\geq 1$. Assume as the inductive hypothesis that for any vertex $w$ of $D$ with $H \left( w\right) <n$, there is exactly one sink of $D$ such that there is a path from $w$ to that sink. We want to show that for a vertex $p$ with $H \tup{p}  =n$, there is exactly one sink $q$ of $D$ such that $p\overset{\ast}{\longrightarrow}q$. 

In the following, an \textit{out-neighbor} of a vertex $x \in D$ means any vertex $y \in D$ such that $D$ has an arc with source $x$ and target $y$.

Consider any vertex $p$ with $H\tup{p} = n$. We have $H \tup{p}  = n \geq 1$, so there is a path in $D$ starting at $p$ that contains at least one edge; thus, $p$ has an out-neighbor. Let $u$ be any out-neighbor of $p$. Then, $H\tup{p}>H\tup{u}$ by Remark~\ref{mt3.2.3} (since there is a path from $p$ to $u$ of nonzero length in $D$), and therefore $H\tup{u} < H\tup{p} = n$. Hence, the inductive hypothesis shows that there is a unique sink of $D$ such that there is a path from $u$ to this sink. Denote this sink by $q_u$. Forget that we fixed $u$. Thus, for each out-neighbor $u$ of $p$, we have defined a sink $q_u$ of $D$ with the property that $q_u$ is the unique sink of $D$ such that there is a path from $u$ to this sink.

Let $u$ and $v$ be two out-neighbors of $p$ (it is possible that $u=v$). Because there is a path from $p$ to $u$ of nonzero length in $D$, we have $H\tup{p}>H\tup{u}$ by Remark~\ref{mt3.2.3}; similarly, $H\tup{p}>H\tup{v}$. Recall that $q_u$ is the unique sink of $D$ such that there is a path from $u$ to $q_u$. Since the no-watershed condition holds (and since $p \longrightarrow u$ and $p \longrightarrow v$), there is a vertex $t$ such that $u\overset{\ast}{\longrightarrow}t$ and $v\overset{\ast}{\longrightarrow}t$. Again by Remark~\ref{mt3.2.3}, this vertex $t$ has a smaller height than $v$, and so its height is smaller than $H \tup{p}  =n$. Thus, by the inductive hypothesis, $t$ has a path in $D$ to exactly one sink of $D$. Call this sink $q$. Because $u\overset{\ast}{\longrightarrow}t$ and $t\overset{\ast}{\longrightarrow}q$, we have $u\overset{\ast}{\longrightarrow}q$. This means that $q$ is a sink of $D$ that $u$ has a path to in $D$. Since we already know that the only such sink is $q_u$ (because this is how we defined $q_u$), we thus obtain $q_u = q$. Similarly, $q_v = q$. Thus, $q_u = q_v$.

Now, forget that we fixed $u$ and $v$. We have shown that any two out-neighbors $u$ and $v$ of $p$ satisfy $q_u = q_v$. Thus, the sink $q_u$ corresponding to an out-neighbor $u$ of $p$ does not depend on $u$.
Hence, there is a sink $q$ of $D$ such that each out-neighbor $u$ of $p$ satisfies $q_u = q$ (since we know that $p$ has an out-neighbor). Consider this $q$.
For each out-neighbor $u$ of $p$, there is a path from $u$ to $q$ (since $q_u = q$). Thus, there is a walk from $p$ to $q$ (since there is a path from $p$ to any of its out-neighbors and a path from any of its out-neighbors to $q$), therefore also a path from $p$ to $q$ (by Lemma~\ref{lem.digraph.acyclic.w=p}). In other words, $p\overset{\ast}{\longrightarrow}q$.

The vertex $p$ is not a sink (since $H\tup{p} \geq 1$). Hence, any path from $p$ to a sink of $D$ must leave $p$, and thus must travel through some out-neighbor $u$ of $p$. Hence, the sink that this path leads to must be $q_u$. In other words, this sink must be $q$ (since $q_u = q$). So we have proven that if there is a path from $p$ to a sink of $D$, then this sink must be $q$. In other words, $q$ is the unique sink of $D$ such that there is a path from $p$ to this sink. Thus, there is exactly one sink $r$ of $D$ such that $p \overset{\ast}{\longrightarrow} r$ (namely, $q$). This completes the inductive step.

Proposition \ref{prop.2.3} is thus proven for any vertex $p$ having $H \tup{p} \geq 0$, and so it is proven for any $p\in V$.
\end{proof}

\section{Exercise 5}
\begin{definition}
Let $G$ be a graph.

\textbf{(a)} An \textit{independent set} of $G$ means a set $S$ of vertices of
$G$ such that no two distinct elements of $S$ are adjacent.

\textbf{(b)} We let $\operatorname*{ind}G$ be the number of all independent
sets of $G$.
\end{definition}
\begin{definition}
Let $G$ be a graph. Let $S$ be a set of vertices of $G$. Then, $G\setminus S$
will denote the graph obtained from $G$ by removing all vertices in $S$ (along
with all edges that use these vertices).
\end{definition}

\begin{definition}
\label{def.intro.path-graph} For each $n\in\mathbb{N}$, we define the
\textit{$n$-th path graph} to be the simple graph
\begin{align*}
&  \left(  \left\{  1,2,\ldots,n\right\}  ,\left\{  \left\{  i,i+1\right\}
\ \mid\ i\in\left\{  1,2,\ldots,n-1\right\}  \right\}  \right) \\
&  =\left(  \left\{  1,2,\ldots,n\right\}  ,\left\{  \left\{  1,2\right\}
,\left\{  2,3\right\}  ,\ldots,\left\{  n-1,n\right\}  \right\}  \right)  .
\end{align*}
This graph is denoted by $P_{n}$.
\end{definition}

\begin{definition}
\label{def.intro.cycle-graph} For each integer $n>1$, we define the
\textit{$n$-th cycle graph} to be the simple graph
\begin{align*}
&  \left(  \left\{  1,2,\ldots,n\right\}  ,\left\{  \left\{  i,i+1\right\}
\ \mid\ i\in\left\{  1,2,\ldots,n-1\right\}  \right\}  \cup\left\{
n,1\right\}  \right) \\
&  =\left(  \left\{  1,2,\ldots,n\right\}  ,\left\{  \left\{  1,2\right\}
,\left\{  2,3\right\}  ,\ldots,\left\{  n-1,n\right\}  ,\left\{  n,1\right\}
\right\}  \right)  .
\end{align*}
This graph is denoted by $C_{n}$.
\end{definition}
\subsection{Problem}
\label{exe.ind-number.Cn}\textbf{(a)} Let $v$ be a vertex of a graph $G$. Let
$N\left(  v\right)  $ be the set of all neighbors of $v$. Let $N^{+}\left(
v\right)  =\left\{  v\right\}  \cup N\left(  v\right)  $. Prove that%
\[
\operatorname*{ind}G=\operatorname*{ind}\left(  G\setminus\left\{  v\right\}
\right)  +\operatorname*{ind}\left(  G\setminus\left(  N^{+}\left(  v\right)
\right)  \right)  .
\]


\textbf{(b)} Compute $\operatorname*{ind}\left(  C_{n}\right)  $ for each
$n\geq2$ (in terms of the Fibonacci sequence).
\subsection{Solution to part a)}
\begin{proposition}
\label{mt3.3.5} Let $G=\left( V,E,\varphi\right)$ be a graph, and let $v \in V$ be one of its vertices. Then,%
\[
\operatorname*{ind}G=\operatorname*{ind}\left(  G\setminus\left\{  v\right\}
\right)  +\operatorname*{ind}\left(  G\setminus\left(  N^{+}\left(  v\right)
\right)  \right).
\]
\end{proposition}
\begin{proof}
There are two types of independent sets of $G$: those that contain $v$ and those that don't. So,
\begin{align}
\operatorname*{ind}G
&= \vert\set{\text{independent sets } S \text{ of } G \text{ with } v \notin S}\vert \nonumber \\
&\qquad + \vert\set{ \text{independent sets } S \text{ of } G \text{ with } v \in S}\vert .
\label{eq.1}
\end{align}
There is a map
\begin{align*}
\set{\text{independent sets } S \text{ of } G \text{ with } v \notin S} & \rightarrow \set{\text{independent sets of } G\setminus\set{v}}, \\
S & \mapsto S
\end{align*}
(this is simply the identity map).
(Indeed, this map is well-defined, because if $S$ is an independent set of $G$ with $v \notin S$, then $S$ is a subset of $V$ containing no vertices which are neighbors in $G$ and satisfying $v \notin S$, so $S$ is also a subset of $V\setminus \set{v}$ containing no vertices which are neighbors in $G\setminus\set{v}$, and therefore $S$ is an independent set of $G\setminus \set{v}$.) \\ This map has an inverse map, which is
\begin{align*}
\set{\text{independent sets of } G\setminus\set{v}} & \rightarrow \set{\text{independent sets } S\text{ of } G \text{ with } v \notin S}, \\
S & \mapsto S .
\end{align*}
(This map is well-defined for similar reasons.) Thus, the map above is a bijection. Hence,
\begin{align}
\vert\set{\text{independent sets } S \text{ of } G \text{ with } v \notin S}\vert
&= \abs{\set{\text{independent sets of } G\setminus\set{v}}} \nonumber\\
& =\operatorname*{ind}\left( G\setminus\set{v}\right) .
\label{eq.2}
\end{align}

In addition, there is a map
\begin{align*}
\set{\text{independent sets } S \text{ of } G \text{ with } v\in S} & \rightarrow \set{\text{independent sets of } G\setminus \left( N^{+}\left( v\right) \right)}, \\ S & \mapsto S\setminus\set{v} .
\end{align*}
(Indeed, this map is well-defined for the following reason: If $S$ is an independent set of $G$ with $v\in S$, then none of the neighbors of $v$ belongs to $S$. Thus, $S\setminus\set{v}$ is a subset of $V\setminus \tup{N^+ \tup{v}}$. Removing an element from an independent set leaves it independent, so $S\setminus\set{v}$ is an independent set and this map is well-defined.) \\
This map, too, has an inverse map, which is the map
\begin{align*}
\set{\text{independent sets of } G\setminus\left( N^{+}\left( v\right) \right)} & \rightarrow \set{\text{independent sets } S \text{ of } G \text{ with } v\in S}, \\ T  & \mapsto T\cup\set{v} .
\end{align*}
(Here is why this map is well-defined: If $T$ is an independent set of $G \setminus \tup{N^+\tup{v}}$, then $T$ contains none of the neighbors of $v$. Thus, adding $v$ to $T$ keeps the set independent. Thus, $T\cup\set{v}$ is an independent set of $G$, and of course it satisfies $v \in T \cup \set{v}$.) Thus, the map above is a bijection. Hence,
\begin{align}
\vert\set{\text{independent sets } S \text{ of } G \text{ with } v \in S}\vert
&= \abs{\set{\text{independent sets of } G\setminus \left( N^{+}\left( v\right) \right)} } \nonumber \\
&=\operatorname*{ind}\left( G\setminus\left( N^{+}\left( v\right)\right)\right) .
\label{eq.3}
\end{align}
Now, \eqref{eq.1} becomes
\begin{align*}
\operatorname*{ind}G
&= \vert\set{\text{independent sets } S \text{ of } G \text{ with } v \notin S}\vert + \vert\set{ \text{independent sets } S \text{ of } G \text{ with } v \in S}\vert \\
&= \operatorname*{ind}\left(  G\setminus\left\{  v\right\}
\right)  +\operatorname*{ind}\left(  G\setminus\left(  N^{+}\left(  v\right)
\right)  \right)
\end{align*}
(by \eqref{eq.2} and \eqref{eq.3}). This is Proposition~\ref{mt3.3.5}.
\end{proof}
\subsection{Solution to part b)}
Recall that the Fibonacci sequence $\tup{f_0,f_1,f_2,\ldots}$ is defined recursively by
\[
 f_0 = 0, \qquad f_1 = 1, \qquad \text{and } f_n = f_{n-1} + f_{n-2} \text{ for all } n \geq 2 .
\]

\begin{lemma}
\label{mt3.3.6} Let $n\in\NN$. Then,
\[
 \operatorname*{ind}\left( P_{n}\right) = f_{n+2} .
\]
\end{lemma}
\begin{proof}
In the $n$-th path graph, two vertices are neighbors if and only if they are consecutive integers. For this reason, an independent set of $P_{n}$ is the same as a subset of $V=\left[ n\right]$ that contains no two consecutive integers. This is what we called a lacunar subset of $\left[ n\right]$. Hence,
\[
 \operatorname*{ind}\left( P_{n}\right) = \left(\text{the number of lacunar subsets of }\left[ n\right]\right) .
\]
But by Proposition 1.22 from \href{http://www.cip.ifi.lmu.de/~grinberg/t/18s/4707-2018feb5.pdf}{the February 5 lecture}, the number of lacunar subsets of $\left[ n\right]$ is $f_{n+2}$. Combining the above, we obtain
\[
 \operatorname*{ind}\left( P_{n}\right) = \left(\text{the number of lacunar subsets of }\left[ n\right]\right) = f_{n+2} .
\]
\end{proof}
\begin{proposition}
Let $n\geq 2$. Then,
\[
 \operatorname*{ind}\left( C_{n}\right) = f_{n+1} + f_{n-1} .
\]
\end{proposition}
\begin{proof}
We WLOG assume that $n \geq 3$, since the case $n = 2$ can be dealt with easily by hand.
Proposition \ref{mt3.3.5} applied to $v=n$ gives
\[
 \operatorname*{ind}\left( C_{n}\right) =\operatorname*{ind}\left(C_{n}\setminus\set{n}\right) + \operatorname*{ind}\left(C_{n}\setminus\left( N^{+}\left( n\right)\right)\right) .
\]
The graph $C_{n}\setminus\set{n}$ is the graph $C_{n}$ with vertex $n$ and edges $\set{n-1, n}$ and $\set{n, 1}$ removed. This is the graph $P_{n-1}$. By Lemma~\ref{mt3.3.6} (applied to $n-1$ instead of $n$), we have
\[
 \operatorname*{ind}\left( P_{n-1}\right) = f_{n+1} .
\]
In $C_{n}$, the neighbors of $n$ are the vertices $1$ and $n-1$, and so the set $N^{+}\left( n\right)$ is $\set{1,n-1,n}$. Thus, the graph $C_{n}\setminus\left( N^{+}\left( n\right)\right)$ is $C_{n}$ with the vertices $1$, $n-1$, and $n$ removed, as well as their connected edges $\set{n-2, n-1}$, $\set{n-1, n}$, $\set{n, 1}$ and $\set{1, 2}$ removed. After relabeling the remaining vertices $2, 3, \ldots, n-2$ as $1, 2, \ldots, n-3$, this graph becomes $P_{n-3}$. Thus, $\operatorname*{ind}\left(C_{n}\setminus\left( N^{+}\left( n\right)\right)\right) = \operatorname*{ind}\left(P_{n-3}\right)$. But by Lemma \ref{mt3.3.6} (applied to $n-3$ instead of $n$), we have
\[
 \operatorname*{ind}\left( P_{n-3}\right) = f_{n-1} .
\]
So,
\begin{align*}
 \operatorname*{ind}\left( C_{n}\right)
 &= \operatorname*{ind}\left(\underbrace{C_{n}\setminus\set{n}}_{= P_{n-1}}\right) + \underbrace{\operatorname*{ind}\left(C_{n}\setminus\left( N^{+}\left( n\right)\right)\right)}_{= \operatorname*{ind}\left(P_{n-3}\right)} \\
 &= \underbrace{\operatorname*{ind}\left(P_{n-1}\right)}_{= f_{n+1}} + \underbrace{\operatorname*{ind}\left(P_{n-3}\right)}_{= f_{n-1}} = f_{n+1} + f_{n-1} .
\end{align*}
\end{proof}
\end{document}
