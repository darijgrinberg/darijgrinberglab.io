% Like most advanced LaTeX files, this one begins with a lot of
% boilerplate. You don't need to understand (or even read) most of it.
% All you need to do is fill in your name, email address,
% and the number of the pset. (Search for "METADATA" to find the place
% for this.) Then, you can go straight to the "EXERCISE 1"
% section and start writing your solutions.
% The "VARIOUS USEFUL COMMANDS" section is probably worth taking a
% look at at some point.

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw graphs.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).
\usepackage{tabls}

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\rev}{\operatorname{rev}} % reversal of a walk
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\verts}[1]{\operatorname{V}\left( #1 \right)}
% $\verts{...}$ compiles to V(...) (vertex set of a graph/digraph).
\newcommand{\edges}[1]{\operatorname{E}\left( #1 \right)}
% $\edges{...}$ compiles to E(...) (edge set of a graph).
\newcommand{\arcs}[1]{\operatorname{A}\left( #1 \right)}
% $\arcs{...}$ compiles to A(...) (arc set of a digraph).
\newcommand{\lf}[2]{#1^{\underline{#2}}}
% $\lf{...1}{...2}$ compiles to $...1^{\underline{...2}}$.
% This is a notation for the falling factorial.
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\newcommand{\nnn}{\nonumber\\} % Don't number this line in an "align" environment, and move on to the next line.
\newcommand{\explain}[1]{\tup{\hspace{-0.5pc}\text{\begin{tabular}{c}#1\end{tabular}\hspace{-0.5pc}}}}
% $\explain{...}$ interprets the "..." as text (you can
% nest some math inside by putting it in $...$'s as you
% would in any other text block) and puts it in parentheses.

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{Darij Grinberg} % ENTER YOUR NAME HERE
\newcommand{\mymail}{darij.grinberg@drexel.edu} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{1} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to homework set \#\psetnumber} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{Drexel University, Department of Mathematics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 222: Enumerative Combinatorics, \\
Fall 2019:
Homework \psetnumber\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}

\begin{document}

\maketitle % This command causes the title part to be printed.

\begin{center} % Delete this if you want to save space!
{\vspace{-1pc} \large due date: \textbf{Monday, 2019-10-07} at the beginning of class, \\
or before that by email or Blackboard.

Please solve \textbf{as many exercises as you can}!}
\end{center}

%----------------------------------------------------------------------------------------
%	EXERCISE 1
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 1}

\subsection{Problem}

Let us define a slight variation on domino tilings.
We shall use the notations of \cite[\S 1.1]{notes}.

A \textit{$2 \times 2$-rectangle} will mean a set of the form
$\set{ \tup{i, j}, \tup{i, j+1}, \tup{i+1, j}, \tup{i+1, j+1} }$
for some $i, j \in \ZZ$.
(Visually, this is just a set of $4$ mutually adjacent
squares forming a $2 \times 2$-rectangle.)

A \textit{pseudomino} will mean a set of squares that is either
a domino or a $2 \times 2$-rectangle.

If $S$ is a set of squares, then a \textit{pseudomino tiling}
of $S$ will mean a set of disjoint pseudominos whose union
is $S$.

For example, here are all five pseudomino tilings of the rectangle
$R_{3, 2}$:
\begin{align*} % Very ugly hacks, please don't imitate.
& \begin{tabular}[c]{|l|l|l|}\hline
\phantom{w} & \phantom{w} & \phantom{w}\\\cline{4-4}%
\phantom{w} & \phantom{w} & \phantom{w}\\\hline
\end{tabular}
\ \ \ \ ,\qquad%
\begin{tabular}[c]{|ll|l|}\hline
\phantom{w} & \phantom{w} & \phantom{w}\\\cline{1-2}%
\phantom{w} & \phantom{w} & \phantom{w}\\\hline
\end{tabular}
\ \ \ \ ,\qquad%
\begin{tabular}[c]{|l|ll|}\hline
\phantom{w} & \phantom{w} & \phantom{w}\\\cline{2-3}%
\phantom{w} & \phantom{w} & \phantom{w}\\\hline
\end{tabular}
\ \ \ \ , \\
&  \begin{tabular}[c]{|ll|l|}\hline
\phantom{w} & \phantom{w} & \phantom{w}\\\cline{4-4}%
\phantom{w} & \multicolumn{1}{l|}{\phantom{w}} & \phantom{w}\\\hline
\end{tabular}
\ \ \ \ ,\qquad%
\begin{tabular}[c]{|l|ll|}\hline
\phantom{w} & \phantom{w} & \phantom{w}\\\cline{4-4}%
\phantom{w} & \phantom{w} & \phantom{w}\\\hline
\end{tabular}
\ \ \ .
\end{align*}

For any $n \in \NN$, we let $p_n$ denote the number of all pseudomino
tilings of the rectangle $R_{n, 2}$.

[\textit{Example:} We have $p_0 = 1$, $p_1 = 1$, $p_2 = 3$, $p_3 = 5$.]

\begin{enumerate}

\item[\textbf{(a)}]
Find a recursive formula that expresses $p_n$ in terms of
$p_{n-1}$ and $p_{n-2}$ when $n \geq 2$.

\item[\textbf{(b)}] Prove that
$p_n = \dfrac{ \tup{-1}^n + 2^{n+1} }{3}$
for each $n \in \NN$.

\end{enumerate}

[\textbf{Hint:} You don't need to be more detailed than in the proof
of \cite[Proposition 1.1.9]{notes}.]

\subsection{Solution}

[...] % Enter your solution here!

%----------------------------------------------------------------------------------------
%	EXERCISE 2
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 2}

\subsection{Problem}

Again, we shall use the notations of \cite[\S 1.1]{notes}.

An \textit{L-tromino} will mean a set of squares that has one
of the four forms
\[
\begin{tabular}[c]{|cc}\hline
\phantom{w} & \multicolumn{1}{c|}{\phantom{w}}\\\cline{2-2}%
\phantom{w} & \multicolumn{1}{|c}{\phantom{w}}\\\cline{1-1}%
\end{tabular}
\ \ \ ,\ \ \
\begin{tabular}[c]{|cc}\cline{1-1}%
\phantom{w} & \multicolumn{1}{|c}{\phantom{w}}\\\cline{2-2}%
\phantom{w} & \multicolumn{1}{c|}{\phantom{w}}\\\hline
\end{tabular}
\ \ \ ,\ \ \
\begin{tabular}[c]{cc|}\hline
\multicolumn{1}{|c}{\phantom{w}} & \phantom{w}\\\cline{1-1}%
\phantom{w} & \multicolumn{1}{|c|}{\phantom{w}}\\\cline{2-2}%
\end{tabular}
\ \ \ ,\ \ \
\begin{tabular}[c]{cc|}\cline{2-2}%
\phantom{w} & \multicolumn{1}{|c|}{\phantom{w}}\\\cline{1-1}%
\multicolumn{1}{|c}{\phantom{w}} & \phantom{w}\\\hline
\end{tabular}
\ \ \ .
\]
(Formally speaking, it is a set of the form
$\set{ \tup{i, j}, \tup{i', j}, \tup{i, j'} }$,
where $i, j \in \ZZ$ and $i' \in \set{i-1, i+1}$
and $j' \in \set{j-1, j+1}$.)

If $S$ is a set of squares, then an \textit{L-tromino tiling}
of $S$ will mean a set of disjoint L-trominos whose union is $S$.

For any $n \in \NN$, we let $L_n$ denote the number of L-tromino
tilings of the rectangle $R_{n, 2}$.

We shall use the Iverson bracket notation\footnote{This
means the following:

If $\mathcal{A}$ is any statement (such as ``$1 + 1 = 2$''
or ``$1 + 1 = 1$'' or ``there exist infinitely many primes''),
then $\ive{\mathcal{A}}$ stands for the number
\[
\begin{cases}
1, & \text{ if } \mathcal{A} \text{ is true} ; \\
0, & \text{ if } \mathcal{A} \text{ is false} .
\end{cases}
\]
This number belongs to $\set{0, 1}$, and
is called the \textit{truth value} of $\mathcal{A}$.
For example,
\[
\ive{1 + 1 = 2} = 1, \qquad
\ive{1 + 1 = 1} = 0, \qquad
\ive{\text{there exist infinitely many primes}} = 1.
\]
}.

Prove that
\[
L_n = \ive{3 \mid n} \cdot 2^{n/3} \qquad
\text{for each $n \in \NN$.}
\]

[\textbf{Hint:} Feel free to take inspiration from
the solution to \cite[Exercise 5]{18f-hw1s}.]

\subsection{Solution}

[...] % Enter your solution here!

%----------------------------------------------------------------------------------------
%	EXERCISE 3
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 3}

\subsection{Problem}

Again, we shall use the notations of \cite[\S 1.1]{notes}.

A \textit{horimino} shall mean a
rectangle of height $1$ and positive width
(i.e., formally speaking, a set of the form
$\set{ \tup{i, 1}, \tup{i+1, 1}, \ldots, \tup{j, 1} }$
for some integers $i \leq j$).

If $S$ is a set of squares, then a \textit{horimino tiling}
of $S$ will mean a set of disjoint horiminos whose union
is $S$.

For example, here are all four horimino tilings of the rectangle
$R_{3, 1}$:
\[
\begin{tabular}[c]{|ccc|} \hline
\phantom{w} & \phantom{w} & \phantom{w} \\\hline
\end{tabular}
\ \ \ ,\ \ \
\begin{tabular}[c]{|c|cc|} \hline
\phantom{w} & \phantom{w} & \phantom{w} \\\hline
\end{tabular}
\ \ \ ,\ \ \
\begin{tabular}[c]{|cc|c|} \hline
\phantom{w} & \phantom{w} & \phantom{w} \\\hline
\end{tabular}
\ \ \ ,\ \ \
\begin{tabular}[c]{|c|c|c|} \hline
\phantom{w} & \phantom{w} & \phantom{w} \\\hline
\end{tabular}
\ \ \ .
\]

Let $n \in \NN$.
Find a simple expression for the number of all
horimino tilings of the rectangle $R_{n, 1}$.

[\textbf{Hint:} Make sure your answer works for $n = 0$
(you might need to handle this case separately).]

\subsection{Solution}

[...] % Enter your solution here!

%----------------------------------------------------------------------------------------
%	EXERCISE 4
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 4}

\subsection{Problem}

Let $n \in \NN$.
Prove that
\[
0^2 \cdot 1!
+ 1^2 \cdot 2!
+ 2^2 \cdot 3!
+ \cdots
+ \tup{n-1}^2 \cdot n!
= \tup{n-2} \cdot \tup{n+1}! + 2 .
\]

\subsection{Solution}

[...] % Enter your solution here!

%----------------------------------------------------------------------------------------
%	EXERCISE 5
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 5}

\subsection{Problem}

Let $\tup{u_0, u_1, u_2, \ldots}$ be a sequence of
real numbers such that every $n \geq 1$ satisfies
\begin{align}
u_n = n u_{n-1} + \tup{-1}^n .
\label{eq.exe.derangements-u.1}
\end{align}

Prove that $u_n = \tup{n-1} \tup{u_{n-1} + u_{n-2}}$
for each $n \geq 2$.

\subsection{Remark}

This shows that the recurrence
$D_n = n D_{n-1} + \tup{-1}^n$
for the derangement numbers implies the
recurrence $D_n = \tup{n-1} \tup{D_{n-1} + D_{n-2}}$.

\subsection{Solution}

[...] % Enter your solution here!

%----------------------------------------------------------------------------------------
%	EXERCISE 6
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 6}

\subsection{Problem}

Let $n$ and $m$ be positive integers.

An $n$-tuple $\tup{i_1, i_2, \ldots, i_n} \in \set{0, 1, \ldots, m}^n$
is said to be \textit{even} if the sum $i_1 + i_2 + \cdots + i_n$ is even.
(For example, the $4$-tuple $\tup{1, 0, 4, 1}$ is even, whereas
$\tup{1, 0, 3, 1}$ is not.)

\begin{enumerate}

\item[\textbf{(a)}]
Find a formula for the number of all even $n$-tuples
$\tup{i_1, i_2, \ldots, i_n} \in \set{0, 1, \ldots, m}^n$
when $m$ is odd.

\item[\textbf{(b)}]
Find a formula for the number of all even $n$-tuples
$\tup{i_1, i_2, \ldots, i_n} \in \set{0, 1, \ldots, m}^n$
when $m$ is even.

\end{enumerate}

[\textbf{Hint:} Particular cases of this exercise
(for $m = 1, 2, 3$) were done in
\cite[Exercise 3]{hw0s} and \cite[Exercises 2 and 1]{18f-hw1s}.
Can you generalize some of that reasoning?]

\subsection{Solution}

[...] % Enter your solution here!


\begin{thebibliography}{99999999}                                                                                         %

% This is the bibliography: The list of papers/books/articles/blogs/...
% cited. The syntax is: "\bibitem[name]{tag}Reference",
% where "name" is the name that will appear in the compiled
% bibliography, and "tag" is the tag by which you will refer to
% the source in the TeX file. For example, the following source
% has name "Math222" (so you will see it referenced as
% "[Math222]" in the compiled PDF) and tag "notes" (so you
% can cite it by writing "\cite{notes}").

\bibitem[Math222]{notes}
Darij Grinberg,
\textit{Enumerative Combinatorics: class notes},
16 December 2019. \\
\url{http://www.cip.ifi.lmu.de/~grinberg/t/19fco/n/n.pdf} 
Also available on the mirror server
\url{http://darijgrinberg.gitlab.io/t/19fco/n/n.pdf} \\
\textbf{Caution:}
The numbering of theorems and formulas in this link might shift when the
project gets updated; for a ``frozen'' version whose
numbering is guaranteed to match that in the citations above, see
\url{https://gitlab.com/darijgrinberg/darijgrinberg.gitlab.io/blob/2dab2743a181d5ba8fc145a661fd274bc37d03be/public/t/19fco/n/n.pdf}
% Check that this is the numbering you want.

\bibitem[18f-hw1s]{18f-hw1s}
Darij Grinberg,
\textit{UMN Fall 2018 Math 5705 homework set \#1 with solutions},
\url{http://www.cip.ifi.lmu.de/~grinberg/t/18f/hw1s.pdf}
% See also http://www.cip.ifi.lmu.de/~grinberg/t/18f/hw1s.tex
% for the sourcecode and http://www.cip.ifi.lmu.de/~grinberg/t/18f/
% for the course.

\bibitem[hw0s]{hw0s}
Darij Grinberg,
\textit{Drexel Fall 2019 Math 222 homework set \#0 with solutions},
\url{http://www.cip.ifi.lmu.de/~grinberg/t/19fco/hw0s.pdf}

\end{thebibliography}

\end{document}

