<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Enumerative Combinatorics (Math 222), Fall 2019">
  <meta name="keywords" content="mathematics, enumerative combinatorics, teaching, courses, drexel, math 222">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Enumerative Combinatorics (Math 222), Fall 2019</title>
</head>
<body>

<h1 style="text-align:center">Math 222: Enumerative Combinatorics, Fall 2019 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd>Classes are over! <!--Monday 11:00--11:50, Wednesday 11:00--11:50, Friday 11:00--11:50 in Academic Building #107.--> </dd>
  <dt>Office hours:</dt>
  <dd>Tuesday 14:00--16:00, Thursday 13:00--14:00 in Korman Center #263. Also by appointment.</dd>
  <dt>Blackboard:</dt>
  <dd><a href="https://learn.dcollege.net/webapps/blackboard/execute/launcher?type=Course&id=_245269_1&url=">Blackboard site for Math 222</a>.</dd>
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
  <dt>Mailbox:</dt>
  <dd>Mathematics Common Room (Korman Center, 2nd floor).
</dl>
</div>

<h2>Course description</h2>

<p>An introduction to enumerative combinatorics covering binomial coefficients, bijective proofs, the twelve-fold way, inclusion-exclusion, permutations, partitions and generating functions. Emphasis on proof writing.</p>

<p><b>Prerequisites:</b> Math 220. </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Required:</dt>
  <dd><ul>
         <li> <a href="n/n.pdf"><cite>Lecture notes</cite></a> that will be updated as the course progresses. (Source code <a href="http://www.cip.ifi.lmu.de/~grinberg/t/19fco/n/">on LMU servers</a> and <a href="https://gitlab.com/darijgrinberg/darijgrinberg.gitlab.io/tree/master/public/t/19fco/n">on GitLab</a>.) </li>
      </ul> </dd>
  <dt>Recommended:</dt>
  <dd><ul>
         <li> <a href="https://www.cip.ifi.lmu.de/~grinberg/t/18f/"><cite>Fall 2018 Math 5705 at University of Minnesota</cite></a>, a similar class I taught a year ago and will mostly follow. </li>
         <li> [Loehr]: Nicholas Loehr, <cite>Bijective Combinatorics</cite>, Chapman & Hall/CRC 2011: comprehensive and high-quality textbook, goes much further into the subject than we will. </li>
         <li> <a href="https://math.stackexchange.com/questions/1454339/undergrad-level-combinatorics-texts-easier-than-stanleys-enumerative-combinator/1454420#1454420">A list of undergraduate-level texts and notes on enumerative combinatorics</a> (including many freely accessible ones). Warning: various difficulties and levels of sophistication. </li>
         <li> Previous courses (with homework solutions): <a href="../18f">Fall 2018</a>, <a href="../18s">Spring 2018</a>, <a href="../17f">Fall 2017</a>. </li>
      </ul> </dd>
  <dt>Remedial:</dt>
  <dd><ul>
         <li> <a href="https://courses.csail.mit.edu/6.042/spring18/mcs.pdf">[LeLeMe]: Eric Lehman, F. Thomson Leighton, Albert R. Meyer, <cite>Mathematics for Computer Science</cite>, 2018</a>: a great introduction to rigorous mathematics. Chapters 1--5 cover the Math 220 basics we will need: proofs, sets, relations, in/sur/bijections. Later chapters go into topics, even covering a bit of what we will do in this course. </li>
         <li> <a href="https://infinitedescent.xyz">[Newstead]: Clive Newstead, <cite>An Infinite Descent into Pure Mathematics</cite></a>: introduction to proofs and mathematical thinking. (Work in progress.) </li>
         <li> <a href="https://www.people.vcu.edu/~rhammack/BookOfProof/">Richard Hammack, <cite>Book of Proof</cite></a>: introduction to proofs. </li>
         <li> <a href="https://math.stackexchange.com/questions/3316114/book-recommendation-for-proof/3316157#3316157">A list of undergraduate-level texts and notes on proofs</a> (freely accessible). </li>
      </ul> </dd>
  <dt>Further:</dt>
  <dd><ul>
         <li> <a href="https://www.cip.ifi.lmu.de/~grinberg/t/17f/60610lectures2017-Galvin.pdf">[Galvin]: David Galvin, <cite>Basic Discrete Mathematics</cite></a>: graduate-level introduction, built around the idea of generating functions. </li>
         <li> <a href="http://www.cip.ifi.lmu.de/~grinberg/primes2015/sols.pdf">[detnotes]: Darij Grinberg, <cite>Notes on the combinatorial fundamentals of algebra</cite></a>: detailed introduction to permutations and determinants; will be cited occasionally for some details we omit. </li>
      </ul> </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  <dt>Week 1:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Domino tilings, basic enumeration principles. </li>
         <li> <a href="hw0s.pdf">"Homework set 0" with solutions</a> (<a href="hw0s.tex">source code</a>).
              This contains three sample exercises with fairly detailed solutions (more detailed than you need to write).
              The source code contains instructions on the use of LaTeX. </li>
         <li> <a href="hw1.pdf">Homework set 1</a> (<a href="hw1.tex">source code</a>) with <a href="hw1s.pdf">solutions</a> (<a href="hw1s.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 2:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Sums of powers, binomial coefficient basics. </li>
         <li> <a href="hw2.pdf">Homework set 2</a> (<a href="hw2.tex">source code</a>) with <a href="hw2s.pdf">solutions</a> (<a href="hw2s.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 3:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Counting basics. </li>
      </ul> </dd>
  <dt>Week 4:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Counting basics, more binomial coefficients. </li>
         <li> <a href="mt1.pdf">Midterm 1</a> (<a href="mt1.tex">source code</a>) with <a href="mt1s.pdf">solutions</a> (<a href="mt1s.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 5:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Counting injections and surjections, Vandermonde convolution. </li>
         <li> <a href="hw3.pdf">Homework set 3</a> (<a href="hw3.tex">source code</a>) with <a href="hw3s.pdf">solutions</a> (<a href="hw3s.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 6:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> More binomial coefficients, inclusion/exclusion. </li>
      </ul> </dd>
  <dt>Week 7:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Compositions, multisets, the twelvefold way. </li>
         <li> <a href="mt2.pdf">Midterm 2</a> (<a href="mt2.tex">source code</a>) with <a href="mt2s.pdf">solutions</a> (<a href="mt2s.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 8:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> The twelvefold way, partitions (introduction). </li>
         <li> <a href="hw4.pdf">Homework set 4</a> (<a href="hw4.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 9:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Permutations. </li>
         <li> <a href="mt3.pdf">Midterm 3</a> (<a href="mt3.tex">source code</a>) with <a href="mt3s.pdf">solution sketches</a> (<a href="mt3s.tex">source code</a>). </li>
      </ul> </dd>
  <dt>Week 10:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Permutations, odds and ends (only Monday). </li>
      </ul> </dd>
  <dt>Week 11:</dt>
  <dd><ul>
         <li> <strong>Material covered:</strong> Generating functions, odds and ends. </li>
         <li> <a href="../18s/4707-2018may2.pdf">Slides from the last class</a>. </li>
      </ul> </dd>
</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 40%: homework sets (the lowest score will be dropped if all homework sets are submitted). </li>
         <li> 20%: midterm 1. </li>
         <li> 20%: midterm 2. </li>
         <li> 20%: midterm 3. </li>
      </ul>
      There will be no final exam. </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A: (80%, 100%]. </li>
         <li> B: (60%, 80%]. </li>
         <li> C: (40%, 60%]. </li>
         <li> D: [0%, 40%]. </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and acknowledge all sources that you used. </li>
         <li> Asking outsiders (anyone apart from Math 222 students and Drexel staff) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> Late homework will <strong>not</strong> be accepted. (But keep in mind that the lowest homework score will be dropped.) </li>
         <li> Solutions have to be either handed in at the beginning of class, or submitted electronically (via Blackboard) in a format I can read (PDF, TeX or plain text if it works; <strong>no doc/docx!</strong>). If you submit a PDF, make sure that it is readable and does not go over the margins. </li>
         <li> This is a proof-based class. All claims have to be proven, unless they were proven in class or are sufficiently elementary to be taken for granted at this level (e.g., uniqueness of prime factorization). Even if the problem asks "find X satisfying A", you will have to prove that your X indeed satisfies A. Solutions will be graded based on both correctness and readability. </li>
      </ul> </dd>
  <dt>Midterm policy:</dt>
  <dd><ul>
         <li> Late midterms will <strong>not</strong> be accepted unless agreed in advance and with serious justification. </li>
         <li> Everything else is the same as for homework (yes, midterms are take-home), but <strong>collaboration is not allowed</strong> on midterms.</li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>Students will understand and be able to reason about the staples of enumerative combinatorics such as binomial coefficients, permutations, and partitions. Students will have some familiarity with bijective, inductive and algebraic proofs of combinatorial identities, and will have seen various approaches to solving enumeration problems. </dd>
</dl>
</div>

<!--

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <dt>Homework help:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/coas/academics/departments-centers/mathematics/math-resource-center/">Math Resource Center</a> (Korman #247). </li>
      </ul> </dd>
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/oed/disabilityResources/students/">Disability resources for students</a>. </li>
      </ul> </dd>
  <dt>Writing center:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/coas/academics/departments-centers/english-philosophy/university-writing-program/drexel-writing-center/">Drexel Writing Center</a> (including English language tutoring). </li>
      </ul> </dd>
</dl>
</div>

-->

<hr>

<p><a href="../">Back to Darij Grinberg's teaching page</a>. </p>

</body>
