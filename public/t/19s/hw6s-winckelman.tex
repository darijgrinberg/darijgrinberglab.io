% Like most advanced LaTeX files, this one begins with a lot of
% boilerplate. You don't need to understand (or even read) most of it.
% All you need to do is fill in your name, UMN ID, email address,
% and the number of the pset. (Search for "METADATA" to find the place
% for this.) Then, you can go straight to the "EXERCISE 1"
% section and start writing your solutions.
% The "VARIOUS USEFUL COMMANDS" section is probably worth taking a
% look at at some point.

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage{mathdots} % for \iddots
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw graphs.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\DD}{{\mathbb{D}}} % dual numbers
\newcommand{\PP}{\mathbb{P}} % positive integers
\newcommand{\KK}{\mathbb{K}} % notation for a ring
\newcommand{\LL}{\mathbb{L}} % notation for a ring
\newcommand{\MM}{\mathbb{M}} % notation for a ring
\newcommand{\FF}{\mathbb{F}} % notation for a field
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\op}{\operatorname{op}} % opposite ring
\newcommand{\tildot}{\left. \widetilde{\cdot} \right.}
% I use \tildot for an alternative multiplication in one of the
% exercises below.
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value; also, set of first n integers).
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
% $\floor{...}$ compiles to |_..._| (floor function).
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\mapeq}[1]{\underset{#1}{\equiv}}
% $\mapeq{f}$ yields $\underset{f}{\equiv}$.
% This is the "equality upon applying $f$" relation.
\newcommand{\eps}{\varepsilon}
% Just a shorthand for a commonly-used symbol.
\newcommand{\N}{\operatorname{N}} % norm of a complex number
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\newcommand{\nnn}{\nonumber\\} % Don't number this line in an "align" environment, and move on to the next line.

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{Tom Winckelman (edited by Darij Grinberg)} % ENTER YOUR NAME HERE
\newcommand{\myid}{} % ENTER YOUR UMN ID HERE
\newcommand{\mymail}{} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{6} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to homework set \#\psetnumber} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname, \myid} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{University of Minnesota, School of Mathematics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 4281: Introduction to Modern Algebra, \\
Spring 2019:
Homework \psetnumber\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	EXERCISE 3
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 3: Entangled inverses}

Let $\KK$ be a ring.

A \textit{left inverse} of an element $x \in \KK$
is defined to be a $y \in \KK$ such that $yx = 1$.

A \textit{right inverse} of an element $x \in \KK$
is defined to be a $y \in \KK$ such that $xy = 1$.

Let $a$ and $b$ be two elements of $\KK$.
Prove the following:

\begin{enumerate}

\item[\textbf{(a)}] If $c$ is a left inverse of $1 - ab$,
then $1 + bca$ is a left inverse of $1 - ba$.

\item[\textbf{(b)}] If $c$ is a right inverse of $1 - ab$,
then $1 + bca$ is a right inverse of $1 - ba$.

\item[\textbf{(c)}] If $c$ is an inverse of $1 - ab$,
then $1 + bca$ is an inverse of $1 - ba$.

\end{enumerate}

Here and in the following, the word ``\textit{inverse}''
(unless qualified with an adjective) means
``multiplicative inverse''.

\subsection*{Solution}

\textbf{(a)} Assume that $c$ is a left inverse of $1 - ab$.
That is, $c(1-ab)=1$.
It follows that:\footnote{Here and in the following, when
we refer to ``distributivity'', we mean distributivity laws
in the wide sense of this word. This includes identities
like $u \tup{x+y+z} = ux + uy + uz$ and
$u \tup{x-y+z} = ux - uy + uz$. All of these identities
can easily be proven from the ring axioms and the definition
of subtraction.}
\begin{align*}
    &(1+bca)(1-ba)
\\
    &= (1-ba) +bca(1-ba)
    &\text{(by distributivity, since $\KK$ is a ring)}
\\
    &= 1-ba +bca-bcaba
    &\text{(by distributivity)}
\\
    &= 1+(-b)(a-ca+caba)
    &\text{(by distributivity)}
\\
    &= 1+(-b)(1-c+cab)a
    &\text{(by distributivity)}
\\
    &= 1+(-b)(1-c(1-ab))a
    &\text{(by distributivity)}
\\
    &= 1+(-b)(1-1)a
    &\text{(since $c(1-ab)=1$)}
\\
    &= 1+(-b)(0)a
    &\text{(since $-1$ is the additive inverse of $1$)}
\\
    &= 1 + 0
    &\text{(since zero annihilates)}
\\
    &= 1.
    &\text{(since zero is the neutral element of addition)}
\end{align*}
In other words, $1 + bca$ is a left inverse of $1 - ba$.
This solves part \textbf{(a)}.

\vspace{0.3cm}

\textbf{(b)} Assume that $c$ is a right inverse of $1 - ab$.
That is, $(1-ab)c=1$.
It follows that:
\begin{align*}
    &(1-ba)(1+bca)
\\
    &= (1+bca) - ba(1+bca)
    &\text{(by distributivity)}
\\
    &= 1+bca - ba - babca
    &\text{(by distributivity)}
\\
    &= 1+b(ca - a - abca)
    &\text{(by distributivity)}
\\
    &= 1+b(c - 1 - abc)a
    &\text{(by distributivity)}
\\
    &= 1+b(c - abc - 1)a
    &\text{(by commutativity of addition, since $\KK$ is a ring)}
\\
    &= 1+b((1 - ab)c - 1)a
    &\text{(by distributivity)}
\\
    &= 1+b(1 - 1)a
    &\text{(since $(1 - ab)c=1$)}
\\
    &= 1+b(0)a
    &\text{(since $-1$ is the additive inverse of $1$)}
\\
    &= 1 + 0
    &\text{(since zero annihilates)}
\\
    &= 1.
    &\text{(since zero is the neutral element of addition)}
\end{align*}
In other words, $1 + bca$ is a right inverse of $1 - ba$.
This solves part \textbf{(b)}.

\vspace{0.3cm}

\textbf{(c)} Assume that $c$ is an inverse of $1 - ab$.
In other words, $c\tup{1-ab} = 1$ and $\tup{1-ab}c = 1$.
Hence, $c$ is a left inverse of $1-ab$ and $c$ is a right inverse of $1-ab$.
Therefore, parts \textbf{(a)} and \textbf{(b)} imply 
that $1 + bca$ is a left inverse of $1 - ba$ and $1 + bca$ is a right inverse of $1 - ba$. In other words,
$$
    (1+bca)(1-ba)=1=(1-ba)(1+bca).
$$
Therefore, by the definition of an inverse, $1 + bca$ is an inverse of $1 - ba$.
This solves part \textbf{(c)}.
\vspace{3mm}


%----------------------------------------------------------------------------------------
%	EXERCISE 4
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 4: Composition of ring homomorphisms}

\subsection*{Problem}

Let $\KK$, $\LL$ and $\MM$ be three rings.
Prove the following:

\begin{enumerate}
\item[\textbf{(a)}] If $f : \KK \to \LL$ and $g : \LL \to \MM$
are two ring homomorphisms, then $g \circ f : \KK \to \MM$
is a ring homomorphism.

\item[\textbf{(b)}] If $f : \KK \to \LL$ and $g : \LL \to \MM$
are two ring isomorphisms, then $g \circ f : \KK \to \MM$
is a ring isomorphism.
\end{enumerate}

\subsection*{Solution}

\textbf{(a)}
Let $f : \KK \to \LL$ and $g : \LL \to \MM$ be two ring homomorphisms.
\\
In order to prove that $g \circ f : \KK \to \MM$ is a ring homomorphism, we must prove four things:
\begin{align*}
    \textbf{(i)} &\quad \tup{g \circ f} (a+b) = \tup{g \circ f}(a) + \tup{g \circ f}(b) \quad \text{ for all } a,b\in\KK . \hspace{40mm}
\\
    \textbf{(ii)} &\quad \tup{g \circ f} (0_\KK) = 0_\MM . \hspace{40mm}
\\
    \textbf{(iii)} &\quad \tup{g \circ f} (ab) = \tup{g \circ f}(a) \cdot \tup{g \circ f}(b) \quad \text{ for all } a,b\in\KK . \hspace{40mm}
\\
    \textbf{(iv)} &\quad \tup{g \circ f} (1_\KK) = 1_\MM . \hspace{40mm}
\end{align*}
We begin by proving \textbf{(i)}. Fix arbitrary $a\in\KK$ and $b\in\KK$.
Thus, we have
$$
    f(a+b) = f(a) + f(b) ,
$$
since $f$ is a ring homomorphism.
Now, let us apply $g$ to both sides, yielding:
\begin{align}
    g\tup{ f(a+b)} = g\tup{f(a) + f(b)} .
    \label{homomorphism}
\end{align}
The left hand side of \eqref{homomorphism} is clearly equal to $\tup{g \circ f} (a+b)$
by the definition of $g \circ f$.
Since $g$ is a ring homomorphism, we obtain:
$$
    g\tup{f(a) + f(b)} = g\tup{f(a)} + g\tup{f(b)} = \tup{g \circ f}(a) + \tup{g \circ f}(b)
$$
(by the definition of $g \circ f$).
Hence, \eqref{homomorphism} rewrites as $\tup{g \circ f} (a+b) = \tup{g \circ f}(a) + \tup{g \circ f}(b)$.
Thus, \textbf{(i)} is proven.
The proof of \textbf{(iii)} is similar.

To see that \textbf{(ii)} is true, observe that $f(0_\KK) = 0_\LL$
(since $f$ is a ring homomorphism)
and $g(0_\LL) = 0_\MM$ (likewise).
Hence, $\tup{g \circ f}(0_\KK) = g \tup{ \underbrace{f(0_\KK)}_{= 0_\LL} } = g(0_\LL) = 0_\MM$.
This proves \textbf{(ii)}.
The proof of \textbf{(iv)} is similar.

Together, \textbf{(i)}, \textbf{(ii)}, \textbf{(iii)}, and \textbf{(iv)} imply that $g \circ f : \KK \to \MM$ is a ring homomorphism.
This solves part \textbf{(a)}.

\vspace{0.3cm}

\textbf{(b)} Let $f : \KK \to \LL$ and $g : \LL \to \MM$ be two ring isomorphisms.

Thus, $f$ and $g$ are invertible, and $f$, $g$, $f^{-1}$, and $g^{-1}$ are ring homomorphisms.
\\

From the fact that $f$ and $g$ are ring homomorphisms, we conclude using part \textbf{(a)} of this exercise that $g \circ f : \KK \to \MM$ is a ring homomorphism.

As well, from the fact that $f$ and $g$ are invertible,
we obtain that $g \circ f$ is invertible by well known properties of functions.

From the fact that $g^{-1}$ and $f^{-1}$ are ring homomorphisms, we conclude using part \textbf{(a)} of the exercise (applied to $\MM$, $\KK$, $g^{-1}$ and $f^{-1}$ instead of $\KK$, $\MM$, $f$ and $g$) that $f^{-1} \circ g^{-1} : \MM \to \KK$ is a ring homomorphism. In other words, $\tup{g \circ f}^{-1}$ is a ring homomorphism (since $\tup{g \circ f}^{-1} = f^{-1} \circ g^{-1}$). Thus, $g \circ f$ is an invertible ring homomorphism whose inverse $\tup{g \circ f}^{-1}$ is a ring homomorphism as well.
In other words, $g \circ f$ is a ring isomorphism.
This proves part \textbf{(b)}.

\vspace{3mm}

%----------------------------------------------------------------------------------------
%	EXERCISE 6
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section*{Exercise 6: The characteristic of a field}

\subsection*{Problem}

Let $\FF$ be a field.
Recall that we have defined $na$ to mean
$\underbrace{a + a + \cdots + a}_{n \text{ times}}$ whenever
$n \in \NN$ and $a \in \FF$.

Assume that there exists a positive integer $n$ such that
$n \cdot 1_{\FF} = 0$.
Let $p$ be the \textbf{smallest} such $n$.

Prove that $p$ is prime.

[\textbf{Hint:}
$\tup{a \cdot 1_{\FF}} \cdot \tup{b \cdot 1_{\FF}}
= ab \cdot 1_{\FF}$
for all $a, b \in \NN$.]

\subsection*{Remark}

The $p$ we just defined is called the \textit{characteristic} of the field
$\FF$ when it exists. (Otherwise, the characteristic of the field
$\FF$ is defined to be $0$.)

Thus, for each prime $p$, the finite field $\ZZ / p$, as well as the
finite field of size $p^2$ that we constructed in class,
have characteristic $p$.

\subsection*{Solution}
In our definition of fields, we have required a field $\KK$ to satisfy $0_\KK \neq 1_\KK$. Thus, $0_\FF \neq 1_\FF$ (since $\FF$ is a field).

We have assumed that there exists a positive integer $n$ such that
$n \cdot 1_{\FF} = 0$.
Hence, by the well ordering property, the minimum
$$
    \min\set{n\in\ZZ^+ : n \cdot 1_\FF = 0} \quad \text{exists}
$$
(where $\ZZ^+$ denotes the set $\set{1, 2, 3, \ldots}$).
Let $m$ be this minimum. In other words, $m := \min\set{n\in\ZZ^+ : n \cdot 1_\FF = 0}$.
Then, $m \cdot 1_\FF = 0 = 0_\FF \neq 1_\FF = 1 \cdot 1_\FF$,
so that $m \neq 1$.
Therefore, $m > 1$ (since $m \in \ZZ^+$).

Of course, our $m$ is exactly the number that was denoted by $p$ in
the exercise. Hence, we need to prove that $m$ is prime.

Suppose that $m=ab$ for some $a,b\in\set{1,2,\ldots, m-1}$.
We shall derive a contradiction.
We have
\begin{align}
    \tup{a \cdot 1_\FF} \cdot \tup{b \cdot 1_\FF} = a \underbrace{\tup{1_\FF \cdot \tup{b \cdot 1_\FF}}}_{ = b \cdot 1_\FF }
    = a \tup{b \cdot 1_\FF} = \underbrace{ab}_{=m} \cdot 1_\FF = m \cdot 1_\FF = 0 .
    \nonumber
\end{align}
This implies that either $a \cdot 1_\FF = 0$ or $b \cdot 1_\FF = 0$.\ \ \ \ \footnote{Why? Recall that $\FF$ is a field. Thus, every nonzero element of $\FF$ is invertible.
Having $\tup{a \cdot 1_\FF} \cdot \tup{b \cdot 1_\FF} = 0$, let us suppose that $a \cdot 1_\FF$ and $b \cdot 1_\FF$ are both nonzero.
Hence, they are both invertible, since $\FF$ is a field. Hence, the following computation is valid:
$$
    \tup{b \cdot 1_\FF}^{-1} \cdot \tup{a \cdot 1_\FF}^{-1} \cdot \underbrace{\tup{a \cdot 1_\FF} \cdot \tup{b \cdot 1_\FF}}_{= 0} = \tup{b \cdot 1_\FF}^{-1} \cdot \tup{a \cdot 1_\FF}^{-1} \cdot 0,
$$
which clearly simplifies to $1_\FF = 0_\FF$, which contradicts $0_\FF \neq 1_\FF$.
This contradiction shows that our assumption was false. 
In other words, $\tup{a \cdot 1_\FF}$ and $\tup{b \cdot 1_\FF}$ are \textbf{not} both not equal to zero.
In other words, either $\tup{a \cdot 1_\FF}=0$ or $\tup{b \cdot 1_\FF}=0$.}
Assume WLOG that $a \cdot 1_\FF = 0$.
Thus, $a \in \set{n\in\ZZ^+ : n \cdot 1_\FF = 0}$.
However, $a<m$ (since $a \in \set{1,2,\ldots, m-1}$),
so this contradicts the fact that $m = \min\set{n\in\ZZ^+ : n \cdot 1_\FF = 0}$.
This contradiction shows that %our assumption was false. Hence,
there \textbf{do not} exist $a,b\in\{1,2,\ldots, m-1\}$ such that $m=ab$.
Hence, the only positive divisors of $m$ are $1$ and $m$
(since any other positive divisor of $m$ would be some
$a \in \set{1, 2, \ldots, m-1}$, and the corresponding ``complementary''
divisor $b := m/a$ would also belong to the set $\set{1, 2, \ldots, m-1}$,
which would yield that $a$ and $b$ are two elemnets of $\set{1, 2, \ldots, m-1}$
satisfying $m = ab$).
Hence, $m$ is prime (since $m > 1$).
This is precisely what we wanted to prove, only that we called it $m$ rather than $p$.
This solves the exercise.

\end{document}
