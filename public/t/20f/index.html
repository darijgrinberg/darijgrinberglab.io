<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Problem Solving (Math 235), Fall 2020">
  <meta name="keywords" content="mathematics, problem solving, olympiad mathematics, putnam, teaching, courses, drexel, math 235">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Mathematical Problem Solving (Math 235), Fall 2020</title>
</head>
<body>

<h1 style="text-align:center">Math 235: Mathematical Problem Solving, Fall 2020 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd><strong>Videos:</strong> up late Monday on this site (see Course Calendar below). <br> <strong>Recitations:</strong> Tue 3:00--3:40 PM (repeated Wed 3:30--4:10 PM) on
      <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>.</dd>
  <dt>Office hours:</dt>
  <dd>Fri 1:00--2:00 PM and Sun 1:00--2:00 PM on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>. Also by appointment.</dd>
  <dt>Notes:</dt>
  <dd><a href="http://www.cip.ifi.lmu.de/~grinberg/t/20f/mps.pdf"><cite>Notes on mathematical problem solving</cite></a>.</dd>
  <dt>Gradescope:</dt>
  <dd><a href="https://www.gradescope.com/courses/193396">https://www.gradescope.com/courses/193396</a>.</dd>
  <dt>Piazza:</dt>
  <dd><a href="https://piazza.com/drexel/fall2020/235">https://piazza.com/drexel/fall2020/235</a>.</dd>
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
  <!--
  <dt>Mailbox:</dt>
  <dd>Mathematics Common Room (Korman Center, 2nd floor).
  -->
</dl>
</div>

<h2>Course description</h2>

<p>An introduction to mathematical problem solving.
We will learn techniques and tools for solving problems of the kind that appear in mathematical competitions and journals.
These techniques (like induction, the Pigeonhole Principle, modular arithmetic or the Cauchy-Schwarz inequality) have uses all over mathematics; we will explore these uses through hands-on problem solving.
<br><br>
Each week will have approx. 50 minutes of video lectures (see the "Course materials" below for the links) and 40 minutes of recitations (i.e., collaborative problem-solving sessions over Zoom). Weekly homework sets will reinforce the training.
</p>

<p><strong>Prerequisites:</strong> Math 200. <!-- [Min Grade: D]. --> </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Required:</dt>
  <dd><ul>
         <li> <a href="http://www.cip.ifi.lmu.de/~grinberg/t/20f/mps.pdf">Darij Grinberg, <cite>Notes on mathematical problem solving</cite></a>: The notes for this class. They will grow as the class progresses. </li>
      </ul> </dd>
  <dt>Recommended:</dt>
  <dd><ul>
         <li> <a href="https://www.overleaf.com/project/5f0c79ee4d77640001dc6056">David Galvin, <cite>Math 43900 - Problem Solving in Math (Fall 2020)</cite></a>. A course similar to ours, with an ever-growing set of notes (allow javascript and note the "Download PDF" button). </li>
         <li> <a href="https://www.math.tamu.edu/~foucart/teaching/notes/PbSolving.pdf">Simon Foucart, <cite>Problem Solving</cite></a>. Math 235 notes from a few years ago. </li>
         <li> <a href="https://doi.org/10.1007/978-3-319-58988-6">Răzvan Gelca, Titu Andreescu, <cite>Putnam and Beyond</cite></a>, 2nd edition. Standard text for university competition training. At 857 pages, it goes far beyond what we can do in a quarter. </li>
         <li> <a href="https://doi.org/10.1007/978-1-4419-9854-5">Dušan Djukić, Vladimir Janković, Ivan Matić, Nikola Petrović, <cite>The IMO Compendium</cite></a>, 2nd edition. Collection of all problems ever suggested for the International Mathematical Olympiads, often with solutions. You can also get official solutions for recent shortlist problems from <a href="https://www.imo-official.org/problems.aspx">imo-official.org</a>. We aren't training for the IMO, but there is a lot of overlap between IMO and Putnam topics, and more importantly, IMO shortlists are among the best sources for high-quality problems. </li>
         <li> <a href="https://web.evanchen.cc/textbooks/OTIS-Excerpts.pdf">Evan Chen, <cite>OTIS Excerpts</cite></a>. Olympiad training lecture notes (with focus on high school contests, but lots of relevance to undergraduate ones too). </li>
         <li> <a href="https://doi.org/10.1007/b97682">Arthur Engel, <cite>Problem-Solving Strategies</cite></a>. One of the first books written explicitly for math contest training. Somewhat dated and not always well-written, but a venerable collection of problems sorted by theme, and a model for newer texts (at least as far as choice of topics is concerned). </li>
         <li> <a href="https://kskedlaya.org/putnam-archive/">Kiran Kedlaya et al., <cite>Putnam archive</cite></a>. Contains solutions to several years of Putnam exams. </li>
         <li> <a href="https://webee.technion.ac.il/people/aditya/www.kalva.demon.co.uk/">John Scholes aka kalva, <cite>Maths problems</cite></a>. Classical (early 2000s) collection of contest problems with (terse) solutions. </li>
         <li> <a href="https://www.cut-the-knot.org/">Alexander Bogomolny, <cite>Cut the Knot</cite></a>. Famous collection of maths puzzles, including various contest problems (most hidden behind the "and more..." links). Unfortunately, the Java applets no longer run on most browsers. Use the <a href="https://web.archive.org/">Wayback Machine</a> for some links that have disappeared. </li>
         <li> <a href="https://web.math.ucsb.edu/~padraic/notes.html">Padraic Bartlett, <cite>various courses</cite></a>. Lots of problem solving materials, very enjoyably written and split into bite-sized (mostly self-contained) pieces. </li>
         <li> <a href="https://mathcircle.berkeley.edu/circle-archives/"><cite>Berkeley Math Circle archives</cite></a>. Olympiad training worksheets on various levels. </li>
      </ul> </dd>
  <dt>Contests:</dt>
  <dd><ul>
         <li> <a href="https://www.maa.org/math-competitions/putnam-competition"><cite>Putnam Competition</cite></a>. Normally happens in December, but this time postponed until February 20, 2021 (if it is to happen at all). </li>
         <li> <a href="https://intranet.math.vt.edu/people/plinnell/Vtregional/"><cite>Virginia Tech Regional Mathematics Contest</cite></a>. Might happen in Spring. </li>
         <li> <a href="https://www.tandfonline.com/toc/uamm20/current"><cite>American Mathematical Monthly</cite></a>: journal with a problem section. </li>
         <li> <a href="https://www.tandfonline.com/toc/ucmj20/current"><cite>College Mathematics Journal</cite></a>: journal with a problem section. </li>
         <li> <a href="https://www.tandfonline.com/toc/umma20/current"><cite>Mathematics Magazine</cite></a>: journal with a problem section. </li>
         <li> <a href="https://cms.math.ca/publications/crux/"><cite>Crux Mathematicorum</cite></a>: high-school contest math journal with a problem section. </li>
      </ul> </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  <dt>Week 1:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/playlist?list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ">Video lectures</a> (corresponding to Chapter 2 of the notes). </li>
         <li> <a href="hw0s-template.tex">Homework set 0 solution template</a> (<a href="hw0s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 2:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=ZL7LH8UGVkQ&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=11">Video lectures</a> (corresponding to Sections 3.1--3.4 of the notes). </li>
         <li> <a href="hw1s-template.tex">Homework set 1 solution template</a> (<a href="hw1s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 3:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=dG7XwV1qn0U&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=16">Video lectures</a> (corresponding to Sections 3.5--4.4 of the notes). </li>
         <li> <a href="hw2s-template.tex">Homework set 2 solution template</a> (<a href="hw2s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 4:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=K-tDCdyo-dk&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=28">Video lectures</a> (corresponding to Sections 4.5--4.6 of the notes). </li>
         <li> <a href="hw3s-template.tex">Homework set 3 solution template</a> (<a href="hw3s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 5:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=s9zkBCtUBpI&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=35">Video lectures</a> (corresponding to Sections 4.7--4.8 of the notes). </li>
         <li> <a href="hw4s-template.tex">Homework set 4 solution template</a> (<a href="hw4s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 6:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=iWmljThSzeA&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=40">Video lectures</a> (corresponding to Sections 4.9--5.2 of the notes). </li>
         <li> <a href="hw5s-template.tex">Homework set 5 solution template</a> (<a href="hw5s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 7:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=m3MDeTtZEBE&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=45">Video lectures</a> (corresponding to Chapter 6 of the notes). </li>
         <li> <a href="hw6s-template.tex">Homework set 6 solution template</a> (<a href="hw6s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 8:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=mkIyMxaoJbU&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=53">Video lectures</a> (corresponding to Sections 7.1--7.4 of the notes). </li>
         <li> <a href="hw7s-template.tex">Homework set 7 solution template</a> (<a href="hw7s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 9:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=HuKUYEyh_2k&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=63">Video lectures</a> (corresponding to Sections 7.5--7.8 of the notes). </li>
         <li> Homework set 8 is not to be handed in. The problems are in the <a href="mps.pdf">notes</a> (Section 8.3). </li>
      </ul> </dd>
  <dt>Week 10:</dt>
  <dd><ul>
         <li> <a href="https://www.youtube.com/watch?v=DncXh-eny9E&list=PLirjeGfR5hVgS6IliAMhKUWeFqpX809EJ&index=71">Video lectures</a> (corresponding to Chapter 8 of the notes). </li>
         <li> <a href="hw9s-template.tex">Homework set 9 solution template</a> (<a href="hw9s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 100%: homework sets. (Homework set #0 is worth 20 points; homework set #8 is worth 0 points; each remaining homework set is worth 50 points.) </li>
         <!--
         <li> 30%: homework sets (the lowest score will be dropped if all homework sets are submitted). </li>
         -->
      </ul> </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A: (50%, 100%]. </li>
         <li> B: (40%, 50%]. </li>
         <li> C: (20%, 45%]. </li>
         <li> D: [0%, 20%]. </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and <strong>acknowledge all sources that you used</strong>. </li>
         <li> Asking outsiders (anyone apart from Math 235 students and Drexel staff) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> Late homework will <strong>not</strong> be accepted. <!-- (But keep in mind that the lowest homework score will be dropped.) --> </li>
         <li> Solutions have to be submitted electronically via Gradescope in a format I can read (PDF, TeX or plain text if it works; <strong>no doc/docx!</strong>). If you submit a PDF, make sure that it is readable and does not go over the margins. If there are problems with submission, send your work to me by email for good measure. </li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>Students should have obtained some hands-on experience solving competition-type mathematical problems. They should be aware of standard problem solving techniques in mathematics (such as the pigeonhole and extremal principles) and be familiar with examples of their application. </dd>
</dl>
</div>

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <dt>Homework help:</dt>
  <dd><ul>
         <li> <a href="https://drexel.zoom.us/meeting/register/tJUtdu-urTwqGtzv55C4K9CccaJjM7wPJ9xd">Math Resource Center</a> (Zoom registration link; use your Drexel email), open Mon-Thu: 10:00 am - 7:00 pm and Fri: 10:00 am - 4:00 pm. Starts September 23rd. </li>
      </ul> </dd>
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <li> <a href="recording-policy.html" target="_blank">Recording of Class Activities</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/incomplete_grades/" target="_blank">Incomplete Grade Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/grade-appeals/" target="_blank">Grade Appeals</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/oed/disabilityResources/students/">Disability resources for students</a>. </li>
      </ul> </dd>
  <dt>Writing center:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/coas/academics/departments-centers/english-philosophy/university-writing-program/drexel-writing-center/">Drexel Writing Center</a> (including English language tutoring). </li>
      </ul> </dd>
</dl>
</div>

</body>
