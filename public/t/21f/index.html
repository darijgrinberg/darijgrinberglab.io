<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Problem Solving (Math 235), Fall 2021">
  <meta name="keywords" content="mathematics, problem solving, olympiad mathematics, putnam, teaching, courses, drexel, math 235">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Mathematical Problem Solving (Math 235), Fall 2021</title>
</head>
<body>

<h1 style="text-align:center">Math 235: Mathematical Problem Solving, Fall 2021 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd><strong>Classes are over now!</strong></dd>
  <!--<dd>Tue 15:30 PM -- 16:50 PM in Alumni Engineering 279.</dd>-->
  <dt>Office hours:</dt>
  <dd>
  Tuesday noon--1PM in Korman 263. Friday 2PM--3PM in Korman 263. Sunday 3PM--5PM on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>. Also by appointment.
  <dt>Notes:</dt>
  <dd>See below for worksheets. See also <a href="../20f/mps.pdf">last year's <cite>Notes on mathematical problem solving</cite></a>, which we are not following but using this year.</dd>
  <dt>Gradescope:</dt>
  <dd><a href="https://www.gradescope.com/courses/313286">https://www.gradescope.com/courses/313286</a>.</dd>
  <dt>Piazza:</dt>
  <dd><a href="http://piazza.com/drexel/fall2021/math235">http://piazza.com/drexel/fall2021/math235</a>.</dd>
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
  <!--
  <dt>Mailbox:</dt>
  <dd>Mathematics Common Room (Korman Center, 2nd floor).
  -->
</dl>
</div>

<h2>Course description</h2>

<p>An introduction to mathematical problem solving.
We will learn techniques and tools for solving problems of the kind that appear in mathematical competitions and journals.
These techniques (like induction, the Pigeonhole Principle, modular arithmetic or the Cauchy-Schwarz inequality) have uses all over mathematics; we will explore these uses through hands-on problem solving.
<br><br>
A typical week will include some preparatory reading, a few problems solved together in class, and a homework set.
</p>

<p><strong>Prerequisites:</strong> Math 200. <!-- [Min Grade: D]. --> </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Recommended:</dt>
  <dd><ul>
         <li> <a href="../20f/mps.pdf">Darij Grinberg, <cite>Notes on mathematical problem solving</cite></a>: The notes for last year's Math 235 course. We will <strong>not</strong> follow them, but we will occasionally refer to them for background and additional examples. </li>
         <li> <a href="https://www.overleaf.com/project/5f0c79ee4d77640001dc6056">David Galvin, <cite>Math 43900 - Problem Solving in Math (Fall 2020)</cite></a>. A course similar to ours, with an ever-growing set of notes (allow javascript and note the "Download PDF" button). </li>
         <li> <a href="https://www.math.tamu.edu/~foucart/teaching/notes/PbSolving.pdf">Simon Foucart, <cite>Problem Solving</cite></a>. Math 235 notes from a few years ago. </li>
         <li> <a href="https://doi.org/10.1007/978-3-319-58988-6">Răzvan Gelca, Titu Andreescu, <cite>Putnam and Beyond</cite></a>, 2nd edition. Standard text for university competition training. At 857 pages, it goes far beyond what we can do in a quarter. </li>
         <li> <a href="https://doi.org/10.1007/978-1-4419-9854-5">Dušan Djukić, Vladimir Janković, Ivan Matić, Nikola Petrović, <cite>The IMO Compendium</cite></a>, 2nd edition. Collection of all problems ever suggested for the International Mathematical Olympiads, often with solutions. You can also get official solutions for recent shortlist problems from <a href="https://www.imo-official.org/problems.aspx">imo-official.org</a>. We aren't training for the IMO, but there is a lot of overlap between IMO and Putnam topics, and more importantly, IMO shortlists are among the best sources for high-quality problems. </li>
         <li> <a href="https://web.evanchen.cc/textbooks/OTIS-Excerpts.pdf">Evan Chen, <cite>OTIS Excerpts</cite></a>. Olympiad training lecture notes (with focus on high school contests, but lots of relevance to undergraduate ones too). </li>
         <li> <a href="https://doi.org/10.1007/b97682">Arthur Engel, <cite>Problem-Solving Strategies</cite></a>. One of the first books written explicitly for math contest training. Somewhat dated and not always well-written, but a venerable collection of problems sorted by theme, and a model for newer texts (at least as far as choice of topics is concerned). </li>
         <li> <a href="https://kskedlaya.org/putnam-archive/">Kiran Kedlaya et al., <cite>Putnam archive</cite></a>. Contains solutions to several years of Putnam exams. </li>
         <li> <a href="https://webee.technion.ac.il/people/aditya/www.kalva.demon.co.uk/">John Scholes aka kalva, <cite>Maths problems</cite></a>. Classical (early 2000s) collection of contest problems with (terse) solutions. </li>
         <li> <a href="https://www.cut-the-knot.org/">Alexander Bogomolny, <cite>Cut the Knot</cite></a>. Famous collection of maths puzzles, including various contest problems (most hidden behind the "and more..." links). Unfortunately, the Java applets no longer run on most browsers. Use the <a href="https://web.archive.org/">Wayback Machine</a> for some links that have disappeared. </li>
         <li> <a href="https://web.math.ucsb.edu/~padraic/notes.html">Padraic Bartlett, <cite>various courses</cite></a>. Lots of problem solving materials, very enjoyably written and split into bite-sized (mostly self-contained) pieces. </li>
         <li> <a href="https://mathcircle.berkeley.edu/circle-archives/"><cite>Berkeley Math Circle archives</cite></a>. Olympiad training worksheets on various levels. </li>
      </ul> </dd>
  <dt>Contests:</dt>
  <dd><ul>
         <li> <a href="https://www.maa.org/math-competitions/putnam-competition"><cite>Putnam Competition</cite></a>. Planned to happen on December 4th this year. </li>
         <li> <a href="https://intranet.math.vt.edu/people/plinnell/Vtregional/"><cite>Virginia Tech Regional Mathematics Contest</cite></a>. May or may not be held this year. </li>
         <li> <a href="https://www.tandfonline.com/toc/uamm20/current"><cite>American Mathematical Monthly</cite></a>: journal with a problem section. </li>
         <li> <a href="https://www.tandfonline.com/toc/ucmj20/current"><cite>College Mathematics Journal</cite></a>: journal with a problem section. </li>
         <li> <a href="https://www.tandfonline.com/toc/umma20/current"><cite>Mathematics Magazine</cite></a>: journal with a problem section. </li>
         <li> <a href="https://cms.math.ca/publications/crux/"><cite>Crux Mathematicorum</cite></a>: high-school contest math journal with a problem section. </li>
      </ul> </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  <dt>Note:</dt>
  <dd><ul>
         <li> See the <strong><a href="../23f/index.html">Fall 2023 edition of this course</a></strong> for a more polished version of Worksheets 1--5 and A below (with many more problems). </li>
      </ul> </dd>
  <dt>Week 1:</dt>
  <dd><ul>
         <li> <a href="lec1.pdf">Worksheet 1: Induction</a> (<a href="lec1.tex">source code</a>). </li>
         <li> <a href="hw1s-template.tex">Homework set 1 solution template</a> (<a href="hw1s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 2:</dt>
  <dd><ul>
         <li> <a href="lec2.pdf">Worksheet 2: Pigeonhole principle</a> (<a href="lec2.tex">source code</a>). </li>
         <li> <a href="hw2s-template.tex">Homework set 2 solution template</a> (<a href="hw2s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 3:</dt>
  <dd><ul>
         <li> <a href="lec3.pdf">Worksheet 3: Modular arithmetic</a> (<a href="lec3.tex">source code</a>). </li>
         <li> <a href="hw3s-template.tex">Homework set 3 solution template</a> (<a href="hw3s-template.pdf">compiled version</a>). </li>
  </ul> </dd>
  <dt>Week 4:</dt>
  <dd><ul>
         <li> <a href="lec4.pdf">Worksheet 4: Algebraic tricks</a> (<a href="lec4.tex">source code</a>). </li>
         <li> <a href="hw4s-template.tex">Homework set 4 solution template</a> (<a href="hw4s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 5:</dt>
  <dd><ul>
         <li> <a href="lec5.pdf">Worksheet 5: p-valuations</a> (<a href="lec5.tex">source code</a>). </li>
         <li> <a href="hw5s-template.tex">Homework set 5 solution template</a> (<a href="hw5s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 6:</dt>
  <dd><ul>
         <li> <a href="lec6.pdf">Worksheet 6: Graphs</a> (<a href="lec6.tex">source code</a>). </li>
         <li> <a href="hw6s-template.tex">Homework set 6 solution template</a> (<a href="hw6s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 7:</dt>
  <dd><ul>
         <li> <a href="lecA.pdf">Worksheet A: Assorted problems</a> (<a href="lecA.tex">source code</a>). </li>
      </ul> </dd>
  <!--
  <dt>Week 7:</dt>
  <dd><ul>
         <li> <a href="lec7.pdf">Worksheet 7: Finite fields</a> (<a href="lec7.tex">source code</a>). </li>
         <li> <a href="hw7s-template.tex">Homework set 7 solution template</a> (<a href="hw7s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 8:</dt>
  <dd><ul>
         <li> <a href="lec8.pdf">Worksheet 8: Linear algebra</a> (<a href="lec8.tex">source code</a>). </li>
         <li> <a href="hw8s-template.tex">Homework set 8 solution template</a> (<a href="hw8s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 9:</dt>
  <dd><ul>
         <li> <a href="lec9.pdf">Worksheet 9: Determinants</a> (<a href="lec9.tex">source code</a>). </li>
         <li> <a href="hw9s-template.tex">Homework set 9 solution template</a> (<a href="hw9s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  <dt>Week 10:</dt>
  <dd><ul>
         <li> <a href="lec10.pdf">Worksheet 10: Convexity</a> (<a href="lec10.tex">source code</a>). </li>
         <li> <a href="hw10s-template.tex">Homework set 10 solution template</a> (<a href="hw10s-template.pdf">compiled version</a>). </li>
      </ul> </dd>
  -->
</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 100%: homework sets. The lowest score will be dropped if all homework sets are submitted. </li>
      </ul> </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A: (50%, 100%]. </li>
         <li> B: (40%, 50%]. </li>
         <li> C: (20%, 40%]. </li>
         <li> D: [0%, 20%]. </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and <strong>acknowledge all sources that you used</strong>. </li>
         <li> Asking outsiders (anyone apart from Math 235 students and Drexel staff) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> Late homework will <strong>not</strong> be accepted. <!-- (But keep in mind that the lowest homework score will be dropped.) --> </li>
         <li> Solutions have to be submitted electronically via Gradescope in a format I can read (PDF, TeX or plain text if it works; <strong>no doc/docx!</strong>). If you submit a PDF, make sure that it is readable and does not go over the margins. If there are problems with submission, send your work to me by email for good measure. </li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>Students should have obtained some hands-on experience solving competition-type mathematical problems. They should be aware of standard problem solving techniques in mathematics (such as the pigeonhole and extremal principles) and be familiar with examples of their application. </dd>
</dl>
</div>

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <li> <a href="recording-policy.html" target="_blank">Recording of Class Activities</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/incomplete_grades/" target="_blank">Incomplete Grade Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/grade-appeals/" target="_blank">Grade Appeals</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/oed/disabilityResources/students/">Disability resources for students</a>. </li>
      </ul> </dd>
</dl>
</div>

<hr>

<p><a href="../">Back to Darij Grinberg's teaching page</a>. </p>

</body>
