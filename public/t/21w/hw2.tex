% LaTeX solution template for Math 533
% -----------------------------------------------------------------

% Like most advanced LaTeX files, this one begins with a lot of
% boilerplate. You don't need to understand (or even read) most of it.
% All you need to do is fill in your name, email address,
% and the number of the pset. (Search for "METADATA" to find the place
% for this.) Then, you can go straight to the "EXERCISE 1"
% section and start writing your solutions.
% The "VARIOUS USEFUL COMMANDS" section is probably worth taking a
% look at at some point.

% If you are here to learn about LaTeX, search for
% "START READING HERE" and start reading there.

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[all]{xy}  % Package for commutative diagrams.
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw commutative diagrams.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).

\usetikzlibrary{lindenmayersystems} % This is for drawing Sierpinski triangles.

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\calF}{\mathcal{F}}
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\diag}{\operatorname{diag}} % diagonal matrix
\newcommand{\ord}{\operatorname{ord}} % order of a group element
\newcommand{\rank}{\operatorname{rank}} % rank of a matrix
\newcommand{\Hom}{\operatorname{Hom}} % group of homomorphisms
\newcommand{\End}{\operatorname{End}} % ring of endomorphisms
\newcommand{\Iso}{\operatorname{Iso}} % set of isomorphisms
\newcommand{\GL}{\operatorname{GL}} % general linear group
\newcommand{\SL}{\operatorname{SL}} % special linear group
\newcommand{\Nil}{\operatorname{Nil}} % nilradical
\newcommand{\Jac}{\operatorname{Jac}} % Jacobson radical
\newcommand{\Tor}{\operatorname{Tor}} % torsion submodule or Tor functor
\newcommand{\Ext}{\operatorname{Ext}} % Ext functor
\newcommand{\Ker}{\operatorname{Ker}} % kernel
\newcommand{\Coker}{\operatorname{Coker}} % cokernel
\newcommand{\op}{\operatorname{op}} % opposite ring
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
% $\floor{...}$ compiles to |_..._| (floor function).
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
% $\ceiling{...}$ compiles to |_..._| (ceiling function).
\newcommand{\tildot}{\mathbin{\widetilde{\cdot}}}
% I use \tildot for an alternative multiplication in one of the
% exercises below.
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\newcommand{\nnn}{\nonumber\\} % Don't number this line in an "align" environment, and move on to the next line.
\newcommand{\explain}[1]{\tup{\hspace{-0.5pc}\text{\begin{tabular}{c}#1\end{tabular}\hspace{-0.5pc}}}}
% $\explain{...}$ interprets the "..." as text (you can
% nest some math inside by putting it in $...$'s as you
% would in any other text block) and puts it in parentheses.

% Arrows for normal use:
\newcommand{\mono}{\hookrightarrow} % Monomorphism/injection.
\newcommand{\epi}{\twoheadrightarrow} % Epimorphism/surjection.
\newcommand{\iso}{\overset{\cong}{\to}} % Epimorphism/surjection.

% Arrows for xymatrix:
\newcommand{\arinj}{\ar@{_{(}->}} % Monomorphism.
\newcommand{\arinjrev}{\ar@{^{(}->}} % Monomorphism.
\newcommand{\arsurj}{\ar@{->>}} % Epimorphism.
\newcommand{\arelem}{\ar@{|->}} % Arrow to be used in describing a map on elements.
\newcommand{\arback}{\ar@{<-}} % Backward arrow.

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}

%----------------------------------------------------------------------------------------
%	METADATA
%----------------------------------------------------------------------------------------
\newcommand{\myname}{Darij Grinberg} % ENTER YOUR NAME HERE
\newcommand{\mymail}{darij.grinberg@drexel.edu} % ENTER YOUR EMAIL HERE
\newcommand{\psetnumber}{2} % ENTER THE NUMBER OF THIS PSET HERE

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\ihead{Solutions to homework set \#\psetnumber} % Page header left
\ohead{page \thepage\ of \pageref{LastPage}} % Page header right
\ifoot{\myname} % left footer
\ofoot{\mymail} % right footer

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\title{	
\normalfont \normalsize 
\textsc{Drexel University, Department of Mathematics} \\ [15pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Math 533: Abstract Algebra I, \\
Winter 2021:
Homework \psetnumber\\% The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
Please solve \textbf{5 of the 10 problems!}
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}
\author{\myname}

\begin{document}

\maketitle % This command causes the title part to be printed.

%----------------------------------------------------------------------------------------
%	EXERCISE 1
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 1}

\subsection{Problem}

Let $R$ be a ring.
Let $a$ be a nilpotent element of $R$.
(Recall that ``nilpotent'' means that there exists some
$n \in \NN$ such that $a^n = 0$.)

\begin{itemize}

\item[\textbf{(a)}]
Prove that $1 - a \in R$ is a unit.

\item[\textbf{(b)}]
Let $u \in R$ be a unit satisfying $ua = au$.
Prove that $u - a \in R$ is a unit.

\end{itemize}

[\textbf{Hint:} Treat the geometric series
$\dfrac{1}{1-x} = 1+x+x^2+\cdots$ as an inspiration.
Nilpotent elements of a ring are an
algebraic analogue of the infamous ``sufficiently
small $\varepsilon > 0$'' from real analysis.
In particular, a nilpotent element $a$ can be
substituted (for $x$) into any formal power series
$r_0 + r_1 x + r_2 x^2 + \cdots$, since the resulting
sum will have only finitely many addends distinct from
$0$.
(You don't actually need to write any infinite sums in
your solution, but they can help you come up with
the solution in the first place.)]

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 2
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 2}

\subsection{Problem}

Let $R$ be a ring.
We define a new binary operation $\tildot$ on $R$ by setting
\[
a \tildot b = ba \qquad \text{for all } a, b \in R .
\]
(Thus, $\tildot$ is the multiplication of $R$, but with the
arguments switched.)

\begin{enumerate}

\item[\textbf{(a)}] Prove that the set $R$, equipped with the
addition $+$, the multiplication $\tildot$, the zero $0_R$
and the unity $1_R$, is a ring.

\end{enumerate}

\noindent This new ring is called the \textit{opposite ring} of $R$,
and is denoted by $R^{\op}$.

Note that the \textbf{sets} $R$ and $R^{\op}$ are identical
(so a map from $R$ to $R$ is the same as a map from
$R$ to $R^{\op}$);
but the \textbf{rings} $R$ and $R^{\op}$ are generally
not the same
(so a ring morphism from $R$ to $R$ is not the same as a
ring morphism from $R$ to $R^{\op}$).

\begin{enumerate}

\item[\textbf{(b)}] Prove that the identity map
$\id : R \to R$ is a ring isomorphism from $R$ to
$R^{\op}$ if and only if $R$ is commutative.

\item[\textbf{(c)}] Now, assume that $R$ is the matrix ring
$S^{n \times n}$ for some commutative ring $S$ and some
$n \in \NN$.
Prove that the map
\[
R \to R^{\op}, \qquad A \mapsto A^T
\]
(where $A^T$, as usual, denotes the transpose of a matrix $A$)
is a ring isomorphism.

\item[\textbf{(d)}] Forget about $S$, and let $R$ be an arbitrary
ring again.
Let $M$ be a right $R$-module.
Prove that $M$ becomes a left $R^{\op}$-module if we define
an action of $R^{\op}$ on $M$ by
\[
rm = mr \qquad \text{ for all $r \in R^{\op}$ and $m \in M$}.
\]
(Here, the left hand side is to be understood as the image
of $\tup{r, m}$ under the new action of $R^{\op}$ on $M$,
whereas the right hand side is the image of $\tup{m, r}$
under the original action of $R$ on $M$.)

\end{enumerate}

[\textbf{Hint:} There are many straightforward axioms to
check. Don't give too many details; one sentence per axiom
should suffice (e.g., in part \textbf{(d)}, you can say
``left distributivity for the left $R^{\op}$-module $M$
follows from right distributivity from the right
$R$-module $M$'', or even ``the distributivity axioms for
the new module boil down to the distributivity axioms for
the old module'').
In parts \textbf{(a)}, \textbf{(b)} and \textbf{(d)}, you
only have to check the axioms that have to do with
multiplication.
In \textbf{(c)}, you can use basic properties of transposes of
matrices without proof, as long as you say clearly which
properties you are using. You will need to use the
commutativity of $S$ in one place.]

\subsection{Remark}

Parts \textbf{(b)} and \textbf{(c)} of this
exercise gives some examples of rings $R$ that are isomorphic
to their opposite rings $R^{\op}$. See
\url{https://mathoverflow.net/questions/64370/} for examples of rings
that are not.

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 3
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 3}

\subsection{Problem}

Let $R$ be an integral domain.
Let $a \in R$ and $b \in R$.
Assume that $a$ and $b$ have an lcm $\ell \in R$.
Prove that $a$ and $b$ have a gcd $g \in R$,
which furthermore satisfies $g\ell = ab$.

[\textbf{Hint:} If $u$ and $v$ are two elements
of an integral domain $R$, with $v \neq 0$,
then you can use the notation $\dfrac{u}{v}$ (or
$u/v$) for the element $w \in R$ satisfying
$u = vw$. This element $w$ does not always exist,
but when it does, it is unique, so the notation
is unambiguous. It is also easy to see that
standard rules for fractions, such as
$\dfrac{u}{v} + \dfrac{x}{y} = \dfrac{uy + vx}{vy}$
and $\dfrac{u}{v} \cdot \dfrac{x}{y} = \dfrac{ux}{vy}$,
hold as long as the fractions $\dfrac{u}{v}$ and
$\dfrac{x}{y}$ exist.]

\subsection{Remark}

The converse is not true: The existence of a gcd
does not imply the existence of an lcm.

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 4
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 4}

\subsection{Problem}

Let $p$ be a prime number.

\begin{itemize}

\item[\textbf{(a)}]
Prove that if $a$ and $b$ are two integers such that
$a^2 \equiv b^2 \mod p^2$, then $a \equiv b \mod p^2$
or $a \equiv -b \mod p^2$
or $a \equiv b \equiv 0 \mod p$.

\item[\textbf{(b)}]
Compute the number of squares in the ring
$\ZZ / p^2$.

\end{itemize}

\subsection{Remark}

This is one more step on our quest to count the squares in
$\ZZ / n$ for an arbitrary positive integer $n$.

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 5
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 5}

\subsection{Problem}

Let $p$ be a prime number.

\begin{itemize}

\item[\textbf{(a)}]
Prove that the only units of the ring
$\ZZ / p$ that are their own inverses
(i.e., the only $m \in \tup{\ZZ / p}^\times$
that satisfy $m^{-1} = m$) are
$\overline{1}$ and $\overline{-1}$.

\item[\textbf{(b)}]
Assume that $p$ is odd.
Let $u = \dfrac{p-1}{2} \in \NN$.
Prove that $u!^2 \equiv -\tup{-1}^u \mod p$.

\end{itemize}

[\textbf{Hint:} The two parts of the exercise are
unrelated, other than both being lemmas in our
Lecture 7. For part \textbf{(b)}, recall Wilson's
theorem.]

\subsection{Remark}

Part \textbf{(b)} of this exercise easily yields
that $u!^2 \equiv -1 \mod p$ if $p \equiv 1 \mod 4$
(since $p \equiv 1 \mod 4$ entails that $u$ is even).
This is one of the facts we used in Lecture 7.]

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 6
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 6}

\subsection{Problem}

Recall the ring $\ZZ\ive{i}$ of Gaussian integers.
Let $N : \ZZ\ive{i} \to \NN$ be the map that sends
each Gaussian integer $z = a + bi \in \ZZ\ive{i}$
(with $a, b \in \ZZ$) to
$a^2 + b^2 = \abs{z}^2$.
(This is the Euclidean norm on $\ZZ\ive{i}$ that
we have already used several times.)

\begin{itemize}

\item[\textbf{(a)}]
Prove that if $z$ and $w$ are two Gaussian integers
satisfying $z \mid w$ in $\ZZ\ive{i}$,
then $N\tup{z} \mid N\tup{w}$ in $\ZZ$.

\item[\textbf{(b)}]
Let $z = a + bi \in \ZZ\ive{i}$ with $a, b \in \ZZ$.
Assume that $z \neq 0$.
Let $n = \floor{\abs{z}} = \floor{\sqrt{a^2 + b^2}}$.
Prove that every divisor of $z$ in $\ZZ\ive{i}$
has the form $c + di$ with
$c, d \in \set{-n, -n+1, \ldots, n}$.

\item[\textbf{(c)}]
Without recourse to the general theory of PIDs and
UFDs, prove that every nonzero element of $\ZZ\ive{i}$
has an irreducible factorization.

\item[\textbf{(d)}]
Let $z \in \ZZ\ive{i}$. Prove that we have the
following logical equivalence:
\[
\tup{z \text{ is a unit of } \ZZ\ive{i}}
\ \iff \ %
\tup{N\tup{z} = 1}
\ \iff \ %
\tup{z \in \set{1, i, -1, -i}} .
\]

\end{itemize}

\subsection{Remark}

Combining parts \textbf{(b)} and \textbf{(c)} of
this exercise yields a (slow) algorithm for finding
an irreducible factorization of a nonzero Gaussian
integer.
(Indeed, part \textbf{(b)} shows that we can list
all divisors of a nonzero Gaussian integer in finite
time. This allows checking whether a Gaussian integer
is prime, and otherwise finding a prime divisor.)

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 7
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 7}

\subsection{Problem}

Consider the ring
\[
\ZZ\ive{\sqrt{-3}}
= \set{a + b \sqrt{-3} \mid a, b \in \ZZ} .
\]
This ring is a subring of $\CC$, and thus is an integral
domain.

Let $u = 2 \in \ZZ\ive{\sqrt{-3}}$ and
$v = 1 + \sqrt{-3} \in \ZZ\ive{\sqrt{-3}}$.
Further let $a = 2u = 4$ and $b = 2v$.

\begin{itemize}

\item[\textbf{(a)}]
Prove that both $u$ and $v$ are common
divisors of $a$ and $b$ in $\ZZ\ive{\sqrt{-3}}$.

\item[\textbf{(b)}]
Prove that the only divisors of $4$ in
$\ZZ\ive{\sqrt{-3}}$ are
$\pm 1$, $\pm 2$, $\pm 4$,
$\pm \tup{1 + \sqrt{-3}}$, and
$\pm \tup{1 - \sqrt{-3}}$.

\item[\textbf{(c)}]
Prove that $a$ and $b$ have no gcd in
$\ZZ\ive{\sqrt{-3}}$.

\end{itemize}

[\textbf{Hint:} For full credits on part \textbf{(b)}, it
suffices to explain how the solution can be reduced to a
finite computation and perform one representative
case of the computation.]

\subsection{Remark}

This shows that $\ZZ\ive{\sqrt{-3}}$ is not a UFD.

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 8
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 8}

\subsection{Problem}

Let $R$ be a ring.
Let $I$ and $J$ be two ideals of $R$ such that
$I \subseteq J$.
Let $J / I$ denote the set of all cosets
$j + I \in R / I$ where $j \in J$.

Prove the following:

\begin{itemize}

\item[\textbf{(a)}]
This set $J / I$ is an ideal of $R / I$.

\item[\textbf{(b)}]
We have
$\tup{R / I} / \tup{J / I} \cong R / J$
(as rings).
More concretely, there is a ring isomorphism
$R / J \to \tup{R / I} / \tup{J / I}$
that sends each residue class
$\overline{r} = r + J$ to
$\overline{r + I} = \tup{r + I} + \tup{J / I}$.

\end{itemize}

\subsection{Remark}

This is known as the \emph{Third Isomorphism Theorem for rings}.

For an example, take $R = \ZZ$ and $I = 6 \ZZ$ and $J = 2 \ZZ$.
In this case, $J / I$ consists of the ``even''
residue classes $\overline{0}, \overline{2}, \overline{4}$
in $R / I = \ZZ / 6$.
Part \textbf{(b)} of the exercise says that if we ``quotient
them out'' of $\ZZ / 6$, then we are left with (an isomorphic
copy of) $R / J = \ZZ / 2$.

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 9
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 9}

\subsection{Problem}

Let $R$ be a ring.
Let $S$ be a subring of $R$.
Let $I$ be an ideal of $R$.
Define $S + I$ to be the subset
$\set{s + i \mid s \in S \text{ and } i \in I}$
of $R$.

Prove the following:

\begin{itemize}

\item[\textbf{(a)}]
This subset $S + I$ is a subring of $R$.

\item[\textbf{(b)}]
The set $I$ is an ideal of the ring $S + I$.

\item[\textbf{(c)}]
The set $S \cap I$ is an ideal of the ring $S$.

\item[\textbf{(d)}]
We have
$\tup{S + I} / I \cong S / \tup{S \cap I}$
(as rings).
More concretely, there is a ring isomorphism
$S / \tup{S \cap I} \to \tup{S + I} / I$
that sends each residue class
$\overline{s} = s + \tup{S \cap I}$ to
$\overline{s} = s + I$.

\end{itemize}

\subsection{Remark}

This is known as the \emph{Second Isomorphism Theorem for rings}.

For an example, we can let

\begin{itemize}

\item $R$ be the polynomial ring
$\QQ\ive{x}$ of all univariate polynomials with
rational coefficients;

\item $I = \set{a_2 x^2 + a_3 x^3 + \cdots + a_n x^n
\mid n \geq 0 \text{ and } a_i \in \QQ}$ be the ideal
consisting of all
polynomials divisible by $x^2$ (that is, all polynomials
whose $x^0$-coefficient and $x^1$-coefficient are $0$);

\item $S$ be the subring $\QQ$ of $R$ (which consists
of all constant polynomials).

\end{itemize}

Then, $S + I = \set{a_0 + a_2 x^2 + a_3 x^3 + \cdots +
a_n x^n \mid n \geq 0 \text{ and } a_i \in \QQ}$ is
the set of all polynomials whose $x^1$-coefficient is
$0$. This is indeed a subring of $R$, as we have seen
in Lecture 6 (where we have used this subring to find
an irreducible element that is not prime).

\subsection{Solution}

...

%----------------------------------------------------------------------------------------
%	EXERCISE 10
%----------------------------------------------------------------------------------------
\horrule{0.3pt} \\[0.4cm]

\section{Exercise 10}

\subsection{Problem}

\begin{itemize}

\item[\textbf{(a)}]
Let $R$ be a commutative ring, and let $u$ and $n$
be two nonnegative integers.
Let $x, y \in R$ be two elements such that $x - y \in uR$.
(Here, $uR := \set{ur \mid r \in R}$; this is a
principal ideal of $R$, since $uR = \tup{u1_R} R$.)

Prove that
\[
x^n - y^n \in guR,
\qquad \text{where } g = \gcd\tup{n, u}.
\]

\item[\textbf{(b)}]
Let $\tup{f_0, f_1, f_2, \ldots}$ be the Fibonacci
sequence, defined as in Exercise 6 on
homework set \#1.
Prove that
\[
\gcd\tup{n, f_d} \cdot f_d \mid f_{dn}
\qquad \text{for any } d, n \in \NN.
\]


\end{itemize}

[\textbf{Hint:} For part \textbf{(a)}, write
$x^n - y^n$ as
$\tup{x-y} \tup{x^{n-1} + x^{n-2}y + \cdots + y^{n-1}}$,
and show that the second factor belongs to $gR$.
For part \textbf{(b)}, define the matrices $A$ and
$B$ and the commutative ring $\calF$ as in Exercise 6 on
homework set \#1, and apply part \textbf{(a)} to
$x = A^d$ and $y = B^d$ and $u = f_d$.]

\subsection{Solution}

...


\horrule{0.3pt} \\[0.4cm]

\begin{thebibliography}{99999999}                                                                                         %

\end{thebibliography}

\end{document}

