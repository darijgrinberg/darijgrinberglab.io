\documentclass[paper=a4, fontsize=12pt]{scrartcl} % A4 paper and 12pt font size
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,amssymb} % Math packages
\usepackage{mathrsfs}    % More math packages
\usepackage{sectsty}  % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all section titles centered, the default font and small caps %remove this to left align section tites
\usepackage{hyperref} % Turns cross-references into hyperlinks,
                      % and defines \url and \href commands.
\usepackage{graphicx} % For embedding graphics files.
\usepackage{framed}   % For the "leftbar" environment used below.
\usepackage{ifthen}   % Used for the \powset command below.
\usepackage{lastpage} % for counting the number of pages
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[height=10in,a4paper,hmargin={1in,0.8in}]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[all]{xy}  % Package for commutative diagrams.
\usepackage{tikz}     % This is a powerful tool to draw vector
                      % graphics inside LaTeX. In particular, you can
                      % use it to draw commutative diagrams.
\usepackage{verbatim} % For the "verbatim" environment, in which
                      % special symbols can be used freely without
                      % confusing the compiler. (And it's typeset in
                      % a constant-width font.)
                      % Useful, e.g., for quoting code (or ASCII art).

\usetikzlibrary{lindenmayersystems} % This is for drawing Sierpinski triangles.

%\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{20pt} % Makes indentation for paragraphs longer.
                           % This makes paragraphs stand out more.

%----------------------------------------------------------------------------------------
%	VARIOUS USEFUL COMMANDS
%----------------------------------------------------------------------------------------
% The commands below might be convenient. For example, you probably
% prefer to write $\powset[2]{V}$ for the set of $2$-element subsets
% of $V$, rather than writing $\mathcal{P}_2(V)$.
% Notice that you can easily define your own commands like this.
% Caveat: Some of these commands need to be properly "guarded" when
% they occur in subscripts or superscripts. So you should not write
% $K_\CC$, but rather $K_{\CC}$.
\newcommand{\CC}{\mathbb{C}} % complex numbers
\newcommand{\RR}{\mathbb{R}} % real numbers
\newcommand{\QQ}{\mathbb{Q}} % rational numbers
\newcommand{\NN}{\mathbb{N}} % nonnegative integers
\newcommand{\calF}{\mathcal{F}}
\newcommand{\DD}{{\mathbb{D}}} % dual numbers
\newcommand{\Z}[1]{\mathbb{Z}/#1\mathbb{Z}} % integers modulo k
                                            % (syntax: "\Z{k}")
\newcommand{\ZZ}{\mathbb{Z}} % integers
\newcommand{\id}{\operatorname{id}} % identity map
\newcommand{\lcm}{\operatorname{lcm}}
% Lowest common multiple. For historical reasons, LaTeX has a \gcd
% command built in, but not an \lcm command. The preceding line
% rectifies that.
\newcommand{\diag}{\operatorname{diag}} % diagonal matrix
\newcommand{\ord}{\operatorname{ord}} % order of a group element
\newcommand{\rank}{\operatorname{rank}} % rank of a matrix
\newcommand{\Hom}{\operatorname{Hom}} % group of homomorphisms
\newcommand{\End}{\operatorname{End}} % ring of endomorphisms
\newcommand{\Iso}{\operatorname{Iso}} % set of isomorphisms
\newcommand{\GL}{\operatorname{GL}} % general linear group
\newcommand{\SL}{\operatorname{SL}} % special linear group
\newcommand{\Nil}{\operatorname{Nil}} % nilradical
\newcommand{\Jac}{\operatorname{Jac}} % Jacobson radical
\newcommand{\Tor}{\operatorname{Tor}} % torsion submodule or Tor functor
\newcommand{\Ext}{\operatorname{Ext}} % Ext functor
\newcommand{\Ker}{\operatorname{Ker}} % kernel
\newcommand{\Coker}{\operatorname{Coker}} % cokernel
\newcommand{\powset}[2][]{\ifthenelse{\equal{#2}{}}{\mathcal{P}\left(#1\right)}{\mathcal{P}_{#1}\left(#2\right)}}
% $\powset[k]{S}$ stands for the set of all $k$-element subsets of
% $S$. The argument $k$ is optional, and if not provided, the result
% is the whole powerset of $S$.
\newcommand{\set}[1]{\left\{ #1 \right\}}
% $\set{...}$ compiles to {...} (set-brackets).
\newcommand{\abs}[1]{\left| #1 \right|}
% $\abs{...}$ compiles to |...| (absolute value, or size of a set).
\newcommand{\tup}[1]{\left( #1 \right)}
% $\tup{...}$ compiles to (...) (parentheses, or tuple-brackets).
\newcommand{\ive}[1]{\left[ #1 \right]}
% $\ive{...}$ compiles to [...] (Iverson bracket, aka truth value).
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
% $\floor{...}$ compiles to |_..._| (floor function).
\newcommand{\eps}{\varepsilon}
% Just a shorthand for a commonly-used symbol.
\newcommand{\underbrack}[2]{\underbrace{#1}_{\substack{#2}}}
% $\underbrack{...1}{...2}$ yields
% $\underbrace{...1}_{\substack{...2}}$. This is useful for doing
% local rewriting transformations on mathematical expressions with
% justifications. For example, try this out:
% $ \underbrack{(a+b)^2}{= a^2 + 2ab + b^2 \\ \text{(by the binomial formula)}} $
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\newcommand{\nnn}{\nonumber\\} % Don't number this line in an "align" environment, and move on to the next line.
\newcommand{\explain}[1]{\tup{\hspace{-0.5pc}\text{\begin{tabular}{c}#1\end{tabular}\hspace{-0.5pc}}}}
% $\explain{...}$ interprets the "..." as text (you can
% nest some math inside by putting it in $...$'s as you
% would in any other text block) and puts it in parentheses.

% Arrows for normal use:
\newcommand{\mono}{\hookrightarrow} % Monomorphism/injection.
\newcommand{\epi}{\twoheadrightarrow} % Epimorphism/surjection.
\newcommand{\iso}{\overset{\cong}{\to}} % Epimorphism/surjection.

% Arrows for xymatrix:
\newcommand{\arinj}{\ar@{_{(}->}} % Monomorphism.
\newcommand{\arinjrev}{\ar@{^{(}->}} % Monomorphism.
\newcommand{\arsurj}{\ar@{->>}} % Epimorphism.
\newcommand{\arelem}{\ar@{|->}} % Arrow to be used in describing a map on elements.
\newcommand{\arback}{\ar@{<-}} % Backward arrow.

%----------------------------------------------------------------------------------------
%	MAKING SUMMATION SIGNS ALWAYS PUT THEIR BOUNDS ABOVE AND BELOW
%	THE SIGN
%----------------------------------------------------------------------------------------
% The following are hacks to ensure that sums (such as
% $\sum_{k=1}^n k$) always put their bounds (i.e., the $k=1$ and the
% $n$) underneath and above the sign, as opposed to on its right.
% Same for products (\prod), set unions (\bigcup) and set
% intersections (\bigcap). Remove the 8 lines below if you do not want
% this behavior.
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}

%----------------------------------------------------------------------------------------
%	ENVIRONMENTS
%----------------------------------------------------------------------------------------
% The incantations below define how theorem environments
% (\begin{theorem} ... \end{theorem}) and their likes will look like.
\newtheoremstyle{plainsl}% <name>
  {8pt plus 2pt minus 4pt}% <Space above>
  {8pt plus 2pt minus 4pt}% <Space below>
  {\slshape}% <Body font>
  {0pt}% <Indent amount>
  {\bfseries}% <Theorem head font>
  {.}% <Punctuation after theorem head>
  {5pt plus 1pt minus 1pt}% <Space after theorem headi>
  {}% <Theorem head spec (can be left empty, meaning `normal')>

% Environments which make the text inside them slanted:
\theoremstyle{plainsl}
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{proposition}[theorem]{Proposition}
  \newtheorem{lemma}[theorem]{Lemma}
  \newtheorem{corollary}[theorem]{Corollary}
  \newtheorem{conjecture}[theorem]{Conjecture}
% Environments that don't:
\theoremstyle{definition}
  \newtheorem{definition}[theorem]{Definition}
  \newtheorem{example}[theorem]{Example}
  \newtheorem{exercise}[theorem]{Exercise}
  \newtheorem{examples}[theorem]{Examples}
  \newtheorem{algorithm}[theorem]{Algorithm}
  \newtheorem{question}[theorem]{Question}
 \theoremstyle{remark}
  \newtheorem{remark}[theorem]{Remark}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}

% \documentclass[12pt]{article}
% \usepackage[margin=1in]{geometry}
% \usepackage[all]{xy}


% \usepackage{amsmath,amsthm,amssymb,color,latexsym}
% \usepackage{geometry}        
% \geometry{letterpaper}


\newcounter{prob}

\newtheorem{problem}[prob]{Problem}

\newenvironment{solution}[1][\textit{Solution}]{\textbf{#1. } }{$\square$}


\begin{document}
\noindent Winter 2021, Math 533: Abstract Algebra \hfill Homework \#2\\
Rose Adkisson

\hrulefill

\setcounter{prob}{0}

\begin{problem}
\textbf{Exercise 1}: 
Let $R$ be a ring.
Let $a$ be a nilpotent element of $R$.
(Recall that ``nilpotent'' means that there exists some
$n \in \NN$ such that $a^n = 0$.)

\begin{itemize}
\item[\textbf{(a)}]
Prove that $1 - a \in R$ is a unit.
\item[\textbf{(b)}]
Let $u \in R$ be a unit satisfying $ua = au$.
Prove that $u - a \in R$ is a unit.
\end{itemize}

\end{problem}
\begin{solution}\\
\begin{itemize}
\item[\textbf{(a)}]
To show that $1-a\in R$ is a unit, we must show that $1-a$ has an inverse in $R$. In other words, we much show that there exists some $p\in R$ such that $(1-a)p=p(1-a)=1$. \\ Note that $a^n=0$ for some large enough $n\in\mathbb{N}$ since $a$ is nilpotent. Thus $1-a^n=1-0=1$. We then find that if we can find $p$ such that $1-a^n=(1-a)p=p(1-a)$, we have found $p$ that is inverse to $1-a$ and therefore shown that $1-a$ is a unit.\\ \\ 
We may let $p=1+a+a^2+...+a^{n-1}$. Clearly $p\in R$ and
\begin{align*}
(1-a)p&=(1-a)(1+a+a^2+...+a^{n-1}) \\
&=(1-a)\cdot 1 + (1-a)\cdot a + (1-a)\cdot a^2 + ... + (1-a)\cdot a^{n-1}\\
&=1\cdot 1-a\cdot 1+1\cdot a-a\cdot a+a\cdot a-...-a\cdot a^{n-2}+1\cdot a^{n-1}-a\cdot a^{n-1}\\
&=1-a+a-a^2+a^2-...-a^{n-1}+a^{n-1}-a^n\\
&=1-a^n\\
&=1 .
\end{align*}
It is easy to see that $p(1-a)$ yields the same process and result. Thus we have found a $p\in R$ that is inverse to $1-a$. Thus $1-a$ is a unit of $R$. 
\item[\textbf{(b)}]
Since $u$ is a unit of $R$, there exists some $u^{-1}\in R$ such that $uu^{-1}=u^{-1}u=1$. Also recall that $au=ua$.
\begin{quote}
Claim: $a$ and $u^{-1}$ commute.\\
Proof:
\begin{align*}
ua=au&\implies u^{-1}ua=u^{-1}au\\
&\implies a=u^{-1}au\\
&\implies au^{-1}=u^{-1}auu^{-1}\\
&\implies au^{-1}=u^{-1}a .
\end{align*}
\end{quote}
Hence, $(u^{-1}a)^n = (u^{-1})^n a^n = 0$, since $a^n = 0$. \\
Similar to (a), we will explicitly find the inverse to $u-a$ when $a^n=0$ for some $n\in\mathbb{N}$. Let $p=u^{-1}+u^{-1}(u^{-1}a)+u^{-1}(u^{-1}a)^2+...+u^{-1}(u^{-1}a)^{n-1}$. Clearly $p\in R$ and
\begin{align*}
&(u-a)p\\
&=(u-a)(u^{-1}+u^{-1}(u^{-1}a)+u^{-1}(u^{-1}a)^2+...+u^{-1}(u^{-1}a)^{n-1}) \\
&=(u-a)\cdot u^{-1}+(u-a)\cdot u^{-1}(u^{-1}a)+(u-a)\cdot u^{-1}(u^{-1}a)^2+...+(u-a)\cdot u^{-1}(u^{-1}a)^{n-1}\\
&=uu^{-1}-au^{-1}+uu^{-1}(u^{-1}a)-au^{-1}(u^{-1}a)+...+uu^{-1}(u^{-1}a)^{n-1}-au^{-1}(u^{-1}a)^{n-1}\\
&=1-u^{-1}a+u^{-1}a-(u^{-1}a)^2+...-(u^{-1}a)^{n-1}+(u^{-1}a)^{n-1}-(u^{-1}a)^{n}\\
& \qquad \qquad \tup{\text{using the commutativity of $a$ with $u^{-1}$}} \\
&=1-(u^{-1}a)^{n}\\
&=1-0\\
&=1.
\end{align*}
It is easy to see that $p(u-a)$ yields the same process and result. Thus we have found a $p\in R$ that is inverse to $u-a$. Thus $u-a$ is a unit of $R$. 




\end{itemize}
\end{solution}

\pagebreak

\setcounter{prob}{2}

\begin{problem}
\textbf{Exercise 3}: Let $R$ be an integral domain.
Let $a \in R$ and $b \in R$.
Assume that $a$ and $b$ have an lcm $\ell \in R$.
Prove that $a$ and $b$ have a gcd $g \in R$,
which furthermore satisfies $g\ell = ab$.
\end{problem}
\begin{solution}
Let $R$ be an integral domain, $a,b\in R$, and let $\ell$ be an lcm of $a$ and $b$.
If $a = 0$, then $\ell=0$ (since $a\mid \ell$) and therefore $b$ is a gcd $g$ of $a$ and $b$ (since $b\mid 0=a$ and $b\mid b$) that satisfies $g\ell = ab$ (since $b\ell = 0 = ab$).
Thus we are done in the case when $a = 0$.
Similarly we can handle the case when $b = 0$.
From now on, we assume that $a \neq 0$ and $b \neq 0$.
Since $R$ is an integral domain, this implies $ab \neq 0$.
Also, since $R$ is an integral domain, fractions of the form $\dfrac{u}{v}$ with $u \in R$ and $v \in R \setminus \set{0}$ are well-defined (i.e., unique) when $v \mid u$.
The following property of such fractions will be used without saying: If $u, u' \in R$ and $v, v' \in R \setminus \set{0}$ are such that $v \mid u$ and $v' \mid u'$, then $vv' \mid uu'$ and $\dfrac{u}{v} \cdot \dfrac{u'}{v'} = \dfrac{uu'}{vv'}$.
 \\ \\
Consider $ab$: this is a common multiple of $a$ and $b$, since $a|ab$ and $b|ab$. Hence, $\ell|ab$ as $\ell$ is an lcm of $a$ and $b$. Thus, $\dfrac{ab}{\ell} \in R$ is well-defined. (Indeed, $\ell | ab$ and $ab \neq 0$ yield $\ell \neq 0$.)\\ \\
We will first show that $\dfrac{ab}{\ell}$ is a common divisor of $a$ and $b$; then we will show that $\dfrac{ab}{\ell}$ is a gcd $g$ of $a$ and $b$ that satisfies $ab=g\ell$. \\
\begin{itemize}
\item \underline{Showing that $\dfrac{ab}{\ell}$ is a common divisor of $a$ and $b$:}
We have $\dfrac{\ell}{b} \in R$ (since $b | \ell$). Now, $$a=\dfrac{ab\ell}{\ell b} = \dfrac{ab}{\ell}\cdot \dfrac{\ell}{b}.$$ Since $\dfrac{\ell}{b} \in R$, this entails $$\dfrac{ab}{\ell} \Big{|} a .$$
Similarly, we find $$\dfrac{ab}{\ell}\Big{|}b .$$
Thus we have that $\dfrac{ab}{\ell}$ is a common divisor of $a$ and $b$.
\item \underline{Showing that $\dfrac{ab}{\ell}$ is a gcd of $a$ and $b$:}
We must show that, for any common divisor $d$ of $a$ and $b$, we have $d|\dfrac{ab}{\ell}$. \\
Suppose $d \in R$ is a common divisor of $a$ and $b$. Then $d|a$ and $d|b$, so $d|ab$. Hence, $\dfrac{ab}{d} \in R$. Since $d|b$, we find $\dfrac{b}{d}\in R$, and thus $$a\Big{|}\left(a\cdot \dfrac{b}{d}\right) = \dfrac{ab}{d}.$$
Similarly, $b\Big{|} \dfrac{ab}{d}$.
Therefore, $\dfrac{ab}{d}$ is a common multiple of $a$ and $b$.
Thus, as $\ell$ is an lcm of $a$ and $b$, we have $$\ell\Big{|}\dfrac{ab}{d} .$$
Thus there exists some $k\in R$ with $\ell k=\dfrac{ab}{d}$, implying $d\ell k = ab$ and therefore $dk=\dfrac{ab}{\ell}$. Thus $$d\Big{|}\dfrac{ab}{\ell}.$$
We have now shown that any common divisor $d$ of $a$ and $b$ is a divisor of $\dfrac{ab}{\ell}$. This gives that $\dfrac{ab}{\ell}$ is a gcd of $a$ and $b$.
\end{itemize}
Thus, $\dfrac{ab}{\ell}$ is a gcd of $a$ and $b$. Of course, if we denote it by $g$, then it satisfies $ab=g\ell$ (by its definition).
We have now shown all of the desired claims.






\end{solution}
\pagebreak

\setcounter{prob}{4}

\begin{problem}
\textbf{Exercise 5}: Let $p$ be a prime number.
\begin{itemize}
\item[\textbf{(a)}]
Prove that the only units of the ring
$\mathbb{Z} / p$ that are their own inverses
(i.e., the only $m \in \tup{\ZZ / p}^\times$
that satisfy $m^{-1} = m$) are
$\overline{1}$ and $\overline{-1}$.
\item[\textbf{(b)}]
Assume that $p$ is odd.
Let $u = \dfrac{p-1}{2} \in \mathbb{N}$.
Prove that $u!^2 \equiv -({-1})^u \mod p$.
\end{itemize}
\end{problem}

\begin{solution}
\begin{itemize}
\item[\textbf{(a)}]
Consider any unit $r\in\mathbb{Z}/p$ where $r=r^{-1}$. We will show that $r = 1$ or $r = -1$ (where $1$ means $1_{\ZZ/p} = \overline{1}$). \\
We have $rr^{-1} = 1$. Since $r=r^{-1}$, this rewrites as $rr = 1$. In other words, $r^2 = 1$, so that $r^2 - 1 = 0$.
This rewrites as $(r-1)(r+1)= 0$.
But $\ZZ / p$ is a field and thus an integral domain; hence, all nonzero $a,b\in\mathbb{Z}/p$ satisfy $ab\neq 0$. Thus, since $(r-1)(r+1)= 0$, we hve either $r-1= 0$ or $r+1= 0$. Thus, $r= 1$ or $r= -1$.
This shows that the only units of the ring $\mathbb{Z}/p$ that are their own inverses are $1 = \overline{1}$ and $-1 = \overline{-1}$.

\item[\textbf{(b)}]
Since $p$ is odd, we have
$$(p-1)!=(p-1)\cdot (p-2)\cdot ...\cdot \dfrac{p+1}{2}\cdot \dfrac{p-1}{2}\cdot ...\cdot 2\cdot 1 .$$
We will now look to rewrite the first half of these factors modulo $p$:
%$$p\equiv0\mod{p}$$
\begin{align*}
p-1 &\equiv-1\mod{p}; \\
p-2 &\equiv-2\mod{p}; \\
& ...; \\
\dfrac{p+1}{2} &= p-\dfrac{p-1}{2}\equiv-\dfrac{p-1}{2} \mod p.
\end{align*}
Thus we may rewrite $(p-1)!$ as follows: 
$$(p-1)!\equiv(-1)\cdot (-2)\cdot ...\cdot \left(-\dfrac{p-1}{2}\right) \cdot \dfrac{p-1}{2}\cdot ...\cdot 2\cdot 1\mod{p} .$$
Notice that there are $\dfrac{p-1}{2}$ negative factors and $\dfrac{p-1}{2}$ positive factors on the right hand side, and the each of the former agrees with exactly one of the latter in its absolute value. Thus, by combining factors with equal absolute value, we can rewrite the above as follows:
$$(p-1)!\equiv (-1)^{(p-1)/2}\left(1\cdot 2\cdot ...\cdot \dfrac{p-1}{2}\right)^2\mod{p} .$$
This rewrites as
$$(p-1)!\equiv (-1)^{(p-1)/2}\left(\dfrac{p-1}{2}\right)!^2\mod{p} .$$
By Wilson's Theorem, we have that $(p-1)!\equiv -1\mod{p}$, so that
$$-1\equiv (-1)^{(p-1)/2}\left(\dfrac{p-1}{2}\right)!^2\mod{p};$$
in other words,
$$\left(\dfrac{p-1}{2}\right)!^2\equiv -1(-1)^{(p-1)/2}\mod{p}.$$
Recalling that $u=\dfrac{p-1}{2}$, we can rewrite this as
$$u!^2\equiv -1(-1)^{u}\mod{p} ,$$
which is our desired result.
\end{itemize}
\end{solution}
\pagebreak







\setcounter{prob}{7}

\begin{problem}
\textbf{Exercise 8}: 
Let $R$ be a ring.
Let $I$ and $J$ be two ideals of $R$ such that
$I \subseteq J$.
Let $J / I$ denote the set of all cosets
$j + I \in R / I$ where $j \in J$.
Prove the following:
\begin{itemize}
\item[\textbf{(a)}]
This set $J / I$ is an ideal of $R / I$.
\item[\textbf{(b)}]
We have
$\tup{R / I} / \tup{J / I} \cong R / J$
(as rings).
More concretely, there is a ring isomorphism
$R / J \to \tup{R / I} / \tup{J / I}$
that sends each residue class
$\overline{r} = r + J$ to
$\overline{r + I} = \tup{r + I} + \tup{J / I}$.
\end{itemize}
\end{problem}
\begin{solution}
\begin{itemize}
\item[\textbf{(a)}]
To be an ideal of $R/I$, the set $J/I$ must be a subset of $R/I$ such that 
\begin{enumerate}
    \item $a+b\in J/I$ for all $a,b\in J/I$;
    \item $ab\in J/I$ and $ba\in J/I$ for all $b\in J/I$ and all $a\in R/I$;
    \item $0_{R/I}\in J/I$.
\end{enumerate}
First note that it is clear that $J/I\subset R/I$ by definition of $J/I$. \\ \\
Next we will address (1), that is, the claim that $J/I$ is closed under addition. Consider any $j_1+I,j_2+I\in J/I$ (with $j_1,j_2 \in J$). Then $j_1+I+j_2+I=(j_1+j_2)+I$. Since $J$ is an ideal, it is closed under addition, thus $j_1+j_2\in J$. Thus $(j_1+j_2)+I\in J/I$ and $J/I$ is closed under addition. Thus (1) is proved. \\ \\
Next we must prove (2), i.e., show that the product of an element of $J/I$ and an element of $R/I$ remains in $J/I$. Consider any $j+I\in J/I$ (with $j \in J$) and any $r+I\in R/I$. Then $$(j+I)(r+I)=jr+I .$$
Note that $J$ is an ideal of $R$ and is therefore closed under multiplication with an element of $R$, so $jr\in J$. Thus $jr+I\in J/I$.
Also note $$(r+I)(j+I)=rj+I .$$
Note that $J$ is an ideal of $R$ and is therefore closed under multiplication with an element of $R$, so $rj\in J$. Thus $rj+I\in J/I$. Thus (2) is proved. \\ \\ 
Since $J$ is an ideal of $R$, we have $0_R\in J$. Thus $0_R+I\in J/I$. Also note $0_{R/I}=0_R+I$. Thus $0_{R/I}\in J/I$. \\ \\
We have now shown the properties necessary for $J/I$ to be an ideal of $R/I$. 
\item[\textbf{(b)}]
Consider the map $f:R/I\to R/J$, $r+I\mapsto r+J$. First we need to show that $f$ is well defined: 
\begin{quote}
For $r_1+I,r_2+I\in R/I$, if $r_1+I=r_2+I$, we must show that $r_1+J=r_2+J$. We will use the fact that $I\subset J$:
\begin{align*}
r_1+I=r_2+I&\implies r_1-r_2\in I\\
&\implies r_1-r_2\in J\\
&\implies r_1+J=r_2+J .
\end{align*}
\end{quote}
It is obvious that the map $f$ respects addition, multiplication, the zero, and the unity; thus, $f$ is a ring morphism.
Now, by the \textit{First Isomorphism Theorem for Rings}, we have 
\begin{align}
(R/I)/\ker{f}\cong f(R/I).
\label{sol.8.4}
\end{align}
Next we will show that $f$ is surjective, giving $f(R/I)=R/J$. \begin{quote}Consider any $r+J\in R/J$. We can find a preimage for it, namely $r+I\in R/I$. Thus every element of $R/J$ has a preimage and $f$ is surjective. \end{quote}
Thus \eqref{sol.8.4} becomes
\begin{align}
(R/I)/\ker{f}\cong R/J.
\label{sol.8.5}
\end{align}
As our last step to achieving the desired result, we will show that $\ker f=J/I$. 
\begin{quote}
Let $r+I\in \ker f$. Then $f(r+I)=0+J$. But $f(r+I)=r+J$. Thus $0+J=r+J$ and therefore $r\in J$. Thus $r+I\in J/I$. Thus we have shown that $\ker f\subset J/I$. 
\end{quote}
\begin{quote}
Let $r+I\in J/I$ with $r\in J$. Then $r+J=0+J$. Thus $f(r+I)=r+J=0+J$ and therefore $r+I\in\ker f$. Thus $J/I\subset \ker f$.
\end{quote}
\begin{quote}
Thus $J/I=\ker f$.
\end{quote}
Hence, \eqref{sol.8.5} becomes $$(R/I)/(J/I)\cong R/J .$$
Moreover, the isomorphism $(R/I)/(J/I)\to R/J$ constructed by the \textit{First Isomorphism Theorem for Rings} sends each $(r+I) + (J/I) = (r+I) + \ker f \in (R/I)/(J/I)$ to $f(r+I) = r + J \in R/J$. Hence, its inverse sends each $r + J$ to $(r+I) + (J/I)$, as claimed in the exercise.
\\ \\
% ***I referenced my Abstract notes from undergraduate to follow this last proof in relation to the isomorphism theorems of groups***
\end{itemize}
\end{solution} 
\pagebreak

\setcounter{prob}{8}

\begin{problem}
\textbf{Exercise 9}:
Let $R$ be a ring.
Let $S$ be a subring of $R$.
Let $I$ be an ideal of $R$.
Define $S + I$ to be the subset
$\set{s + i \mid s \in S \text{ and } i \in I}$
of $R$.

Prove the following:
\begin{itemize}
\item[\textbf{(a)}]
This subset $S + I$ is a subring of $R$.
\item[\textbf{(b)}]
The set $I$ is an ideal of the ring $S + I$.
\item[\textbf{(c)}]
The set $S \cap I$ is an ideal of the ring $S$.
\item[\textbf{(d)}]
We have
$\tup{S + I} / I \cong S / \tup{S \cap I}$
(as rings).
More concretely, there is a ring isomorphism
$S / \tup{S \cap I} \to \tup{S + I} / I$
that sends each residue class
$\overline{s} = s + \tup{S \cap I}$ to
$\overline{s} = s + I$.
\end{itemize}
\end{problem}
\begin{solution}
\begin{itemize}
\item[\textbf{(a)}]
Since $S$ is a subring of $R$ and $I$ is an ideal of $R$, first note that it is obvious that $S+I\subset R$. Next it is clear that $S+I:=\{s+i|s\in S, i\in I\}$ contains the unity of $R$, the zero of $R$, and respects addition. Thus we will simply show that $S+I$ respects negation and multiplication. \\ \\
Consider any $a, b \in S+I$. Thus, $a = s_1 + i_1$ and $b = s_2 + i_2$ for some $s_1,s_2\in S$ and $i_1,i_2\in I$. We then have $$a-b = (s_1+i_1)-(s_2+i_2)=(s_1-s_2)+(i_1-i_2).$$
Note that $s_1-s_2\in S$ as $S$ is a ring, and $i_1-i_2\in I$ as $I$ is an ideal. Thus we have $(s_1-s_2)+(i_1-i_2)\in S+I$. That is, $a-b\in S+I$. This shows that $S+I$ is closed under subtraction, hence closed under negation (since $0 \in S+I$).\\ \\
Consider any $a, b \in S+I$. Thus, $a = s_1 + i_1$ and $b = s_2 + i_2$ for some $s_1,s_2\in S$ and $i_1,i_2\in I$. Then, $$ab = (s_1+i_1)(s_2+i_2)=s_1s_2+s_1i_2+i_1s_2+i_1i_2.$$ Note $s_1s_2\in S$ as $S$ is a ring and closed under multiplication. Also note $s_1i_2+i_1s_2+i_1i_2\in I$ since $I$ is an ideal and is therefore closed under multiplication with all elements of $R$ and closed under addition as well. Thus $s_1s_2+s_1i_2+i_1s_2+i_1i_2\in S+I$. In other words, $ab \in S+I$. Thus $S+I$ is closed under multiplication. \\ \\
Since $S+I$ contains the unity of $R$, contains the zero of $R$, is closed under addition, is closed under negation, and is closed under multiplication, we conclude that $S+I$ is a subring of $R$.
\item[\textbf{(b)}] 
To be an ideal of $S+I$, the set $I$ must be a subset of $S+I$ such that 
\begin{enumerate}
    \item $a+b\in I$ for all $a,b\in I$;
    \item $ab\in I$ and $ba\in I$ for all $b\in I$ and all $a\in S+I$;
    \item $0_{S+I}\in I$.
\end{enumerate}
First note that it is obvious that $ I \subset S+I$, since $0 \in S$. \\ \\
Next, since $I$ is an ideal of $R$ and $S+I$ is a subring of $R$, we find that $I$ inherits the properties of closure under addition and containing $0_R=0_{S+I}$. Thus all we must check is for any $b\in I$ and any $a\in S+I$ that $ab\in I$ and $ba\in I$. \\ \\ 
Consider any $b\in I$ and any $a\in S+I$. Write $a \in S+I$ as $a = s+i$ with $s \in S$ and $i \in I$. Then $$ab = (s+i)b = sb + ib \in I$$
(indeed, since $I$ is closed under multiplication with elements of $R$, we have $sb \in I$ and $ib \in I$, and therefore we get $sb + ib \in I$ because $I$ is closed under addition).
Similarly $ba \in I$. We have now shown the properties necessary for $I$ to be an ideal of $S+I$. 
\item[\textbf{(c)}]
To be an ideal of $S$, the set $S\cap I$ must be a subset of $S$ such that 
\begin{enumerate}
    \item $a+b\in S\cap I$ for all $a,b\in S\cap I$;
    \item $ab\in S\cap I$ and $ba\in S\cap I$ for all $b\in S\cap I$ and all $a\in S$;
    \item $0_S\in S\cap I$.
\end{enumerate}
First note that it is clear $S\cap I\subset S$. \\ \\
It should also be clear, since $S$ is a subring of $R$, that $0_S=0_R$ and $0_R\in S$. Since $I$ is an ideal of $R$, we have $0_R\in I$. Thus $0_R\in S\cap I$. In other words, $0_S \in S \cap I$.\\ \\
Next we will address (1),  that is, the claim that $S\cap I$ is closed under addition. Let $a,b\in S\cap I$. Then $a,b\in S$ and $a,b\in I$. Since $S$ is closed under addition as a ring, we find $a+b\in S$. Since $I$ is an ideal of $R$, $I$ is also closed under addition, so that $a+b\in I$. Thus $a+b\in S\cap I$.
\\ \\
Next we must prove (2), i.e., show that the product of an element of $S\cap I$ and an element of $S$ remains in $S\cap I$. Let $b\in S\cap I$ and let $a\in S$. Then $b\in S$, $b\in I$, and $a\in S \subset R$. Since $I$ is an ideal of $R$, it is closed under multiplication with elements of $R$; thus, $ab,ba\in I$. Since $S$ a ring, it is closed under multiplication, so we conclude from $a\in S$ and $b\in S$ that $ab,ba\in S$. Thus we have $ab,ba\in S\cap I$.
\\ \\
We have now shown the properties necessary for $S\cap I$ to be an ideal of $S$. 
\item[\textbf{(d)}]
Consider the map $f:S\to (S+I)/I$, $s\mapsto s+I$. We will show that $f$ is a ring morphism. First note that $S$ is a ring, as is $(S+I)/I$ by parts \textbf{(a)} and \textbf{(b)}. Next we note that $f$ clearly respects addition, multiplication, the zero, and the unity; therefore, $f$ is a ring morphism.
% \begin{quote}
% For $s_1,s_2\in S$, if $s_1=s_2$, we must show that $f(s_1)=f(s_2)$. \\
% Let $s_1,s_2\in S$. Then $f(s_1)=s_1+I$ and $f(s_2)=s_2+I$. If $s_1=s_2$, then we have $f(s_1)=s_2+I$. But $f(s_2)=s_2+I$, so $f(s_1)=f(s_2)$. 
% \end{quote} 
Hence, by the \textit{First Isomorphism Theorem for Rings}, we have 
\begin{align}
S/\ker{f}\cong f(S).
\label{sol.9.4}
\end{align}
Next we will show that $f$ is surjective, giving $f(S)=(S+I)/I$. \begin{quote}To show that $f$ is surjective, we must show that every element of $(S+I)/I$ has a preimage in $S$. Let $a+I \in (S+I)/I$. Thus, $a \in S+I$, so that $a = s+i$ for some $s \in S$ and $i \in I$. Hence, $a+I = s+I$ (since $a=s+i \in s+I$). Now, the definition of $f$ shows that $f(s) = s+I = a+I$. This shows that $a+I$ has a preimage in $S$. Thus, we have shown that each element of $(S+I)/I$ has a preimage, so that $f$ is surjective.\end{quote} Since $f$ is surjective, have $f(S) = (S+I)/I$, so that we can rewrite \eqref{sol.9.4} as
\begin{align}
S/\ker{f}\cong (S+I)/I .
\label{sol.9.5}
\end{align}
As our last step to achieving the desired result, we will show that $\ker f=S\cap I$. 
\begin{quote}
Let $s\in \ker f$. Then $f(s)=0+I$. But $f(s)=s+I$, so $s+I=0+I$. Thus $s\in I$. Also note that the kernel is a subset of the domain $S$, thus $s\in S$. Thus we have $s\in S\cap I$. This proves that $\ker f\subset S\cap I$. 
\end{quote}
\begin{quote}
Let $s\in S\cap I$. Then $s\in S$ and $s\in I$. Then $f(s)=s+I$. Since $s\in I$, we have $s+I=0+I$. Thus $f(s)=0+I$. Thus $s\in \ker f$. This proves $S\cap I\subset \ker f$.
\end{quote}
\begin{quote}
Thus $S\cap I=\ker f$.
\end{quote}
Hence, \eqref{sol.9.5} rewrites as $$S/(S\cap I)\cong (S+I)/I .$$
Moreover, the isomorphism $S/(S\cap I)\to (S+I)/I$ constructed by the \textit{First Isomorphism Theorem for Rings} sends each $s + \tup{S \cap I} \in S/(S\cap I)$ to $f(s) = s+I \in (S+I)/I$, as claimed in the exercise.
\\ \\
% ***I referenced my Abstract notes from undergraduate to follow this last proof in relation to the isomorphism theorems of groups***
\end{itemize}
\end{solution}


\end{document}
