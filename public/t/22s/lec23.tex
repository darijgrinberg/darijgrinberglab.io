\documentclass[numbers=enddot,12pt,final,onecolumn,notitlepage]{scrartcl}%
\usepackage[headsepline,footsepline,manualmark]{scrlayer-scrpage}
\usepackage[all,cmtip]{xy}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{framed}
\usepackage{comment}
\usepackage{color}
\usepackage[breaklinks=True]{hyperref}
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\usepackage{needspace}
\usepackage{tabls}
\usepackage{tikz}
\usepackage{bbding}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2960}
%TCIDATA{LastRevised=Saturday, June 10, 2023 19:32:05}
%TCIDATA{SuppressPackageManagement}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{Language=American English}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\usetikzlibrary{arrows.meta}
\usetikzlibrary{chains}
\newcounter{exer}
\newcounter{exera}
\theoremstyle{definition}
\newtheorem{theo}{Theorem}[subsection]
\newenvironment{theorem}[1][]
{\begin{theo}[#1]\begin{leftbar}}
{\end{leftbar}\end{theo}}
\newtheorem{lem}[theo]{Lemma}
\newenvironment{lemma}[1][]
{\begin{lem}[#1]\begin{leftbar}}
{\end{leftbar}\end{lem}}
\newtheorem{prop}[theo]{Proposition}
\newenvironment{proposition}[1][]
{\begin{prop}[#1]\begin{leftbar}}
{\end{leftbar}\end{prop}}
\newtheorem{defi}[theo]{Definition}
\newenvironment{definition}[1][]
{\begin{defi}[#1]\begin{leftbar}}
{\end{leftbar}\end{defi}}
\newtheorem{remk}[theo]{Remark}
\newenvironment{remark}[1][]
{\begin{remk}[#1]\begin{leftbar}}
{\end{leftbar}\end{remk}}
\newtheorem{coro}[theo]{Corollary}
\newenvironment{corollary}[1][]
{\begin{coro}[#1]\begin{leftbar}}
{\end{leftbar}\end{coro}}
\newtheorem{conv}[theo]{Convention}
\newenvironment{convention}[1][]
{\begin{conv}[#1]\begin{leftbar}}
{\end{leftbar}\end{conv}}
\newtheorem{quest}[theo]{Question}
\newenvironment{question}[1][]
{\begin{quest}[#1]\begin{leftbar}}
{\end{leftbar}\end{quest}}
\newtheorem{warn}[theo]{Warning}
\newenvironment{warning}[1][]
{\begin{warn}[#1]\begin{leftbar}}
{\end{leftbar}\end{warn}}
\newtheorem{conj}[theo]{Conjecture}
\newenvironment{conjecture}[1][]
{\begin{conj}[#1]\begin{leftbar}}
{\end{leftbar}\end{conj}}
\newtheorem{exam}[theo]{Example}
\newenvironment{example}[1][]
{\begin{exam}[#1]\begin{leftbar}}
{\end{leftbar}\end{exam}}
\newtheorem{exmp}[exer]{Exercise}
\newenvironment{exercise}[1][]
{\begin{exmp}[#1]\begin{leftbar}}
{\end{leftbar}\end{exmp}}
\newenvironment{statement}{\begin{quote}}{\end{quote}}
\newenvironment{fineprint}{\begin{small}}{\end{small}}
\iffalse
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\newenvironment{convention}[1][Convention]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\newenvironment{question}[1][Question]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\fi
\let\sumnonlimits\sum
\let\prodnonlimits\prod
\let\cupnonlimits\bigcup
\let\capnonlimits\bigcap
\renewcommand{\sum}{\sumnonlimits\limits}
\renewcommand{\prod}{\prodnonlimits\limits}
\renewcommand{\bigcup}{\cupnonlimits\limits}
\renewcommand{\bigcap}{\capnonlimits\limits}
\newcommand\arxiv[1]{\href{http://www.arxiv.org/abs/#1}{\texttt{arXiv:#1}}}
\setlength\tablinesep{3pt}
\setlength\arraylinesep{3pt}
\setlength\extrarulesep{3pt}
\setlength\textheight{22.5cm}
\setlength\textwidth{14.8cm}
\newenvironment{verlong}{}{}
\newenvironment{vershort}{}{}
\newenvironment{noncompile}{}{}
\excludecomment{verlong}
\includecomment{vershort}
\excludecomment{noncompile}
\newcommand{\defn}[1]{{\color{darkred}\emph{#1}}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\KK}{\mathbb{K}}
\newcommand{\set}[1]{\left\{ #1 \right\}}
\newcommand{\abs}[1]{\left| #1 \right|}
\newcommand{\tup}[1]{\left( #1 \right)}
\newcommand{\ive}[1]{\left[ #1 \right]}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\are}{\ar@{-}}
\newtheoremstyle{plainsl}
{8pt plus 2pt minus 4pt}
{8pt plus 2pt minus 4pt}
{\slshape}
{0pt}
{\bfseries}
{.}
{5pt plus 1pt minus 1pt}
{}
\theoremstyle{plainsl}
\ihead{Lecture 23, version \today}
\ohead{page \thepage}
\cfoot{}
\begin{document}
\section*{Math 530 Spring 2022, Lecture 23: Independent sets}

\textbf{website:} \url{https://www.cip.ifi.lmu.de/~grinberg/t/22s}

\section{Independent sets}

\subsection{Definition and lower bound}

Next, we define one of the most fundamental notions in graph theory:

\begin{definition}
\label{def.indep.indep}An \textbf{independent set} of a multigraph $G$ means a
subset $S$ of $\operatorname*{V}\left(  G\right)  $ such that no two elements
of $S$ are adjacent.
\end{definition}

In other words, an independent set of $G$ means an induced subgraph of $G$
that has no edges\footnote{This is a somewhat sloppy statement. Of course, an
independent set is not literally an induced subgraph, since the former is just
a set, while the latter is a graph. What I mean is that a subset $S$ of
$\operatorname*{V}\left(  G\right)  $ is independent if and only if the
induced subgraph $G\left[  S\right]  $ has no edges.}. Note that
\textquotedblleft no two elements of $S$\textquotedblright\ doesn't mean
\textquotedblleft no two distinct elements of $S$\textquotedblright.

Thus, for example, what we called an \textquotedblleft
anti-triangle\textquotedblright\ (back in Lecture 1) is an independent set of
size $3$.

\begin{remark}
Independent sets are closely related to proper colorings. Indeed, let $G$ be a
graph, and let $k\in\mathbb{N}$. Let $f:V\rightarrow\left\{  1,2,\ldots
,k\right\}  $ be a $k$-coloring. For each $i\in\left\{  1,2,\ldots,k\right\}
$, let
\begin{align*}
V_{i}:=  &  \left\{  v\in V\ \mid\ f\left(  v\right)  =i\right\} \\
=  &  \left\{  \text{all vertices of }G\text{ that have color }i\right\}  .
\end{align*}
Then, the $k$-coloring $f$ is proper if and only if the $k$ sets $V_{1}%
,V_{2},\ldots,V_{k}$ are independent sets of $G$. (Proving this is a matter of
unraveling the definitions of \textquotedblleft independent
sets\textquotedblright\ and \textquotedblleft proper $k$%
-colorings\textquotedblright.)
\end{remark}

One classical computational problem in graph theory is to find a maximum-size
independent set of a given graph. This problem is NP-hard, so don't expect a
quick algorithm or even a good formula for the maximum size of an independent
set. However, there are some lower bounds for this maximum size. Here is one:

\begin{theorem}
\label{thm.indep.lower-bd}Let $G=\left(  V,E,\varphi\right)  $ be a loopless
multigraph. Then, $G$ has an independent set of size
\[
\geq\sum_{v\in V}\dfrac{1}{1+\deg v}.
\]

\end{theorem}

\begin{example}
Let $G$ be the following loopless multigraph:%
\[%
%TCIMACRO{\TeXButton{tikz G}{\begin{tikzpicture}
%\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
%\node(A) at (0:2) {$1$};
%\node(B) at (60:2) {$2$};
%\node(C) at (120:2) {$3$};
%\node(D) at (180:2) {$4$};
%\node(E) at (240:2) {$5$};
%\node(F) at (300:2) {$6$};
%\end{scope}
%\begin{scope}[every edge/.style={draw=black,very thick}]
%\path
%[-] (A) edge (B) (B) edge (C) (C) edge (A) (C) edge (D) (D) edge (E) (E) edge (F) (F) edge (A);
%\end{scope}
%\end{tikzpicture}}}%
%BeginExpansion
\begin{tikzpicture}
\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
\node(A) at (0:2) {$1$};
\node(B) at (60:2) {$2$};
\node(C) at (120:2) {$3$};
\node(D) at (180:2) {$4$};
\node(E) at (240:2) {$5$};
\node(F) at (300:2) {$6$};
\end{scope}
\begin{scope}[every edge/.style={draw=black,very thick}]
\path
[-] (A) edge (B) (B) edge (C) (C) edge (A) (C) edge (D) (D) edge (E) (E) edge (F) (F) edge (A);
\end{scope}
\end{tikzpicture}%
%EndExpansion
\ \ .
\]
Then, the degrees of the vertices of $G$ are $3,2,3,2,2,2$. Hence, Theorem
\ref{thm.indep.lower-bd} yields that $G$ has an independent set of size%
\[
\geq\dfrac{1}{1+3}+\dfrac{1}{1+2}+\dfrac{1}{1+3}+\dfrac{1}{1+2}+\dfrac{1}%
{1+2}+\dfrac{1}{1+2}=\dfrac{11}{6}\approx1.83.
\]
Since the size of an independent set is always an integer, we can round this
up and conclude that $G$ has an independent set of size $\geq2$. In truth, $G$
actually has an independent set of size $3$ (namely, $\left\{  2,4,6\right\}
$), but there is no way to tell this from the degrees of its vertices alone.
For example, the vertices of the graph%
\[%
%TCIMACRO{\TeXButton{tikz H}{\begin{tikzpicture}
%\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
%\node(A) at (0:2) {$1$};
%\node(B) at (60:2) {$2$};
%\node(C) at (120:2) {$3$};
%\node(D) at (180:2) {$4$};
%\node(E) at (240:2) {$5$};
%\node(F) at (300:2) {$6$};
%\end{scope}
%\node(H) at (-3.3, 0) {$H:=$};
%\begin{scope}[every edge/.style={draw=black,very thick}]
%\path
%[-] (A) edge (B) (B) edge (C) (C) edge (A) (C) edge[bend right=40] (A) (D) edge (E) (E) edge (F) (F) edge (D);
%\end{scope}
%\end{tikzpicture}}}%
%BeginExpansion
\begin{tikzpicture}
\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
\node(A) at (0:2) {$1$};
\node(B) at (60:2) {$2$};
\node(C) at (120:2) {$3$};
\node(D) at (180:2) {$4$};
\node(E) at (240:2) {$5$};
\node(F) at (300:2) {$6$};
\end{scope}
\node(H) at (-3.3, 0) {$H:=$};
\begin{scope}[every edge/.style={draw=black,very thick}]
\path
[-] (A) edge (B) (B) edge (C) (C) edge (A) (C) edge[bend right=40] (A) (D) edge (E) (E) edge (F) (F) edge (D);
\end{scope}
\end{tikzpicture}%
%EndExpansion
\]
have the same degrees as those of $G$, but $H$ has no independent set of size
$3$.
\end{example}

We shall give two proofs of Theorem \ref{thm.indep.lower-bd}, both of them
illustrating useful techniques.\footnote{Note that the looplessness
requirement in Theorem \ref{thm.indep.lower-bd} is important: If $G$ has a
loop at each vertex, then the only independent set of $G$ is $\varnothing$.}

\begin{proof}
[First proof of Theorem \ref{thm.indep.lower-bd}.]Assume the contrary. Thus,
each independent set $S$ of $G$ has size%
\begin{equation}
\left\vert S\right\vert <\sum_{v\in V}\dfrac{1}{1+\deg v}.
\label{pf.thm.indep.lower-bd.1}%
\end{equation}


A $V$\textbf{-listing} shall mean a list of all vertices in $V$, with each
vertex occurring exactly once in the list. If $\sigma$ is a $V$-listing, then
we define a subset $J_{\sigma}$ of $V$ as follows:%
\[
J_{\sigma}:=\left\{  v\in V\ \mid\ v\text{ occurs \textbf{before} all
neighbors of }v\text{ in }\sigma\right\}  .
\]


[\textbf{Example:} Let $G$ be the following graph:%
\[%
%TCIMACRO{\TeXButton{tikz G}{\begin{tikzpicture}[scale=1.4]
%\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
%\node(5) at (0, 0) {$5$};
%\node(1) at (0, 1) {$1$};
%\node(6) at (1, 0) {$6$};
%\node(4) at (1, 1) {$4$};
%\node(2) at (1, 2) {$2$};
%\node(3) at (2, 2) {$3$};
%\node(7) at (2, 1) {$7$};
%\end{scope}
%\begin{scope}[every edge/.style={draw=black,very thick}]
%\path[-] (5) edge (1) edge (6);
%\path[-] (4) edge (1) edge (6) edge (3);
%\path[-] (3) edge (2) edge (7);
%\path[-] (1) edge (2) (6) edge (7);
%\end{scope}
%\end{tikzpicture}}}%
%BeginExpansion
\begin{tikzpicture}[scale=1.4]
\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
\node(5) at (0, 0) {$5$};
\node(1) at (0, 1) {$1$};
\node(6) at (1, 0) {$6$};
\node(4) at (1, 1) {$4$};
\node(2) at (1, 2) {$2$};
\node(3) at (2, 2) {$3$};
\node(7) at (2, 1) {$7$};
\end{scope}
\begin{scope}[every edge/.style={draw=black,very thick}]
\path[-] (5) edge (1) edge (6);
\path[-] (4) edge (1) edge (6) edge (3);
\path[-] (3) edge (2) edge (7);
\path[-] (1) edge (2) (6) edge (7);
\end{scope}
\end{tikzpicture}%
%EndExpansion
\ \ .
\]
Let $\sigma$ be the $V$-listing $\left(  1,2,7,5,3,4,6\right)  $. Then, the
vertex $1$ occurs before all its neighbors ($2$, $4$ and $5$) in $\sigma$, and
thus we have $1\in J_{\sigma}$. Likewise, the vertex $7$ occurs before all its
neighbors ($3$ and $6$) in $\sigma$, so that we have $7\in J_{\sigma}$. But
the vertex $2$ does not occur before all its neighbors in $\sigma$ (indeed, it
occurs after its neighbor $1$), so that we have $2\notin J_{\sigma}$.
Likewise, the vertices $5,3,4,6$ don't belong to $J_{\sigma}$. Altogether, we
thus obtain $J_{\sigma}=\left\{  1,7\right\}  $.] \medskip

The set $J_{\sigma}$ is an independent set of $G$ (because if two vertices $u$
and $v$ in $J_{\sigma}$ were adjacent, then $u$ would have to occur before $v$
in $\sigma$, but $v$ would have to occur before $u$ in $\sigma$; but these two
statements clearly contradict each other). Thus,
(\ref{pf.thm.indep.lower-bd.1}) (applied to $S=J_{\sigma}$) yields%
\[
\left\vert J_{\sigma}\right\vert <\sum_{v\in V}\dfrac{1}{1+\deg v}.
\]


This inequality holds for \textbf{each} $V$-listing $\sigma$. Thus, summing
this inequality over all $V$-listings $\sigma$, we obtain%
\begin{align}
\sum_{\sigma\text{ is a }V\text{-listing}}\left\vert J_{\sigma}\right\vert  &
<\sum_{\sigma\text{ is a }V\text{-listing}}\ \ \sum_{v\in V}\dfrac{1}{1+\deg
v}\nonumber\\
&  =\left(  \text{\# of all }V\text{-listings}\right)  \cdot\sum_{v\in
V}\dfrac{1}{1+\deg v}. \label{pf.thm.indep.lower-bd.3}%
\end{align}


On the other hand, I claim the following:

\begin{statement}
\textit{Claim 1:} For each $v\in V$, we have%
\[
\left(  \text{\# of all }V\text{-listings }\sigma\text{ satisfying }v\in
J_{\sigma}\right)  \geq\dfrac{\left(  \text{\# of all }V\text{-listings}%
\right)  }{1+\deg v}.
\]

\end{statement}

[\textit{Proof of Claim 1:} Fix a vertex $v\in V$. Define $\deg^{\prime}v$ to
be the \# of all neighbors of $v$. Clearly, $\deg^{\prime}v\leq\deg v$.

We shall call a $V$-listing $\sigma$ \textbf{good} if the vertex $v$ occurs in
it before all its neighbors. In other words, a $V$-listing $\sigma$ is good if
and only if it satisfies $v\in J_{\sigma}$ (because $v\in J_{\sigma}$ means
that the vertex $v$ occurs in $\sigma$ before all its neighbors\footnote{This
follows straight from the definition of $J_{\sigma}$.}). Thus, we must show
that
\[
\left(  \text{\# of all good }V\text{-listings}\right)  \geq\dfrac{\left(
\text{\# of all }V\text{-listings}\right)  }{1+\deg v}.
\]


We define a map%
\[
\Gamma:\left\{  \text{all }V\text{-listings}\right\}  \rightarrow\left\{
\text{all good }V\text{-listings}\right\}
\]
as follows: Whenever $\tau$ is a $V$-listing, we let $\Gamma\left(
\tau\right)  $ be the $V$-listing obtained from $\tau$ by swapping $v$ with
the first neighbor of $v$ that occurs in $\tau$ (or, if $\tau$ is already
good, then we just do nothing, i.e., we set $\Gamma\left(  \tau\right)  =\tau
$). This map $\Gamma$ is a $\left(  1+\deg^{\prime}v\right)  $-to-$1$
correspondence -- i.e., for each good $V$-listing $\sigma$, there are exactly
$1+\deg^{\prime}v$ many $V$-listings $\tau$ that satisfy $\Gamma\left(
\tau\right)  =\sigma$ (in fact, one of these $\tau$'s is $\sigma$ itself, and
the remaining $\deg^{\prime}v$ many of these $\tau$'s are obtained from
$\sigma$ by switching $v$ with some neighbor of $v$). Hence, by the
multijection principle\footnote{See a footnote in Lecture 17 for the statement
of the multijection principle.}, we conclude that
\[
\left\vert \left\{  \text{all }V\text{-listings}\right\}  \right\vert =\left(
1+\deg^{\prime}v\right)  \cdot\left\vert \left\{  \text{all good
}V\text{-listings}\right\}  \right\vert .
\]
In other words,%
\[
\left(  \text{\# of all }V\text{-listings}\right)  =\left(  1+\deg^{\prime
}v\right)  \cdot\left(  \text{\# of all good }V\text{-listings}\right)  .
\]
Hence,%
\[
\left(  \text{\# of all good }V\text{-listings}\right)  =\dfrac{\left(
\text{\# of all }V\text{-listings}\right)  }{1+\deg^{\prime}v}\geq
\dfrac{\left(  \text{\# of all }V\text{-listings}\right)  }{1+\deg v}%
\]
(since $\deg^{\prime}v\leq\deg v$). This proves Claim 1 (since the good
$V$-listings are precisely the $V$-listings $\sigma$ satisfying $v\in
J_{\sigma}$).] \medskip

Next, we recall a basic property of the Iverson bracket notation\footnote{See,
e.g., Lecture 18 for the definition of the Iverson bracket notation.}: If $T$
is a subset of a finite set $S$, then
\begin{equation}
\left\vert T\right\vert =\sum_{v\in S}\left[  v\in T\right]  .
\label{pf.thm.indep.lower-bd.4}%
\end{equation}
(Indeed, the sum $\sum_{v\in S}\left[  v\in T\right]  $ contains an addend
equal to $1$ for each $v\in T$, and an addend equal to $0$ for each $v\in
S\setminus T$. Thus, this sum amounts to $\left\vert T\right\vert
\cdot1+\left\vert S\setminus T\right\vert \cdot0=\left\vert T\right\vert $.)

Now, (\ref{pf.thm.indep.lower-bd.3}) yields%
\begin{align*}
&  \left(  \text{\# of all }V\text{-listings}\right)  \cdot\sum_{v\in V}%
\dfrac{1}{1+\deg v}\\
&  >\sum_{\sigma\text{ is a }V\text{-listing}}\underbrace{\left\vert
J_{\sigma}\right\vert }_{\substack{=\sum\limits_{v\in V}\left[  v\in
J_{\sigma}\right]  \\\text{(by (\ref{pf.thm.indep.lower-bd.4}))}%
}}=\underbrace{\sum_{\sigma\text{ is a }V\text{-listing}}\ \ \sum\limits_{v\in
V}}_{=\sum\limits_{v\in V}\ \ \sum_{\sigma\text{ is a }V\text{-listing}}%
}\left[  v\in J_{\sigma}\right] \\
&  =\sum\limits_{v\in V}\ \ \underbrace{\sum_{\sigma\text{ is a }%
V\text{-listing}}\left[  v\in J_{\sigma}\right]  }%
_{\substack{_{\substack{=\left(  \text{\# of all }V\text{-listings }%
\sigma\text{ satisfying }v\in J_{\sigma}\right)  }}\\\text{(because the sum
}\sum_{\sigma\text{ is a }V\text{-listing}}\left[  v\in J_{\sigma}\right]
\\\text{contains an addend equal to }1\text{ for each }V\text{-listing }%
\sigma\text{ satisfying }v\in J_{\sigma}\text{,}\\\text{and an addend equal to
}0\text{ for each other }V\text{-listing }\sigma\text{)}}}\\
&  =\sum\limits_{v\in V}\underbrace{\left(  \text{\# of all }V\text{-listings
}\sigma\text{ satisfying }v\in J_{\sigma}\right)  }_{\substack{\geq
\dfrac{\left(  \text{\# of all }V\text{-listings}\right)  }{1+\deg
v}\\\text{(by Claim 1)}}}\\
&  \geq\sum\limits_{v\in V}\dfrac{\left(  \text{\# of all }V\text{-listings}%
\right)  }{1+\deg v}=\left(  \text{\# of all }V\text{-listings}\right)
\cdot\sum_{v\in V}\dfrac{1}{1+\deg v}.
\end{align*}
This is absurd (since no real number $x$ can satisfy $x>x$). So we got a
contradiction, and our proof of Theorem \ref{thm.indep.lower-bd} is complete.
\end{proof}

\begin{remark}
This proof is an example of a \textbf{probabilistic proof}. Why? We have been
manipulating sums, but we could easily replace these sums by averages. Claim 1
then would say the following: For any given vertex $v\in V$, the
\textbf{probability} that a (uniformly random) $V$-listing $\sigma$ satisfies
$v\in J_{\sigma}$ is $\geq\dfrac{1}{1+\deg v}$. Thus, the expectation of
$\left\vert J_{\sigma}\right\vert $ is $\geq\sum\limits_{v\in V}\dfrac
{1}{1+\deg v}$ (by linearity of expectation). Therefore, at least one
$V$-listing $\sigma$ actually satisfies $\left\vert J_{\sigma}\right\vert
\geq\sum\limits_{v\in V}\dfrac{1}{1+\deg v}$. So the whole proof can be
restated in terms of probabilities and expectations.

Note that this proof (as it stands) is fairly useless as it comes to actually
\textbf{finding} an independent set of size $\geq\sum\limits_{v\in V}\dfrac
{1}{1+\deg v}$. It does not give any better algorithm than \textquotedblleft
try the subsets $J_{\sigma}$ for all possible $V$-listings $\sigma$; one of
them will work\textquotedblright, which is even slower than trying all subsets
of $V$.

Note also that the proof does \textbf{not} entail that at least half of the
$V$-listings $\sigma$ will satisfy $\left\vert J_{\sigma}\right\vert \geq
\sum\limits_{v\in V}\dfrac{1}{1+\deg v}$. The mean is not the median!
\end{remark}

Let us now give a second proof of the theorem, which does provide a good algorithm:

\begin{proof}
[Second proof of Theorem \ref{thm.indep.lower-bd}.]We proceed by strong
induction on $\left\vert V\right\vert $. Thus, we fix $p\in\mathbb{N}$, and we
assume (as the induction hypothesis) that Theorem \ref{thm.indep.lower-bd} is
already proved for all loopless multigraphs $G$ with $<p$ vertices. We must
now prove it for a loopless multigraph $G=\left(  V,E,\varphi\right)  $ with
$p$ vertices.

If $\left\vert V\right\vert =0$, then this is clear (since $\varnothing$ is an
independent set of appropriate size). Thus, we WLOG assume that $\left\vert
V\right\vert \neq0$. We furthermore assume WLOG that $G$ is a simple graph
(because otherwise, we can replace $G$ by $G^{\operatorname*{simp}}$; this can
only decrease the degrees $\deg v$ of the vertices $v\in V$, and thus our
claim only becomes stronger).

Since $\left\vert V\right\vert \neq0$, there exists a vertex $u\in V$ with
$\deg_{G}u$ minimum\footnote{Here, the notation $\deg_{H}u$\ means the degree
of a vertex $u$ in a graph $H$.}. Pick such a $u$. Thus,%
\begin{equation}
\deg_{G}v\geq\deg_{G}u\ \ \ \ \ \ \ \ \ \ \text{for each }v\in V.
\label{pf.thm.indep.lower-bd.2nd.mindeg}%
\end{equation}


Let $U:=\left\{  u\right\}  \cup\left\{  \text{all neighbors of }u\right\}  $.
Thus, $U\subseteq V$ and $\left\vert U\right\vert =1+\deg_{G}u$ (this is a
honest equality, since $G$ is a simple graph).

Let $G^{\prime}$ be the induced subgraph of $G$ on the set $V\setminus U$.
This is the simple graph obtained from $G$ by removing all vertices belonging
to $U$ (that is, removing the vertex $u$ along with all its neighbors) and
removing all edges that require these vertices. Then, $G^{\prime}$ has fewer
vertices than $G$. Hence, $G^{\prime}$ has $<p$ vertices (since $G$ has $p$
vertices). Hence, by the induction hypothesis, Theorem
\ref{thm.indep.lower-bd} is already proved for $G^{\prime}$. In other words,
$G^{\prime}$ has an independent set of size
\[
\geq\sum_{v\in V\setminus U}\dfrac{1}{1+\deg_{G^{\prime}}v}.
\]
Let $T$ be such an independent set. Set $S:=\left\{  u\right\}  \cup T$. Then,
$S$ is an independent set of $G$ (since $T\subseteq V\setminus U$, so that $T$
contains no neighbors of $u$). Moreover, I claim that $\left\vert S\right\vert
\geq\sum\limits_{v\in V}\dfrac{1}{1+\deg_{G}v}$. Indeed, this follows from%
\begin{align*}
\sum\limits_{v\in V}\dfrac{1}{1+\deg_{G}v}  &  =\sum\limits_{v\in
U}\underbrace{\dfrac{1}{1+\deg_{G}v}}_{\substack{\leq\dfrac{1}{1+\deg_{G}%
u}\\\text{(since }\deg_{G}v\geq\deg_{G}u\\\text{(by
(\ref{pf.thm.indep.lower-bd.2nd.mindeg})))}}}+\sum\limits_{v\in V\setminus
U}\underbrace{\dfrac{1}{1+\deg_{G}v}}_{\substack{\leq\dfrac{1}{1+\deg
_{G^{\prime}}v}\\\text{(since }\deg_{G}v\geq\deg_{G^{\prime}}v\\\text{(because
}G^{\prime}\text{ is a subgraph of }G\text{))}}}\\
&  \leq\underbrace{\sum\limits_{v\in U}\dfrac{1}{1+\deg_{G}u}}%
_{\substack{=\left\vert U\right\vert \cdot\dfrac{1}{1+\deg_{G}u}%
\\=1\\\text{(since }\left\vert U\right\vert =1+\deg_{G}u\text{)}%
}}+\underbrace{\sum\limits_{v\in V\setminus U}\dfrac{1}{1+\deg_{G^{\prime}}v}%
}_{\substack{\leq\left\vert T\right\vert \\\text{(since }T\text{ has size
}\geq\sum_{v\in V\setminus U}\dfrac{1}{1+\deg_{G^{\prime}}v}\text{)}}}\\
&  \leq1+\left\vert T\right\vert =\left\vert S\right\vert
\ \ \ \ \ \ \ \ \ \ \left(  \text{since }S=\left\{  u\right\}  \cup T\right)
.
\end{align*}
So we have found an independent set of $G$ having size $\geq\sum\limits_{v\in
V}\dfrac{1}{1+\deg_{G}v}$ (namely, $S$). This means that Theorem
\ref{thm.indep.lower-bd} holds for our $G$. This completes the induction step,
and Theorem \ref{thm.indep.lower-bd} is proved.
\end{proof}

\begin{remark}
The second proof of Theorem \ref{thm.indep.lower-bd} (unlike the first one)
does give a fairly efficient algorithm for finding an independent set of the
appropriate size. However, the second proof is actually not that much
different from the first proof; it can in fact be recovered from the first
proof by \textbf{derandomization}, specifically using the
\textbf{\href{https://en.wikipedia.org/wiki/Method_of_conditional_probabilities}{\textbf{method
of conditional probabilities}}}. (This is a general technique for
\textquotedblleft derandomizing\textquotedblright\ probabilistic proofs, i.e.,
turning them into algorithmic ones. It often requires some ingenuity and is
not guaranteed to always work, but the above is an example where it can be
applied. See \cite[Chapter 13]{Aspnes23} for much more about derandomization.)
\end{remark}

\subsection{A weaker (but simpler) lower bound}

Let us now weaken Theorem \ref{thm.indep.lower-bd} a bit:

\begin{corollary}
\label{cor.indep.anti-turan}Let $G$ be a loopless multigraph with $n$ vertices
and $m$ edges. Then, $G$ has an independent set of size%
\[
\geq\dfrac{n^{2}}{n+2m}.
\]

\end{corollary}

In order to prove this, we will need the following inequality:

\begin{lemma}
\label{lem.indep.cauchy-lame}Let $a_{1},a_{2},\ldots,a_{n}$ be $n$ positive
reals. Then,%
\[
\dfrac{1}{a_{1}}+\dfrac{1}{a_{2}}+\cdots+\dfrac{1}{a_{n}}\geq\dfrac{n^{2}%
}{a_{1}+a_{2}+\cdots+a_{n}}.
\]

\end{lemma}

\begin{proof}
[Proof of Lemma \ref{lem.indep.cauchy-lame}.]There are several ways to prove this:

\begin{itemize}
\item Apply \href{https://en.wikipedia.org/wiki/Jensen_inequality}{Jensen's
inequality} to the convex function $\mathbb{R}^{+}\rightarrow\mathbb{R}%
^{+},\ x\mapsto\dfrac{1}{x}$.

\item Apply \href{https://en.wikipedia.org/wiki/Cauchy-Schwarz_inequality}{the
Cauchy-Schwarz inequality} to get
\begin{align*}
&  \left(  a_{1}+a_{2}+\cdots+a_{n}\right)  \left(  \dfrac{1}{a_{1}}+\dfrac
{1}{a_{2}}+\cdots+\dfrac{1}{a_{n}}\right) \\
&  \geq\left(  \underbrace{\sqrt{a_{1}\dfrac{1}{a_{1}}}+\sqrt{a_{2}\dfrac
{1}{a_{2}}}+\cdots+\sqrt{a_{n}\dfrac{1}{a_{n}}}}_{=n}\right)  ^{2}=n^{2}.
\end{align*}


\item Apply \href{https://en.wikipedia.org/wiki/HM-GM-AM-QM_inequalities}{the
AM-HM inequality}.

\item Apply \href{https://en.wikipedia.org/wiki/HM-GM-AM-QM_inequalities}{the
AM-GM inequality} twice, then multiply.

\item There is a direct proof, too: First, recall the famous inequality
\begin{equation}
\dfrac{u}{v}+\dfrac{v}{u}\geq2, \label{pf.lem.indep.cauchy-lame.uvvu}%
\end{equation}
which holds for any two positive reals $u$ and $v$. (This follows by observing
that $\dfrac{u}{v}+\dfrac{v}{u}-2=\dfrac{\left(  u-v\right)  ^{2}}{uv}\geq0$.)
Now,
\begin{align*}
&  \left(  a_{1}+a_{2}+\cdots+a_{n}\right)  \left(  \dfrac{1}{a_{1}}+\dfrac
{1}{a_{2}}+\cdots+\dfrac{1}{a_{n}}\right) \\
&  =\left(  \sum_{i=1}^{n}a_{i}\right)  \left(  \sum_{j=1}^{n}\dfrac{1}{a_{j}%
}\right)  =\sum_{i=1}^{n}\ \ \sum_{j=1}^{n}a_{i}\dfrac{1}{a_{j}}=\sum
_{i=1}^{n}\ \ \sum_{j=1}^{n}\dfrac{a_{i}}{a_{j}}\\
&  =\dfrac{1}{2}\left(  \sum_{i=1}^{n}\ \ \sum_{j=1}^{n}\dfrac{a_{i}}{a_{j}%
}+\sum_{i=1}^{n}\ \ \sum_{j=1}^{n}\dfrac{a_{i}}{a_{j}}\right)
\ \ \ \ \ \ \ \ \ \ \left(  \text{since }x=\dfrac{1}{2}\left(  x+x\right)
\text{ for any }x\in\mathbb{R}\right) \\
&  =\dfrac{1}{2}\left(  \sum_{i=1}^{n}\ \ \sum_{j=1}^{n}\dfrac{a_{i}}{a_{j}%
}+\sum_{j=1}^{n}\ \ \sum_{i=1}^{n}\dfrac{a_{j}}{a_{i}}\right)
\ \ \ \ \ \ \ \ \ \ \left(
\begin{array}
[c]{c}%
\text{here, we renamed }i\text{ and }j\text{ as }j\text{ and }i\\
\text{in the second double sum}%
\end{array}
\right) \\
&  =\dfrac{1}{2}\left(  \sum_{i=1}^{n}\ \ \sum_{j=1}^{n}\dfrac{a_{i}}{a_{j}%
}+\sum_{i=1}^{n}\ \ \sum_{j=1}^{n}\dfrac{a_{j}}{a_{i}}\right)
\ \ \ \ \ \ \ \ \ \ \left(
\begin{array}
[c]{c}%
\text{here, we swapped the two}\\
\text{summation signs in the}\\
\text{second double sum}%
\end{array}
\right) \\
&  =\dfrac{1}{2}\sum_{i=1}^{n}\ \ \sum_{j=1}^{n}\underbrace{\left(
\dfrac{a_{i}}{a_{j}}+\dfrac{a_{j}}{a_{i}}\right)  }_{\substack{\geq
2\\\text{(by (\ref{pf.lem.indep.cauchy-lame.uvvu}))}}}\geq\dfrac{1}%
{2}\underbrace{\sum_{i=1}^{n}\ \ \sum_{j=1}^{n}2}_{=n^{2}\cdot2}=\dfrac{1}%
{2}n^{2}\cdot2=n^{2},
\end{align*}
from which the claim of Lemma \ref{lem.indep.cauchy-lame} follows.
\end{itemize}
\end{proof}

\begin{proof}
[Proof of Corollary \ref{cor.indep.anti-turan}.]Write the multigraph $G$ as
$G=\left(  V,E,\varphi\right)  $. Thus, $\left\vert V\right\vert =n$ and
$\left\vert E\right\vert =m$. We WLOG assume that $V=\left\{  1,2,\ldots
,n\right\}  $ (since $\left\vert V\right\vert =n$). Hence,%
\begin{align*}
\sum_{v=1}^{n}\deg v  &  =\sum_{v\in V}\deg v=2\cdot\underbrace{\left\vert
E\right\vert }_{=m}\ \ \ \ \ \ \ \ \ \ \left(  \text{by Proposition 1.1.3 in
Lecture 2}\right) \\
&  =2m.
\end{align*}
However, Theorem \ref{thm.indep.lower-bd} yields that $G$ has an independent
set of size%
\begin{align*}
&  \geq\sum_{v\in V}\dfrac{1}{1+\deg v}=\sum_{v=1}^{n}\dfrac{1}{1+\deg
v}\ \ \ \ \ \ \ \ \ \ \left(  \text{since }V=\left\{  1,2,\ldots,n\right\}
\right) \\
&  \geq\dfrac{n^{2}}{\sum_{v=1}^{n}\left(  1+\deg v\right)  }%
\ \ \ \ \ \ \ \ \ \ \left(
\begin{array}
[c]{c}%
\text{by Lemma \ref{lem.indep.cauchy-lame}, applied to the }n\text{
positive}\\
\text{reals }a_{v}=1+\deg v\text{ for all }v\in\left\{  1,2,\ldots,n\right\}
\end{array}
\right) \\
&  =\dfrac{n^{2}}{n+2m}\ \ \ \ \ \ \ \ \ \ \left(  \text{since }\sum_{v=1}%
^{n}\left(  1+\deg v\right)  =n+\underbrace{\sum_{v\in V}\deg v}%
_{=2m}=n+2m\right)  .
\end{align*}
This proves Corollary \ref{cor.indep.anti-turan}.
\end{proof}

\subsection{A proof of Turan's theorem}

Recall Turan's theorem, which we stated but did not prove in Lecture 2:

\begin{theorem}
[Turan's theorem]\label{thm.indep.turan}Let $r$ be a positive integer. Let $G$
be a simple graph with $n$ vertices and $e$ edges. Assume that%
\[
e>\dfrac{r-1}{r}\cdot\dfrac{n^{2}}{2}.
\]
Then, there exist $r+1$ distinct vertices of $G$ that are mutually adjacent
(i.e., any two distinct vertices among these $r+1$ vertices are adjacent).
\end{theorem}

We can now easily derive it from Corollary \ref{cor.indep.anti-turan}:

\begin{proof}
[Proof of Theorem \ref{thm.indep.turan}.] Write the simple graph $G$ as
$G=\left(  V,E\right)  $. Thus, $\left\vert V\right\vert =n$ and $\left\vert
E\right\vert =e$ and $E\subseteq\mathcal{P}_{2}\left(  V\right)  $.

Let $E^{\prime}:=\mathcal{P}_{2}\left(  V\right)  \setminus E$. Thus, the set
$E^{\prime}$ consists of all \textquotedblleft non-edges\textquotedblright\ of
$G$ -- that is, of all $2$-element subsets of $V$ that are not edges of $G$.
Clearly,%
\[
\left\vert E^{\prime}\right\vert =\left\vert \mathcal{P}_{2}\left(  V\right)
\setminus E\right\vert =\underbrace{\left\vert \mathcal{P}_{2}\left(
V\right)  \right\vert }_{=\dbinom{n}{2}}-\underbrace{\left\vert E\right\vert
}_{=e}=\dbinom{n}{2}-e.
\]


Now, let $G^{\prime}$ be the simple graph $\left(  V,E^{\prime}\right)  $.
This simple graph $G^{\prime}$ is called the \textbf{complementary graph} of
$G$; it has $n$ vertices and $\left\vert E^{\prime}\right\vert =\dbinom{n}%
{2}-e$ edges.\footnote{For example, if $%
%TCIMACRO{\TeXButton{tikz graph}{\begin{tikzpicture}[scale=0.8]
%\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
%\node(A) at (0:2) {$1$};
%\node(B) at (360/5:2) {$2$};
%\node(C) at (2*360/5:2) {$3$};
%\node(D) at (3*360/5:2) {$4$};
%\node(E) at (4*360/5:2) {$5$};
%\end{scope}
%\node(X) at (-3.2, 0) {$G = $};
%\begin{scope}[every edge/.style={draw=black,very thick}]
%\path[-] (A) edge (B) (B) edge (C) (C) edge (D) (D) edge (E);
%\end{scope}
%\end{tikzpicture}}}%
%BeginExpansion
\begin{tikzpicture}[scale=0.8]
\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
\node(A) at (0:2) {$1$};
\node(B) at (360/5:2) {$2$};
\node(C) at (2*360/5:2) {$3$};
\node(D) at (3*360/5:2) {$4$};
\node(E) at (4*360/5:2) {$5$};
\end{scope}
\node(X) at (-3.2, 0) {$G = $};
\begin{scope}[every edge/.style={draw=black,very thick}]
\path[-] (A) edge (B) (B) edge (C) (C) edge (D) (D) edge (E);
\end{scope}
\end{tikzpicture}%
%EndExpansion
$ , then $%
%TCIMACRO{\TeXButton{tikz complement}{\begin{tikzpicture}[scale=0.8]
%\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
%\node(A) at (0:2) {$1$};
%\node(B) at (360/5:2) {$2$};
%\node(C) at (2*360/5:2) {$3$};
%\node(D) at (3*360/5:2) {$4$};
%\node(E) at (4*360/5:2) {$5$};
%\end{scope}
%\node(X) at (-3.2, 0) {$G' = $};
%\begin{scope}[every edge/.style={draw=black,very thick}]
%\path
%[-] (A) edge (C) (C) edge (E) (E) edge (B) (B) edge (D) (D) edge (A) (A) edge (E);
%\end{scope}
%\end{tikzpicture}}}%
%BeginExpansion
\begin{tikzpicture}[scale=0.8]
\begin{scope}[every node/.style={circle,thick,draw=green!60!black}]
\node(A) at (0:2) {$1$};
\node(B) at (360/5:2) {$2$};
\node(C) at (2*360/5:2) {$3$};
\node(D) at (3*360/5:2) {$4$};
\node(E) at (4*360/5:2) {$5$};
\end{scope}
\node(X) at (-3.2, 0) {$G' = $};
\begin{scope}[every edge/.style={draw=black,very thick}]
\path
[-] (A) edge (C) (C) edge (E) (E) edge (B) (B) edge (D) (D) edge (A) (A) edge (E);
\end{scope}
\end{tikzpicture}%
%EndExpansion
$ .} Hence, Corollary \ref{cor.indep.anti-turan} (applied to $G^{\prime}$ and
$\dbinom{n}{2}-e$ instead of $G$ and $m$) yields that $G^{\prime}$ has an
independent set of size%
\[
\geq\dfrac{n^{2}}{n+2\cdot\left(  \dbinom{n}{2}-e\right)  }.
\]
Let $S$ be this independent set. Its size is%
\[
\left\vert S\right\vert \geq\dfrac{n^{2}}{n+2\cdot\left(  \dbinom{n}%
{2}-e\right)  }=\dfrac{n^{2}}{n+n\left(  n-1\right)  -2e}=\dfrac{n^{2}}%
{n^{2}-2e}>r
\]
(this inequality follows by high-school algebra from $e>\dfrac{r-1}{r}%
\cdot\dfrac{n^{2}}{2}$). Hence, $\left\vert S\right\vert \geq r+1$ (since
$\left\vert S\right\vert $ and $r$ are integers). However, $S$ is an
independent set of $G^{\prime}$. Thus, any two distinct vertices in $S$ are
non-adjacent in $G^{\prime}$ and therefore adjacent in $G$ (by the definition
of $G^{\prime}$). Since $\left\vert S\right\vert \geq r+1$, we have thus found
$r+1$ (or more) distinct vertices of $G$ that are mutually adjacent in $G$.
This proves Theorem \ref{thm.indep.turan}.
\end{proof}

Several other beautiful proofs of Theorem \ref{thm.indep.turan} can be found
in \cite[Chapter 41]{AigZie18}.

\begin{thebibliography}{99999999}                                                                                         %


\bibitem[AigZie18]{AigZie18}%
\href{https://doi.org/10.1007/978-3-662-57265-8}{Martin Aigner, G\"{u}nter M.
Ziegler, \textit{Proofs from THE BOOK}, 6th edition, Springer 2018.}

\bibitem[Aspnes23]{Aspnes23}James Aspnes, \textit{Notes on Randomized
Algorithms (CPSC 469/569: Spring 2023)}, 1 May 2023.\newline\url{https://www.cs.yale.edu/homes/aspnes/classes/469/notes.pdf}
\end{thebibliography}


\end{document}