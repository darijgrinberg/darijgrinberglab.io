<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Discrete Mathematics (Math 221), Winter 2023">
  <meta name="keywords" content="mathematics, discrete mathematics, combinatorics, arithmetic, number theory, graphs, teaching, courses, drexel, math 221">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Discrete Mathematics (Math 221), Winter 2023</title>
</head>
<body>

<h1 style="text-align:center">Math 221: Discrete Mathematics, Winter 2023 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd>Classes are over now. <!-- Tue 9:30--10:50 in Pearlstein Learning Center, 307. --> </dd>
  <!--
  <dt>Office hours:</dt>
  <dd>Tue 11:30 AM -- 12:30 PM and Tue 1:30--2:00 PM in my office (Korman Center 263). Also by appointment on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>.</dd>
  -->
  <dt>Notes:</dt>
  <dd>see below.</dd>
  <dt>Blackboard:</dt>
  <dd><a href="https://learn.dcollege.net/ultra/courses/_331219_1/cl/outline">https://learn.dcollege.net/ultra/courses/_331219_1/cl/outline</a>.</dd>
  <dt>Gradescope:</dt>
  <dd><a href="https://www.gradescope.com/courses/487830">https://www.gradescope.com/courses/487830</a>.</dd>
  <!--
  <dt>Piazza:</dt>
  <dd><a href="https://piazza.com/drexel/fall2020/235">https://piazza.com/drexel/fall2020/235</a>.</dd>
  -->
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
</dl>
</div>

<h2>Course description</h2>

<p>This course introduces a range of topics in Discrete Mathematics, including set theory, counting, number theory, 
graphs, and cryptography.
</p>

<p><strong>Level:</strong> undergraduate. </p>

<p><strong>Prerequisites:</strong> Math 220 (Introduction to Proofs) or CS 270 (Mathematical Foundations of Computer Science) or ECE 200 (Digital Logic Design). </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Recommended:</dt>
  <dd><ul>
         <li> <a href="https://www.math.miami.edu/~armstrong/309fa22.php">[Armstrong]: Drew Armstrong, <cite>Fall 2022 Math 309 Discrete Math</cite></a>: Lecture notes by a good expositor. </li>
         <li> <a href="https://courses.csail.mit.edu/6.042/spring18/mcs.pdf">[LeLeMe]: Eric Lehman, F. Thomson Leighton, Albert R. Meyer, <cite>Mathematics for Computer Science</cite>, 2018</a>: a great introduction to rigorous mathematics. Chapters 1--5 cover the Math 220 basics we will need: proofs and sets. Our course mostly draws from the later chapters. </li>
         <li> <a href="https://discrete.openmathbooks.org/dmoi3.html">[Levin]: Oscar Levin, <cite>Discrete Mathematics: An open introduction</cite></a>: another text covering most of our material. </li>
         <li> <a href="https://infinitedescent.xyz">[Newstead]: Clive Newstead, <cite>An Infinite Descent into Pure Mathematics</cite></a>: introduction to proofs and mathematical thinking. (Work in progress.) </li>
      </ul> </dd>
  <dt>Remedial:</dt>
  <dd><ul>
         <li> If your logic basics get rusty, some of the above recommended texts include introductions to proof. There are also texts dedicated to this topic alone: </li>
         <li> <a href="https://www.people.vcu.edu/~rhammack/BookOfProof/">[Hammack]: Richard Hammack, <cite>Book of Proof</cite></a>: introduction to proofs. </li>
         <li> <a href="https://math.stackexchange.com/questions/3316114/book-recommendation-for-proof/3316157#3316157">A list of undergraduate-level texts and notes on proofs</a> (freely accessible). </li>
      </ul> </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  <dt>Note:</dt>
  <dd><ul>
         <li> The notes below have evolved into <a href="../24wd/24wd.pdf">my <cite>Introduction to Discrete
Mathematics</cite> text</a> (which includes all the material covered in the notes as well as some additions). While the notes below occasionally see corrections, all new development happens only in the just-mentioned text. </li>
  </ul></dd>

  <dt>All weeks:</dt>
  <dd><a href="https://gitlab.com/darijgrinberg/darijgrinberg.gitlab.io/-/archive/master/darijgrinberg.gitlab.io-master.zip?path=public/t/23wd">ZIP archive containing all the lecture notes and assignments listed below</a>.</dd>

  <dt>Week 0:</dt>
  <dd><ul>
         <li> This week's material: none yet :) </li>
         <li> For first-time LaTeX users: <a href="https://kconrad.math.uconn.edu/math5210f20/latex/latexintro.pdf">Keith Conrad's introduction to LaTeX</a>. Also, an amply commented <a href="../19s/hw0s.tex">sample homework solution file</a> written in LaTeX for an old class (<a href="../19s/hw0s.pdf">compiled version</a>). </li>
         <li> Advice on mathematical writing <a href="https://kconrad.math.uconn.edu/blurbs/proofs/writingtips.pdf">from Keith Conrad</a> (beginner level), <a href="https://www.math.ucla.edu/~pak/papers/how-to-write1.pdf">from Igor Pak</a> (intermediate level), and <a href="http://tex.loria.fr/typographie/mathwriting.pdf">from Donald E. Knuth, Tracy Larrabee and Paul M. Roberts</a> (expert level). </li>
  </ul></dd>

  <dt>Week 1:</dt>
  <dd><ul>
         <li> <a href="lec01.pdf">Lecture 1 (Tower of Hanoi)</a> (<a href="lec01.tex">source code</a>). </li>
         <li> <a href="lec02.pdf">Lecture 2 (Induction)</a> (<a href="lec02.tex">source code</a>). </li>
         <li> More examples of induction proofs: see Lectures 11-15 of <a href="../21fin">Fall 2021 Math 220</a> or Chapter 5 in <a href="https://courses.csail.mit.edu/6.042/spring18/mcs.pdf">[LeLeMe]</a>. </li>
         <li> <a href="hw1.pdf">Homework set 1</a> (<a href="hw1.tex">source code</a>). </li>
  </ul></dd>


  <dt>Week 2:</dt>
  <dd><ul>
         <li> <a href="lec03.pdf">Lecture 3 (more induction)</a> (<a href="lec03.tex">source code</a>). </li>
         <li> <a href="lec04.pdf">Lecture 4 (strong induction)</a> (<a href="lec04.tex">source code</a>). </li>
  </ul></dd>


  <dt>Week 3:</dt>
  <dd><ul>
         <li> <a href="lec05.pdf">Lecture 5 (summation and product signs; factorials; binomial coefficients)</a> (<a href="lec05.tex">source code</a>). </li>
         <li> <a href="hw2.pdf">Homework set 2</a> (<a href="hw2.tex">source code</a>). </li>
         <li> <a href="lec06.pdf">Lecture 6 (binomial coefficients; binomial formula)</a> (<a href="lec06.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 4:</dt>
  <dd><ul>
         <li> <a href="lec07.pdf">Lecture 7 (divisibility; modular arithmetic)</a> (<a href="lec07.tex">source code</a>). </li>
         <li> <a href="lec08.pdf">Lecture 8 (division with remainder; base-b representation)</a> (<a href="lec08.tex">source code</a>). </li>
         <li> For some alternative proofs, see Lectures 18-20 of <a href="../21fin">Fall 2021 Math 220</a>. </li>
         <li> <a href="hw3.pdf">Homework set 3</a> (<a href="hw3.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 5:</dt>
  <dd><ul>
         <li> <a href="lec09.pdf">Lecture 9 (gcds; the Euclidean algorithm)</a> (<a href="lec09.tex">source code</a>). </li>
         <li> <a href="gcdvariant.pdf">Alternative treatment of gcds</a> (<a href="gcdvariant.tex">source code</a>). This covers roughly the same material as the Lecture 9 part of Section 3.4, but is organized somewhat differently. </li>
         <li> <a href="lec10.pdf">Lecture 10 (more gcds; prime numbers; Fermat's little theorem)</a> (<a href="lec10.tex">source code</a>). </li>
         <li> <a href="mt1.pdf">Midterm 1</a> (<a href="mt1.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 6:</dt>
  <dd><ul>
         <li> <a href="lec11.pdf">Lecture 11 (p-valuations; prime factorization; lcm; Sylvester's two-coin theorem)</a> (<a href="lec11.tex">source code</a>). </li>
         <li> <a href="hw4.pdf">Homework set 4</a> (<a href="hw4.tex">source code</a>). </li>
         <li> <a href="lec12.pdf">Lecture 12 (sets refresher; introducing enumeration)</a> (<a href="lec12.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 7:</dt>
  <dd><ul>
         <li> <a href="lec13.pdf">Lecture 13 (informal counting)</a> (<a href="lec13.tex">source code</a>). </li>
         <li> <a href="lec14.pdf">Lecture 14 (functions)</a> (<a href="lec14.tex">source code</a>). </li>
         <li> <a href="mt2.pdf">Midterm 2</a> (<a href="mt2.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 8:</dt>
  <dd><ul>
         <li> <a href="lec15.pdf">Lecture 15 (jectivities)</a> (<a href="lec15.tex">source code</a>). </li>
         <li> <a href="lec16.pdf">Lecture 16 (basic principles of counting)</a> (<a href="lec16.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 9:</dt>
  <dd><ul>
         <li> <a href="lec17.pdf">Lecture 17 (counting lacunar subsets and compositions)</a> (<a href="lec17.tex">source code</a>). </li>
         <li> <a href="hw5.pdf">Homework set 5</a> (<a href="hw5.tex">source code</a>). </li>
         <li> <a href="lec18.pdf">Lecture 18 (counting injective tuples and multisets; counting anagrams; multinomial coefficients)</a> (<a href="lec18.tex">source code</a>). </li>
         <li> <a href="mt3.pdf">Midterm 3</a> (<a href="mt3.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 10:</dt>
  <dd><ul>
         <li> Lecture 19 (graphs introduction, dominant and independent sets, Hamiltonian paths and cycles). </li>
         <li> Lecture 20 (paths, cycles, trees, coloring). </li>
         <li> For the material of these last two lectures, see <a href="../22s/index.html">my Math 530 notes</a>, which does it all in more detail and with proofs. You are also welcome to attend my Math 530 class this spring! </li>
  </ul></dd>

<!--

  Week 11 = finals week

  -->

  <dt>No Copyright:</dt>
  <dd>The above lecture notes and assignments have been released under <a href="https://creativecommons.org/publicdomain/zero/1.0/">the CC0 license</a>, i.e., are dedicated to the public domain. They can be copied, modified and distributed without permission. See <a href="https://creativecommons.org/publicdomain/zero/1.0/">the license</a> for details.
  </dd>

</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 40%: homework sets. (Your lowest homework score will be dropped.) </li>
         <li> 20%: midterm 1. </li>
         <li> 20%: midterm 2. </li>
         <li> 20%: midterm 3 (due in finals week). </li>
      </ul> </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A+, A, A-: (80%, 100%]. </li>
         <li> B+, B, B-: (60%, 80%]. </li>
         <li> C+, C, C-: (40%, 60%]. </li>
         <li> D+, D, D-: (20%, 40%]. </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and <strong>acknowledge all sources that you used</strong>. </li>
         <li> Asking outsiders (anyone apart from Math 221 students and Drexel staff) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> Late homework will <strong>not</strong> be accepted. (But keep in mind that the lowest homework score will be dropped.) </li>
         <li> Solutions have to be submitted electronically via Gradescope. Make sure that what you submit is readable (e.g., your text is not cut off). If there are problems with submission, send your work to me by email for good measure. </li>
      </ul> </dd>
  <dt>Midterm policy:</dt>
  <dd><ul>
         <li> Late midterms will <strong>not</strong> be accepted unless agreed in advance and with serious justification. </li>
         <li> <strong>Collaboration is not allowed</strong> on midterms. </li>
         <li> Everything else is the same as for homework (yes, midterms are take-home). </li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>The students should have gained experience and familiarity with the mainstays of discrete mathematics: the language of sets and maps; proofs by induction; basic principles of enumeration; graphs and their major features; integers and their divisibility properties; prime numbers and possibly their use in cryptography. </dd>
</dl>
</div>

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <dt>Homework help:</dt>
  <dd><ul>
         <li> <a href="https://drexel.zoom.us/j/83973461576">Math Resource Center</a> (Zoom registration link; use your Drexel email); see <a href="https://drexel.edu/coas/academics/departments-centers/mathematics/math-resource-center/">schedule</a>. Starts January 10th. </li>
      </ul> </dd>
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <!-- <li> <a href="../20f/recording-policy.html" target="_blank">Recording of Class Activities</a>. </li> -->
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/incomplete_grades/" target="_blank">Incomplete Grade Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/grade-appeals/" target="_blank">Grade Appeals</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/disability-resources/support-accommodations/student-family-resources/">Disability resources for students</a>. </li>
      </ul> </dd>
</dl>
</div>

</body>

<hr>

<p><a href="../">Back to Darij Grinberg's teaching page</a>. </p>

