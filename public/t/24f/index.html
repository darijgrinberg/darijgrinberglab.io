<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Problem Solving (Math 235), Fall 2024">
  <meta name="keywords" content="mathematics, problem solving, olympiad mathematics, putnam, teaching, courses, drexel, math 235">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Mathematical Problem Solving (Math 235), Fall 2024</title>
</head>
<body>

<h1 style="text-align:center">Math 235: Mathematical Problem Solving, Fall 2024 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd>Tue 15:30 PM -- 16:50 PM 
  <!-- on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a> (until November 3) and -->
  in GL 41.</dd>
  <dt>Recitations:</dt>
  <dd>Fri 12:00 -- 13:00 PM
  <!-- on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a> (until November 3) and -->
  in Korman 241 (maths department common room).
  <dt>Office hours:</dt>
  <dd>Wed 12:00 -- 13:00 PM
  <!-- on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a> (until November 3) and -->
  in Korman 263.
  <!--??? on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>. Also by appointment.-->
  <dt>Notes:</dt>
  <dd>Mainly <a href="../20f/mps.pdf">my <cite>Notes on mathematical problem solving</cite> from 2020</a>. See below for other sources.</dd>
  <dt>Gradescope:</dt>
  <dd><a href="https://www.gradescope.com/courses/871285">https://www.gradescope.com/courses/871285</a>.</dd>
  <dt>Blackboard:</dt>
  <dd><a href="https://learn.dcollege.net/ultra/courses/_374478_1/cl/outline">https://learn.dcollege.net/ultra/courses/_374478_1/cl/outline</a>.</dd>
  <dt>Piazza:</dt>
  <dd><a href="http://piazza.com/drexel/fall2024/math235">http://piazza.com/drexel/fall2024/math235</a>.</dd>
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
</dl>
</div>

<h2>Course description</h2>

<p>An introduction to mathematical problem solving.
We will learn techniques and tools for solving problems of the kind that appear in mathematical competitions and journals.
These techniques (like induction, the Pigeonhole Principle, modular arithmetic or the Cauchy-Schwarz inequality) have uses all over mathematics; we will explore these uses through hands-on problem solving.
</p>

<p><strong>Prerequisites:</strong> Math 200. <!-- [Min Grade: D]. --> </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Recommended:</dt>
  <dd><ul>
         <li> <a href="../20f/mps.pdf">Darij Grinberg, <cite>Notes on mathematical problem solving</cite></a>: The notes for my Math 235 course in Fall 2020. We will follow them much of the time, but we will not use the homework problems in them. </li>
         <li> <a href="../23f/">Darij Grinberg, <cite>Worksheets on mathematical problem solving</cite></a>: Last year's iteration of the course, containing <strong>different</strong> topics (despite some overlap). Recommended as additional reading. </li>
         <li> <a href="https://intranet.math.vt.edu/people/plinnell/Vtregional/"><cite>Virginia Tech Regional Mathematics Contest</cite></a>. A discontinued competition similar to the Putnam. </li>
         <li> <a href="https://www3.nd.edu/~dgalvin1/43900/43900_F20/index.html">David Galvin, <cite>Math 43900 - Problem Solving in Math (Fall 2020)</cite></a>. Lecture notes from a course similar to ours (follow the link to Overleaf, allow javascript and note the "Download PDF" button). </li>
         <li> <a href="https://foucart.github.io//teaching/notes/PbSolving.pdf">Simon Foucart, <cite>Problem Solving</cite></a>. Brief Math 235 notes from a few years ago. </li>
         <li> <a href="https://doi.org/10.1007/978-3-319-58988-6">Răzvan Gelca, Titu Andreescu, <cite>Putnam and Beyond</cite></a>, 2nd edition. Standard text for university competition training. At 857 pages, it goes far beyond what we can do in a quarter. </li>
         <li> <a href="https://kskedlaya.org/putnam-archive/">Kiran Kedlaya et al., <cite>Putnam archive</cite></a>. Contains solutions to several years of Putnam exams. </li>
         <li> <a href="https://webee.technion.ac.il/people/aditya/www.kalva.demon.co.uk/">John Scholes aka kalva, <cite>Maths problems</cite></a>. Classical (early 2000s) collection of contest problems with (terse) solutions. </li>
         <li> <a href="https://doi.org/10.1007/978-1-4419-9854-5">Dušan Djukić, Vladimir Janković, Ivan Matić, Nikola Petrović, <cite>The IMO Compendium</cite></a>, 2nd edition. Collection of all problems ever suggested for the International Mathematical Olympiads, often with solutions. You can also get official solutions for recent shortlist problems from <a href="https://www.imo-official.org/problems.aspx">imo-official.org</a>. We aren't training for the IMO, but there is a lot of overlap between IMO and Putnam topics, and IMO shortlists are among the best sources for high-quality problems. </li>
         <li> <a href="https://web.evanchen.cc/textbooks/OTIS-Excerpts.pdf">Evan Chen, <cite>OTIS Excerpts</cite></a>. Olympiad training lecture notes (with focus on high school contests, but lots of relevance to undergraduate ones too). </li>
         <li> <a href="https://doi.org/10.1007/b97682">Arthur Engel, <cite>Problem-Solving Strategies</cite></a>. One of the first books written explicitly for math contest training. Somewhat dated and not always well-written, but a venerable collection of problems sorted by theme, and an inspiration for newer texts. </li>
         <li> <a href="https://www.cut-the-knot.org/">Alexander Bogomolny, <cite>Cut the Knot</cite></a>. Famous collection of maths puzzles, including various contest problems (most hidden behind the "and more..." links). Unfortunately, the Java applets no longer run on most browsers. Use the <a href="https://web.archive.org/">Wayback Machine</a> for some links that have disappeared. </li>
         <li> <a href="https://web.archive.org/web/20240122033238/https://web.math.ucsb.edu/~padraic/notes.html">Padraic Bartlett, <cite>various courses</cite></a>. Lots of problem solving materials, very enjoyably written and split into bite-sized (mostly self-contained) pieces. </li>
         <li> <a href="https://mathcircle.berkeley.edu/circle-archives/"><cite>Berkeley Math Circle archives</cite></a>. Olympiad training worksheets on various levels. </li>
      </ul> </dd>
  <dt>Contests:</dt>
  <dd><ul>
         <li> <a href="https://www.maa.org/math-competitions/putnam-competition"><cite>Putnam Competition</cite></a>. Will be held on December 7th this year. </li>
         <li> <a href="https://www.tandfonline.com/toc/uamm20/current"><cite>American Mathematical Monthly</cite></a>: journal with a problem section. </li>
         <li> <a href="https://www.tandfonline.com/toc/ucmj20/current"><cite>College Mathematics Journal</cite></a>: journal with a problem section. </li>
         <li> <a href="https://www.tandfonline.com/toc/umma20/current"><cite>Mathematics Magazine</cite></a>: journal with a problem section. </li>
         <li> <a href="https://cms.math.ca/publications/crux/"><cite>Crux Mathematicorum</cite></a>: high-school contest math journal with a problem section. </li>
      </ul> </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  <dt>Week 0:</dt>
  <dd><ul>
         <li> Diagnostic homework set: <a href="hw0.pdf">Homework set 0</a> (with <a href="hw0.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 1:</dt>
  <dd><ul>
         <li> <a href="lec01.pdf">Lecture 1 stenogram</a>. (See the <a href="../20f/mps.pdf">notes</a> for properly explained proofs with details and much more.) </li>
      </ul> </dd>
  <dt>Week 2:</dt>
  <dd><ul>
         <li> <a href="lec02.pdf">Lecture 2 stenogram</a>. </li>
         <li> <a href="hw1.pdf">Homework set 1</a> (with <a href="hw1.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 3:</dt>
  <dd><ul>
         <li> <a href="lec03.pdf">Lecture 3 stenogram</a>. </li>
		 <li> Recommended reading in the <a href="../20f/mps.pdf">notes</a>: §3.3 (Exercise 3.3.3); §3.4.7 (in full); §3.5.3 (Exercise 3.5.3 and solution; Theorem 3.5.9); §3.5.7 (at least the statements); §3.6 (the statements); §3.7 (Exercise 3.7.3 and solution in §A.2.3); §4.5 (Exercise 4.5.3 and solution in §A.3.3). </li>
         <li> <a href="hw2.pdf">Homework set 2</a> (with <a href="hw2.tex">source code for convenience</a>). </li>
  </ul> </dd>
  <dt>Week 4:</dt>
  <dd><ul>
         <li> <a href="lec04.pdf">Lecture 4 stenogram</a>. </li>
         <li> <a href="hw3.pdf">Homework set 3</a> (with <a href="hw3.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 5:</dt>
  <dd><ul>
         <li> <a href="lec05.pdf">Lecture 5 stenogram</a>. </li>
         <li> <a href="24frecit3.pdf">Recitation on Farey series</a>. </li>
         <li> <a href="hw4.pdf">Homework set 4</a> (with <a href="hw4.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 6:</dt>
  <dd><ul>
         <li> <a href="lec06.pdf">Lecture 6 stenogram</a>. </li>
         <li> <a href="hw5.pdf">Homework set 5</a> (with <a href="hw5.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 7:</dt>
  <dd><ul>
         <li> <a href="lec07.pdf">Lecture 7 stenogram</a>. </li>
         <li> <a href="hw6.pdf">Homework set 6</a> (with <a href="hw6.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 8:</dt>
  <dd><ul>
         <li> <a href="lec08.pdf">Lecture 8 stenogram</a>. </li>
         <li> <a href="hw7.pdf">Homework set 7</a> (with <a href="hw7.tex">source code for convenience</a>). </li>
      </ul> </dd>
  <dt>Week 9:</dt>
  <dd><ul>
         <li> <a href="lec09.pdf">Lecture 9 stenogram</a>. </li>
         <li> <a href="hw9.pdf">Homework set 9</a> (with <a href="hw9.tex">source code for convenience</a>). </li>
      </ul> </dd>
<!--
  <dt>Week 10:</dt>
  <dd><ul>
         <li> <a href="lec10.pdf">Lecture 10 stenogram</a>. </li>
         <li> <a href="hw9.pdf">Homework set 9</a> (with <a href="hw9.tex">source code for convenience</a>). </li>
      </ul> </dd>
-->
  <dt>No Copyright:</dt>
  <dd>The above lecture notes and assignments have been released under <a href="https://creativecommons.org/publicdomain/zero/1.0/">the CC0 license</a>, i.e., are dedicated to the public domain. They can be copied, modified and distributed without permission. See <a href="https://creativecommons.org/publicdomain/zero/1.0/">the license</a> for details.
  </dd>
</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 100%: homework sets. The lowest score will be dropped if all homework sets are submitted. </li>
      </ul> </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A: (60%, 100%]. </li>
         <li> B: (40%, 60%]. </li>
         <li> C: (20%, 40%]. </li>
         <li> D: [0%, 20%]. </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and <strong>acknowledge all sources that you used</strong>. </li>
         <li> Asking outsiders (anyone apart from Math 235 students and Drexel staff) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> If you have already seen a homework problem before, you are free to reuse the solution that you know. Anything you come across in the literature is fine, but you must not <strong>deliberately</strong> seek solutions to homework problems in the literature or contact <strong>outsiders</strong> (anyone apart from Math 235 students and Drexel staff) for help with the problems. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> Do <strong>not</strong> use generative AI in solving the homework problems. </li>
         <li> Late homework will <strong>not</strong> be accepted. <!-- (But keep in mind that the lowest homework score will be dropped.) --> </li>
         <li> All results from the standard undergraduate curriculum (linear algebra, analysis) and school mathematics, all results proved in <a href="../20f/mps.pdf">the notes</a>, and all previous homework exercises (even if you did not submit them), can be used without proof in your solutions. If you use more obscure results, you should include their proofs. </li>
         <li> Solutions have to be submitted electronically via Gradescope. If there are problems with submission, send your work to me by email for good measure (I can read PDF and TeX, but not doc/docx). </li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>Students should have obtained some hands-on experience solving competition-type mathematical problems. They should be aware of some standard results and techniques that are commonly used in competition-style problems (such as the pigeonhole and extremal principles) and be familiar with examples of their application. </dd>
</dl>
</div>

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <li> <a href="../20f/recording-policy.html" target="_blank">Recording of Class Activities</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/incomplete_grades/" target="_blank">Incomplete Grade Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/grade-appeals/" target="_blank">Grade Appeals</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/oed/disabilityResources/students/">Disability resources for students</a>. </li>
      </ul> </dd>
</dl>
</div>

<hr>

<p><a href="../">Back to Darij Grinberg's teaching page</a>. </p>

</body>
