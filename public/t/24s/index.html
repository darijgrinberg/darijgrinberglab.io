<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Introduction to the symmetric group algebra (Math 701), Spring 2024">
  <meta name="keywords" content="mathematics, symmetric group algebra, symmetric group, permutations, algebraic combinatorics, symmetric functions, Young tableaux, algebra, combinatorics, teaching, courses, drexel, math 701">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Introduction to the symmetric group algebra (Math 701), Spring 2024</title>
</head>
<body>

<h1 style="text-align:center">Math 701: Introduction to the symmetric group algebra, Spring 2024 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd>Classes are over now! (Was: MWF 1:00 PM -- 1:50 PM in One Drexel Plaza, GL46.)</dd>
  <dt>Office hours:</dt>
  <dd>
  Monday 2--3 PM in Korman Center 263.
  Saturday 1--2 PM on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>.
  Also by appointment.
  </dd>
  <dt>Text:</dt>
  <dd><a href="sga.pdf"><strong><cite>Lecture notes</cite></strong></a> (<a href="sga.tex">source code</a>). Work in progress.</dd>
  <dt>Blackboard:</dt>
  <dd><a href="https://learn.dcollege.net/ultra/courses/_358806_1/cl/outline">https://learn.dcollege.net/ultra/courses/_358806_1/cl/outline</a>.</dd>
  <dt>Gradescope:</dt>
  <dd> Not in this course. Just send homework to <i><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></i>. </dd>
  <!--
  <dd><a href="https://www.gradescope.com/courses/??????">https://www.gradescope.com/courses/??????</a>.</dd>
  -->
  <!--
  <dt>Piazza:</dt>
  <dd><a href="https://piazza.com/drexel/spring2024/701">https://piazza.com/drexel/spring2024/701</a>.</dd>
  -->
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
</dl>
</div>

<h2>Course description</h2>

<p>A survey of the symmetric group algebra and various families of elements living therein: random-to-top and random-to-random shuffles, Young-Jucys-Murphy elements, cycle sums, the Murphy and Young seminormal bases and others.
Representations of the symmetric group will also be discussed, as well as their connection to symmetric functions.
</p>
<p>See <a href="https://www.cip.ifi.lmu.de/~grinberg/algebra/dc2023.pdf">my talk</a> for a teaser.</p>

<p><strong>Level:</strong> graduate. </p>

<p><strong>Prerequisites:</strong> a good understanding of rings and modules (as provided, e.g., by <a href="../23wa/23wa.pdf">Math 332</a>). Some familiarity with enumerative combinatorics (<a href="../22fco">Math 222</a>) and algebraic combinatorics (<a href="../21s/lecs.pdf">Math 531</a>) will be helpful but not strictly required. Knowledge of representation theory is not needed -- we will construct the relevant representations with our bare hands. </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Recommended:</dt>
  <dd><ul>
         <li> [Wildon] <a href="https://www.ma.rhul.ac.uk/~uvah099/Maths/Sym/SymGroup2014.pdf">Mark Wildon, <cite>Representation theory of the symmetric group</cite>, 2018</a>: Short but very readable introduction to the topic at hand. </li>
         <li> [Fulton] <a href="https://doi.org/10.1017/CBO9780511626241">William Fulton, <cite>Young tableaux</cite>, CUP 1996</a>: The standard text (and probably the most readable) on Young tableaux. Includes brief treatments of symmetric functions, representation-theoretical as well as algebraic geometry connections. <a href="https://mathoverflow.net/questions/456463/errata-for-fultons-young-tableaux">Some errata.</a> </li>
         <li> [CSST] <a href="https://doi.org/10.1017/CBO9781139192361">Tullio Ceccherini-Silberstein, Fabio Scarabotti, Filippo Tolli, <cite>Representation Theory of the Symmetric Groups</cite>, CUP 2010</a>: One of the best modern texts on representation theory of symmetric groups. </li>
         <li> [r2t-elem] <a href="https://mathoverflow.net/questions/308536/is-this-sum-of-cycles-invertible-in-mathbb-qs-n/308600#308600">Darij Grinberg, <cite>Is this sum of cycles invertible in <b>Q</b>S<sub>n</sub>?</cite>, MathOverflow #308536</a>: Detailed elementary proof of the identities for top-to-random shuffles. </li>
         <li> [Sagan] <a href="https://doi.org/10.1007/978-1-4757-6804-6">Bruce E. Sagan, <cite>The Symmetric Group</cite>, 2nd edition, Springer 2001</a>: This is mainly about representations of the symmetric group, but Chapters 3 and 4 form an introduction to symmetric functions. <a href="https://users.math.msu.edu/users/bsagan/Books/Sym/errata.pdf">Errata</a>. </li>
         <li> [21s] <a href="../21s/lecs.pdf">Darij Grinberg, <cite>An introduction to algebraic combinatorics</cite>, 2024</a>: Notes for a prerequisite course. We will cover Chapter 7 (on symmetric functions), since we didn't get around to it last quarter. </li>
         <li> [Rutherford] <a href="https://store.doverpublications.com/products/9780486491202">Daniel Edwin Rutherford, <cite>Substitutional Analysis</cite>, Dover reprint 2013 (originally 1948)</a>: Despite the dated notations, this is still one of the best sources on the Young seminormal form. </li>
         <li> [James] <a href="https://www-users.cse.umn.edu/~webb/oldteaching/Year2010-11/the-representation-theory-of-the-symmetric-groups-SLN.pdf">G. D. James, <cite>The Representation Theory of the Symmetric Groups</cite>, Springer 1978</a>: Somewhat dated text, but still quite useful. </li>
         <li> [Clausen] <a href="https://doi.org/10.1016/S0747-7171(08)80117-4">Michael Clausen, <cite>Multivariate Polynomials Standard Tableaux and Representations of Symmetric Groups</cite>, J. Symbolic Computation 11 (1991)</a>: Self-contained and elementary approach to several properties of Specht modules. </li>
         <li> [Etingof] <a href="https://math.mit.edu/~etingof/reprbook.pdf">Pavel Etingof, Oleg Golberg, Sebastian Hensel, Tiankai Liu, Alex Schwendner, Dmitry Vaintrob, Elena Yudovina, Slava Gerovitch, <cite>Introduction to representation theory</cite>, AMS 2011</a>: An introduction to representation theory. Sections 5.12--5.15 are the most relevant to us. </li>
         <li> [RSW] <a href="https://arxiv.org/abs/1102.2460">Victor Reiner, Franco Saliola, Volkmar Welker, <cite>Spectra of Symmetrized Shuffling Operators</cite>, Memoirs AMS 2014</a>: The first major work on random-to-random shuffles. Not an easy read. </li>
         <li> [DiSa] <a href="https://doi.org/10.1016/j.aim.2017.10.034">A.B. Dieker, F.V. Saliola, <cite>Spectral analysis of random-to-random Markov chains</cite>, Advances in Mathematics, 2018</a>: The second major work on random-to-random shuffles. </li>
         <li> [Lafr] <a href="https://arxiv.org/abs/1912.07718">Nadia Lafrenière, <cite>Valeurs propres des opérateurs de mélanges symétrisés</cite>, PhD thesis, 2019</a>: The third major work on random-to-random shuffles; proves commutativity again and finishes the determination of eigenvalues. </li>
         <li> [Meliot] <a href="https://doi.org/10.1201/9781315371016">Pierre-Loic Meliot, <cite>Representation Theory of Symmetric Groups</cite>, CRC Press 2017</a>: A new textbook, particularly strong on probabilistic applications; I am not particularly familiar with it yet. </li>
         <li> [Wildon-Sol] <a href="https://www.ma.rhul.ac.uk/~uvah099/Maths/Bidigare4.pdf">Mark Wildon, <cite>On Bidigare's proof of Solomon's theorem</cite>, 2021</a>: This short note is here to represent the whole rich literature on Solomon's descent algebra. (I hope to go deeper into the topic in class.) </li>
         <li> [Purbhoo] <a href="https://arxiv.org/abs/2207.05743">Kevin Purbhoo, <cite>An identity in the Bethe subalgebra of <b>C</b>S<sub>n</sub></cite>, arXiv:2207.05743v2</a>: Sections 3-5 here (re)prove the commutativity of the Gaudin Bethe subalgebra, originally found by Mukhin, Tarasov and Varchenko. I hope to improve on this proof in class. Note that <a href="https://arxiv.org/abs/2309.04645">a followup paper</a> generalizes this commutativity further. </li>
         <!--
         <li> [SHORT] <a href="LINK">AUTHOR, <cite>TITLE</cite>, PUBLISHER YEAR</a>: DESCRIPTION </li>
		 * Saliola descent
		 * Dieker-Saliola paper
		 * something about riffle shuffles
		 * James-Kerber
		 * Blessenohl-Schocker
		 * Grinberg-Lafreniere
		 * ...
         -->
      </ul>
      </dd>
  <dt>Other:</dt>
  <dd><ul>
  <li> See <a href="#literature">Literature</a> below for further sources. </li>
  <li> <a href="hooktalk.pdf">Talk slides on the hook length formula</a> (<a href="hooktalk.tex">source code</a>). </li>
  <li> Related: <a href="../../algebra/yd2023.pdf">Slides of a talk on Naruse's skew hook length formula</a>. </li>
  <li> Related: Three open problems on symmetric group algebras presented <a href="../../algebra/harvard2024.pdf">at Harvard</a> and <a href="../../algebra/nfe2024.pdf">at the Lie-St&oslash;rmer center Nordfjordeid</a>.
  (The Nordfjordeid version is more detailed.) </li>
  </ul>
  </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  
  <!--
  <dt>Diary:</dt>
  <dd><a href="diary.pdf">Lecture diary</a> (unedited text typed in class; does not replace the notes).</dd>
  -->
  
  <dt>Homework:</dt>
  <dd><ul>
		 <li> By <b>June 2nd</b> at 11:59 PM, make sure to submit at least a first draft of your homework (aiming at 30 points or more). </li>
         <li> The final deadline is <b>June 10th </b> at 11:59 PM. No more work (new or corrected) will be accepted after that. </li>
         <!-- <li> Homework set 1 (Sections A.1--A.2) due <b>February 9 at 10:00 PM</b>. You need to collect 20 experience points. </li>
		 <li> Homework set 2 (Section A.3) due <b>February 26 at 10:00 PM</b>. You need to collect 20 experience points. </li>
         <li> Homework set 3 (Section A.4) due <b>March 11 at 10:00 PM</b>. You need to collect 20 experience points. </li>
		 <li> Homework set 4 (Section A.5) due <b>March 18 at 10:00 PM</b>. You need to collect 20 experience points. </li>
		 <li> Homework set 5 (Section A.6) due <b>March 22 at 10:00 PM</b>. You need to collect 20 experience points. </li> -->
		 <li> <b>Note:</b> You can submit multiple revisions until the deadline. By submitting some work early, you can get feedback ahead of the deadline, thus allowing you to correct mistakes or add extra solutions. (For this and other reasons, I recommend typesetting your solutions.) </li>
  </ul></dd>
  
  <dt>Plan:</dt>
  <dd>
  The following is HIGHLY TENTATIVE. Major changes likely.
  <ul>
  <li>
  A first look: definitions and teasers.
  </li><li>
  Young-Jucys-Murphy elements: commutativity and cycle sums.
  </li><li>
  Cycles and the center of the symmetric group algebra.
  </li><li>
  Study of the random-to-top shuffles.
  </li><li>
  The descent algebra (following Wildon) and its denizens.
  </li><li>
  Riffle shuffles.
  </li><li>
  The one-sided cycle shuffles.
  </li><li>
  The Murphy elements.
  </li><li>
  Young tableaux and Young symmetrizers.
  </li><li>
  Algebraic intermezzo: Modules and representations; the Maschke theorem.
  </li><li>
  Examples of representations of Sn.
  </li><li>
  Specht modules: definitions and basic properties.
  </li><li>
  Standard tableaux and Garnir relations.
  </li><li>
  The basis theorem for Specht modules.
  </li><li>
  Semistandard tableaux and semistandard homomorphisms.
  </li><li>
  Specht modules in the Murphy basis.
  </li><li>
  How random-to-top and one-sided cycle shuffles act on Specht modules.
  </li><li>
  James's Speng modules, following Ceccherini-Silberstein et al.
  </li><li>
  The Littlewood-Richardson rule for skew shapes.
  </li><li>
  Characters.
  </li><li>
  Symmetric polynomials and functions.
  </li><li>
  Random-to-random elements: commutativity.
  </li><li>
  The Gaudin Bethe subalgebra: commutativity.
  </li><li>
  The Young seminormal form, following Rutherford.
  </li><li>
  Eigenvalues of the Young-Jucys-Murphy elements.
  </li><li>
  The hook length formula.
  </li>
  </ul></dd>

</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 100%: homework.
		 <br>
		 Homework problems can be found all over the notes (search for "Exercise").
		 <br>
		 The boxed number at the beginning of the exercise is the number of experience points you gain for this exercise.
		 It is roughly proportional to the difficulty of the exercise (somewhat reduced if the exercise is tangential or likely to be known from prerequisite courses).
		 A typical homework exercise ranges from 2 to 5 experience points, but there will be both easier and harder ones in the notes.
		 <br>
		 For 100% course percentage, you have to gain at least 50 experience points through the entire quarter.
         Partial credit will be given for half-correct solutions and for parts of a multipart problem.
		 You can (and are encouraged to) submit piecemeal to gradually collect points. This will be much easier for me than grading 20-page long submissions on the last day of class.
		 You are also welcome to submit preliminary versions to get early feedback and then correct your solutions from the feedback (unless you ask for spoilers/solutions, in which case you forfeit any further points on the given problem).
		 <br>
		 Submit solutions to <i><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></i>.
		 </li>
      </ul> </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A+, A, A-: (80%, 100%] (that is, (40, 50] experience points). </li>
         <li> B+, B, B-: (60%, 80%] (that is, (30, 40] experience points). </li>
         <li> C+, C, C-: (40%, 60%] (that is, (20, 30] experience points). </li>
         <li> D+, D, D-: (20%, 40%] (that is, (10, 20] experience points). </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and <strong>acknowledge all sources that you used</strong>. </li>
         <li> Asking outsiders (anyone apart from classmates and Drexel employees) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> Late homework will <strong>not</strong> be accepted. </li>
         <li> Solutions have to be submitted electronically by email in a format I can read (PDF, TeX or plain text if it works; <strong>no doc/docx!</strong>). If you submit a PDF, make sure that it is readable and does not go over the margins. If there are problems with submission, send your work to me by email for good measure. </li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>The students will have a familiarity with the group algebras of symmetric groups, including their Artin-Wedderburn decomposition in characteristic 0, and the basic combinatorics of Young tableaux relevant to it, such as the hook-length formula.
  They will have seen the classical representation theory of symmetric groups from the viewpoint of Specht modules, and in particular the Littlewood-Richardson rule for decomposing a skew Specht module into straight ones.
  They will have also have studied some remarkable families of elements in these group algebras, such as the Young-Jucys-Murphy elements, the somewhere-to-below shuffles, and the random-to-random shuffles, as well as the descent algebra and its properties.
  </dd>
</dl>
</div>

<h2>Further literature</h2><a id="literature">

<div class="boldtable">
<dl>
  <dt>General:</dt>
  <dd><ul>
         <li> [Aigner] <a href="https://web.archive.org/web/20210323094124/http://www.math.ecnu.edu.cn/~jwguo/maths/book.pdf">Martin Aigner, <cite>A Course in Enumeration</cite>, Springer 2007</a>: Highly useful source of exercises and additional material. </li>
         <li> [Stanley-EC1] and [Stanley-EC2] <a href="http://www-math.mit.edu/~rstan/ec/">Richard P. Stanley, <cite>Enumerative Combinatorics: volumes 1 and 2</cite>, CUP 2012/2024</a>: Famous semi-encyclopedic text on the subject. Includes many exercises of varying difficulty. Corrections and a draft of volume 1 available from the website. </li>
         <li> [Egge] <a href="https://bookstore.ams.org/stml-91/">Eric S. Egge, <cite>An Introduction to Symmetric Functions and Their Combinatorics</cite>, AMS 2019</a>: Elementary combinatorial introduction to symmetric functions. <a href="https://www.ericegge.net/cofsf/corrections.pdf">Corrections</a> and <a href="https://www.ericegge.net/cofsf/commentary.pdf">comments</a> are available from <a href="https://www.ericegge.net/cofsf/index.html">the author's website</a>. </li>
         <li> [Wildon] <a href="http://www.ma.rhul.ac.uk/~uvah099/Maths/Sym/SymFuncs2020.pdf">Mark Wildon, <cite>An involutive introduction to symmetric functions</cite>, 2020</a>: Terse but to-the-point introduction to symmetric functions theory with a focus on Young tableaux. <a href="http://www.cip.ifi.lmu.de/~grinberg/algebra/symfuncs2017-2020-05-08-errata.pdf">Some comments by yours truly</a>. </li>
         <li> [Macdonald] <a href="https://global.oup.com/academic/product/symmetric-functions-and-hall-polynomials-9780198739128">Ian G. Macdonald, <cite>Symmetric Functions and Hall Polynomials</cite>, 2nd edition, OUP 1995</a>: One of the oldest texts on symmetric functions (in some ways, the Hartshorne of the subject), with a minimum of combinatorics but lots of little-known algebraic results. </li>
         <li> [Prasad1] <a href="https://gradmath.org/2020/10/05/an-introduction-to-schur-polynomials/">Amritanshu Prasad, <cite>An introduction to Schur polynomials</cite>, Graduate J. Math. 4 (2019), 62--84</a>: A short introduction. </li>
         <!--
         <li> [SHORT] <a href="LINK">AUTHOR, <cite>TITLE</cite>, PUBLISHER YEAR</a>: DESCRIPTION </li>
         -->
      </ul>
      </dd>
</dl>
</div>

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/incomplete_grades/" target="_blank">Incomplete Grade Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/grade-appeals/" target="_blank">Grade Appeals</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/disability-resources/support-accommodations/student-family-resources/">Disability resources for students</a>. </li>
      </ul> </dd>
</dl>
</div>

<hr>

<p><a href="../">Back to Darij Grinberg's teaching page</a>. </p>

</body>
