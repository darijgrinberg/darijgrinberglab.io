<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Darij Grinberg: Undergraduate Abstract Algebra II (Math 332), Winter 2025">
  <meta name="keywords" content="mathematics, abstract algebra, rings, modules, fields, teaching, courses, drexel, math 332">
  <meta name="author" content="Darij Grinberg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="all">
  .boldtable
    dt {
      font-weight: bold;
    }
  </style>
  <style media="screen">
  .boldtable dl {
      display: grid;
      grid-template-columns: max-content auto;
      grid-gap: 10px;
    }

    dt {
      grid-column-start: 1;
      font-weight: bold;
    }

    dd {
      grid-column-start: 2;
    }
    
    ul {
      margin: 0;
      padding: 0;
    }
    
  </style>
  <title>Darij Grinberg: Undergraduate Abstract Algebra II (Math 332), Winter 2025</title>
</head>
<body>

<h1 style="text-align:center">Math 332: Undergraduate Abstract Algebra II, Winter 2025 <br>
Professor: <a href="https://www.cip.ifi.lmu.de/~grinberg/">Darij Grinberg</a></h1>

<hr>

<h2>Organization</h2>

<div class="boldtable">
<dl>
  <dt>Classes:</dt>
  <dd>Mon, Wed, Fri 11:00 AM -- 11:50 AM in GL 41.</dd>
  <dt>Office hours:</dt>
  <dd>Mon 1:00--3:00 PM in my office (Korman Center 263) starting week 2. Also by appointment on <a href="https://drexel.zoom.us/j/2350700617">https://drexel.zoom.us/j/2350700617</a>.</dd>
  <dt>Text:</dt>
  <dd><a href="../23wa/23wa.pdf"><cite>An introduction to the algebra of rings and fields</cite> (work in progress)</a>. <a href="../23wa/23wa.tex">Source code</a>. Also, see the <a href="../23wa/index.html">class notes from Winter 2023</a>.</dd>
  <dt>Blackboard:</dt>
  <dd><a href="https://learn.dcollege.net/ultra/courses/_380313_1/cl/outline">https://learn.dcollege.net/ultra/courses/_380313_1/cl/outline</a>.</dd>
  <dt>Gradescope:</dt>
  <dd><a href="https://www.gradescope.com/courses/930606">https://www.gradescope.com/courses/930606</a>.</dd>
  <dt>Piazza:</dt>
  <dd><a href="https://piazza.com/drexel/winter2025/math332">https://piazza.com/drexel/winter2025/math332</a>.</dd>
  <dt>Instructor email:</dt>
  <dd><address><a href="mailto:darij.grinberg@drexel.edu">darij.grinberg@drexel.edu</a></address>
  <!--
  <dt>Mailbox:</dt>
  <dd>Mathematics Common Room (Korman Center, 2nd floor).
  -->
</dl>
</div>

<h2>Course description</h2>

<p>An introduction to rings and modules, including the structure of finite fields and field extensions and some number-theoretical applications. Polynomial rings and Gr&ouml;bner bases will also be discussed, as will multilinear algebra and Galois theory if time allows.
</p>

<p><strong>Level:</strong> undergraduate. </p>

<p><strong>Prerequisites:</strong> Math 331 (Undergraduate Abstract Algebra I). <!-- [Min Grade: D]. --> </p>

<h2>Course materials</h2>

<div class="boldtable">
<dl>
  <dt>Required:</dt>
  <dd><ul>
         <li> Darij Grinberg, <a href="../23wa/23wa.pdf"><cite>An introduction to the algebra of rings and fields</cite> (work in progress)</a>. <a href="../23wa/23wa.tex">Source code</a>. </li>
      </ul> </dd>

  <dt>Recommended:</dt>
  <dd><ul>
         <li> <a href="../23wa/index.html">The Winter 2023 iteration of this course</a>, also available as a
		 <a href="https://gitlab.com/darijgrinberg/darijgrinberg.gitlab.io/-/archive/master/darijgrinberg.gitlab.io-master.zip?path=public/t/23wa">ZIP archive for downloading</a>. We will likely reuse most of the homework exercises. </li>
         <li> David S. Dummit, Richard M. Foote, <cite>Abstract algebra</cite>, 3rd edition, Wiley 2004: Long and comprehensive text containing almost all the abstract algebra that anyone could teach in an undergraduate sequence and then some. Available at the usual places in various formats (e.g., <a href="https://web.archive.org/web/20201209221644/https://nms.kcl.ac.uk/ashwin.iyengar/df.pdf">PDF</a>). Mind <a href="https://site.uvm.edu/rfoote/files/2022/06/errata_3rd_edition.pdf">the errata</a>. We will use Chapters 7-14. </li>
      </ul> </dd>
  <dt>Other:</dt>
  <dd>
      The following list gravitates towards freely available and new sources. See <a href="https://personal.math.vt.edu/plinnell/Teaching/Algprelims/syllabus.html">here</a> or <a href="https://math.stackexchange.com/questions/2527159/">here</a> or <a href="https://math.stackexchange.com/questions/627545/">here</a> or <a href="https://math.stackexchange.com/questions/339500">here</a> for more standard references.
      <ul>
         <li> <a href="https://kconrad.math.uconn.edu/blurbs/">Keith Conrad, <cite>Expository papers</cite>, 2024</a>. Specifically the ones on "Ring Theory", "Linear/Multilinear algebra", "Fields and Galois theory". Conrad mostly sticks to the commutative case of everything, but what he explains he explains really well. </li>
         <li> <a href="https://www.math.miami.edu/~armstrong/oldcourses.php">Drew Armstrong, <cite>various classes</cite></a>, specifically <a href="https://www.math.miami.edu/~armstrong/562sp24/562sp24textbook.pdf">his textbook</a> and his notes from <a href="https://www.math.miami.edu/~armstrong/561fa18.php">Math 561 Fall 2018 (groups)</a>, <a href="https://www.math.miami.edu/~armstrong/562sp19.php">Math 562 Spring 2019 (rings and Galois theory)</a> and <a href="https://www.math.miami.edu/~armstrong/562sp22.php">Math 562 Spring 2022 (rings in more detail)</a>. Like everything by Armstrong, these emphasize the geometry and the history. Highly recommended. </li>
         <li> <a href="https://www.math.ucla.edu/~rse/algebra_book.pdf">Richard Elman, <cite>Lectures on Abstract Algebra</cite>, preliminary version 2024</a>. Long set of notes; goes rather deep. </li>
         <li> <a href="https://homepage.divms.uiowa.edu/~goodman/algebrabook.dir/book.2.6.pdf">Frederick M. Goodman, <cite>Algebra: Abstract and Concrete</cite>, edition 2.6, 2016</a>. Relatively introductory, with a focus on groups, geometric symmetry and Galois theory. </li>
         <li> <a href="http://www.math.hawaii.edu/~tom/algebra.pdf">Mark Steinberger, <cite>Algebra</cite>, 2006</a>. </li>
         <li> <a href="http://www.math.stonybrook.edu/~aknapp/download.html">Anthony Knapp, <cite>Basic Algebra</cite> and <cite>Advanced Algebra</cite>, digital 2nd editions, 2016</a>. </li>
         <li> <a href="https://tim4datfau.github.io/Timothy-Ford-at-FAU/preprints/Algebra_Book_1.pdf">Timothy J. Ford, <cite>Introduction to Abstract Algebra</cite>, 2024</a>. </li>
         <li> <a href="https://tim4datfau.github.io/Timothy-Ford-at-FAU/preprints/Algebra_Book.pdf">Timothy J. Ford, <cite>Abstract Algebra</cite>, 2024</a>. </li>
         <li> <a href="https://projecteuclid.org/euclid.ndml/1175197041">Emil Artin, Arthur N. Milgram, <cite>Galois Theory: Lectures Delivered at the University of Notre Dame</cite><!--, Notre Dame Math Lectures #2-->, 1971</a>. A classic introduction to Galois theory, freely available. </li>
         <li> <a href="https://webusers.imj-prg.fr/~antoine.chambert-loir/publications/teach/sv-commalg.pdf">Antoine Chambert-Loir, <cite>(Mostly) Commutative Algebra</cite>, 2021</a>. A Bourbaki-style monograph (mostly above the level of this course). </li>
         <li> <a href="https://alistairsavage.ca/mat3143/notes/MAT3143-Rings_and_modules.pdf">Alistair Savage, <cite>MAT 3143: Rings and Modules</cite>, 2020</a>. Introductory notes. </li>
         <li> <a href="http://ozren.weebly.com/uploads/4/2/7/3/4273045/cameron_p.j._introduction_to_algebra.pdf">Peter J. Cameron, <cite>Introduction to Algebra</cite>, 2nd edition 2008</a>. Well-regarded British text. </li>
         <li> <a href="https://www.jmilne.org/math/CourseNotes/ft.html">James S. Milne, <cite>Fields and Galois Theory</cite>, 2022</a>. Rather concise text written by a famous arithmetic geometer. </li>
         <li> <a href="https://doi.org/10.1007/978-3-319-16721-3">David A. Cox, John Little, Donal O'Shea, <cite>Ideals, Varieties, and Algorithms</cite>, 4th edition 2015</a>. An introduction to commutative algebra and algebraic geometry based on Gr&ouml;bner bases. It starts rather elementary (with univariate polynomials). </li>
         <li> <a href="http://personal.unizar.es/elduque/files/GroupsGalois.pdf">Alberto Elduque, <cite>Groups and Galois theory</cite>, 2024</a> and <a href="http://personal.unizar.es/elduque/files/IAElduqueRef.pdf">Alberto Elduque, <cite>Introduction to Algebra</cite>, 2017</a>. </li>
         <li> <a href="https://darkwing.uoregon.edu/~koch/Galois.pdf">Richard Koch, <cite>Galois Theory</cite>, 2017</a>. </li>
         <li> <a href="https://degraaf.maths.unitn.it/notes.html">Willem A. de Graaf, <cite>Lecture notes on algebra</cite>, 2022</a>. </li>
         <li> <a href="https://www.ams.org/open-math-notes/omn-view-listing?listingId=110819">Andrew Baker, <cite>An introduction to Galois theory</cite>, 2022</a> with <a href="http://www.maths.gla.ac.uk/~ajb/dvi-ps/Galois-solutions.pdf">solutions</a>. </li>
         <li> <a href="https://homepages.warwick.ac.uk/~masda/MA3D5/Galois.pdf">Miles Reid, <cite>MA3D5 Galois theory</cite>, 2020</a>. </li>
      </ul> </dd>
</dl>
</div>

<h2>Course calendar</h2>

<div class="boldtable">
<dl>
  <dt>Diary:</dt>
  <dd><ul>
         <li> <a href="diary.pdf">Diary</a>. <b>Note:</b> These are unedited and unpolished archives of blackboard writings; they are not a replacement for the <a href="../23wa/23wa.pdf">lecture notes</a>. </li>
  </ul></dd>

  <dt>Week 0:</dt>
  <dd><ul>
         <li> This week's material: none yet :) </li>
         <li> For first-time LaTeX users: <a href="https://kconrad.math.uconn.edu/math5210f20/latex/latexintro.pdf">Keith Conrad's introduction to LaTeX</a>. Also, an amply commented <a href="../19s/hw0s.tex">sample homework solution file</a> written in LaTeX for an old class (<a href="../19s/hw0s.pdf">compiled version</a>). </li>
         <li> Advice on mathematical writing <a href="https://kconrad.math.uconn.edu/blurbs/proofs/writingtips.pdf">from Keith Conrad</a> (beginner level), <a href="https://www.math.ucla.edu/~pak/papers/how-to-write1.pdf">from Igor Pak</a> (intermediate level), and <a href="https://jmlr.csail.mit.edu/reviewing-papers/knuth_mathematical_writing.pdf">from Donald E. Knuth, Tracy Larrabee and Paul M. Roberts</a> (expert level). </li>
         <li> <a href="hw0.pdf">Homework set 0 (diagnostic)</a> (<a href="hw0.tex">source code</a>) and <a href="hw0s.pdf">solution sketches</a> (<a href="hw0s.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 1:</dt>
  <dd><ul>
         <li> Some extra background on definitions: <a href="https://math.mit.edu/~poonen/papers/ring.pdf">Bjorn Poonen, <cite>Why all rings should have a 1</cite>, 2018</a> and <a href="https://kconrad.math.uconn.edu/blurbs/ringtheory/ringdefs.pdf">Keith Conrad, <cite>Standard definitions for rings</cite></a>. </li>
         <li> <a href="hw1.pdf">Homework set 1</a> (<a href="hw1.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 2:</dt>
  <dd><ul>
         <li> <a href="hw2.pdf">Homework set 2</a> (<a href="hw2.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 3:</dt>
  <dd><ul>
         <li> <a href="hw3.pdf">Homework set 3</a> (<a href="hw3.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 4:</dt>
  <dd><ul>
         <li> <a href="mt1.pdf">Midterm 1</a> (<a href="mt1.tex">source code</a>). </li>
         <li> FYI: <a href="../21w/hw2.pdf">Homework set 2 from Winter 2021</a> (<a href="../21w/hw2.tex">source code</a>). <a href="../21w/hw2s.pdf">Solutions</a> (<a href="../21w/hw2s.tex">source code</a>). <a href="../21w/hw2s-g.pdf">Solutions to problems 1-9 (minus 6(c)) by a student</a> (<a href="../21w/hw2s-g.tex">source code</a>). <a href="../21w/hw2s-r.pdf">Solutions to problems 1, 3, 5, 8, 9 by Rose Adkisson</a> (<a href="../21w/hw2s-r.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 5:</dt>
  <dd><ul>
         <li> <a href="hw4.pdf">Homework set 4</a> (<a href="hw4.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 6:</dt>
  <dd><ul>
         <li> <a href="hw5.pdf">Homework set 5</a> (<a href="hw5.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 7:</dt>
  <dd><ul>
         <li> <a href="mt2.pdf">Midterm 2</a> (<a href="mt2.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 8:</dt>
  <dd><ul>
         <li> <a href="hw6.pdf">Homework set 6</a> (<a href="hw6.tex">source code</a>). </li>
  </ul></dd>

  <dt>Week 9:</dt>
  <dd><ul>
         <li> <a href="mt3.pdf">Midterm 3</a> (<a href="mt3.tex">source code</a>). </li>
  </ul></dd>

<!--
  <dt>Week 10:</dt>
  <dd><ul>
         <li> <a href="lec27.pdf">Lecture 27 (root adjunction)</a> (<a href="lec27.tex">source code</a>). </li>
         <li> <a href="lec28.pdf">Lecture 28 (finite fields)</a> (<a href="lec28.tex">source code</a>). </li>
         <li> <a href="lec29.pdf">Lecture 29 (multivariate polynomials)</a> (<a href="lec29.tex">source code</a>). </li>
  </ul></dd>
-->

<!--
    Week 11 = finals week

  Possible further topics:
  - Do linear algebra over fields in more detail. e.g., show Steinitz.
  - Matrix-morphism correspondence if not widely known.
  - Determinants if needed?
  - Tensor products (first give universal property of direct product, then compare).
  - Reed-Muller codes.
  - Galois: some very basic intro? Worth discussing cuberoot(2) and 17gon.
-->

  <dt>No Copyright:</dt>
  <dd>The above lecture notes and assignments have been released under <a href="https://creativecommons.org/publicdomain/zero/1.0/">the CC0 license</a>, i.e., are dedicated to the public domain. They can be copied, modified and distributed without permission. See <a href="https://creativecommons.org/publicdomain/zero/1.0/">the license</a> for details.
  </dd>

</dl>
</div>

<h2>Grading and policies</h2>

<div class="boldtable">
<dl>
  <dt>Grading matrix:</dt>
  <dd><ul>
         <li> 40%: homework sets. (Homework set #0 is worth 20 points; each remaining homework set is worth 50 points, no matter what gradescope says. Your lowest homework score, not counting homework set #0, will be dropped.) </li>
         <li> 20%: midterm 1. </li>
         <li> 20%: midterm 2. </li>
         <li> 20%: midterm 3 (due on finals week). </li>
      </ul> </dd>
  <dt>Grade scale:</dt>
  <dd>These numbers are <strong>tentative and subject to change</strong>:
      <ul>
         <li> A+, A, A-: (80%, 100%]. </li>
         <li> B+, B, B-: (60%, 80%]. </li>
         <li> C+, C, C-: (40%, 60%]. </li>
         <li> D+, D, D-: (20%, 40%]. </li>
      </ul> </dd>
  <dt>Homework policy:</dt>
  <dd><ul>
         <li> Collaboration and reading is allowed, but you have to write solutions in your own words and <strong>acknowledge all sources that you used</strong>. </li>
         <li> Asking outsiders (anyone apart from Math 332 students and Drexel employees) for help with the problems is <strong>not</strong> allowed. (In particular, you cannot post homework as questions on math.stackexchange before the due date!) </li>
         <li> The use of generative AI (ChatGPT, Bard, etc.) in solving problems or formulating the solutions is <strong>not</strong> allowed. However, it is permitted to consult AI for typesetting and formatting questions. </li>
         <li> Late homework will <strong>not</strong> be accepted. (But keep in mind that the lowest homework score will be dropped.) </li>
         <li> Solutions have to be submitted electronically via Gradescope. Solutions <strong>must be typeset</strong>; handwriting will not be accepted! To typeset text with formulas, you can use any of <a href="https://en.wikipedia.org/wiki/LaTeX">LaTeX</a> (a free online editor is <a href="https://www.overleaf.com/">Overleaf</a>), <a href="https://en.wikipedia.org/wiki/Markdown">Markdown</a> (a free online editor is <a href="https://stackedit.io/">Stackedit</a>), <a href="https://www.libreoffice.org/">LibreOffice</a> (which has a built-in equation editor), <a href="https://docs.google.com/">Google Docs</a> (which, too, has an equation editor inside), or many other tools. You can even type in a text editor if you use standard conventions to write your formulas in ASCII. In either case, convert to PDF (e.g., by printing to PDF) and submit to Gradescope.
		 If there are problems with submission, send your work to me by email for good measure. </li>
      </ul> </dd>
  <dt>Midterm policy:</dt>
  <dd><ul>
         <li> Late midterms will <strong>not</strong> be accepted unless agreed in advance and with serious justification. </li>
         <li> <strong>Collaboration is not allowed</strong> on midterms. </li>
         <li> Everything else is the same as for homework (yes, midterms are take-home). </li>
      </ul> </dd>
  <dt>Expected outcomes:</dt>
  <dd>The students should have an understanding of the basic objects of abstract algebra including rings and modules. They should also gain an understanding of homomorphisms, direct sums and products. They should have a knowledge of the basic theorems in this area including isomorphism theorems and universal properties. They should be familiar with the elementary properties of Gr&ouml;bner bases, finite fields and field extensions. </dd>
</dl>
</div>

<h2>Other resources</h2>

<div class="boldtable">
<dl>
  <!--
  <dt>Homework help:</dt>
  <dd><ul>
         <li> <a href="https://drexel.zoom.us/j/83973461576">Math Resource Center</a> (Zoom registration link; use your Drexel email); see <a href="https://drexel.edu/coas/academics/departments-centers/mathematics/math-resource-center/">schedule</a>. Starts January 10th. </li>
      </ul> </dd>
  -->
  <dt>University policies:</dt>
  <dd><ul>
         <li> <a href="https://www.drexel.edu/provost/policies/academic_dishonesty.asp" target="_blank">Academic Dishonesty</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-add-drop/" target="_blank">Course Add/Drop Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/course-withdrawal/" target="_blank">Course Withdrawal Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/incomplete_grades/" target="_blank">Incomplete Grade Policy</a>. </li>
         <li> <a href="https://drexel.edu/provost/policies/grade-appeals/" target="_blank">Grade Appeals</a>. </li>
         <li> <a href="https://drexel.edu/studentlife/community_standards/code-of-conduct/" target="_blank">Code of Conduct</a>. </li>
      </ul> </dd>
  <dt>Disability resources:</dt>
  <dd><ul>
         <li> <a href="https://drexel.edu/disability-resources/support-accommodations/student-family-resources/">Disability resources for students</a>. </li>
      </ul> </dd>
</dl>
</div>

<hr>

<p><a href="../">Back to Darij Grinberg's teaching page</a>. </p>

</body>

